!è// JavaScript source code

var input, output, div;

function Vertaal(input)
{
    findClass(input);
    input = input.replace(/String/gi, "var");
    /*gi , i -> als er string dus kleine letter staat => ook vervangen
            g -> overloopt heel de tekst en stopt niet bij de eerst gevonden "String"
    */
    //nieuwe replace => input = input.replace(,);
   // input = input.replace(/ArrayList<TRecord> aList = new ArrayList<TRecord>\(\);/gi, "var aList = new Array();");
    
    
    output = input;
}
function findClass(theString) {

    var thisClass = { imports: "", declaration: "", body: "" };

    if (typeof theString == "string" ) {
        var firstPublic = theString.indexOf("public"),
            firstPrivate = theString.indexOf("private"),
            firstBracket = theString.indexOf("{"),
            lastBracket = theString.lastIndexOf("}");

        if (firstPublic < firstPrivate) {
            sliceClass(firstPublic, firstBracket, lastBracket, thisClass, theString);
        } else {
            sliceClass(firstPrivate, firstBracket, lastBracket, thisClass, theString);
        };
    };

    return thisClass;

}

function sliceClass(beginDeclaration, beginBody, endBody, thisClass, theString) {
    thisClass.imports = theString.slice(0, beginDeclaration);
    thisClass.declaration = theString.slice(beginDeclaration, beginBody);
    thisClass.body = theString.slice(beginBody + 1, endBody);

    for (var property in thisClass) {
        property.trim(); //remove all extraneous whitespace from the beginning and ending of the string
    }

    return thisClass;
}
function toonOutput() {
    
    Vertaal(input);
    
   // output = console.log();
    div.innerText = output;
}


window.onload = function () {
    div = document.getElementById('vertaling');
    var fileInput = document.getElementById('fileInput');

    fileInput.addEventListener('change', function () {
        var file = fileInput.files[0];

        var extension = file.name.split('.').pop();
        var requiredExtention = "java";
        console.log("extention: " + extension);
        if (extension == requiredExtention) {
            var reader = new FileReader();

            reader.onload = function () {
                input = reader.result;
                console.log("hier");
            };

            reader.readAsText(file);
        }
        else {
            console.log("wrong file type");

        }

    });
};
