/**
 * Created by niels on 18/11/14.
 */
var addFunction = require('./node/add');
var changeFunction = require('./node/change');
var deleteFunction = require('./node/delete');
var searchFunction = require('./node/search');

//OK
exports.translateWhen = function translateWhen(code)
{
    //////////////////////////////////////////////////////
    var translatedCode = "",
        codeToTranslate = code,
        totalOccurrences = getNumberOfWhens(codeToTranslate);


    //translates the begin- and endtag of otherwise and endtag of when
    translateWhenAndOtherwiseTags();

    for(var i = totalOccurrences+1; i!=0; i--)
    {
        //place all unused code in translatedCode
        addFunction.moveUnusedCode(0, searchFunction.getBeginIndexOfStructure('choose', codeToTranslate));
        moveUnusedCode(0, getIndexBeginOfWhenStructure(codeToTranslate));
        if(codeToTranslate.indexOf("<c:when") >= 0)
            translatedCode += translateWhenBeginTag(getWhenStructure(0, getIndexEndOfWhenStructure(codeToTranslate)));
        codeToTranslate = codeToTranslate.replace(getWhenStructure(0, getIndexEndOfWhenStructure(codeToTranslate)), "");
    }
    addFunction.appendCode();
    return translatedCode;
    /////////////////////////////////////////////////////

    //FUNCTIONS

    //returns a when-structure
    function getWhenStructure(beginIndex, endIndex)
    {
        return codeToTranslate.slice(beginIndex, endIndex);
    }

    //translates the <:when ...>-tag
    function translateWhenBeginTag(code)
    {
        //count starting at endindex of value="${
        var beginIndexContent = code.indexOf("<c:when")+16;
        var endIndexContent = code.indexOf('}">', beginIndexContent);
        var innerContent = code.slice(beginIndexContent, endIndexContent);

        //newline to
        return "<% if(" + innerContent + "){ %>\n";
    }

    //returns begin-index of when-structure
    function getIndexBeginOfWhenStructure(code)
    {
        //aangepast door QUNIT-test
        return code.indexOf("<c:when");
    }

    //returns end-index of when-structure
    function getIndexEndOfWhenStructure(code)
    {
        var beginIndex = code.indexOf("<c:when");
        return (code.indexOf('">', beginIndex)) +3;
    }

    //moves the code that does not need translation to translatedCode
    //then removes that code from codeToTranslate
    function moveUnusedCode(beginIndex, endIndex)
    {
        var removedCode = codeToTranslate.slice(beginIndex, endIndex);
        translatedCode += removedCode;
        //just to be sure index 0 is the index where code starts again
        codeToTranslate = codeToTranslate.replace(removedCode, "");
    }

    //returns the number of occurrences os <c:when
    function getNumberOfWhens(code)
    {
        //count the number of WHEN
        var numberOfWHENs = code.match(/\<c\:when/g).length;
        return numberOfWHENs;
    }

    //translates all the tags that stay the same in all situations
    //also removes the choose-tags
    function translateWhenAndOtherwiseTags()
    {
        //REMOVE <c:choose></c:choose>
        codeToTranslate = codeToTranslate.replace(/\<c\:choose\>/g, "");
        codeToTranslate = codeToTranslate.replace(/\<\/c\:choose\>/g, "");
        //verwijdert newlines in het begin van de code
        codeToTranslate = codeToTranslate.replace(/\r?\n|\r/, "");
        //CONVERT WHEN-endtag to IF-endtag
        codeToTranslate = codeToTranslate.replace( /\<\/c\:when\>/g, '<% } %>' );
        //CONVERT <c:otherwise></c:otherwise> to ELSE-structure
        codeToTranslate = codeToTranslate.replace( /\<c\:otherwise\>/g, '<% else{ %>' );
        codeToTranslate = codeToTranslate.replace( /\<\/c\:otherwise\>/g, '<% } %>' );
    }
}