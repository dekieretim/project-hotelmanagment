/**
 * Created by niels on 9/10/14.
 */

exports.translateForEach = function (code){

    var translatedCode = "",
        codeToTranslate = code,
        totalOccurrencesForEach = getNumberOfForEachs(codeToTranslate);

    //loop the whole document till all IF's are translated
    //with numberOfIFs used, the document is only searched once, instead of every time for each loop
    for(var i = totalOccurrencesForEach; i!=0; i--)
    {
        //place all unused code in translatedCode
        moveUnusedCode(0, getIndexBeginOfForEachStructure(codeToTranslate));
        translatedCode += translateForEachStructure(getForEachStructure(0, getIndexEndOfForEachStructure(codeToTranslate)));
        codeToTranslate = codeToTranslate.replace(getForEachStructure(0, getIndexEndOfForEachStructure(codeToTranslate)), "");
        //if there is still code left and no foreach to translate, append code to translatedCode
    }
    appendCode();
    return translatedCode;



    //FUNCTIONS

    //appends rest of the code to translatedCode
    function appendCode()
    {
        if(codeToTranslate.length > 0)
        {
            translatedCode += codeToTranslate;
        }
    }

    //returns begin-index of FOREACH
    function getIndexBeginOfForEachStructure(code)
    {
        return code.indexOf("<c:forEach");
    }

    //returns end-index of FOREACH
    function getIndexEndOfForEachStructure(code)
    {
        return (code.indexOf("</c:forEach")+12);
    }


    //moves "not to be translated"-code to translatedCode
    //removes "not to be translated"-code from codeToTranslate
    function moveUnusedCode(beginIndex, endIndex)
    {
        var removedCode = codeToTranslate.slice(beginIndex, endIndex);
        translatedCode += removedCode;
        //just to be sure index 0 is the index where code starts again
        codeToTranslate = codeToTranslate.replace(removedCode, "");
    }


    //returns a complete ForEach-structure
    function getForEachStructure(beginIndex, endIndex)
    {
        return codeToTranslate.slice(beginIndex, endIndex);
    }


    //returns the number of FOREACH's present in code
    function getNumberOfForEachs(code)
    {
        return code.match(/\<\/c\:forEach/g).length;
    }

    //returns the whole innercontent between the FOREACH-tags
    function getForEachInnerContent(code)
    {
        var index0 = code.indexOf('">') +2;
        var index1 = code.indexOf("</c:forEach>", index0);

        return code.slice(index0, index1);
    }

    //Translates any forEachStructure
    function translateForEachStructure(forEachStructure){

        //get the array-element
        var elements = getForEachVarItems(forEachStructure);
        //get the single element
        var element = getForEachVar(forEachStructure);
        //get the inner content (html, other structures, comments...)
        var innerContent = getForEachInnerContent(forEachStructure);
        //endtag to keep things neat
        var endTag = "<% }) %>";

        var statusPresent = false;
        var varStatusElement;

        //get the element representing varStatus
        if(forEachStructure.indexOf("varStatus") >= 0)
        {
            //statusPresent is used in string newForEachStructure
            statusPresent = true;
            varStatusElement = getForEachVarStatus(forEachStructure);
        }

        //assembles a .forEach(function(){})-structure from scratch
        var newForEachStructure = "<% " + elements +".forEach(function(" + element
            + (statusPresent? ", ": "") + (statusPresent? varStatusElement : "") + "){ %>" +
            innerContent +
            endTag;

        return newForEachStructure;


            //FUNCTIONS


            //returns the variable representing varStatus
            function getForEachVarStatus(forEachStructure)
            {
                var beginIndex = forEachStructure.indexOf("varStatus") +11;
                var endIndex = forEachStructure.indexOf('">', beginIndex) ;

                return forEachStructure.slice(beginIndex, endIndex);
            }

            //returns the variable of FOREACH (element)
            function getForEachVar(forEachStructure)
            {
                var argumentsBegin = forEachStructure.indexOf("var") + 5;
                var argumentsEnd = forEachStructure.indexOf("items") - 2;
                var forEachVar = forEachStructure.slice(argumentsBegin, argumentsEnd);

                return forEachVar;
            }

            //returns the items-collection of FOREACH (elements)
            function getForEachVarItems(forEachStructure)
            {
                var argumentsBegin = forEachStructure.indexOf("items") + 9;
                var argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);
                var items = forEachStructure.slice(argumentsBegin, argumentsEnd);

                return items;
            }
    }



}