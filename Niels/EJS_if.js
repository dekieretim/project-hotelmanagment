exports.translateBeginTagsIf = function translateBeginTagsIf(code){

    //////////////////////////////////////////////////////
    var codeToTranslate = code,
        translatedCode = '',
        totalOccurrencesIf = getNumberOfIFs(codeToTranslate);

    while(totalOccurrencesIf !=0)
    {
        var structure = getIfStructure(getIndexBeginOfIFStructure(codeToTranslate), getIndexEndOfIFStructure(codeToTranslate));
        //removes the 'useless' code from codeToTranslate to translatedCode
        codeToTranslate = moveUnusedCode(0, getIndexBeginOfIFStructure(codeToTranslate), codeToTranslate);
        //adds the translated structure to translatedCode
        translatedCode += translateIfStructure(structure);
        //remove original if structure from codeToTranslate
        codeToTranslate = codeToTranslate.replace(structure, "");
        totalOccurrencesIf--;
    }
    appendCode();

    return translatedCode;
    //////////////////////////////////////////////////////



    //FUNCTIONS

    //appends the left-over code
    function appendCode()
    {
        if(codeToTranslate.length > 0)
        {
            translatedCode += codeToTranslate;
        }
    }

    //returns begin-index of IF
    function getIndexBeginOfIFStructure(code)
    {
        //aangepast door QUNIT-test
        return code.indexOf("<c:if");
    }

    //new
    function getIndexEndOfIFStructure(code)
    {
        var beginIndex = code.indexOf('<c:if');
        var endIndex = code.indexOf('}">', beginIndex)+3;
        return endIndex;
    }

    //OK
    //moves "not to be translated"-code to translatedCode
    //removes "not to be translated"-code from codeToTranslate
    function moveUnusedCode(beginIndex, endIndex, code)
    {
        var removedCode = code.slice(beginIndex, endIndex);
        //move unused code to translatedCode
        translatedCode += removedCode;
        //remove the unused code from codeToTranslate
        code = code.replace(removedCode, "");
        return code;
    }

    //OK
    //returns a complete IF-structure
    function getIfStructure(beginIndex, endIndex)
    {
        return codeToTranslate.slice(beginIndex, endIndex);
    }

    //OK
    //returns the number of IF's present in code
    function getNumberOfIFs(code)
    {
        //count the number of IFs
        var numberOfIFs = code.match(/\<c\:if/g).length;
        return numberOfIFs;
    }

//    function translateAllEndTags()
//    {
//        translatedCode = translatedCode.replace(/\<\/c:if>/g, '<% } %>');
//    }



    //OK
    function translateIfStructure(code)
    {
        var ifStructure = code;
        //ifStructure = translateTags(ifStructure);

        if(hasAttribute("length") | hasAttribute("empty") | hasAttribute("startsWith"))
        {
            ifStructure = translateAttributes(ifStructure);
        }
        else
        {
            ifStructure = translateTags(ifStructure);
        }

        return ifStructure;



        function hasAttribute(code)
        {
            return (ifStructure.indexOf("" +code+ "") >= 0 ? true: false);
        }

        function translateTags(code)
        {
            //ifStructure.replace(/\<c\:if test\=\"\$\{/g, '<% if(');
            code = code.replace(/\<c\:if/g, '<% if(');
            code = code.replace(/test\=\"\$\{/g, '');
            code = code.replace(/\}\"\>/g, '){ %>');
            return code;
        }

        function translateAttributes(code)
        {
            var ifStructure = code;
            //if EMPTY is present
            if(hasAttribute("empty"))
            {
                if(getNumberOfEmpty(ifStructure) > 1)
                {
                    var part1 = "<% if((typeof ",
                        part2 = " == undefined && ",
                        part3 = " == '') && (typeof ",
                        part4 = " == undefined && ",
                        part5 = " == '') %>",
                        variable = getVariables(ifStructure);
//                        content = getContent(ifStructure),
//                        endtag = "<% } %>";
                    return (part1+variable+part2+variable+part3+variable+part4+variable+part5);
                }
                else
                {
                    if(hasAttribute("not empty") | hasAttribute("! empty"))
                    {
                        var part1 = "<% if(typeof ",
                            part2 = " != undefined && ",
                            part3 = " != '') %>",
                            variable = getVariables(ifStructure);
//                            content = getContent(code),
//                            endtag = "<% } %>";

                        return (part1+variable+part2+variable+part3);
                    }
                    else
                    {
                        var part1 = "<% if(typeof ",
                            part2 = " == undefined && ",
                            part3 = " == '') %>",
                            variable = getVariables(ifStructure);
//                            content = getContent(code),
//                            endtag = "<% } %>";
                        return (part1+variable+part2+variable+part3);
                    }
                }
            }

            //if startsWith is present
            if(hasAttribute("startsWith"))
            {
                code = code.replace(/fn\:startsWith\(/, '');
                code = code.replace(/\, \'/, '.charAt(0)=="');
                code = code.replace(/\'\)/, '"');
                return code;
            }

            //if LENGTH is present
            if(hasAttribute("length"))
            {
                code = code.replace(/fn\:length\(/g, '' );
                code = code.replace(/\)/, '.length() ');
                return code;
            }

            //returns the number of 'empty' present
            function getNumberOfEmpty(code)
            {
                //count the number of "EMPTY"
                var numberOfEmpty = code.match(/empty/g).length;
                return numberOfEmpty;
            }

            //returns the variable present inside the IF
            //OK
            function getVariables(code)
            {
                var index0 = code.indexOf("empty") +6;
                var index1 = code.indexOf(")", index0) ;
                //return code.slice(index0, index1);
                return code.slice(index0, index1);
            }
        }
    }
}