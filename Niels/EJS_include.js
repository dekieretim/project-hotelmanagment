/**
 * Created by niels on 20/10/14.
 */

//De include met een SET-variabele is nog te vertalen

exports.translateInclude = function(code){

    var translatedCode = "",
        codeToTranslate = code,
        totalOfOccurrencesInclude = getNumberOfInclude(codeToTranslate);


    while((codeToTranslate.indexOf("<%@ include")) >= 0)
    {
        //place all unused code in translatedCode
        moveUnusedCode(0, getIndexBeginOfIncludeStructure(codeToTranslate));
        translatedCode += translateIncludeStructure(getIncludeStructure(codeToTranslate));
        codeToTranslate = codeToTranslate.replace(getIncludeStructure(codeToTranslate), "");
    }

    return translatedCode;


    //FUNCTIONS


    function getNumberOfInclude(code)
    {
        //count the number of includes
        var numberOfIncludes = code.match(/<%@ include/g).length;
        return numberOfIncludes;
    }

    //returns the begin-index of includestructure
    function getIndexBeginOfIncludeStructure(code)
    {

        return code.indexOf("<%@");
    }

    function getEndIndexOfIncludeStructure(code)
    {
        var beginInclude = code.indexOf("<%@ include");
        return code.indexOf('.jsp"%>', beginInclude) +7;
    }

    //OK
    function moveUnusedCode(beginIndex, endIndex)
    {
        var removedCode = codeToTranslate.slice(beginIndex, endIndex);
        translatedCode += removedCode;
        //just to be sure index 0 is the index where code starts again
        codeToTranslate = codeToTranslate.replace(removedCode, "");
    }

    //OK
    //returns a complete includestructure
    function getIncludeStructure(code)
    {
        //volledige include
        var beginInclude = code.indexOf("<%@ include");
        var endInclude = code.indexOf('.jsp"%>', beginInclude) +7;

        return code.slice(beginInclude, endInclude);
    }


    //OK
    //translates an includestructure
    function translateIncludeStructure(includeStructure)
    {
        // trim onnodige code
        //VOOR: <%@ include file="/views/....jsp" %>
        //NA:   <%  include ... %>
        //of    <% include admin/... %>

        includeStructure = includeStructure.replace('@', '');
        includeStructure = includeStructure.replace('file="/views/', '');
        includeStructure = includeStructure.replace('.jsp"', ' ');

        return includeStructure
    }
}