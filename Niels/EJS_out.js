/**
 * Created by niels on 4/11/14.
 */

//escapeXml is standaard true; er is slechts 1 voorkomen hiervan in de code en die is true = weglaten
function translateOUT(code)
{
    var translatedCode = "",
    codeToTranslate = code

    const index0 = 0;

    while((codeToTranslate.indexOf(/\<\/c\:out/g)) >= 0)
    {
        //place all unused code in translatedCode
        //does this leave whitespaces?????
        moveUnusedCode(index0, getIndexBeginOfOUTStructure());
        translatedCode += translateOut(getIfStructure(index0, getIndexEndOfOUTStructure(codeToTranslate)));
        codeToTranslate = codeToTranslate.replace(getOutStructure(index0, getIndexEndOfOUTStructure(codeToTranslate)), "");
    }



    //returns the number of keyword "<:out" present
    function getNumberOfOUTs(code)
    {
        //count the number of IFs
        var numberOfOUTs = code.match(/\<\/c\:out/g).length;
        return numberOfOUTs;
    }

    //main
    function translateOut(code)
    {
        while(code.indexOf(/\<c\:out/g) >= 0)
        {
            var structure = getOutStructure(code);
            var varToTranslate = getOutVariable(structure);
            varToTranslate = addVariableTags(varToTranslate);
        }
    }

    //returns a OUTstructure
    //OK
    function getOutStructure(index0, index1)
    {
//        var index0 = code.indexOf(/\<\:out/);
//        var index1 = code.indexOf(/\/\>/, index0) +1;   //+1 omdat anders de '>' niet meegerekent wordt
        return code.slice(index0, index1);
    }

    //returns the variable of the OUTstructure
    //OK
    function getOutVariable(code)
    {
        var index0 = code.indexOf("value") +9;
        var index1 = code.indexOf("/>") -3;
        return code.slice(index0, index1);
    }

    //translates the variable of the OUTstructure
    function addVariableTags(variable)
    {
        return "<%= " + variable + "%>";
    }

    //FUNCTIONS

    //returns the begin-index of the out structure
    //OK
    function getIndexBeginOfOUTStructure(code)
    {
        return (code.indexOf("<c:out") - 1)
    }

    //returns the end-index of the out structure
    //OK
    function getIndexEndOfOUTStructure(code)
    {
        var index = code.indexOf("/>")+2;
        return index;
    }


    function moveUnusedCode(beginIndex, endIndex)
    {
        var removedCode = codeToBeTranslated.slice(beginIndex, endIndex);
        translatedCode += codeToTranslate.slice(beginIndex, endIndex);
        //just to be sure index 0 is the index where code starts again
        codeToTranslate.replace(removedCode, "");
    }
}