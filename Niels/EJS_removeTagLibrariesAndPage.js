/**
 * Created by niels on 28/11/14.
 */
//kan eventueel gebeuren vlak voor de code die alles vertaald?????????????????????????????????????????
function removeImports(code)
{
    //remove all occurrences of page and tag libraries
    code = code.replace(/\<\%\@\spage.*\%\>/g, "");
    code = code.replace(/\<\%\@\staglib.*\%\>/g, "");
    return code;
}

//verwijdert commentaar
function removeComments(code)
{
    //zoek naar comments en verwijder die. (Tim Dekiere)
    code = code.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, "");
    return code;
}