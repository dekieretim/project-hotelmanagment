/**
 * Created by niels on 20/10/14.
 */



//!!!!!!!variabelen mogen slechts vertaald worden NADAT de if, while, foreach... structuren vertaald zijn!!!!!!!!

// want die bevatten ook gelijkaardige structuren die als var kunnen aanzien worden.


exports.translateAllVariables = function(code)
{
    var translatedCode = "",
        codeToTranslate = code,
        totalOccurrencesVariables = (codeToTranslate.match(/\$\{/g).length);

    const index0 = 0;


    ////////////////////////////////////////
    //translate all occurrences of Out-variables
    translateOutVariables();

    //translate all occurrences of variables
    for(var i = 0; i<totalOccurrencesVariables; i++)
    {
        //place all unused code in translatedCode
        moveUnusedCode(index0, getIndexBeginOfVariable(codeToTranslate));
        translatedCode += translateVariable(getVariableStructure(codeToTranslate));
        codeToTranslate = codeToTranslate.replace(getVariableStructure(codeToTranslate), "");
    }
    appendCode();
    return translatedCode;
    /////////////////////////////////////////////////////////

    //FUNCTIONS

    //appends the left over code
    function appendCode()
    {
        if(codeToTranslate.length > 0)
        {
            translatedCode += codeToTranslate;
        }
    }

    //returns the begin-index of the variable
    function getIndexBeginOfVariable(code)
    {
        //aangepast door QUNIT-test
        return code.indexOf("${");
    }

    //moves unused code to translatedCode
    function moveUnusedCode(beginIndex, endIndex)
    {
        var removedCode = codeToTranslate.slice(beginIndex, endIndex);
        translatedCode += removedCode;
        //just to be sure index 0 is the index where code starts again
        codeToTranslate = codeToTranslate.replace(removedCode, "");
    }

    //returns the variable
    function getVariableStructure(code)
    {
        //zoeken naar ${
        var beginVar = code.indexOf("${");
        //zoeken naar de END-tag
        var endVar = code.indexOf("}", beginVar) + 1;
        varStructure = code.slice(beginVar, endVar);

        return varStructure;
    }

    //translates the variable returned
    function translateVariable(variableStructure)
    {
        var variable = variableStructure;
        variable = variable.replace(/\$\{/, '<%= ');
        variable = variable.replace(/\}/, '%>');

        return variable;
    }

    //translates all Out-variables
    function translateOutVariables()
    {
        //  first remove the escapeXml-tag
        codeToTranslate = codeToTranslate.replace(/escapeXml="true" /g, "");
        //  <c:out value="${ --> <%=
        codeToTranslate = codeToTranslate.replace(/\<c\:out value\=\"\$\{/g, "<%= ");
        //  }" />  --> %>
        codeToTranslate = codeToTranslate.replace(/\}" \/\>/g, "%>");
    }
}