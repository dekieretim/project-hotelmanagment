var input, output, div, originalJspCode, typer, outputButton, filename, jspFile;

var jspToEjs = require("../node/TranslateJSPtoEJS");



window.onload = function () {

    initializeAllElements();

    addAllListeners();


};

function initializeAllElements(){
    jspFile = document.getElementById('fileInput');
    typer = document.getElementById("translatedCode");
    outputButton = document.getElementById("download");
    //filename die de user kan meegeven
    filename = document.getElementById("fileName").value;
    originalJspCode = document.getElementById("originalCode");
    div = document.getElementById('translatedCode');
}

function addAllListeners() {


//    hulpmethod om de default actie bij events te overschrijven
    function cancel(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        return false;
    }


    //    Zorgt ervoor dat de file niet geöpend wordt in de browser door "dragover" en "dragenter" events te annuleren, zodat alleen de (overgeschreven) drop functie wordt aangeroepen
    window.addEventListener('dragover', cancel, false);
    window.addEventListener('dragenter', cancel, false);


//    deze functie zorgt ervoor dat als je een file drag-en-dropt in je browservenster, de readFile method wordt aangeroepen op deze file.
    window.addEventListener('drop', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        } // stops the browser from redirecting off to the image.

        var dt = e.dataTranser;
        readFile(dt);
    });

//    deze functie zorgt ervoor dat als je een file kiest met behulp van de upload-knop, de readFile method wordt aangroepen op deze file.
    jspFile.addEventListener('change', function () {
        readFile(jspFile);
    });

}

function showInput()
{
    if(input != undefined)
    {
        document.getElementById('originalFile').innerText = input;
    }
    else
    {
        document.getElementById('originalFile').innerText = "Oh noos, meni error!";
    }



    //innerHTML
}

function showOutput() {

    //translate the input jsp file to an output EJS file

    output = jspToEjs.translateJSPtoEJS(input);

    if(output == null | output == "")
    {
        translatedCode.innerText = "lol";
    }

    //transLate opvullen met de translatedCode
    translatedCode.innerText = output;
}

function readFile(userData) {

    var file = userData.files[0],
        extension = file.name.split('.').pop(),
        requiredExtension = "jsp",
        reader;
//        test of de extensie ".jsp" is
    if (extension == requiredExtension) {
        reader = new FileReader();
        reader.onload = function () {
            input = reader.result;
            console.log("succesfully read file: \n" + input);
            original.innerText = input;

        };
        reader.readAsText(file);
    }
    else {
        console.log("wrong file type");
    }


}

//a function called by the html buttons, to translate the
function downloadFile(extention) {

    const MIME_TYPE = "text/plain";
    console.log("typer" + typer);
    //creates a blob (file that can be filled with text)
    var bb = new Blob([typer.innerText], {type: MIME_TYPE});

    //creates a hyperlink
    var translatedFile = document.createElement('a');
    translatedFile.download = filename + extention;
    translatedFile.href = window.URL.createObjectURL(bb);
    translatedFile.textContent = "Download";
    translatedFile.dataset.downloadurl = [MIME_TYPE, translatedFile.download, translatedFile.href].join(':');

    outputButton.innerHTML = "";

    //adds the hyperlink to the button
    outputButton.appendChild(translatedFile);

    //output.disabled = false;

}

