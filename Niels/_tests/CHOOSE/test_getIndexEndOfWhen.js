/**
 * Created by niels on 9/12/14.
 */
function getIndexEndOfWhenStructure(code)
{
    var beginIndex = code.indexOf("<c:when");
    return (code.indexOf('">', beginIndex)) +2;
}


QUnit.module("getEndIndexOfWhenStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Choose1: "01234" +
                '<c:when test="${test=true}">' +
                "</c:when>" +
                "<c:otherwise>" +
                "</c:otherwise>" +
                "</c:choose>1234",
            expected1: 33
        }

    ]).test("Test getIndexEndOfWhenStructure function", function(params){
        var actualResult1 = getIndexEndOfWhenStructure(params.Choose1);
        strictEqual(actualResult1, params.expected1);
        //var actualResult2 = translateChooseLoop(params.Choose2);
        //strictEqual(actualResult2, params.expected2);

    });