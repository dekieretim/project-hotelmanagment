/**
 * Created by niels on 9/12/14.
 */
function translateWhenBeginTag(code)
{
    var beginIndex = code.indexOf("<c:when")+16;
    var endIndex = code.indexOf('}">', beginIndex);
    var innerContent = code.slice(beginIndex, endIndex);

    return "<% if(" + innerContent + "){ %>";
}


QUnit.module("translateWhenStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Choose1:    '<c:when test="${test=true}">' ,

            expected1:  '<% if(test=true){ %>',


            Choose2:    '<c:when test="${editItem.templateName == || editItem.templateName == }">',

            expected2:  "<% if(editItem.templateName == || editItem.templateName == ){ %>"

        }
    ]).test("Test translateWhenStructure function", function(params){
        var actualResult1 = translateWhenBeginTag(params.Choose1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = translateWhenBeginTag(params.Choose2);
        strictEqual(actualResult2, params.expected2);

    });