/**
 * Created by niels on 4/12/14.
 */
function getIndexBeginOfForEachStructure(code)
{
    //aangepast door QUNIT-test
    return code.indexOf("<c:forEach");
}


QUnit.module("ForEachBeginIndex");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            ForEach1:
                '<c:forEach var="element" items="${editElements}">' +
                    'randomcode'+
                    '</c:forEach>' +
                    'TEVERWIJDEREN randomcode',
            expected1: 0,
            ForEach2: 'randomcode'+
                '<c:forEach var="element" items="${editElements}">'+
                'randomcode'+
                '</c:forEach>' +
                'TEVERWIJDEREN randomcode',
            expected2: 10
        }

    ]).test("Test getBeginIndexForEach function", function(params){
        var actualResult1 = getIndexBeginOfForEachStructure(params.ForEach1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = getIndexBeginOfForEachStructure(params.ForEach2);
        strictEqual(actualResult2, params.expected2);
    });