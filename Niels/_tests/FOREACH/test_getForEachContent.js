/**
 * Created by niels on 4/12/14.
 */
function getForEachInnerContent(code)
{
    var index0 = code.indexOf('">') +2;

    var index1 = code.indexOf("</c:forEach>", index0);
    return code.slice(index0, index1);
}


QUnit.module("getForEachContent");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            forEach1:
                '<c:forEach var="p" items="${path}">' +
                    '&gt; ${p.title}' +
                    '<p>${content.data}</p>'+
                    '</c:forEach></h3>',

            expected: '&gt; ${p.title}' +
                '<p>${content.data}</p>'
        }
    ]).test("Test getForEachContent function", function(params){
        var actualResult = getForEachInnerContent(params.forEach1);
        strictEqual(actualResult, params.expected);
    });