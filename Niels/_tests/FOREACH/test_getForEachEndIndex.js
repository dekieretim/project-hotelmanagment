/**
 * Created by niels on 4/12/14.
 */
function getIndexEndOfForEachStructure(code)
{
    //+4 omdat anders niet de hele IF gereturnd wordt
    var beginIndex = code.indexOf('<c:forEach');
    if(beginIndex !=-1){
        var endIndex = code.indexOf('}">', beginIndex)+3;
        return endIndex;
    }
    return console.log('All Foreach structures translated')
}

QUnit.module("ForEachEndIndex");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            ForEach1: '0123456' +
                '<c:forEach }">'+
                'randomcode'+
                    '</c:forEach>',

            expected1: 21,
            ForEach2: 'randomcode'+
                '<c:forEach var="element" items="${editElements}">'+
                'randomcode'+
                '</c:forEach>' +
                'TEVERWIJDEREN randomcode',
            expected2: 59
        }
    ]).test("Test getEndIndexForEach function", function(params){
        var actualResult1 = getIndexEndOfForEachStructure(params.ForEach1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = getIndexEndOfForEachStructure(params.ForEach2);
        strictEqual(actualResult2, params.expected2);
    });