/**
 * Created by niels on 20/11/14.
 */


var codeToTranslateForEach = 'randomcode'+
    '<c:forEach var="element" items="${editElements}">'+
    'randomcode'+
    '</c:forEach>' +
    'TEVERWIJDEREN randomcode';

function getForEachStructure(beginIndex, endIndex)
{
    return codeToTranslateForEach.slice(beginIndex, endIndex);
}



QUnit.module("getForEachStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Index1: 10, Index2: 81,
            expected1: '<c:forEach var="element" items="${editElements}">'+
                'randomcode'+
                '</c:forEach>'

        }
    ]).test("Test getForEachStructure function", function(params){
        var actualResult1 = getForEachStructure(params.Index1, params.Index2);
        strictEqual(actualResult1, params.expected1);
    });