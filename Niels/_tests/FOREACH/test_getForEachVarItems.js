/**
 * Created by niels on 20/11/14.
 */
function getForEachVarItems(forEachStructure){

    //VarITEMS
    argumentsBegin = forEachStructure.indexOf("items") + 9;
    argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);

    var items = forEachStructure.slice(argumentsBegin, argumentsEnd);

    return items;
}


QUnit.module("getForEachItems");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Index1:'<c:forEach var="element" items="${editElements}">'+
                'randomcode'+
                '</c:forEach>',
            expected: "editElements"}
    ]).test("Test getForEach Var Items function", function(params){
        var actualResult = getForEachVarItems(params.Index1);
        strictEqual(actualResult, params.expected);
    });