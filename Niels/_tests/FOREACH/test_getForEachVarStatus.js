/**
 * Created by niels on 9/12/14.
 */
function getForEachVarStatus(forEachStructure)
{
    var beginIndex = forEachStructure.indexOf("varStatus") +11;
    var endIndex = forEachStructure.indexOf('">', beginIndex) ;

    return forEachStructure.slice(beginIndex, endIndex);
}

QUnit.module("getForEachVarStatus");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Index1:'<c:forEach var="element" items="${editElements}" varStatus="status">'+
                'randomcode'+
                '</c:forEach>',
            expected: "status"}
    ]).test("Test getForEachVarStatus function", function(params){
        var actualResult = getForEachVarStatus(params.Index1);
        strictEqual(actualResult, params.expected);
    });