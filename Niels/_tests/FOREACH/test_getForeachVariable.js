/**
 * Created by niels on 20/11/14.
 */
function getForEachVar(forEachStructure){


    argumentsBegin = forEachStructure.indexOf("var") + 5;
    //argumentsEnd = forEachStructure.indexOf('"')[2] - 1;
    argumentsEnd = forEachStructure.indexOf("items") - 2;
    //var arguments = IfStructure.splice(argumentsBegin, argumentsEnd);

    var forEachVar = forEachStructure.slice(argumentsBegin, argumentsEnd);

    return forEachVar;
}



QUnit.module("getForeachVariable");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Index1:'<c:forEach var="element" items="${editElements}">'+
                    'randomcode'+
                    '</c:forEach>',

            expected: "element"}
    ]).test("Test getForeachVariable function", function(params){
        var actualResult = getForEachVar(params.Index1);
        strictEqual(actualResult, params.expected);
    });