/**
 * Created by niels on 9/12/14.
 */
//returns the number of FOREACH's present in code
function getNumberOfForEachs(code)
{
    //count the number of IFs
    //return code.match("</c:forEach").length;
    return code.match(/\<\/c\:forEach/g).length;
}



QUnit.module("getNumberOfForEachs");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Structure1:'<c:forEach var="element" items="${editElements}">\n'+
                'randomcode\n'+
                '</c:forEach>'+
                'randomcode\n'+
                '<c:forEach var="element" items="${editElements}" varStatus"status">\n'+
                'randomcode\n'+
                '</c:forEach>'+
                'randomcode\n'+
                '<c:forEach var="element" items="${editElements}" varStatus"status">\n'+
                'randomcode\n'+
                '</c:forEach>',
            expected1: 3,
            Structure2:'<c:forEach var="element" items="${editElements}" varStatus"status">\n'+
                'randomcode\n'+
                '</c:forEach>'+
                '<c:forEach var="element" items="${editElements}" varStatus"status">\n'+
                'randomcode\n'+
                '</c:forEach>',
            expected2: 2,
            Structure3: '<div id="content">\n'+
                '<div id="left">\n'+
                '</div>\n'+
                '<div id="mid">\n'+
                '<h1>${page.title}</h1>\n'+
                '<p>${content.data}</p>\n'+
                '<c:forEach var="element" items="${editElements}">\n'+
                '<br clear="all">\n'+
                '</c:forEach>\n'+
                '</div>\n'+
                '<div id="right">\n'+
                '<c:forEach var="element" items="${editElements}" varStatus="status">\n'+
                '<br clear="all">\n'+
                '</c:forEach>\n'+
                '</div>\n'+
                '</div>\n',
            expected3: 2

        }
    ]).test("Test getNumberOfForEachs function", function(params){
        var actualResult1 = getNumberOfForEachs(params.Structure1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = getNumberOfForEachs(params.Structure2);
        strictEqual(actualResult2, params.expected2);
        var actualResult3 = getNumberOfForEachs(params.Structure3);
        strictEqual(actualResult3, params.expected3);
    });