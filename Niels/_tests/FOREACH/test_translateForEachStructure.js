/**
 * Created by niels on 9/12/14.
 */
function translateForEachStructure(ForEachStructure){

    if((forEachStructure.indexOf("begin")>=0) & (forEachStructure.indexOf("end")>=0))
    {
        //with given step
        if(forEachStructure.indexOf("step")>=0)
        {
            var begin = getBegin(forEachStructure),
                end = getEnd(forEachStructure),
                step = getStep(forEachStructure),
                variable = getVar(forEachStructure);

            return "<% for(var " +variable+ " = " +begin+ "; i < " +end+ "; i + " +step+ ") { %>";
        }

        //without given step (step = 1)
        else
        {
            var begin = getBegin(forEachStructure),
                end = getEnd(forEachStructure),
                variable = getVar(forEachStructure);

            return "<% for(var " +variable+ " = " +begin+ "; i < " +end+ "; i++ ) { %>";
        }

        //FUNCTIONS
        function getVar(structure)
        {
            var beginVar = structure.indexOf("var") + 5;
            var endVar = structure.indexOf('"', beginVar);

            return structure.slice(beginVar, endVar);
        }

        function getBegin(structure)
        {
            var beginStart = structure.indexOf("begin") + 7;
            var beginEnd = structure.indexOf('"', beginStart);

            return structure.slice(beginStart, beginEnd);
        }

        function getEnd(structure)
        {
            var endStart = structure.indexOf("end") + 5;
            var beginStart = structure.indexOf('"', endStart);

            return structure.slice(endStart, beginStart);
        }

        function getStep(structure)
        {
            var stepBegin = structure.indexOf("step") + 6;
            //argumentsEnd = forEachStructure.indexOf('"')[2] - 1;
            var stepEnd = structure.indexOf('"', stepBegin);
            //var arguments = IfStructure.splice(argumentsBegin, argumentsEnd);

            return structure.slice(stepBegin, stepEnd);
        }
    }
    else
    {
        //get the array
        var elements = getForEachVarItems(ForEachStructure);
        //get the single element
        var element = getForEachVar(ForEachStructure);

        //get the element representing varStatus
        if(ForEachStructure.indexOf("varStatus") >= 0)
        {
            var statusPresent = true;
            var varStatusElement = getForEachVarStatus(ForEachStructure);
        }


        //get the inner content (html, other structures, comments...)
        var innerContent = getForEachInnerContent(ForEachStructure);
        var endTag = "<% }) %>";

        //assembles a .forEach(function(){})-structure from scratch
        var newForEachStructure = "<% " + elements +".forEach(function(" + element
            + (statusPresent? ", ": "") + (statusPresent? varStatusElement : "") + "){ %>" +
            innerContent +
            endTag;


        return newForEachStructure;



        //FUNCTIONS
        //OK
        //returns the variable representing varStatus
        function getForEachVarStatus(forEachStructure)
        {
            var beginIndex = forEachStructure.indexOf("varStatus") +10;
            var endIndex = forEachStructure.indexOf('">', beginIndex) ;

            return forEachStructure.slice(beginIndex, endIndex);
        }



        //OK
        //returns the variable of FOREACH
        function getForEachVar(forEachStructure){


            var argumentsBegin = forEachStructure.indexOf("var") + 5;
            //argumentsEnd = forEachStructure.indexOf('"')[2] - 1;
            var argumentsEnd = forEachStructure.indexOf("items") - 2;
            //var arguments = IfStructure.splice(argumentsBegin, argumentsEnd);

            var forEachVar = forEachStructure.slice(argumentsBegin, argumentsEnd);

            return forEachVar;
        }

        //OK
        //returns the items-collection of FOREACH
        function getForEachVarItems(forEachStructure){

            //VarITEMS
            var argumentsBegin = forEachStructure.indexOf("items") + 9;
            var argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);

            var items = forEachStructure.slice(argumentsBegin, argumentsEnd);

            return items;
        }
    }

}



QUnit.module("translateForEachStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Structure1:'<c:forEach var="element" items="${editElements}">\n'+
                'randomcode\n'+
                '</c:forEach>',
            expected1: '<% editElements.forEach(function(element){ %>\n'+
                'randomcode\n'+
                '<% }) %>',
            Structure2:'<c:forEach var="element" items="${editElements}" varStatus"status">\n'+
                'randomcode\n'+
                '</c:forEach>',
            expected2: '<% editElements.forEach(function(element, status){ %>\n'+
                'randomcode\n'+
                '<% }) %>'

        }
    ]).test("Test translateForEachStructure function", function(params){
        var actualResult1 = translateForEachStructure(params.Structure1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = translateForEachStructure(params.Structure2);
        strictEqual(actualResult2, params.expected2);
    });