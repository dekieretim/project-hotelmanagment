/**
 * Created by niels on 13/11/14.
 */

function getIndexBeginOfIFStructure(code)
{
    return code.indexOf("<c:if");
}





QUnit.module("getBeginIndex");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            IFstructure: "01234567<c:if (test)> //code //code //code </c:if>;",
            expected: 8},
        {title: "basic2",
            IFstructure: "0123<c:if (test)> //code //code //code </c:if>;",
            expected: 4}
    ]).test("Test getBeginIndexIF function", function(params){
        var actualResult = getIndexBeginOfIFStructure(params.IFstructure);
        strictEqual(actualResult, params.expected);
        notStrictEqual(actualResult, params.IFstructure);
    });
