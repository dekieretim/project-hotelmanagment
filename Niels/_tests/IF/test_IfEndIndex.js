/**
 * Created by niels on 13/11/14.
 */
function getIndexEndOfIFStructure(code)
{
    //+4 omdat anders niet de hele IF gereturnd wordt
    var beginIndex = code.indexOf('<c:if');
    var endIndex = code.indexOf('}">', beginIndex)+3;
//    return (code.indexOf("</c:if>")+7);
    return endIndex;
}


QUnit.module("getEndIndex");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            ifStructure1: '<c:if test="${ object.extention ==  }">\n'+
                '<input type="hidden" name="extention" value="" />\n'+
                '<input type="hidden" name="caption" value="" />\n'+
                '</c:if>\n',
            expected1: 39

}
    ]).test("Test getEndIndexIF function", function(params){
        var actualResult1 = getIndexEndOfIFStructure(params.ifStructure1);
        strictEqual(actualResult1, params.expected1);

    });
