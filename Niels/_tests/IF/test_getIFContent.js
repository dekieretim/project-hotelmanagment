/**
 * Created by niels on 2/12/14.
 */
function getContent(code)
{
    var index0 = code.indexOf("%>") +2;
    var index1 = code.indexOf("<%", index0);
    return code.slice(index0, index1);
}


QUnit.module("getIFContent");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            IF1: '<% if(test){ %>' +
                'code code code' +
                'code code code' +
                '<% } %>',
            IF2: '<% if(test2){ %> code code code <% } %>',

            //zorgt voor problemen bij contentvertaling translateIf
            IF3: '<% if(empty fields){ %>' +
                '${thankyou}' +
                '<% } %>',
            //

            expected1: "code code code" +
                "code code code",
            expected2: " code code code ",
            expected3: "${thankyou}"}
    ]).test("Test getContent for IF-function", function(params){
        var actualResult1 = getContent(params.IF1);
        var actualResult2 = getContent(params.IF2);
        var actualResult3 = getContent(params.IF3);
        strictEqual(actualResult1, params.expected1);
        strictEqual(actualResult2, params.expected2);
        strictEqual(actualResult3, params.expected3);
    });
