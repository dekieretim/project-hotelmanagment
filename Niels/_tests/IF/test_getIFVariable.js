/**
 * Created by niels on 2/12/14.
 */
function getVariables(code)
{
    var index0 = code.indexOf("empty") +6;
    var index1 = code.indexOf(")", index0) ;
    //return code.slice(index0, index1);
    return code.slice(index0, index1);
}


QUnit.module("getIFVariable");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            IF1: '<% if(! empty savedObject.id){ %>',
            IF2: '<% if(empty fetchnode){ %>',
            IF3: '<% if(empty fields){ %>' +
                '${thankyou}' +
                '<% } %>',
            expected1: "savedObject.id",
            expected2: "fetchnode",
            expected3: "fields"}
    ]).test("Test getVariable for IF-function", function(params){
        var actualResult1 = getVariables(params.IF1);
        var actualResult2 = getVariables(params.IF2);
        var actualResult3 = getVariables(params.IF3);
        strictEqual(actualResult1, params.expected1);
        strictEqual(actualResult2, params.expected2);
        strictEqual(actualResult3, params.expected3);
    });