/**
 * Created by niels on 13/11/14.
 */
var codeToTranslate = "0123456789<c:if > </c:if>0123456789"


function getIfStructure(beginIndex, endIndex)
{
    var ifStructure = codeToTranslate.slice(beginIndex, endIndex);
//    console.log(beginIndex, endIndex, ifStructure, codeToTranslate);
    return ifStructure;
}



QUnit.module("getIfStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Index1: "10", Index2: "25",
            expected: "<c:if > </c:if>"}
    ]).test("Test getIfStructure function", function(params){
        var actualResult = getIfStructure(params.Index1, params.Index2);
        strictEqual(actualResult, params.expected);
    });
