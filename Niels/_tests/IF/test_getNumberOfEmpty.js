/**
 * Created by niels on 2/12/14.
 */
function getNumberOfEmpty(code)
{
    //count the number of "EMPTY"
    var numberOfEmpty = code.match(/empty/g).length;
    return numberOfEmpty;
}


QUnit.module("getNumberOfEmpty");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            IF1: '<c:if test="${empty fields && !empty object}">${thankyou}</c:if>',
            IF2: '<c:if test="${empty fields}">${thankyou}</c:if>',
            expected1: 2,
            expected2: 1
             }
    ]).test("Test getNumberOfEmpty function", function(params){
        var actualResult1 = getNumberOfEmpty(params.IF1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = getNumberOfEmpty(params.IF2);
        strictEqual(actualResult2, params.expected2);

    });