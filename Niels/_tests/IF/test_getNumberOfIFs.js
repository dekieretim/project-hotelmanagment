/**
 * Created by niels on 17/11/14.
 */


function getNumberOfIFs(code)
{
    //count the number of IFs
    var numberOfIFs = code.match(/<c:if/g).length;
    return numberOfIFs;
}


QUnit.module("getNumberOfIFs");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code: "blablabla if blablabla <c:if> </c:if>blablabla if blabla <c:if></c:if>",
            expected: 2}
    ]).test("Test getNumberOfIFs function", function(params){
        var actualResult = getNumberOfIFs(params.code);
        strictEqual(actualResult, params.expected);

    });
