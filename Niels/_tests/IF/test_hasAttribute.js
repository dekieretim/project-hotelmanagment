/**
 * Created by niels on 28/11/14.
 */

function hasAttribute(attribute, ifStructure)
{
    return (ifStructure.indexOf("" +attribute+ "") >= 0 ? true: false);
}


//TODO mee bezig
QUnit.module("hasAttribute");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            ATT1: "length", ATT2: "empty", ATT3: "startsWith",

            IF1: '<c:if test="${fn:length(kijkers)!=0}">' +
               '</c:if>',
            IF2: '<c:if test="${empty test}">' +
            '</c:if>',
            IF3: '<c:if test="${! empty test}">' +
            '</c:if>',
            IF4: '<c:if test="${not empty test}">' +
            '</c:if>',
            IF5: '<c:if test="${fn:startsWith(cat, )}">' +
                '</c:if>',
            IF6: '<c:if test="${fn:startWith(cat, )}">' +
               '</c:if>',


            expected1: true,
            expected2: true,
            expected3: true,
            expected4: true,
            expected5: true,
            expected6: false
        }
    ]).test("Test hasAttribute function for IF-structures", function(params){
        var actualResult1 = hasAttribute(params.ATT1, params.IF1);
        var actualResult2 = hasAttribute(params.ATT2, params.IF2);
        var actualResult3 = hasAttribute(params.ATT2, params.IF3);
        var actualResult4 = hasAttribute(params.ATT2, params.IF4);
        var actualResult5 = hasAttribute(params.ATT3, params.IF5);
        var actualResult6 = hasAttribute(params.ATT3, params.IF6);


        strictEqual(actualResult1, params.expected1);
        strictEqual(actualResult2, params.expected2);
        strictEqual(actualResult3, params.expected3);
        strictEqual(actualResult4, params.expected4);
        strictEqual(actualResult5, params.expected5);
        strictEqual(actualResult6, params.expected6);
    });


//ATT1: '<c:if test="${empty test}">' +
//    '</c:if>',
//    ATT2: '<c:if test="${not empty test}">' +
//    '</c:if>',
//    ATT3: '<c:if test="${! empty test}">' +
//    '</c:if>',
//    ATT4: '<c:if test="${fn:length(kijkers)!=0}">' +
//    '</c:if>',
//    ATT4: '<c:if test="${fn:startsWith(cat, 'w')}">' +
//    '</c:if>',