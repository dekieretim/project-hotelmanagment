/**
 * Created by niels on 28/11/14.
 */
function translateTags(code)
{
    code = code.replace(    /\<c\:if test\=\"\$\{/g,    '<% if('  );
    code = code.replace(    /\}\"\>/g,                    '){ %>'   );
    code = code.replace(    /\<\/c\:if\>/g,               '<% } %>' );
    return code;
}



QUnit.module("translate IF tags");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            IF1: '<c:if test="${test}">' +
            '</c:if>',

            expected1: '<% if(test){ %>' +
                '<% } %>',
            IF2: '<c:if test="${test}"> htmlcode </c:if>',

            expected2: '<% if(test){ %> htmlcode <% } %>'
             }
    ]).test("Test translateIFTags function", function(params){
        var actualResult1 = translateTags(params.IF1);
        var actualResult2 = translateTags(params.IF2);

        strictEqual(actualResult1, params.expected1);
        strictEqual(actualResult2, params.expected2);
    });

