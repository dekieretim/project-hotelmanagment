/**
 * Created by niels on 17/11/14.
 */
function translateIfStructure(code)
{

    var ifStructure = code;
    ifStructure = translateTags(ifStructure);

    if(hasAttribute("length") | hasAttribute("empty") | hasAttribute("startsWith")){
        ifStructure = translateAttributes(ifStructure); }

    return ifStructure;



    function hasAttribute(code)
    {
        return (ifStructure.indexOf("" +code+ "") >= 0 ? true: false);
    }

    function translateTags(code)
    {
//        ifStructure.replace(/\<c\:if test\=\"\$\{/g, '<% if(');
        code = code.replace(/\<c\:if/g, '<% if(');
        code = code.replace(/test\=\"\$\{/g, '');
        code = code.replace(/\}\"\>/g, '){ %>');
        code = code.replace(/\<\/c\:if\>/g, ' <% } %>');
        return code;
    }

    function translateAttributes(codenietvertaald)
    {
        var code = codenietvertaald;
        //if EMPTY is present
        if(hasAttribute("empty"))
        {
            if(getNumberOfEmpty(code) > 1)
            {
                var part1 = "<% if((typeof ",
                    part2 = " == undefined && ",
                    part3 = " == '') && (typeof ",
                    part4 = " == undefined && ",
                    part5 = " == '') %>",
                    variable = getVariables(code),
                    content = getContent(code),
                    endtag = "<% } %>";
                return (part1+variable+part2+variable+part3+variable+part4+variable+part5+content+endtag);
                //ifStructure2 = (part1+variable+part2+variable+part3+variable+part4+variable+part5+content+endtag);
            }
            else
            {
                if(hasAttribute("not empty") | hasAttribute("! empty"))
                {
                    var part1 = "<% if(typeof ",
                        part2 = " != undefined && ",
                        part3 = " != '') %>",
                        variable = getVariables(code),
                        content = getContent(code),
                        endtag = "<% } %>";

                    return (part1+variable+part2+variable+part3+content+endtag);
                    //ifStructure2 = (part1+variable+part2+variable+part3+content+endtag);
                }
                else
                {
                    var part1 = "<% if(typeof ",
                        part2 = " == undefined && ",
                        part3 = " == '') %>",
                        variable = getVariables(code),
                        content = getContent(code),
                        endtag = "<% } %>";
                    return (part1+variable+part2+variable+part3+content+endtag);
                    //ifStructure2 = (part1+variable+part2+variable+part3+content+endtag);
                    //ifStructure2 = (part1+variable+part2+variable+part3+endtag);
                }
            }
        }

        //if startsWith is present
        if(hasAttribute("startsWith"))
        {
//                    var firstBracket = IfStructure.indexof("(");
//                    var lastBracket = IfStructure.indeox(")");
//                    var comma = IfStructure.indexOf(",");

            //het !-teken wordt niet vertaald en blijft staat
            //je krijgt dan if(! cat.startsWith("w"){}

//            var stringToBeFound = IfStructure.slice(comma, lastBracket);
//
//            IfStructure.replace(/fn\:startsWith\(/g, '' );
//            IfStructure.replace(/\,/, '.lastIndexOf(');
//            IfStructure.replace(/\)/, ',0) == 0)');

            code = code.replace(/fn\:startsWith\(/, '');
            code = code.replace(/\, \'/, '.charAt(0)=="');
            code = code.replace(/\'\)/, '"');
            return code;
        }

        //if LENGTH is present
        if(hasAttribute("length"))
        {
            code = code.replace(/fn\:length\(/g, '' );
            code = code.replace(/\)/, '.length() ');
            return code;
        }





        //returns the number of 'empty' present
        function getNumberOfEmpty(code)
        {
            //count the number of "EMPTY"
            var numberOfEmpty = code.match(/empty/g).length;
            return numberOfEmpty;
        }

        //returns the variable present inside the IF
        //OK
        function getVariables(code)
        {
            var index0 = code.indexOf("empty") +6;
            var index1 = code.indexOf(")", index0) ;
            //return code.slice(index0, index1);
            return code.slice(index0, index1);
        }

        //returns all miscellaneous code between begin- and endtag
        //OK
        function getContent(code)
        {
            var index0 = code.indexOf("%>") +2;
            var index1 = code.indexOf("<%", index0);
            return code.slice(index0, index1);
        }
    }


}
var hulp = "'w'";


QUnit.module("translateIfStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            IF1: '<c:if test="${empty fields}">' +
                '${thankyou}' +
                '</c:if>',

            IF2 : '<c:if test="${not empty fields}">' +
                '${thankyou}' +
                '</c:if>',

            IF3 : '<c:if test="${! empty fields}">' +
                '${thankyou}' +
                '</c:if>',

            IF4: '<c:if test="${fn:length(kijkers)!=0}">' +
                '${thankyou}' +
                '</c:if>',

            //ik gebruik HULP omdat 'w' enkele quotes heeft
            IF5: '<c:if test="${fn:startsWith(cat, ' +hulp+ ')}">' +
                '${thankyou}' +
                '</c:if>',
//
            IF7: '<c:if test="${(login.level >= 99) && (editPage.link != "global")}">' +
                '${thankyou}' +
                '<div class="page">\n'+
                '</c:if>',

            expected1: "<% if(typeof fields == undefined && fields == '') %>" +
                "${thankyou}" +
                " <% } %>",
            expected2: "<% if(typeof fields != undefined && fields != '') %>" +
                "${thankyou}" +
                " <% } %>",
            expected3: "<% if(typeof fields != undefined && fields != '') %>"+
                "${thankyou}" +
                " <% } %>",
            expected4: '<% if( kijkers.length() !=0){ %>' +
                "${thankyou}" +
                " <% } %>",
            expected5: '<% if( cat.charAt(0)=="w"){ %>' +
                "${thankyou}" +
                " <% } %>",
//            expected6: "<% if(object.extention != 0){ %>" +
//                "${thankyou}" +
//                "<% } %>",
            expected7: '<% if( (login.level >= 99) && (editPage.link != "global")){ %>' +
                '${thankyou}' +
                '<div class="page">\n'+
                ' <% } %>'
        }
    ]).test("Test translateIfStructure function", function(params){
        var actualResult1 = translateIfStructure(params.IF1);
        var actualResult2 = translateIfStructure(params.IF2);
        var actualResult3 = translateIfStructure(params.IF3);
        var actualResult4 = translateIfStructure(params.IF4);
        var actualResult5 = translateIfStructure(params.IF5);
//        var actualResult6 = translateIf(params.IF6);
        var actualResult7 = translateIfStructure(params.IF7);
        strictEqual(actualResult1, params.expected1);
        strictEqual(actualResult2, params.expected2);
        strictEqual(actualResult3, params.expected3);
        strictEqual(actualResult4, params.expected4);
        strictEqual(actualResult5, params.expected5);
//        strictEqual(actualResult6, params.expected6);
        strictEqual(actualResult7, params.expected7);
    });


//ATT1: "length", ATT2: "empty", ATT3: "startsWith",
//
//    IF1: '<c:if test="${fn:length(kijkers)!=0}">' +
//    '</c:if>',
//    IF2: '<c:if test="${empty test}">' +
//    '</c:if>',
//    IF3: '<c:if test="${! empty test}">' +
//    '</c:if>',
//    IF4: '<c:if test="${not empty test}">' +
//    '</c:if>',
//    IF5: '<c:if test="${fn:startsWith(cat, )}">' +
//    '</c:if>',
//    IF6: '<c:if test="${fn:startWith(cat, )}">' +
//    '</c:if>',