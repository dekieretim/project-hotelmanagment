/**
 * Created by niels on 18/11/14.
 */
function getIncludeStructure(code)
{
    //volledige include
    var beginInclude = code.indexOf("<%@ include");
    var endInclude = code.indexOf('.jsp"%>', beginInclude) +7;
    return code.slice(beginInclude, endInclude);

}



QUnit.module("getIncludeStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code: 'blablabla if blablabla <%@ include file="/views/menu-top.jsp"%>blablabla',
            expected: '<%@ include file="/views/menu-top.jsp"%>' }
    ]).test("Test getIncludeStructure function", function(params){
        var actualResult = getIncludeStructure(params.code);
        strictEqual(actualResult, params.expected);

    });