/**
 * Created by niels on 18/11/14.
 */
function translateIncludeStructure(includeStructure)
{
    // trim onnodige code
    //VOOR: <%@ include file="/views/....jsp" %>
    //NA:   <%  include ... %>
    //of    <% include admin/... %>

    includeStructure = includeStructure.replace('@', '');
    includeStructure = includeStructure.replace('file="/views/', '');
    includeStructure = includeStructure.replace('.jsp"', '');

    return includeStructure
}



QUnit.module("translateInclude");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Include1: '<%@ include file="/views/menu-top.jsp" %>',
            expected1: "<% include menu-top %>"}
    ]).test("Test translateIncludeStructure function", function(params){
        var actualResult1 = translateIncludeStructure(params.Include1);
        strictEqual(actualResult1, params.expected1);
    });