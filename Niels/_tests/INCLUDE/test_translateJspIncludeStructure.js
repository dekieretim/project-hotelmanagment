/**
 * Created by niels on 30/12/14.
 */

function translateJspIncludeStructure(structure)
{
//    <jsp:include page="header.jsp">                             //vertalen
//        <jsp:param name="title" value="${page.title}"/>         //verwijderd
//    </jsp:include>                                              //verwijderd

    structure = structure.replace('<jsp:include page="', '<% include ');
    structure = structure.replace('.jsp"', ' %');

    return structure;
}



QUnit.module("translateJSPInclude");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Include1: '<jsp:include page="header.jsp">',
            expected1: "<% include header %>"}
]).test("Test translateJSPIncludeStructure function", function(params){
        var actualResult1 = translateJspIncludeStructure(params.Include1);
        strictEqual(actualResult1, params.expected1);
    });
                                       //verwijderen