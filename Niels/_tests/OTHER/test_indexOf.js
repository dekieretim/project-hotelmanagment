/**
 * Created by niels on 8/12/14.
 */
var hulpstring = '0123test8910';

function testingIndexOf()
{
    return hulpstring.slice(4,8);
}




QUnit.module("test indexOf");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",


            expected: "test" }
    ]).test("Test INDEXOF function", function(params){
        var actualResult1 = testingIndexOf(params.code1);

        strictEqual(actualResult1, params.expected);

    });