/**
 * Created by niels on 13/11/14.
 */
var translatedCode = "";
var codeToTranslate = "0123456789<c:if > </c:if>0123456789";

function moveUnusedCode(beginIndex, endIndex)
{
    var removedCode = codeToTranslate.slice(beginIndex, endIndex);
    translatedCode += removedCode;
    //just to be sure index 0 is the index where code starts again
    codeToTranslate = codeToTranslate.replace(removedCode, "");
}




QUnit.module("moveUnusedCode");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Index1: "0", Index2: "10",
            expected1: "<c:if > </c:if>0123456789",
            expected2: "0123456789"}
    ]).test("Test moveUnusedCode function", function(params){
        moveUnusedCode(params.Index1, params.Index2);
        strictEqual(translatedCode, params.expected2);
        strictEqual(codeToTranslate, params.expected1);
    });

