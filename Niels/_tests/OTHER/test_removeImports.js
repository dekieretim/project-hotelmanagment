/**
 * Created by niels on 9/12/14.
 */
function removeImports(code)
{
    //remove all occurrences of page and tag libraries
    code = code.replace(/\<\%\@\spage.*\%\>/g, "");
    code = code.replace(/\<\%\@\staglib.*\%\>/g, "");
    return code;
}


QUnit.module("test removeImports");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code:   '<!doctype html>'+
                    '<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>'+
                    '<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>'+
                    '<html lang="nl">',

            expected:   '<!doctype html>'+

                        '<html lang="nl">'
        }
    ]).test("Test removeImports function", function(params){
        var actualResult1 = removeImports(params.code);

        strictEqual(actualResult1, params.expected);

    });






