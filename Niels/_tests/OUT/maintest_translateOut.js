/**
 * Created by niels on 11/12/14.
 */
/**
 * Created by niels on 4/11/14.
 */

//escapeXml is standaard true; er is slechts 1 voorkomen hiervan in de code en die is true = weglaten
function translateOut(code)
{
    var translatedCode = "",
        codeToTranslate = code,
        totalOccurrencesOut = getNumberOfOUTs(codeToTranslate);

    const index0 = 0;

    for(var i = 0; i <= totalOccurrencesOut;i++)    {
    //while((codeToTranslate.match(/\<c\:out/g)).length>= 0){
        //place all unused code in translatedCode
        //moveUnusedCode(index0, getIndexBeginOfOUTStructure(codeToTranslate));
        //translatedCode += translateOutStructure(getOutStructure(index0, getIndexEndOfOUTStructure(codeToTranslate)));
        //codeToTranslate = codeToTranslate.replace(getOutStructure(index0, getIndexEndOfOUTStructure(codeToTranslate)), "");
//        if((codeToTranslate.indexOf("<c:out")) < 0)
//        {
//            translatedCode += codeToTranslate;
//        }
        codeToTranslate = codeToTranslate.replace('escapeXml="true" ', "");
        codeToTranslate = codeToTranslate.replace('<c:out value="${', "<%= ");
        codeToTranslate = codeToTranslate.replace('}" />', "%>");


    }

return codeToTranslate;

    //returns the number of keyword "<:out" present
    function getNumberOfOUTs(code)
    {
        //count the number of Out
        var numberOfOUTs = code.match(/\<c\:out/g).length;
        return numberOfOUTs;
    }



    //main
    //OK
    function translateOutStructure(code)
    {
            var varToTranslate = getOutVariable(code);
            var translatedVar = addVariableTags(varToTranslate);

        return translatedVar;

        //returns the variable of the OUTstructure
        //OK
        function getOutVariable(code)
        {
            var beginIndexOfKeywordOUT = code.indexOf("<c:out");
            var index0 = code.indexOf('value="${', beginIndexOfKeywordOUT) +9;
            var index1 = code.indexOf('}" />', index0);
            return code.slice(index0, index1);
        }

        //translates the variable of the OUTstructure
        function addVariableTags(variable)
        {
            if(variable != null){
            var test = "<%= " + variable + "%>";
            }
            return test;
        }
    }

    //returns a OUTstructure
    //OK
    function getOutStructure(index0, index1)
    {
//        var index0 = code.indexOf(/\<\:out/);
//        var index1 = code.indexOf(/\/\>/, index0) +1;   //+1 omdat anders de '>' niet meegerekent wordt
        return codeToTranslate.slice(index0, index1);
    }



    //FUNCTIONS

    //returns the begin-index of the out structure
    //OK
    function getIndexBeginOfOUTStructure(code)
    {
        return code.indexOf("<c:out");
    }

    //returns the end-index of the out structure
    //OK
    function getIndexEndOfOUTStructure(code)
    {
        var beginIndex = code.indexOf("<c:out");
        var endIndex = code.indexOf('}" />', beginIndex)+5;

        return endIndex
    }


    function moveUnusedCode(beginIndex, endIndex)
    {
        var removedCode = codeToTranslate.slice(beginIndex, endIndex);
        translatedCode += removedCode;
        //just to be sure index 0 is the index where code starts again
        codeToTranslate.replace(removedCode, "");
    }
}



//omdat de vertaling hier in quotes geplaatst moet worden, wordt bij gevallen als deze
//  variabelen gebruikt om de single quotes uit de code te halen, zodat er geen mengeling is van single
//  en double quotes BINNENIN single of double quotes. Er derde soort quotes zou hier handig zijn :p
var body = "'#body'";
//error invalid string
//var htmlContent = "'${fn:replace(fn:replace(submit.body,'\'','"'),'"','&quot;')}'";
var open = "'Open'";


QUnit.module("maintest_translateOut");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code:       '</form>\n'+
                '<table id="records">\n'+
                '<tr><th>Received on</th><th>At</th><th>Status</th><th>User</th><th>Page</th><th>Action</th></tr>\n'+
                '<c:forEach var="submit" items="${submits}">\n'+
                '<tr><td><fmt:formatDate value="${submit.date}" pattern="dd MMM yyyy" type="date"/></td>\n'+
                '<td><c:out value="${submit.time}" /></td>\n'+
                '<td><c:out value="${submit.status}" /></td>\n'+
                '<td><c:out value="${submit.userName}" /></td>\n'+
                '<td><c:out value="${submit.pageTitle}" /></td>\n'+
                '<c:if test="${submit.status == ' +open+ '}"> | <a href="#" onClick="doDone(${submit.id})">done</a> </c:if></td></tr>\n'+
                '</c:forEach>\n'+
                '</table>\n'+
                '<hr />\n'+
                '<p id="body">\n'+
                '&nbsp;\n'+
                '</p>\n'+
                '</c:if>\n'+
            '</div>\n' +
                '<c:out value="${editData}" escapeXml="true" />',

            expected:   '</form>\n'+
                '<table id="records">\n'+
                '<tr><th>Received on</th><th>At</th><th>Status</th><th>User</th><th>Page</th><th>Action</th></tr>\n'+
                '<c:forEach var="submit" items="${submits}">\n'+
                '<tr><td><fmt:formatDate value="${submit.date}" pattern="dd MMM yyyy" type="date"/></td>\n'+
                '<td><%= submit.time%></td>\n'+
                '<td><%= submit.status%></td>\n'+
                '<td><%= submit.userName%></td>\n'+
                '<td><%= submit.pageTitle%></td>\n'+
                '<c:if test="${submit.status == ' +open+ '}"> | <a href="#" onClick="doDone(${submit.id})">done</a> </c:if></td></tr>\n'+
                '</c:forEach>\n'+
                '</table>\n'+
                '<hr />\n'+
                '<p id="body">\n'+
                '&nbsp;\n'+
                '</p>\n'+
                '</c:if>\n'+
                '</div>\n'+
                '<%= editData%>'

        }
    ]).test("MainTest translateOut function", function(params){
        var actualResult = translateOut(params.code);
        strictEqual(actualResult, params.expected);
    });

