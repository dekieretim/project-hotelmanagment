/**
 * Created by niels on 4/12/14.
 */
function getIndexBeginOfOUTStructure(code)
{
    return code.indexOf("<c:out")
}

QUnit.module("getIndexBeginOfOutStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            OUT1: 'randomcode' +
            '<c:out value="${submit.time}" />' +
                'randomcode',
            expected: 10
        }
    ]).test("Test getIndexBeginOfOutStructure function", function(params){
        var actualResult = getIndexBeginOfOUTStructure(params.OUT1);
        strictEqual(actualResult, params.expected);
    });
