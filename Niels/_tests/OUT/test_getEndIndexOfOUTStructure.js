/**
 * Created by niels on 4/12/14.
 */
function getIndexEndOfOUTStructure(code)
{
    var beginIndex = code.indexOf("<c:out");
    var endIndex = code.indexOf('}" />', beginIndex)+5;

    return endIndex
}

QUnit.module("getIndexEndOfOutStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            OUT1: 'randomcode' +
                '<c:out value="${submit.time}" />' +
                'randomcode' +
                '<c:out value="${out.out2}" />' +
                'randomcode',

            expected1: 42,
            OUT2: '' +
                '<c:out value="${submit.time}" />' +
                'randomcode' +
                '<c:out value="${out.out2}" />' +
                'randomcode',

            expected2: 32
        }
    ]).test("Test getIndexEndOfOutStructure function", function(params){
        var actualResult1 = getIndexEndOfOUTStructure(params.OUT1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = getIndexEndOfOUTStructure(params.OUT2);
        strictEqual(actualResult2, params.expected2);
    });