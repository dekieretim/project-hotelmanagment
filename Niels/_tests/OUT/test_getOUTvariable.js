/**
 * Created by niels on 4/12/14.
 */
function getOutVariable(code)
{
    var index0 = code.indexOf('value="${') +9;
    var index1 = code.indexOf('}" />');
    return code.slice(index0, index1);
}



QUnit.module("getOutVariable");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Out1:
                'randomcode' +
                '<c:out value="${submit.time}" />' +
                'randomcode',
            expected1:'submit.time',
            Out2:
                '<c:out value="${submit.time}" />' ,

            expected2:'submit.time'
        }
    ]).test("Test getOutVariable function", function(params){
        var actualResult1 = getOutVariable(params.Out1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = getOutVariable(params.Out2);
        strictEqual(actualResult2, params.expected2);
    });