/**
 * Created by niels on 4/12/14.
 */

var code =  'randomcode' +
            '<c:out value="${submit.time}" />' +
            'randomcode';

function getOutStructure(index0, index1)
{
//        var index0 = code.indexOf(/\<\:out/);
//        var index1 = code.indexOf(/\/\>/, index0) +1;   //+1 omdat anders de '>' niet meegerekent wordt
    return code.slice(index0, index1);
}




QUnit.module("getOutStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Index1: "10", Index2: "42",
            expected:'<c:out value="${submit.time}" />'
        }
    ]).test("Test getOutStructure function", function(params){
        var actualResult = getOutStructure(params.Index1, params.Index2);
        strictEqual(actualResult, params.expected);
    });












