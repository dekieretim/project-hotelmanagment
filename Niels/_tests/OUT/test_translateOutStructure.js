/**
 * Created by niels on 11/12/14.
 */
function translateOutStructure(structure)
{
    var varToTranslate = getOutVariable(structure);
    var translatedVar = addVariableTags(varToTranslate);

    return translatedVar;


    //additional functions
    function getOutVariable(code)
    {
        var beginIndexOfKeywordOUT = code.indexOf("<c:out");
        var index0 = code.indexOf('value="${', beginIndexOfKeywordOUT) +9;
        var index1 = code.indexOf('}" />', index0);
        return code.slice(index0, index1);
    }

    //translates the variable of the OUTstructure
    function addVariableTags(variable)
    {
        var test = "<%= " + variable + "%>";
        return test;
    }
}



QUnit.module("translateOutStructure");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code:
                    '<c:out value="${submit.time}" />',


            expected:
                    '<%= submit.time%>'
        }
    ]).test("MainTest translateOutStructure function", function(params){
        var actualResult = translateOutStructure(params.code);
        strictEqual(actualResult, params.expected);
    });