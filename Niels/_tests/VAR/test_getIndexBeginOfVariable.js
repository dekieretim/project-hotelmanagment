/**
 * Created by niels on 8/12/14.
 */
function getIndexBeginOfVariable(code)
{
    //aangepast door QUNIT-test
    return code.indexOf("${");
}

QUnit.module("getIndexBeginOfVariable");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code: '01234567${page.title}'+
                '<h3>Home<c:forEach var="p" items="${path}"> &gt; ${p.title}</c:forEach></h3>'+
                '<p>${content.data}</p>',
            expected: 8 }
    ]).test("Test getIndexBeginOfVariable function", function(params){
        var actualResult = getIndexBeginOfVariable(params.code);
        strictEqual(actualResult, params.expected);
    });