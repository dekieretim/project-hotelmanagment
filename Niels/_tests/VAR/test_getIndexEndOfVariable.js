/**
 * Created by niels on 8/12/14.
 */
function getIndexEndOfVariable(code)
{
    var beginVar = code.indexOf("${");
    return code.indexOf("}", beginVar) + 1;
}

QUnit.module("getIndexEndOfVariable");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code: '01234567${page.title}'+
                '<h3>Home<c:forEach var="p" items="${path}"> &gt; ${p.title}</c:forEach></h3>'+
                '<p>${content.data}</p>',
            expected: 21 }
    ]).test("Test getIndexEndOfVariable function", function(params){
        var actualResult = getIndexEndOfVariable(params.code);
        strictEqual(actualResult, params.expected);
    });
