/**
 * Created by niels on 24/11/14.
 */
function getVariable(code)
{
    //zoeken naar ${
    var beginVar = code.indexOf("${");
    //zoeken naar de END-tag
    var endVar = code.indexOf("}", beginVar) + 1;
    varStructure = code.slice(beginVar, endVar);

    return varStructure;
}



QUnit.module("getVariable");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code1: '0123456789'+
            '<h1>${page.title}</h1>'+
            '<h3>Home<c:forEach var="p" items="${path}"> &gt; ${p.title}</c:forEach></h3>'+
            '<p>${content.data}</p>',
            expected1: "${page.title}",
            code2: '${page.title}</h1>'+
                '<h3>Home<c:forEach var="p" items="${path}"> &gt; ${p.title}</c:forEach></h3>'+
                '<p>${content.data}</p>',
            expected2: "${page.title}"
        }
    ]).test("Test getVariable function", function(params){
        var actualResult1= getVariable(params.code1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = getVariable(params.code2);
        strictEqual(actualResult2, params.expected2);
    });