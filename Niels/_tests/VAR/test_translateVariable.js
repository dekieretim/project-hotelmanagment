/**
 * Created by niels on 24/11/14.
 */
function translateVariable(variable)
{
    var help = variable;
    help = help.replace(/\$\{/g, '<%= ');
    help = help.replace(/\}/g, '%>');

    return help;
}


QUnit.module("translateVariable");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code1: '${page.title}',
            expected: "<%= page.title%>" }
    ]).test("Test translateVariable function", function(params){
        var actualResult1 = translateVariable(params.code1);

        strictEqual(actualResult1, params.expected);

    });