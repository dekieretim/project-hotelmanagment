/**
 * Created by niels on 28/12/14.
 */
function maintestTranslate(jspCode) {

    if(typeof jspCode == "string")
    {
        var codeToTranslate = jspCode;
        codeToTranslate = translateAllBeginTagStructures(codeToTranslate);
        codeToTranslate = translateNonChangeableTags(codeToTranslate);
        codeToTranslate = deleteUOverflowingCode(codeToTranslate);
        codeToTranslate = translateAllVariables(codeToTranslate);

        //returns the translated code
        return codeToTranslate;
    }
    else
    {
        console.log("ERROR WHILE LOADING STRING");
        return null;
    }

    //FUNCTIONS

    //CHANGE
    function translateAllBeginTagStructures(code)
    {

//        if(getNumberOfOccurrences("if", code) > 0)
//        {
//            code = translateBeginTagsIf(code);
//        }
//        if(getNumberOfOccurrences("forEach", code) > 0)
//        {
//            code = translateBeginTagsForEach(code);
//        }
//        if(getNumberOfOccurrences("include", code) > 0)
//        {
//            code = translateInclude(code);
//        }
//        if(getNumberOfOccurrences("when", code) > 0)
//        {
//            code = translateWhen(code);
//        }
        code = translateBeginTagsIf(code);
        code = translateBeginTagsForEach(code);
        code = translateWhen(code);
        code = translateInclude(code);
        return code;

        //FUNCTIONS
        function translateBeginTagsIf(code){

            //////////////////////////////////////////////////////
            var codeToTranslate = code,
                translatedCode = '',
                totalOccurrencesIf = getNumberOfIFs(codeToTranslate);

            while(totalOccurrencesIf !=0)
            {
                var structure = getIfStructure(getIndexBeginOfIFStructure(codeToTranslate), getIndexEndOfIFStructure(codeToTranslate));
                //removes the 'useless' code from codeToTranslate to translatedCode
                codeToTranslate = moveUnusedCode(0, getIndexBeginOfIFStructure(codeToTranslate), codeToTranslate);
                //adds the translated structure to translatedCode
                translatedCode += translateIfStructure(structure);
                //remove original if structure from codeToTranslate
                codeToTranslate = codeToTranslate.replace(structure, "");
                totalOccurrencesIf--;
            }
            appendCode();

            return translatedCode;
            //////////////////////////////////////////////////////


            //FUNCTIONS

            //appends the left-over code
            function appendCode()
            {
                if(codeToTranslate.length > 0)
                {
                    translatedCode += codeToTranslate;
                }
            }

            //returns begin-index of IF
            function getIndexBeginOfIFStructure(code)
            {
                //aangepast door QUNIT-test
                return code.indexOf("<c:if");
            }

            //new
            function getIndexEndOfIFStructure(code)
            {
                //+4 omdat anders niet de hele IF gereturnd wordt
                var beginIndex = code.indexOf('<c:if');
                var endIndex = code.indexOf('}">', beginIndex)+3;
                return endIndex;
            }

            //OK
            //moves "not to be translated"-code to translatedCode
            //removes "not to be translated"-code from codeToTranslate
            function moveUnusedCode(beginIndex, endIndex, code)
            {
                var removedCode = code.slice(beginIndex, endIndex);
                //move unused code to translatedCode
                translatedCode += removedCode;
                //remove the unused code from codeToTranslate
                code = code.replace(removedCode, "");
                return code;
            }

            //OK
            //returns a complete IF-structure
            function getIfStructure(beginIndex, endIndex)
            {
                return codeToTranslate.slice(beginIndex, endIndex);
            }

            //OK
            //returns the number of IF's present in code
            function getNumberOfIFs(code)
            {
                if(code.match(/<c:if/g) != null)
                {
                    return code.match(/\<c\:if/g).length;
                }
                else
                {
                    return 0;
                }
            }



            //OK
            function translateIfStructure(code)
            {
                var ifStructure = code;
                //ifStructure = translateTags(ifStructure);

                if(hasAttribute("length") | hasAttribute("empty") | hasAttribute("startsWith"))
                {
                    ifStructure = translateAttributes(ifStructure);
                }
                else
                {
                    ifStructure = translateTags(ifStructure);
                }

                return ifStructure;



                function hasAttribute(code)
                {
                    return (ifStructure.indexOf("" +code+ "") >= 0 ? true: false);
                }

                function translateTags(code)
                {
                    //ifStructure.replace(/\<c\:if test\=\"\$\{/g, '<% if(');
                    code = code.replace(/\<c\:if/g, '<% if(');
                    code = code.replace(/test\=\"\$\{/g, '');
                    code = code.replace(/\}\"\>/g, '){ %>');
                    return code;
                }

                function translateAttributes(code)
                {
                    var ifStructure = code;
                    //if EMPTY is present
                    if(hasAttribute("empty"))
                    {
                        if(getNumberOfEmpty(ifStructure) > 1)
                        {
                            var part1 = "<% if((typeof ",
                                part2 = " == undefined && ",
                                part3 = " == '') && (typeof ",
                                part4 = " == undefined && ",
                                part5 = " == '') %>",
                                variable = getVariables(ifStructure);
//                        content = getContent(ifStructure),
//                        endtag = "<% } %>";
                            return (part1+variable+part2+variable+part3+variable+part4+variable+part5);
                        }
                        else
                        {
                            if(hasAttribute("not empty") | hasAttribute("! empty"))
                            {
                                var part1 = "<% if(typeof ",
                                    part2 = " != undefined && ",
                                    part3 = " != '') %>",
                                    variable = getVariables(ifStructure);
//                            content = getContent(code),
//                            endtag = "<% } %>";

                                return (part1+variable+part2+variable+part3);
                            }
                            else
                            {
                                var part1 = "<% if(typeof ",
                                    part2 = " == undefined && ",
                                    part3 = " == '') %>",
                                    variable = getVariables(ifStructure);
//                            content = getContent(code),
//                            endtag = "<% } %>";
                                return (part1+variable+part2+variable+part3);
                            }
                        }
                    }

                    //if startsWith is present
                    if(hasAttribute("startsWith"))
                    {
                        code = code.replace(/fn\:startsWith\(/, '');
                        code = code.replace(/\, \'/, '.charAt(0)=="');
                        code = code.replace(/\'\)/, '"');
                        return code;
                    }

                    //if LENGTH is present
                    if(hasAttribute("length"))
                    {
                        code = code.replace(/fn\:length\(/g, '' );
                        code = code.replace(/\)/, '.length() ');
                        return code;
                    }

                    //returns the number of 'empty' present
                    function getNumberOfEmpty(code)
                    {
                        //count the number of "EMPTY"
                        var numberOfEmpty = code.match(/empty/g).length;
                        return numberOfEmpty;
                    }

                    //returns the variable present inside the IF
                    //OK
                    function getVariables(code)
                    {
                        var index0 = code.indexOf("empty") +6;
                        var index1 = code.indexOf(")", index0) ;
                        //return code.slice(index0, index1);
                        return code.slice(index0, index1);
                    }

                    //returns all miscellaneous code between begin- and endtag
                    //OK
//            function getContent(code)
//            {
//                var index0 = code.indexOf("%>") +2;
//                var index1 = code.indexOf("<%", index0);
//                return code.slice(index0, index1);
//            }
                }

            }
        }
        function translateBeginTagsForEach(code){

            //////////////////////////////////////////////////////
            var codeToTranslate = code,
                translatedCode = '',
                totalOccurrencesForeach = getNumberOfForEachs(code);

            while(totalOccurrencesForeach !=0)
            {
                var structure = getForEachStructure(getIndexBeginOfForEachStructure(codeToTranslate), getIndexEndOfForEachStructure(codeToTranslate));
                //removes the 'useless' code from codeToTranslate to translatedCode
                codeToTranslate = moveUnusedCode(codeToTranslate);
                //adds the translated structure to translatedCode
                translatedCode += translateForEachStructure(structure);
                //remove original forEach structure from codeToTranslate
                codeToTranslate = codeToTranslate.replace(structure, "");
                totalOccurrencesForeach--;
            }
            appendCode();

            return translatedCode;
            //////////////////////////////////////////////////////



            //FUNCTIONS

            //appends the left over code
            function appendCode()
            {
                if(codeToTranslate.length != 0)
                {
                    translatedCode += codeToTranslate;
                }
            }

            //returns begin-index of FOREACH
            //OK
            function getIndexBeginOfForEachStructure(code)
            {
                //aangepast door QUNIT-test
                return code.indexOf("<c:forEach");
            }

            //returns end-index of FOREACH
            //OK
            function getIndexEndOfForEachStructure(code)
            {
                var beginIndex = code.indexOf('<c:forEach');
                if(beginIndex !=-1){
                    var endIndex = code.indexOf('">', beginIndex)+3;
                    return endIndex;
                }
                return console.log('All Foreach structures translated')
            }


            //moves "not to be translated"-code to translatedCode
            //removes "not to be translated"-code from codeToTranslate
            function moveUnusedCode(code)
            {
                var removedCode = code.slice(0, getIndexBeginOfForEachStructure(code));
                //move unused code to translatedCode
                translatedCode += removedCode;
                //remove the unused code from codeToTranslate
                code = code.replace(removedCode, "");
                return code;
            }


            //returns a complete ForEach-structure
//OK
            function getForEachStructure(beginIndex, endIndex)
            {
                return codeToTranslate.slice(beginIndex, endIndex);
            }


            //returns the number of FOREACH's present in code
            function getNumberOfForEachs(code)
            {
                if(code.match(/<c:forEach/g) != null)
                {
                    return code.match(/<c:forEach/g).length;
                }
                else
                {
                    return 0;
                }
                //return code.match(/<c:forEach/g).length;
            }

//OK
            //returns the whole innercontent between the FOREACH-tags
            function getForEachInnerContent(code)
            {
                var index0 = code.indexOf('">') +2;

                var index1 = code.indexOf("</c:forEach>", index0);
                return code.slice(index0, index1);
            }




            //OK
            //Translates any forEachStructure
            function translateForEachStructure(ForEachStructure){

                //get the array
                var elements = getForEachVarItems(ForEachStructure);
                //get the single element
                var element = getForEachVar(ForEachStructure);

                //get the element representing varStatus
                if(ForEachStructure.indexOf("varStatus") >= 0)
                {
                    var statusPresent = true;
                    var varStatusElement = getForEachVarStatus(ForEachStructure);
                }


                //get the inner content (html, other structures, comments...)
                //var innerContent = getForEachInnerContent(ForEachStructure);
                //var endTag = "<% }) %>";

                //assembles a .forEach(function(){})-structure from scratch
                var newForEachStructure = "<% " + elements +".forEach(function(" + element
                    + (statusPresent? ", ": "") + (statusPresent? varStatusElement : "") + "){ %>\n";


                return newForEachStructure;



                //FUNCTIONS
                //OK
                //returns the variable representing varStatus
                function getForEachVarStatus(forEachStructure)
                {
                    var beginIndex = forEachStructure.indexOf("varStatus") +11;
                    var endIndex = forEachStructure.indexOf('">', beginIndex) ;

                    return forEachStructure.slice(beginIndex, endIndex);
                }



                //OK
                //returns the variable of FOREACH
                function getForEachVar(forEachStructure){


                    var argumentsBegin = forEachStructure.indexOf("var") + 5;
                    //argumentsEnd = forEachStructure.indexOf('"')[2] - 1;
                    var argumentsEnd = forEachStructure.indexOf("items") - 2;
                    //var arguments = IfStructure.splice(argumentsBegin, argumentsEnd);

                    var forEachVar = forEachStructure.slice(argumentsBegin, argumentsEnd);

                    return forEachVar;
                }

                //OK
                //returns the items-collection of FOREACH
                function getForEachVarItems(forEachStructure){

                    //VarITEMS
                    var argumentsBegin = forEachStructure.indexOf("items") + 9;
                    var argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);

                    var items = forEachStructure.slice(argumentsBegin, argumentsEnd);

                    return items;
                }
            }



        }
        function translateWhen(code){
            //////////////////////////////////////////////////////
            var translatedCode = "",
                codeToTranslate = code,
                totalOccurrencesWhen = getNumberOfWhens(codeToTranslate);

            const index0 = 0;

            //translates the begin- and endtag of otherwise and endtag of when
            translateWhenAndOtherwiseTags();
            //loop the whole document till all IF's are translated
            //with numberOfIFs used, the document is only searched once, instead of every time for each loop
            while(totalOccurrencesWhen !=0)
            {
                //place all unused code in translatedCode
                var structure = getWhenStructure(getIndexBeginOfWhenStructure(codeToTranslate), getIndexEndOfWhenStructure(codeToTranslate));
                //removes the 'useless' code from codeToTranslate to translatedCode
                codeToTranslate = moveUnusedCode(codeToTranslate);
                //if(codeToTranslate.indexOf("<c:when") >= 0)
                //adds the translated structure to translatedCode
                translatedCode += translateWhenBeginTag(structure);
                codeToTranslate = codeToTranslate.replace(structure, "");

                totalOccurrencesWhen--;
            }
            appendCode();
            return translatedCode;
            /////////////////////////////////////////////////////

            //FUNCTIONS

            //appends the left over code
            function appendCode()
            {
                if(codeToTranslate.length > 0)
                {
                    translatedCode += codeToTranslate;
                }
            }

            //returns a WHEN-structure
            function getWhenStructure(beginIndex, endIndex)
            {
                return codeToTranslate.slice(beginIndex, endIndex);
            }

            //OK
            function translateWhenBeginTag(code)
            {
                var beginIndex = code.indexOf("<c:when")+16;
                var endIndex = code.indexOf('}">', beginIndex);
                var innerContent = code.slice(beginIndex, endIndex);

                return "<% if(" + innerContent + "){ %>\n";
            }

            //OK
            //returns begin-index of IF
            function getIndexBeginOfWhenStructure(code)
            {
                //aangepast door QUNIT-test
                return code.indexOf("<c:when");
            }

            //OK
            //returns end-index of When
            function getIndexEndOfWhenStructure(code)
            {
                var beginIndex = code.indexOf("<c:when");
                return (code.indexOf('">', beginIndex) +3);
            }

            //OK
            //moves "not to be translated"-code to translatedCode
            //removes "not to be translated"-code from codeToTranslate
            function moveUnusedCode(code)
            {
                var removedCode = code.slice(0, getIndexBeginOfWhenStructure(code));
                //move unused code to translatedCode
                translatedCode += removedCode;
                //remove the unused code from codeToTranslate
                code = code.replace(removedCode, "");
                return code;
            }

            //OK
            function getNumberOfWhens(code)
            {
                if(code.match(/<c:when/g))
                {
                    return code.match(/<c:when/g).length;
                }
                else
                {
                    return 0;
                }
            }

            function translateWhenAndOtherwiseTags()
            {
                //REMOVE <c:choose></c:choose>
                codeToTranslate = codeToTranslate.replace(/\<c\:choose\>/g, "");
                codeToTranslate = codeToTranslate.replace(/\<\/c\:choose\>/g, "");
                //verwijdert newlines in het begin van de code
                codeToTranslate = codeToTranslate.replace(/\r?\n|\r/, "");
                //CONVERT WHEN-endtag to IF-endtag
                codeToTranslate = codeToTranslate.replace( /\<\/c\:when\>/g, '<% } %>' );
                //CONVERT <c:otherwise></c:otherwise> to ELSE-structure
                codeToTranslate = codeToTranslate.replace( /\<c\:otherwise\>/g, '<% else{ %>' );
                codeToTranslate = codeToTranslate.replace( /\<\/c\:otherwise\>/g, '<% } %>' );
            }
        }
        function translateInclude(code){

            //VERTAALD
            var translatedCode = "";

            //NOG TE VERTALEN
            var codeToTranslate = code;

            var totalOccurrencesInclude = getNumberOfInclude(codeToTranslate);

            0
            //while((codeToTranslate.indexOf("<%@ include")) >= 0)
            while(totalOccurrencesInclude !=0)
            {
                var structure = getIncludeStructure(codeToTranslate);
                //removes the 'useless' code from codeToTranslate to translatedCode
                codeToTranslate = moveUnusedCode(codeToTranslate);
                //adds the translated structure to translatedCode
                translatedCode += translateIncludeStructure(structure);
                //remove original if structure from codeToTranslate
                codeToTranslate = codeToTranslate.replace(structure, "");
                totalOccurrencesInclude--;
            }
            appendCode();
            return translatedCode;


            //appends rest of the code to translatedCode
            function appendCode()
            {
                if(codeToTranslate.length > 0)
                {
                    translatedCode += codeToTranslate;
                }
            }

            function getNumberOfInclude(code)
            {
                if(code.match(/<%@ include/g) != null)
                {
                    return code.match(/<%@ include/g).length;
                }
                else
                {
                    return 0;
                }
            }

            //returns the begin-index of includestructure
            function getIndexBeginOfIncludeStructure(code)
            {

                return code.indexOf("<%@");
            }

            function getIndexEndOfIncludeStructure(code)
            {
                //volledige include
                var beginInclude = code.indexOf("<%@ include");
                return code.indexOf('.jsp"%>', beginInclude) +7;

            }

            //OK
            function moveUnusedCode(code)
            {
                var removedCode = code.slice(0, getIndexBeginOfIncludeStructure(code));
                //move unused code to translatedCode
                translatedCode += removedCode;
                //remove the unused code from codeToTranslate
                code = code.replace(removedCode, "");
                return code;
            }

            //OK
            //returns a complete includestructure
            function getIncludeStructure(code)
            {
                //volledige include
                var beginInclude = code.indexOf("<%@ include");
                var endInclude = code.indexOf('.jsp"%>', beginInclude) +7;

                return code.slice(beginInclude, endInclude);
            }


            //OK
            //translates an includestructure
            function translateIncludeStructure(includeStructure)
            {
                // trim onnodige code
                //VOOR: <%@ include file="/views/....jsp" %>
                //NA:   <%  include ... %>
                //of    <% include admin/... %>

                includeStructure = includeStructure.replace('@', '');
                includeStructure = includeStructure.replace('file="/views/', '');
                includeStructure = includeStructure.replace('.jsp"', ' ');

                return includeStructure
            }
        }

    }
//translates all endtags that never changes, no matter what the situation
//CODE is the complete .jsp-file
    function translateNonChangeableTags(code)
    {
        //translates all </c:if>-endtags to <% } %>
        code = code.replace(/\<\/c:if>/g, '<% } %>');

        //CONVERT WHEN-endtag to IF-endtag
        code = code.replace( /\<\/c\:when\>/g, '<% } %>' );

        //CONVERT <c:otherwise></c:otherwise> to ELSE-structure
        code = code.replace( /\<c\:otherwise\>/g, '<% else{ %>' );
        code = code.replace( /\<\/c\:otherwise\>/g, '<% } %>' );

        //  first remove the escapeXml-tag
        code = code.replace(/escapeXml="true" /g, "");
        //  <c:out value="${ --> <%=
        code = code.replace(/\<c\:out value\=\"\$\{/g, "<%= ");
        //  }" />  --> %>
        code = code.replace(/\}" \/\>/g, "%>");


        return code;
    }
//STRUCTURE is the name of the structure we want to translate here
//CODE is the structure to apply the translation on
    function translateStructure(structureTag, code)
    {
        var stringToTranslate = code;
        switch(structureTag)
        {
            case '<c:if': returnTranslationIfStructure(stringToTranslate);
                break;
            case '<c:forEach': returnTranslationForEachStructure(stringToTranslate);
                break;
            case '<%@ include': returnTranslationIncludeStructure(stringToTranslate);
                break;
            //choose-tags are removed, so we search for the first when end index
            case '<c:when': returnTranslationWhenStructure(stringToTranslate);
                break;
            default : console.log("Unknown parameter; if, forEach, include, choose or variable expected!");
                break;
        }

        return stringToTranslate;

        //translates only the begin-tag
        function returnTranslationWhenStructure(whenStructure)
        {
            var beginIndexContent = whenStructure.indexOf("<c:when")+16;
            var endIndexContent = whenStructure.indexOf('}">', beginIndexContent);
            var innerContent = whenStructure.slice(beginIndexContent, endIndexContent);

            //newline to
            return "<% if(" + innerContent + "){ %>\n";
        }

        //Translates any forEachStructure
        function returnTranslationForEachStructure(forEachStructure)
        {

            //get the array-element
            var elements = getForEachVarItems(forEachStructure);
            //get the single element
            var element = getForEachVar(forEachStructure);

            var statusPresent = false;
            var varStatusElement;

            //get the element representing varStatus
            if(forEachStructure.indexOf("varStatus") >= 0)
            {
                //statusPresent is used in string newForEachStructure
                statusPresent = true;
                varStatusElement = getForEachVarStatus(forEachStructure);
            }

            //assembles a .forEach(function(){})-structure from scratch
            var newForEachStructure = "<% " + elements +".forEach(function(" + element
                + (statusPresent? ", ": "") + (statusPresent? varStatusElement : "") + "){ %>";

            return newForEachStructure;


            //FUNCTIONS


            //returns the variable representing varStatus
            function getForEachVarStatus(forEachStructure)
            {
                var beginIndex = forEachStructure.indexOf("varStatus") +11;
                var endIndex = forEachStructure.indexOf('">', beginIndex) ;

                return forEachStructure.slice(beginIndex, endIndex);
            }

            //returns the variable of FOREACH (element)
            function getForEachVar(forEachStructure)
            {
                var argumentsBegin = forEachStructure.indexOf("var") + 5;
                var argumentsEnd = forEachStructure.indexOf("items") - 2;
                var forEachVar = forEachStructure.slice(argumentsBegin, argumentsEnd);

                return forEachVar;
            }

            //returns the items-collection of FOREACH (elements)
            function getForEachVarItems(forEachStructure)
            {
                var argumentsBegin = forEachStructure.indexOf("items") + 9;
                var argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);
                var items = forEachStructure.slice(argumentsBegin, argumentsEnd);

                return items;
            }
        }

        //translates a any if-structure
        function returnTranslationIfStructure(ifStructure)
        {
            var ifStructure = translateTags(ifStructure);

            if(hasAttribute("length") | hasAttribute("empty") | hasAttribute("startsWith")){
                ifStructure = translateAttributes(ifStructure); }

            return ifStructure;

            function hasAttribute(code)
            {
                return (ifStructure.indexOf("" +code+ "") >= 0 ? true: false);
            }

            function translateTags(code)
            {
                //        ifStructure.replace(/\<c\:if test\=\"\$\{/g, '<% if(');
                code = code.replace(/\<c\:if/g, '<% if(');
                code = code.replace(/test\=\"\$\{/g, '');
                code = code.replace(/\}\"\>/g, '){ %>');
                //translate </c:if>
                code = code.replace(/\<\/c:if>/g, '<% } %>');
                return code;
            }

            function translateAttributes(codenietvertaald)
            {
                var code = codenietvertaald;
                //if EMPTY is present
                if(hasAttribute("empty"))
                {
                    if(getNumberOfEmpty(code) > 1)
                    {
                        var part1 = "<% if((typeof ",
                            part2 = " == undefined && ",
                            part3 = " == '') && (typeof ",
                            part4 = " == undefined && ",
                            part5 = " == '') %>",
                            variable = getVariables(code);
                        return (part1+variable+part2+variable+part3+variable+part4+variable+part5);
                    }
                    else
                    {
                        if(hasAttribute("not empty") | hasAttribute("! empty"))
                        {
                            var part1 = "<% if(typeof ",
                                part2 = " != undefined && ",
                                part3 = " != '') %>",
                                variable = getVariables(code);

                            return (part1+variable+part2+variable+part3);
                        }
                        else
                        {
                            var part1 = "<% if(typeof ",
                                part2 = " == undefined && ",
                                part3 = " == '') %>",
                                variable = getVariables(code);
                            return (part1+variable+part2+variable+part3);
                        }
                    }
                }

                //if startsWith is present
                if(hasAttribute("startsWith"))
                {
                    code = code.replace(/fn\:startsWith\(/, '');
                    code = code.replace(/\, \'/, '.charAt(0)=="');
                    code = code.replace(/\'\)/, '"');
                    return code;
                }

                //if LENGTH is present
                if(hasAttribute("length"))
                {
                    code = code.replace(/fn\:length\(/g, '' );
                    code = code.replace(/\)/, '.length() ');
                    return code;
                }

                //returns the number of 'empty' present
                function getNumberOfEmpty(code)
                {
                    //count the number of "EMPTY"
                    var numberOfEmpty = code.match(/empty/g).length;
                    return numberOfEmpty;
                }

                //returns the variable present inside the IF
                function getVariables(code)
                {
                    var index0 = code.indexOf("empty") +6;
                    var index1 = code.indexOf(")", index0) ;
                    //return code.slice(index0, index1);
                    return code.slice(index0, index1);
                }

                //returns all miscellaneous code between begin- and endtag
                function getContent(code)
                {
                    var index0 = code.indexOf("%>") +2;
                    var index1 = code.indexOf("<%", index0);
                    return code.slice(index0, index1);
                }
            }

            //translates an includestructure
            function translateIncludeStructure(includeStructure)
            {
                // trim onnodige code
                //VOOR: <%@ include file="/views/....jsp" %>
                //NA:   <%  include ... %>
                //of    <% include admin/... %>

                includeStructure = includeStructure.replace('@', '');
                includeStructure = includeStructure.replace('file="/views/', '');
                includeStructure = includeStructure.replace('.jsp"', ' ');

                return includeStructure
            }
        }

        //translates an include structure
        function returnTranslationIncludeStructure(includeStructure)
        {
            // trim onnodige code
            //VOOR: <%@ include file="/views/....jsp" %>
            //NA:   <%  include ... %>
            //of    <% include admin/... %>

            includeStructure = includeStructure.replace('@', '');
            includeStructure = includeStructure.replace('file="/views/', '');
            includeStructure = includeStructure.replace('.jsp"', ' ');

            return includeStructure
        }

        function returnTranslationVariable(variableStructure)
        {
            var variable = variableStructure;
            variable = variable.replace(/\$\{/, '<%= ');
            variable = variable.replace(/\}/, '%>');

            return variable;
        }
    }

    function translateAllVariables(code)
    {
        var translatedCode = "",
            codeToTranslate = code,
        //totalOccurrencesVariables = (codeToTranslate.match(/\$\{/g).length);
            totalOccurrencesVariables = getNumberOfOccurrences('variable', codeToTranslate);

        const index0 = 0;


        ////////////////////////////////////////
        //translate all occurrences of Out-variables
        translateOutVariables();

        //translate all occurrences of variables
        for(var i = 0; i<totalOccurrencesVariables; i++)
        {
            //place all unused code in translatedCode
            moveUnusedCode(index0, getIndexBeginOfVariable(codeToTranslate));
            translatedCode += translateVariable(getVariableStructure(codeToTranslate));
            codeToTranslate = codeToTranslate.replace(getVariableStructure(codeToTranslate), "");
        }
        appendCode();
        return translatedCode;
        /////////////////////////////////////////////////////////

        //FUNCTIONS

        //appends the left over code
        function appendCode()
        {
            if(codeToTranslate.length > 0)
            {
                translatedCode += codeToTranslate;
            }
        }

        //returns the begin-index of the variable
        function getIndexBeginOfVariable(code)
        {
            //aangepast door QUNIT-test
            return code.indexOf("${");
        }

        //moves unused code to translatedCode
        function moveUnusedCode(beginIndex, endIndex)
        {
            var removedCode = codeToTranslate.slice(beginIndex, endIndex);
            translatedCode += removedCode;
            //just to be sure index 0 is the index where code starts again
            codeToTranslate = codeToTranslate.replace(removedCode, "");
        }

        //returns the variable
        function getVariableStructure(code)
        {
            //zoeken naar ${
            var beginVar = code.indexOf("${");
            //zoeken naar de END-tag
            var endVar = code.indexOf("}", beginVar) + 1;
            var varStructure = code.slice(beginVar, endVar);

            return varStructure;
        }

        //translates the variable returned
        function translateVariable(variableStructure)
        {
            var variable = variableStructure;
            variable = variable.replace(/\$\{/, '<%= ');
            variable = variable.replace(/\}/, '%>');

            return variable;
        }

        //translates all Out-variables
        function translateOutVariables()
        {
            //  first remove the escapeXml-tag
            codeToTranslate = codeToTranslate.replace(/escapeXml="true" /g, "");
            //  <c:out value="${ --> <%=
            codeToTranslate = codeToTranslate.replace(/\<c\:out value\=\"\$\{/g, "<%= ");
            //  }" />  --> %>
            codeToTranslate = codeToTranslate.replace(/\}" \/\>/g, "%>");
        }
    }

    //DELETE
    function deleteUOverflowingCode(code)
    {
        //delete choose-tags
        code = code.replace(/\<c\:choose\>/g, "");
        code = code.replace(/\<\/c\:choose\>/g, "");

        //delete all code starting with '<%@ page' or '<%@ taglib' and ends with '%>'.
        code = code.replace(/\<\%\@\spage.*\%\>/g, "");
        code = code.replace(/\<\%\@\staglib.*\%\>/g, "");

        //delete all comments (Tim)
        code = code.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, "");

        //removes newlines at the beginning of the code
        code = code.replace(/\r?\n|\r/, "");

        return code;
    }

    //SEARCH
    //returns the begin index of any structure defined in the function
    function getBeginIndexOfStructure(code, structure)
    {
        //STRUCTURE
        //when  : <c:when
        //if    : <c:if
        //choose  : <c:when
        //include : <%@
        //foreach : <c:forEach
        //variables : ${

        return code.indexOf(structure);
    }

//returns the end index of any structured defined in the function
    function getEndIndexOfStructure(code, structure)
    {
        //STRUCTURE
        //choose  : </c:when
        //if    : </c:if
        //choose  : </c:when
        //include : .jsp"%>
        //foreach : </c:forEach
        //variables : }

        switch(structure)
        {
            case 'if': returnEndIndexOfIFStructure(code);
                break;
            case 'forEach': returnEndIndexOfForEachStructure(code);
                break;
            case 'include': returnEndIndexOfIncludeStructure(code);
                break;
            //choose-tags are removed, so we search for the first when end index
            case 'choose': returnEndIndexOfWhenStructure(code);
                break;
            case 'variable': returnEndIndexVariableStructure(code);
                break;
        }

        //FUNCTIONS
        function returnEndIndexOfWhenStructure(code)
        {
            var beginIndex = code.indexOf("<c:when");
            return (code.indexOf('">', beginIndex)) +3;
        }

        function returnEndIndexOfForEachStructure(code)
        {
            return (code.indexOf("</c:forEach")+12);
        }

        function returnEndIndexOfIFStructure(code)
        {
            return (code.indexOf("</c:if")+7);
        }

        function returnEndIndexOfIncludeStructure(code)
        {
            var beginInclude = code.indexOf("<%@ include");
            return code.indexOf('.jsp"%>', beginInclude) +7;
        }

        function returnEndIndexVariableStructure(code)
        {
            var beginVar = code.indexOf("${");
            return code.indexOf("}", beginVar) + 1;
        }
    }

//returns the structure from beginIndex till endIndex
    function getStructure(beginIndex, endIndex)
    {
        return codeToTranslate.slice(beginIndex, endIndex);
    }

// DEZE KLASSE MAG ENKEL AANGEROEPEN WORDEN ALS DE MATCH VOOR DE REGEX NIET GELIJK IS AAN NULL
    function getNumberOfOccurrences(structure, code)
    {
        switch(structure)
        {
            case 'if': return code.match(/\<c\:if/g).length;
                break;
            case 'forEach': return code.match(/\<c\:forEach/g).length;
                break;
            case 'include': return code.match(/<%@ include/g).length;
                break;
            //choose-tags are removed, so we search for the first when end index
            case 'choose': return code.match(/\<c\:when/g).length;
                break;
            case 'variable': return code.match(/\$\{/g).length;
                break;
        }
    }

    //output = jspToEjs.translateJSPtoEJS(input);
}


var singleQuotes = "''";
var singleQuotesxxx = "'xxx'";

QUnit.module("MAINTEST translate jsp to ejs");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code1: '<fieldset>\n'+
                '<div class="page">\n'+
                '<div>\n'+
                '<label for="Item">Name</label>\n'+
                '<input name="name" id="name" type="text" value="${object.name}" size="62" />\n'+
                '</div>\n'+
                '<div class="clearfix">\n'+
                '<label for="info">Info</label>\n'+
                '<p class="content_text">Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                '</div>\n'+
                '<c:if test="${ object.extention == '+singleQuotes+' }">\n'+
                '<input type="hidden" name="extention" value="" />\n'+
                '<input type="hidden" name="caption" value="" />\n'+
                '</c:if>\n'+
                '<c:if test="${ object.extention != '+singleQuotes+' }">\n'+
                '<c:if test="${ object.extention != '+singleQuotesxxx+' }">\n'+
                '<div>'+
                '<label for="extention">Extention</label>\n'+
                '<input name="extention" id="extention" type="text" value="${object.extention}" size="4" />\n'+
                '<div>\n'+
                '</c:if>\n'+
                '<div>\n'+
                '<label for="caption">Caption</label>\n'+
                '<textarea name="caption" id="caption" cols="60" rows="3">${object.caption}</textarea>\n'+
                '<div>\n'+

                '<div>'+
                '<label for="fileupload">Image</label>\n'+
                '<input id="fileToUpload" type="file" size="30" name="fileToUpload" class="input" />\n'+
                '<div>\n'+

                '<c:if test="${object.extention != '+singleQuotesxxx+' }">\n'+
                '<div>\n'+
                '<label for="preview">Preview</label>\n'+
                '<img id="preview" src="${datapath}/${object.id}.${object.extention}?now=${request.time}" />\n'+
                '<div>\n'+
                '</c:if>\n'+
                '</c:if>\n'+
                '<div>\n'+
                '</fieldset>' ,

            expected1: '<fieldset>\n'+
                '<div class="page">\n'+
                '<div>\n'+
                '<label for="Item">Name</label>\n'+
                '<input name="name" id="name" type="text" value="${object.name}" size="62" />\n'+
                '</div>\n'+
                '<div class="clearfix">\n'+
                '<label for="info">Info</label>\n'+
                '<p class="content_text">Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                '</div>\n'+
                "<% if( object.extention == '' ){ %>\n"+
                '<input type="hidden" name="extention" value="" />\n'+
                '<input type="hidden" name="caption" value="" />\n'+
                '<% } %>\n'+
                "<% if( object.extention != '' ){ %>\n"+
                "<% if( object.extention != 'xxx' ){ %>\n"+
                '<div>'+
                '<label for="extention">Extention</label>\n'+
                '<input name="extention" id="extention" type="text" value="${object.extention}" size="4" />\n'+
                '<div>\n'+
                '<% } %>\n'+
                '<div>\n'+
                '<label for="caption">Caption</label>\n'+
                '<textarea name="caption" id="caption" cols="60" rows="3">${object.caption}</textarea>\n'+
                '<div>\n'+

                '<div>'+
                '<label for="fileupload">Image</label>\n'+
                '<input id="fileToUpload" type="file" size="30" name="fileToUpload" class="input" />\n'+
                '<div>\n'+

                "<% if( object.extention != 'xxx' ){ %>\n"+
                '<div>\n'+
                '<label for="preview">Preview</label>\n'+
                '<img id="preview" src="${datapath}/${object.id}.${object.extention}?now=${request.time}" />\n'+
                '<div>\n'+
                '<% } %>\n'+
                '<% } %>\n'+
                '<div>\n'+
                '</fieldset>',

            code2:
                '<div class=>\n'+
                    '<label for=>Info</label>\n'+
                    '<p class=>Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                    '</div>\n'+
                    '<c:if test="${ object.extention == }">\n'+
                    '<input type= name= value= />\n'+
                    '<input type= name= value= />\n'+
                    '</c:if>\n'+
                    '<c:if test="${ object.extention != }">\n'+
                    '<div>'+
                    '<label for=>Extention</label>\n'+
                    '<input name= id= type= value="${object.extention}" size="4" />\n'+
                    '<div>\n'+
                    '</c:if>\n'+
                    '<div>\n'+
                    '<label for=>Caption</label>\n'+
                    '<textarea name= id= cols= rows=>${object.caption}</textarea>\n'+
                    '<div>\n'+

                    '<div>'+
                    '<label for=>Image</label>\n'+
                    '<input id= type= size= name= class= />\n'+
                    '<div>\n'+

                    '<c:if test="${ object.extention != }">\n'+
                    '<div>\n'+
                    '<label for=>Preview</label>\n'+
                    '<img id= src= />\n'+
                    '<div>\n'+
                    '</c:if>\n'+
                    '</c:if>\n'+
                    '<div>\n'+
                    '</fieldset>' ,

            expected2:
                '<div class=>\n'+
                    '<label for=>Info</label>\n'+
                    '<p class=>Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                    '</div>\n'+
                    "<% if( object.extention == ){ %>\n"+
                    '<input type= name= value= />\n'+
                    '<input type= name= value= />\n'+
                    '<% } %>\n'+
                    "<% if( object.extention != ){ %>\n"+
                    '<div>'+
                    '<label for=>Extention</label>\n'+
                    '<input name= id= type= value="${object.extention}" size="4" />\n'+
                    '<div>\n'+
                    '<% } %>\n'+
                    '<div>\n'+
                    '<label for=>Caption</label>\n'+
                    '<textarea name= id= cols= rows=>${object.caption}</textarea>\n'+
                    '<div>\n'+

                    '<div>'+
                    '<label for=>Image</label>\n'+
                    '<input id= type= size= name= class= />\n'+
                    '<div>\n'+

                    "<% if( object.extention != ){ %>\n"+
                    '<div>\n'+
                    '<label for=>Preview</label>\n'+
                    '<img id= src= />\n'+
                    '<div>\n'+
                    '<% } %>\n'+
                    '<% } %>\n'+
                    '<div>\n'+
                    '</fieldset>',
            testingOutputCode1:
                '<fieldset>\n'+
                    '<div class=>\n'+
                    '<div>\n'+
                    '<label for=>Name</label>\n'+
                    '<input name= id= type= value= size= />\n'+
                    '</div>\n'+
                    '<div class=>\n'+
                    '<label for=>Info</label>\n'+
                    '<p class=>Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                    '</div>\n'+
                    '<c:if test="${object.extention == '+singleQuotes+' }">\n'+
                    '<input type= name= value= />\n'+
                    '<input type= name= value= />\n'+
                    '</c:if>',
            expectedTestingOutput: '<fieldset>\n'+
                '<div class=>\n'+
                '<div>\n'+
                '<label for=>Name</label>\n'+
                '<input name= id= type= value= size= />\n'+
                '</div>\n'+
                '<div class=>\n'+
                '<label for=>Info</label>\n'+
                '<p class=>Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                '</div>\n'+
                "<% if( object.extention == '' ){ %>\n"+
                '<input type= name= value= />\n'+
                '<input type= name= value= />\n'+
                '<% } %>'
        }
    ]).test("MAINTEST translate jsp to ejs", function(params){

        var actualResult1 = maintestTranslate(params.code1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = maintestTranslate(params.code2);
        strictEqual(actualResult2, params.expected2);

        var actualResult3 = maintestTranslate(params.testingOutputCode1);
        strictEqual(actualResult3, params.expectedTestingOutput);
    });