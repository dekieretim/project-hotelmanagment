/**
 * Created by niels on 18/11/14.
 */

//OK
function translateWhen(code)
{
    //////////////////////////////////////////////////////
    var translatedCode = "",
        codeToTranslate = code,
        totalOccurrencesWhen = getNumberOfWhens(codeToTranslate);

    const index0 = 0;

    //translates the begin- and endtag of otherwise and endtag of when
    translateWhenAndOtherwiseTags();
    //loop the whole document till all IF's are translated
    //with numberOfIFs used, the document is only searched once, instead of every time for each loop
    while(totalOccurrencesWhen !=0)
    {
        //place all unused code in translatedCode
        var structure = getWhenStructure(getIndexBeginOfWhenStructure(codeToTranslate), getIndexEndOfWhenStructure(codeToTranslate));
        //removes the 'useless' code from codeToTranslate to translatedCode
        codeToTranslate = moveUnusedCode(codeToTranslate);
        //if(codeToTranslate.indexOf("<c:when") >= 0)
        //adds the translated structure to translatedCode
            translatedCode += translateWhenBeginTag(structure);
        codeToTranslate = codeToTranslate.replace(structure, "");





        totalOccurrencesWhen--;
    }
    appendCode();
    return translatedCode;
    /////////////////////////////////////////////////////

    //FUNCTIONS

    //appends the left over code
    function appendCode()
    {
        if(codeToTranslate.length > 0)
        {
            translatedCode += codeToTranslate;
        }
    }

    //returns a WHEN-structure
    function getWhenStructure(beginIndex, endIndex)
    {
        return codeToTranslate.slice(beginIndex, endIndex);
    }

    //OK
    function translateWhenBeginTag(code)
    {
        var beginIndex = code.indexOf("<c:when")+16;
        var endIndex = code.indexOf('}">', beginIndex);
        var innerContent = code.slice(beginIndex, endIndex);

        return "<% if(" + innerContent + "){ %>\n";
    }

    //OK
    //returns begin-index of IF
    function getIndexBeginOfWhenStructure(code)
    {
        //aangepast door QUNIT-test
        return code.indexOf("<c:when");
    }

    //OK
    //returns end-index of When
    function getIndexEndOfWhenStructure(code)
    {
        var beginIndex = code.indexOf("<c:when");
        return (code.indexOf('">', beginIndex) +3);
    }

    //OK
    //moves "not to be translated"-code to translatedCode
    //removes "not to be translated"-code from codeToTranslate
    function moveUnusedCode(code)
    {
        var removedCode = code.slice(0, getIndexBeginOfWhenStructure(code));
        //move unused code to translatedCode
        translatedCode += removedCode;
        //remove the unused code from codeToTranslate
        code = code.replace(removedCode, "");
        return code;
    }

    //OK
    function getNumberOfWhens(code)
    {
        //count the number of WHEN
        var numberOfWHENs = code.match(/\<c\:when/g).length;
        return numberOfWHENs;
    }

    function translateWhenAndOtherwiseTags()
    {
        //REMOVE <c:choose></c:choose>
        codeToTranslate = codeToTranslate.replace(/\<c\:choose\>/g, "");
        codeToTranslate = codeToTranslate.replace(/\<\/c\:choose\>/g, "");
        //verwijdert newlines in het begin van de code
        codeToTranslate = codeToTranslate.replace(/\r?\n|\r/, "");
        //CONVERT WHEN-endtag to IF-endtag
        codeToTranslate = codeToTranslate.replace( /\<\/c\:when\>/g, '<% } %>' );
        //CONVERT <c:otherwise></c:otherwise> to ELSE-structure
        codeToTranslate = codeToTranslate.replace( /\<c\:otherwise\>/g, '<% else{ %>' );
        codeToTranslate = codeToTranslate.replace( /\<\/c\:otherwise\>/g, '<% } %>' );
    }
}





QUnit.module("translateChooseLoop");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Choose1:
                "<c:choose>\n" +
                    '<c:when test="${test=true}">\n' +
                    "</c:when>\n" +
                    "<c:otherwise>\n" +
                    "</c:otherwise>\n" +
                    "</c:choose>",

            expected1:
                "<% if(test=true){ %>\n" +
                    "<% } %>\n" +
                    "<% else{ %>\n" +
                    "<% } %>\n",

            Choose2: "<c:choose>\n" +
                '<p>htmlcode</p>\n' +
                '<p>htmlcode</p>\n' +
                '<p>htmlcode</p>\n' +
                '<c:when test="${test=true}">\n' +
                '<p>htmlcode</p>\n' +
                "</c:when>\n" +
                '<p>htmlcode</p>\n' +
                "<c:otherwise>\n" +
                '<p>htmlcode</p>\n' +
                "</c:otherwise>\n" +
                '<p>htmlcode</p>\n' +
                "</c:choose>",
            expected2: '<p>htmlcode</p>\n' +
                '<p>htmlcode</p>\n' +
                '<p>htmlcode</p>\n' +
                "<% if(test=true){ %>\n" +
                '<p>htmlcode</p>\n' +
                "<% } %>\n" +
                '<p>htmlcode</p>\n' +
                "<% else{ %>\n" +
                '<p>htmlcode</p>\n' +
                "<% } %>\n" +
                '<p>htmlcode</p>\n',

            Choose3: '<div>\n' +
                '<c:choose>\n'+
                '<c:when test="${editItem.templateName == || editItem.templateName == }">\n'+
                '<c:if test="${element.name == }">\n'+
                '<div>\n'+
                '<label for="S${element.id}">In de kijker</label>\n'+
                '<input name="K${element.id}" id="K${element.id}" type="hidden" value="${element.kind}" />\n'+
                '<input name="N${element.id}" id="N${element.id}" type="hidden" value="${element.name}" />\n'+
                '<input name="S${element.id}" id="S${element.id}" type="checkbox" value="Y" <c:if test="${element.string==}">checked</c:if> />\n'+
                '</div>\n'+
                '</c:if>\n'+
                '<c:if test="${element.name == }">\n'+
                '<div>\n'+
                '<label for="S${element.id}">ZZZZZZZ</label>\n'+
                '<input name="K${element.id}" id="K${element.id}" type="hidden" value="${element.kind}" />\n'+
                '<input name="N${element.id}" id="N${element.id}" type="hidden" value="${element.name}" />\n'+
                '<input name="S${element.id}" id="S${element.id}" type="text" value="${element.string}" size="10"/>\n'+
                '</div>\n'+
                '</c:if>\n'+
                '</c:when>\n'+
                '<c:otherwise>\n'+
                '<c:if test="${element.kind ==}">\n'+
                '<div class="Area">\n'+
                '<label for="${element.name}">${element.name}</label>\n'+
                '<input name="K${element.id}" id="K${element.id}" type="hidden" value="${element.kind}" />\n'+
                '<input name="N${element.id}" id="N${element.id}" type="hidden" value="${element.name}" />\n'+
                '<textarea name="S${element.id}" id="S${element.id}" cols="60" rows="4">${element.string}</textarea>\n'+
                '</div>\n'+
                '</c:if>\n'+

                '<c:if test="${element.kind ==}">\n'+
                '<div class="String">\n'+
                '<label for="S${element.id}">${element.name}</label>\n'+
                '<input name="K${element.id}" id="K${element.id}" type="hidden" value="${element.kind}" />\n'+
                '<input name="N${element.id}" id="N${element.id}" type="hidden" value="${element.name}" />\n'+
                '<input name="S${element.id}" id="S${element.id}" type="text" value="${element.string}" size="62"/>\n'+
                '</div>\n'+
                '</c:if>\n'+
                '</c:otherwise>\n'+
                '</c:choose>',

            expected3: '<div>\n'+
                "<% if(editItem.templateName == || editItem.templateName == ){ %>\n"+
                '<c:if test="${element.name == }">\n'+
                '<div>\n'+
                '<label for="S${element.id}">In de kijker</label>\n'+
                '<input name="K${element.id}" id="K${element.id}" type="hidden" value="${element.kind}" />\n'+
                '<input name="N${element.id}" id="N${element.id}" type="hidden" value="${element.name}" />\n'+
                '<input name="S${element.id}" id="S${element.id}" type="checkbox" value="Y" <c:if test="${element.string==}">checked</c:if> />\n'+
                '</div>\n'+
                '</c:if>\n'+
                '<c:if test="${element.name == }">\n'+
                '<div>\n'+
                '<label for="S${element.id}">ZZZZZZZ</label>\n'+
                '<input name="K${element.id}" id="K${element.id}" type="hidden" value="${element.kind}" />\n'+
                '<input name="N${element.id}" id="N${element.id}" type="hidden" value="${element.name}" />\n'+
                '<input name="S${element.id}" id="S${element.id}" type="text" value="${element.string}" size="10"/>\n'+
                '</div>\n'+
                '</c:if>\n'+
                '<% } %>\n'+
                '<% else{ %>\n'+
                '<c:if test="${element.kind ==}">\n'+
                '<div class="Area">\n'+
                '<label for="${element.name}">${element.name}</label>\n'+
                '<input name="K${element.id}" id="K${element.id}" type="hidden" value="${element.kind}" />\n'+
                '<input name="N${element.id}" id="N${element.id}" type="hidden" value="${element.name}" />\n'+
                '<textarea name="S${element.id}" id="S${element.id}" cols="60" rows="4">${element.string}</textarea>\n'+
                '</div>\n'+
                '</c:if>\n'+

                '<c:if test="${element.kind ==}">\n'+
                '<div class="String">\n'+
                '<label for="S${element.id}">${element.name}</label>\n'+
                '<input name="K${element.id}" id="K${element.id}" type="hidden" value="${element.kind}" />\n'+
                '<input name="N${element.id}" id="N${element.id}" type="hidden" value="${element.name}" />\n'+
                '<input name="S${element.id}" id="S${element.id}" type="text" value="${element.string}" size="62"/>\n'+
                '</div>\n'+
                '</c:if>\n'+
                '<% } %>\n'
        }

    ]).test("Test translateChooseLoop function", function(params){
        var actualResult1 = translateWhen(params.Choose1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = translateWhen(params.Choose2);
        strictEqual(actualResult2, params.expected2);
        var actualResult3 = translateWhen(params.Choose3);
        strictEqual(actualResult3, params.expected3);

    });
