/**
 * Created by niels on 9/10/14.
 */

function translateBeginTagsForEach(code){

    //////////////////////////////////////////////////////
    var codeToTranslate = code,
        translatedCode = '',
        totalOccurrencesForeach = getNumberOfForEachs(codeToTranslate);

    while(totalOccurrencesForeach !=0)
    {
        var structure = getForEachStructure(getIndexBeginOfForEachStructure(codeToTranslate), getIndexEndOfForEachStructure(codeToTranslate));
        //removes the 'useless' code from codeToTranslate to translatedCode
        codeToTranslate = moveUnusedCode(codeToTranslate);
        //adds the translated structure to translatedCode
        translatedCode += translateForEachStructure(structure);
        //remove original forEach structure from codeToTranslate
        codeToTranslate = codeToTranslate.replace(structure, "");
        totalOccurrencesForeach--;
    }
    appendCode();

    return translatedCode;
    //////////////////////////////////////////////////////



    //FUNCTIONS

    //appends the left over code
    function appendCode()
    {
        if(codeToTranslate.length != 0)
        {
            translatedCode += codeToTranslate;
        }
    }

    //returns begin-index of FOREACH
    //OK
    function getIndexBeginOfForEachStructure(code)
    {
        //aangepast door QUNIT-test
        return code.indexOf("<c:forEach");
    }

    //returns end-index of FOREACH
    //OK
    function getIndexEndOfForEachStructure(code)
    {
        var beginIndex = code.indexOf('<c:forEach');
        if(beginIndex !=-1){
            var endIndex = code.indexOf('">', beginIndex)+3;
            return endIndex;
        }
        return console.log('All Foreach structures translated')
    }


    //moves "not to be translated"-code to translatedCode
    //removes "not to be translated"-code from codeToTranslate
    function moveUnusedCode(code)
    {
        var removedCode = code.slice(0, getIndexBeginOfForEachStructure(code));
        //move unused code to translatedCode
        translatedCode += removedCode;
        //remove the unused code from codeToTranslate
        code = code.replace(removedCode, "");
        return code;
    }


    //returns a complete ForEach-structure
//OK
    function getForEachStructure(beginIndex, endIndex)
    {
        return codeToTranslate.slice(beginIndex, endIndex);
    }


    //returns the number of FOREACH's present in code
    function getNumberOfForEachs(code)
    {
        return code.match(/\<c\:forEach/g).length;
    }

//OK
    //returns the whole innercontent between the FOREACH-tags
    function getForEachInnerContent(code)
    {
        var index0 = code.indexOf('">') +2;

        var index1 = code.indexOf("</c:forEach>", index0);
        return code.slice(index0, index1);
    }




    //OK
    //Translates any forEachStructure
    function translateForEachStructure(ForEachStructure){

        //get the array
        var elements = getForEachVarItems(ForEachStructure);
        //get the single element
        var element = getForEachVar(ForEachStructure);

        //get the element representing varStatus
        if(ForEachStructure.indexOf("varStatus") >= 0)
        {
            var statusPresent = true;
            var varStatusElement = getForEachVarStatus(ForEachStructure);
        }


        //get the inner content (html, other structures, comments...)
        //var innerContent = getForEachInnerContent(ForEachStructure);
        //var endTag = "<% }) %>";

        //assembles a .forEach(function(){})-structure from scratch
        var newForEachStructure = "<% " + elements +".forEach(function(" + element
            + (statusPresent? ", ": "") + (statusPresent? varStatusElement : "") + "){ %>\n";


        return newForEachStructure;



        //FUNCTIONS
        //OK
        //returns the variable representing varStatus
        function getForEachVarStatus(forEachStructure)
        {
            var beginIndex = forEachStructure.indexOf("varStatus") +11;
            var endIndex = forEachStructure.indexOf('">', beginIndex) ;

            return forEachStructure.slice(beginIndex, endIndex);
        }



        //OK
        //returns the variable of FOREACH
        function getForEachVar(forEachStructure){


            var argumentsBegin = forEachStructure.indexOf("var") + 5;
            //argumentsEnd = forEachStructure.indexOf('"')[2] - 1;
            var argumentsEnd = forEachStructure.indexOf("items") - 2;
            //var arguments = IfStructure.splice(argumentsBegin, argumentsEnd);

            var forEachVar = forEachStructure.slice(argumentsBegin, argumentsEnd);

            return forEachVar;
        }

        //OK
        //returns the items-collection of FOREACH
        function getForEachVarItems(forEachStructure){

            //VarITEMS
            var argumentsBegin = forEachStructure.indexOf("items") + 9;
            var argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);

            var items = forEachStructure.slice(argumentsBegin, argumentsEnd);

            return items;
        }
    }



}



QUnit.module("MainTest translateForEach");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code1: '<div id="content">\n'+
                        '<div id="left">\n'+
                        '</div>\n'+
                        '<div id="mid">\n'+
                                '<h1>${page.title}</h1>\n'+
                                '<p>${content.data}</p>\n'+
                                '<c:forEach var="element" items="${editElements}">\n'+
                                        '<br clear="all">\n'+
                                '</c:forEach>\n'+
                        '</div>\n'+
                        '<div id="right">\n'+
                                '<c:forEach var="element" items="${editElements}" varStatus="status">\n'+
                                '<br clear="all">\n'+
                                '</c:forEach>\n'+
                        '</div>\n'+
                    '</div>',
            expected1: '<div id="content">\n'+
                '<div id="left">\n'+
                '</div>\n'+
                '<div id="mid">\n'+
                '<h1>${page.title}</h1>\n'+
                '<p>${content.data}</p>\n'+
                '<% editElements.forEach(function(element){ %>\n'+
                '<br clear="all">\n'+
                '<% }) %>\n'+
                '</div>\n'+
                '<div id="right">\n'+
                '<% editElements.forEach(function(element, status){ %>\n'+
                '<br clear="all">\n'+
                '<% }) %>\n'+
                '</div>\n'+
                '</div>',

            code2: '<fieldset>\n'+
                '<div class="page">\n'+
                '<div>\n'+
                '<label for="Item">Name</label>\n'+
                '<input name="name" id="name" type="text" value="${object.name}" size="62" />\n'+
                '</div>\n'+
                '<div class="clearfix">\n'+
                '<label for="info">Info</label>\n'+
                '<p class="content_text">Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                '</div>\n'+
                '<c:if test="${ object.extention == }">\n'+
                '<input type="hidden" name="extention" value="" />\n'+
                '<input type="hidden" name="caption" value="" />\n'+
                '</c:if>\n'+
                '<c:if test="${ object.extention != }">\n'+
                '<c:if test="${ object.extention != }">\n'+
                '<div>'+
                '<label for="extention">Extention</label>\n'+
                '<input name="extention" id="extention" type="text" value="${object.extention}" size="4" />\n'+
                '<div>\n'+
                '</c:if>\n'+
                '<div>\n'+
                '<label for="caption">Caption</label>\n'+
                '<textarea name="caption" id="caption" cols="60" rows="3">${object.caption}</textarea>\n'+
                '<div>\n'+

                '<div>'+
                '<label for="fileupload">Image</label>\n'+
                '<input id="fileToUpload" type="file" size="30" name="fileToUpload" class="input" />\n'+
                '<div>\n'+

                '<c:if test="${ object.extention != }">\n'+
                '<div>\n'+
                '<label for="preview">Preview</label>\n'+
                '<img id="preview" src="${datapath}/${object.id}.${object.extention}?now=${request.time}" />\n'+
                '<div>\n'+
                '</c:if>\n'+
                '</c:if>\n'+
                '<div>\n'+
                '</fieldset>' ,

            expected2: '<fieldset>\n'+
                '<div class="page">\n'+
                '<div>\n'+
                '<label for="Item">Name</label>\n'+
                '<input name="name" id="name" type="text" value="${object.name}" size="62" />\n'+
                '</div>\n'+
                '<div class="clearfix">\n'+
                '<label for="info">Info</label>\n'+
                '<p class="content_text">Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                '</div>\n'+
                "<% if( object.extention == ){ %>\n"+
                '<input type="hidden" name="extention" value="" />\n'+
                '<input type="hidden" name="caption" value="" />\n'+
                '<% } %>\n'+
                "<% if( object.extention != ){ %>\n"+
                "<% if( object.extention != ){ %>\n"+
                '<div>'+
                '<label for="extention">Extention</label>\n'+
                '<input name="extention" id="extention" type="text" value="${object.extention}" size="4" />\n'+
                '<div>\n'+
                '<% } %>\n'+
                '<div>\n'+
                '<label for="caption">Caption</label>\n'+
                '<textarea name="caption" id="caption" cols="60" rows="3">${object.caption}</textarea>\n'+
                '<div>\n'+

                '<div>'+
                '<label for="fileupload">Image</label>\n'+
                '<input id="fileToUpload" type="file" size="30" name="fileToUpload" class="input" />\n'+
                '<div>\n'+

                "<% if( object.extention != ){ %>\n"+
                '<div>\n'+
                '<label for="preview">Preview</label>\n'+
                '<img id="preview" src="${datapath}/${object.id}.${object.extention}?now=${request.time}" />\n'+
                '<div>\n'+
                '<% } %>\n'+
                '<% } %>\n'+
                '<div>\n'+
                '</fieldset>'
        }
    ]).test("MainTest translateForEach function", function(params){

        var actualResult1 = translateBeginTagsForEach(params.code1);
        strictEqual(actualResult1, params.expected1);
        //var actualResult2 = translateForEach(params.code2);
        //strictEqual(actualResult2, params.expected2);
    });