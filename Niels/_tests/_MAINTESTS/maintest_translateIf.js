/**
 * Created by niels on 9/12/14.
 */


function translateBeginTagsIf(code){

    //////////////////////////////////////////////////////
    var codeToTranslate = code,
        translatedCode = '',
        totalOccurrencesIf = getNumberOfIFs(codeToTranslate);

    while(totalOccurrencesIf !=0)
    {
        var structure = getIfStructure(getIndexBeginOfIFStructure(codeToTranslate), getIndexEndOfIFStructure(codeToTranslate));
        //removes the 'useless' code from codeToTranslate to translatedCode
        codeToTranslate = moveUnusedCode(0, getIndexBeginOfIFStructure(codeToTranslate), codeToTranslate);
        //adds the translated structure to translatedCode
        translatedCode += translateIfStructure(structure);
        //remove original if structure from codeToTranslate
        codeToTranslate = codeToTranslate.replace(structure, "");
        totalOccurrencesIf--;
    }
    appendCode();

    return translatedCode;
    //////////////////////////////////////////////////////


    //FUNCTIONS

    //appends the left-over code
    function appendCode()
    {
        if(codeToTranslate.length > 0)
        {
            translatedCode += codeToTranslate;
        }
    }

    //returns begin-index of IF
    function getIndexBeginOfIFStructure(code)
    {
        return code.indexOf("<c:if");
    }

    //returns the end-index of the begin-tag
    function getIndexEndOfIFStructure(code)
    {
        //+4 omdat anders niet de hele IF gereturnd wordt
        var beginIndex = code.indexOf('<c:if');
        var endIndex = code.indexOf('}">', beginIndex)+3;
        return endIndex;
    }

    //moves "not to be translated"-code to translatedCode
    //removes "not to be translated"-code from codeToTranslate
    function moveUnusedCode(beginIndex, endIndex, code)
    {
        var removedCode = code.slice(beginIndex, endIndex);
        //move unused code to translatedCode
        translatedCode += removedCode;
        //remove the unused code from codeToTranslate
        code = code.replace(removedCode, "");
        return code;
    }

    //returns a complete IF-structure
    function getIfStructure(beginIndex, endIndex)
    {
        return codeToTranslate.slice(beginIndex, endIndex);
    }

    //returns the number of IF's present in code
    function getNumberOfIFs(code)
    {
        //count the number of IFs
        var numberOfIFs = code.match(/\<c\:if/g).length;
        return numberOfIFs;
    }



    //OK
    function translateIfStructure(code)
    {
        var ifStructure = code;
        //ifStructure = translateTags(ifStructure);

        if(hasAttribute("length") | hasAttribute("empty") | hasAttribute("startsWith"))
        {
            ifStructure = translateAttributes(ifStructure);
        }
        else
        {
            ifStructure = translateTags(ifStructure);
        }

        return ifStructure;



        function hasAttribute(code)
        {
            return (ifStructure.indexOf("" +code+ "") >= 0 ? true: false);
        }

        function translateTags(code)
        {
            //ifStructure.replace(/\<c\:if test\=\"\$\{/g, '<% if(');
            code = code.replace(/\<c\:if/g, '<% if(');
            code = code.replace(/test\=\"\$\{/g, '');
            code = code.replace(/\}\"\>/g, '){ %>');
            return code;
        }

        function translateAttributes(code)
        {
            var ifStructure = code;
            //if EMPTY is present
            if(hasAttribute("empty"))
            {
                if(getNumberOfEmpty(ifStructure) > 1)
                {
                    var part1 = "<% if((typeof ",
                        part2 = " == undefined && ",
                        part3 = " == '') && (typeof ",
                        part4 = " == undefined && ",
                        part5 = " == '') %>",
                        variable = getVariables(ifStructure);
//                        content = getContent(ifStructure),
//                        endtag = "<% } %>";
                    return (part1+variable+part2+variable+part3+variable+part4+variable+part5);
                }
                else
                {
                    if(hasAttribute("not empty") | hasAttribute("! empty"))
                    {
                        var part1 = "<% if(typeof ",
                            part2 = " != undefined && ",
                            part3 = " != '') %>",
                            variable = getVariables(ifStructure);
//                            content = getContent(code),
//                            endtag = "<% } %>";

                        return (part1+variable+part2+variable+part3);
                    }
                    else
                    {
                        var part1 = "<% if(typeof ",
                            part2 = " == undefined && ",
                            part3 = " == '') %>",
                            variable = getVariables(ifStructure);
//                            content = getContent(code),
//                            endtag = "<% } %>";
                        return (part1+variable+part2+variable+part3);
                    }
                }
            }

            //if startsWith is present
            if(hasAttribute("startsWith"))
            {
                code = code.replace(/fn\:startsWith\(/, '');
                code = code.replace(/\, \'/, '.charAt(0)=="');
                code = code.replace(/\'\)/, '"');
                return code;
            }

            //if LENGTH is present
            if(hasAttribute("length"))
            {
                code = code.replace(/fn\:length\(/g, '' );
                code = code.replace(/\)/, '.length() ');
                return code;
            }

            //returns the number of 'empty' present
            function getNumberOfEmpty(code)
            {
                //count the number of "EMPTY"
                var numberOfEmpty = code.match(/empty/g).length;
                return numberOfEmpty;
            }

            //returns the variable present inside the IF
            //OK
            function getVariables(code)
            {
                var index0 = code.indexOf("empty") +6;
                var index1 = code.indexOf(")", index0) ;
                //return code.slice(index0, index1);
                return code.slice(index0, index1);
            }
        }

    }
}




var singleQuotes = "''";
var singleQuotesxxx = "'xxx'";

QUnit.module("MainTest translateIf");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code1: '<fieldset>\n'+
                '<div class="page">\n'+
            '<div>\n'+
                '<label for="Item">Name</label>\n'+
                '<input name="name" id="name" type="text" value="${object.name}" size="62" />\n'+
            '</div>\n'+
            '<div class="clearfix">\n'+
    '<label for="info">Info</label>\n'+
    '<p class="content_text">Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
'</div>\n'+
'<c:if test="${ object.extention == '+singleQuotes+' }">\n'+
    '<input type="hidden" name="extention" value="" />\n'+
    '<input type="hidden" name="caption" value="" />\n'+
    '</c:if>\n'+
        '<c:if test="${ object.extention != '+singleQuotes+' }">\n'+
            '<c:if test="${ object.extention != '+singleQuotesxxx+' }">\n'+
            '<div>'+
                    '<label for="extention">Extention</label>\n'+
                    '<input name="extention" id="extention" type="text" value="${object.extention}" size="4" />\n'+
            '<div>\n'+
            '</c:if>\n'+
            '<div>\n'+
                '<label for="caption">Caption</label>\n'+
                '<textarea name="caption" id="caption" cols="60" rows="3">${object.caption}</textarea>\n'+
                '<div>\n'+

                    '<div>'+
                '<label for="fileupload">Image</label>\n'+
                '<input id="fileToUpload" type="file" size="30" name="fileToUpload" class="input" />\n'+
                        '<div>\n'+

            '<c:if test="${object.extention != '+singleQuotesxxx+' }">\n'+
            '<div>\n'+
                    '<label for="preview">Preview</label>\n'+
                    '<img id="preview" src="${datapath}/${object.id}.${object.extention}?now=${request.time}" />\n'+
            '<div>\n'+
            '</c:if>\n'+
            '</c:if>\n'+
            '<div>\n'+
    '</fieldset>' ,

            expected1: '<fieldset>\n'+
                '<div class="page">\n'+
                '<div>\n'+
                '<label for="Item">Name</label>\n'+
                '<input name="name" id="name" type="text" value="${object.name}" size="62" />\n'+
                '</div>\n'+
                '<div class="clearfix">\n'+
                '<label for="info">Info</label>\n'+
                '<p class="content_text">Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                '</div>\n'+
                "<% if( object.extention == '' ){ %>\n"+
                '<input type="hidden" name="extention" value="" />\n'+
                '<input type="hidden" name="caption" value="" />\n'+
                '<% } %>\n'+
                "<% if( object.extention != '' ){ %>\n"+
                "<% if( object.extention != 'xxx' ){ %>\n"+
                '<div>'+
                '<label for="extention">Extention</label>\n'+
                '<input name="extention" id="extention" type="text" value="${object.extention}" size="4" />\n'+
                '<div>\n'+
                '<% } %>\n'+
                '<div>\n'+
                '<label for="caption">Caption</label>\n'+
                '<textarea name="caption" id="caption" cols="60" rows="3">${object.caption}</textarea>\n'+
                '<div>\n'+

                '<div>'+
                '<label for="fileupload">Image</label>\n'+
                '<input id="fileToUpload" type="file" size="30" name="fileToUpload" class="input" />\n'+
                '<div>\n'+

                "<% if( object.extention != 'xxx' ){ %>\n"+
                '<div>\n'+
                '<label for="preview">Preview</label>\n'+
                '<img id="preview" src="${datapath}/${object.id}.${object.extention}?now=${request.time}" />\n'+
                '<div>\n'+
                '<% } %>\n'+
                '<% } %>\n'+
                '<div>\n'+
                '</fieldset>',

            code2:
                '<div class=>\n'+
                '<label for=>Info</label>\n'+
                '<p class=>Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                '</div>\n'+
                '<c:if test="${ object.extention == }">\n'+
                '<input type= name= value= />\n'+
                '<input type= name= value= />\n'+
                '</c:if>\n'+
                '<c:if test="${ object.extention != }">\n'+
                '<div>'+
                '<label for=>Extention</label>\n'+
                '<input name= id= type= value="${object.extention}" size="4" />\n'+
                '<div>\n'+
                '</c:if>\n'+
                '<div>\n'+
                '<label for=>Caption</label>\n'+
                '<textarea name= id= cols= rows=>${object.caption}</textarea>\n'+
                '<div>\n'+

                '<div>'+
                '<label for=>Image</label>\n'+
                '<input id= type= size= name= class= />\n'+
                '<div>\n'+

                '<c:if test="${ object.extention != }">\n'+
                '<div>\n'+
                '<label for=>Preview</label>\n'+
                '<img id= src= />\n'+
                '<div>\n'+
                '</c:if>\n'+
                '</c:if>\n'+
                '<div>\n'+
                '</fieldset>' ,

            expected2:
                '<div class=>\n'+
                '<label for=>Info</label>\n'+
                '<p class=>Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                '</div>\n'+
                "<% if( object.extention == ){ %>\n"+
                '<input type= name= value= />\n'+
                '<input type= name= value= />\n'+
                '<% } %>\n'+
                "<% if( object.extention != ){ %>\n"+
                '<div>'+
                '<label for=>Extention</label>\n'+
                '<input name= id= type= value="${object.extention}" size="4" />\n'+
                '<div>\n'+
                '<% } %>\n'+
                '<div>\n'+
                '<label for=>Caption</label>\n'+
                '<textarea name= id= cols= rows=>${object.caption}</textarea>\n'+
                '<div>\n'+

                '<div>'+
                '<label for=>Image</label>\n'+
                '<input id= type= size= name= class= />\n'+
                '<div>\n'+

                "<% if( object.extention != ){ %>\n"+
                '<div>\n'+
                '<label for=>Preview</label>\n'+
                '<img id= src= />\n'+
                '<div>\n'+
                '<% } %>\n'+
                '<% } %>\n'+
                '<div>\n'+
                '</fieldset>',
            testingOutputCode1:
                '<fieldset>\n'+
    '<div class=>\n'+
    '<div>\n'+
    '<label for=>Name</label>\n'+
    '<input name= id= type= value= size= />\n'+
    '</div>\n'+
    '<div class=>\n'+
    '<label for=>Info</label>\n'+
    '<p class=>Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
    '</div>\n'+
    '<c:if test="${object.extention == '+singleQuotes+' }">\n'+
    '<input type= name= value= />\n'+
    '<input type= name= value= />\n'+
'</c:if>',
            expectedTestingOutput: '<fieldset>\n'+
                '<div class=>\n'+
                '<div>\n'+
                '<label for=>Name</label>\n'+
                '<input name= id= type= value= size= />\n'+
                '</div>\n'+
                '<div class=>\n'+
                '<label for=>Info</label>\n'+
                '<p class=>Created: ${object.created}, Last Modified: ${object.updated}</p>\n'+
                '</div>\n'+
                "<% if( object.extention == '' ){ %>\n"+
                '<input type= name= value= />\n'+
                '<input type= name= value= />\n'+
                '<% } %>'
        }
    ]).test("MainTest translateIf function", function(params){

        var actualResult1 = translateBeginTagsIf(params.code1);
        strictEqual(actualResult1, params.expected1);
        var actualResult2 = translateBeginTagsIf(params.code2);
        strictEqual(actualResult2, params.expected2);

        var actualResult3 = translateBeginTagsIf(params.testingOutputCode1);
        strictEqual(actualResult3, params.expectedTestingOutput);
    });


