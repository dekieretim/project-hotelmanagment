/**
 * Created by niels on 20/10/14.
 */

//De include met een SET-variabele is nog te vertalen

function translateInclude(code){

    //VERTAALD
    var translatedCode = "";

    //NOG TE VERTALEN
    var codeToTranslate = code;

    var totalOccurrencesInclude = getNumberOfInclude(codeToTranslate);

0
    //while((codeToTranslate.indexOf("<%@ include")) >= 0)
    while(totalOccurrencesInclude !=0)
    {
        var structure = getIncludeStructure(codeToTranslate);
        //removes the 'useless' code from codeToTranslate to translatedCode
        codeToTranslate = moveUnusedCode(codeToTranslate);
        //adds the translated structure to translatedCode
        translatedCode += translateIncludeStructure(structure);
        //remove original if structure from codeToTranslate
        codeToTranslate = codeToTranslate.replace(structure, "");
        totalOccurrencesInclude--;
    }
    appendCode();
    return translatedCode;


    //appends rest of the code to translatedCode
    function appendCode()
    {
        if(codeToTranslate.length > 0)
        {
            translatedCode += codeToTranslate;
        }
    }

    function getNumberOfInclude(code)
    {
        //count the number of includes
        var numberOfIncludes = code.match(/<%@ include/g).length;
        return numberOfIncludes;
    }

    //returns the begin-index of includestructure
    function getIndexBeginOfIncludeStructure(code)
    {

        return code.indexOf("<%@");
    }

    function getIndexEndOfIncludeStructure(code)
    {
        //volledige include
        var beginInclude = code.indexOf("<%@ include");
        var endInclude = code.indexOf('.jsp"%>', beginInclude) +7;
        return code.slice(beginInclude, endInclude);

    }

    //OK
    function moveUnusedCode(code)
    {
        var removedCode = code.slice(0, getIndexBeginOfIncludeStructure(code));
        //move unused code to translatedCode
        translatedCode += removedCode;
        //remove the unused code from codeToTranslate
        code = code.replace(removedCode, "");
        return code;
    }

    //OK
    //returns a complete includestructure
    function getIncludeStructure(code)
    {
        //volledige include
        var beginInclude = code.indexOf("<%@ include");
        var endInclude = code.indexOf('.jsp"%>', beginInclude) +7;

        return code.slice(beginInclude, endInclude);
    }


    //OK
    //translates an includestructure
    function translateIncludeStructure(includeStructure)
    {
        // trim onnodige code
        //VOOR: <%@ include file="/views/....jsp" %>
        //NA:   <%  include ... %>
        //of    <% include admin/... %>

        includeStructure = includeStructure.replace('@', '');
        includeStructure = includeStructure.replace('file="/views/', '');
        includeStructure = includeStructure.replace('.jsp"', ' ');

        return includeStructure
    }
}



QUnit.module("maintest_translateInclude");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code:       '<div id="content">\n'+
                        '<%@ include file="/views/header.jsp"%>\n'+
                        '<div id="wrapper">\n'+
                        '<div id="header">\n'+
                        '<%@ include file="/views/menu-top.jsp"%>\n'+
                        '</div>\n'+
                        '<div id="navigationDiv">\n'+
                        '<%@ include file="/views/menu-left.jsp"%>\n'+
                        '</div>\n'+
                        '<div id="content">\n'+
                        '<h1>${page.title}</h1>\n'+
                        '<p>${content.data}</p>\n'+
                        '<c:forEach var="element" items="${elements}">\n'+
                        '<c:if test="${element.kind ==  || element.kind == }"><p>${element.string}</p></c:if>\n'+
                        '<%@ include file="/views/admin/navigation.jsp"%>\n'+
                        '<c:if test="${element.kind == }"><img src="${element.link}" alt="${element.name}"></c:if>\n'+
                        '<c:if test="${element.kind == }"><div></div></c:if>\n'+
                        '<c:if test="${element.kind != }"><br clear="all"></c:if>\n'+
                        '</c:forEach>\n'+
                        '</div>\n'+
                        '<%@ include file="/views/footer.jsp"%>\n'+
                        '</div>',

            expected:   '<div id="content">\n'+
                        '<% include header %>\n'+
                        '<div id="wrapper">\n'+
                        '<div id="header">\n'+
                        '<% include menu-top %>\n'+
                        '</div>\n'+
                        '<div id="navigationDiv">\n'+
                        '<% include menu-left %>\n'+
                        '</div>\n'+
                        '<div id="content">\n'+
                        '<h1>${page.title}</h1>\n'+
                        '<p>${content.data}</p>\n'+
                        '<c:forEach var="element" items="${elements}">\n'+
                        '<c:if test="${element.kind ==  || element.kind == }"><p>${element.string}</p></c:if>\n'+
                        '<% include admin/navigation %>\n'+
                        '<c:if test="${element.kind == }"><img src="${element.link}" alt="${element.name}"></c:if>\n'+
                        '<c:if test="${element.kind == }"><div></div></c:if>\n'+
                        '<c:if test="${element.kind != }"><br clear="all"></c:if>\n'+
                        '</c:forEach>\n'+
                        '</div>\n'+
                        '<% include footer %>\n'+
                        '</div>'
        }
    ]).test("MainTest translateInclude function", function(params){
        var actualResult = translateInclude(params.code);
        strictEqual(actualResult, params.expected);

    });