/**
 * Created by niels on 20/10/14.
 */



//!!!!!!!variabelen mogen slechts vertaald worden NADAT de if, while, foreach... structuren vertaald zijn!!!!!!!!

// want die bevatten ook gelijkaardige structuren die als var kunnen aanzien worden.


function translateAllVariables(code)
{
    var translatedCode = "",
        codeToTranslate = code,
        totalOccurrencesVariables = (codeToTranslate.match(/\$\{/g).length);

    const index0 = 0;


    ////////////////////////////////////////
    //translate all occurrences of Out-variables
    translateOutVariables();

    //translate all occurrences of variables
    for(var i = 0; i<totalOccurrencesVariables; i++)
    {
        //place all unused code in translatedCode
        moveUnusedCode(index0, getIndexBeginOfVariable(codeToTranslate));
        translatedCode += translateVariable(getVariableStructure(codeToTranslate));
        codeToTranslate = codeToTranslate.replace(getVariableStructure(codeToTranslate), "");
    }
    appendCode();
    return translatedCode;
    /////////////////////////////////////////////////////////

    //FUNCTIONS

    //appends the left over code
    function appendCode()
    {
        if(codeToTranslate.length > 0)
        {
            translatedCode += codeToTranslate;
        }
    }

    //returns the begin-index of the variable
    function getIndexBeginOfVariable(code)
    {
        //aangepast door QUNIT-test
        return code.indexOf("${");
    }

    //moves unused code to translatedCode
    function moveUnusedCode(beginIndex, endIndex)
    {
        var removedCode = codeToTranslate.slice(beginIndex, endIndex);
        translatedCode += removedCode;
        //just to be sure index 0 is the index where code starts again
        codeToTranslate = codeToTranslate.replace(removedCode, "");
    }

    //returns the variable
    function getVariableStructure(code)
    {
        //zoeken naar ${
        var beginVar = code.indexOf("${");
        //zoeken naar de END-tag
        var endVar = code.indexOf("}", beginVar) + 1;
        varStructure = code.slice(beginVar, endVar);

        return varStructure;
    }

    //translates the variable returned
    function translateVariable(variableStructure)
    {
        var variable = variableStructure;
        variable = variable.replace(/\$\{/, '<%= ');
        variable = variable.replace(/\}/, '%>');

        return variable;
    }

    //translates all Out-variables
    function translateOutVariables()
    {
        //  first remove the escapeXml-tag
        codeToTranslate = codeToTranslate.replace(/escapeXml="true" /g, "");
        //  <c:out value="${ --> <%=
        codeToTranslate = codeToTranslate.replace(/\<c\:out value\=\"\$\{/g, "<%= ");
        //  }" />  --> %>
        codeToTranslate = codeToTranslate.replace(/\}" \/\>/g, "%>");
    }
}



//omdat de vertaling hier in quotes geplaatst moet worden, wordt bij gevallen als deze
//  variabelen gebruikt om de single quotes uit de code te halen, zodat er geen mengeling is van single
//  en double quotes BINNENIN single of double quotes. Er derde soort quotes zou hier handig zijn :p
var body = "'#body'";
//error invalid string
//var htmlContent = "'${fn:replace(fn:replace(submit.body,'\'','"'),'"','&quot;')}'";
var open = "'Open'";


QUnit.module("maintest_translateAllVariables");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            code:       '</form>\n'+
                '<table id="records">\n'+
                '<c:out value="${editData}" escapeXml="true" />\n'+
                '<tr><th>Received on</th><th>At</th><th>Status</th><th>User</th><th>Page</th><th>Action</th></tr>\n'+
                '<tr><td><fmt:formatDate value="${submit.date}" pattern="dd MMM yyyy" type="date"/></td>\n'+
                '<td><c:out value="${submit.time}" /></td>\n'+
                '<td><c:out value="${submit.status}" /></td>\n'+
                '<td><c:out value="${submit.userName}" /></td>\n'+
                '<td><c:out value="${submit.pageTitle}" /></td>\n'+
                '</table>\n'+
                '<hr />\n'+
                '<p id="body">\n'+
                '&nbsp;\n'+
                '</p>\n'+
                '</c:if>\n'+
                '</div>\n' +
                '<c:out value="${editData}" escapeXml="true" />',


                expected:   '</form>\n'+
                '<table id="records">\n'+
                '<%= editData%>\n'+
                '<tr><th>Received on</th><th>At</th><th>Status</th><th>User</th><th>Page</th><th>Action</th></tr>\n'+
                '<tr><td><fmt:formatDate value="<%= submit.date%>" pattern="dd MMM yyyy" type="date"/></td>\n'+
                '<td><%= submit.time%></td>\n'+
                '<td><%= submit.status%></td>\n'+
                '<td><%= submit.userName%></td>\n'+
                '<td><%= submit.pageTitle%></td>\n'+
                '</table>\n'+
                '<hr />\n'+
                '<p id="body">\n'+
                '&nbsp;\n'+
                '</p>\n'+
                '</c:if>\n'+
                '</div>\n'+
                '<%= editData%>'


        }
    ]).test("MainTest translateAllVariables function", function(params){
        var actualResult = translateAllVariables(params.code);
        strictEqual(actualResult, params.expected);
    });