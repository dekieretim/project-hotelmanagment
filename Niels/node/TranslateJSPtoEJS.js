/**
 * Created by niels on 20/11/14.
 */

var change = require('./change'),
    remove = require('./delete');

exports.translateJSPtoEJS = function translateJSPtoEJS(jspCode)
{
    if(typeof jspCode == "string")
    {
        var codeToTranslate = jspCode;
        codeToTranslate = change.translateAllBeginTagStructures(codeToTranslate);
        codeToTranslate = change.translateNonChangeableTags(codeToTranslate);
        codeToTranslate = remove.deleteAllExtranousCode(codeToTranslate);
        codeToTranslate = change.translateAllVariables(codeToTranslate);

        //returns the translated code
        return codeToTranslate;
    }
    else
    {
        //errorhandling
    }
}