/**
 * Created by niels on 17/12/14.
 */

var search = require('./search');
var whenStructure = require('./../EJS_choose'),
    ifStructure = require('./../EJS_if'),
    forEachStructure = require('./../EJS_foreach'),
    includeStructure = require('./../EJS_include'),
    varStructure = require('./../EJS_variables');

//translates all begin-tags of IF, FOREACH, INCLUDE, WHEN...
exports.translateAllBeginTagStructures = function translateStructures(code)
{
    var Occurrences = search.getStructuresPresent(code);

        if(Occurrences[0] == true)
        {
            ifStructure.translateBeginTagsIf(code);
        }
        if(Occurrences[1] == true)
        {
            forEachStructure.translateForEach(code);
        }
        if(Occurrences[2] == true)
        {
            includeStructure.translateInclude(code);
        }
        if(Occurrences[3] == true)
        {
            whenStructure.translateWhen(code);
        }
}


//translates all endtags that never changes, no matter what the situation
//CODE is the complete .jsp-file
exports.translateNonChangeableTags = function translateNonChangeableTags(code)
{
    //translates all </c:if>-endtags to <% } %>
    code = code.replace(/\<\/c:if>/g, '<% } %>');

    //CONVERT WHEN-endtag to IF-endtag
    code = code.replace( /\<\/c\:when\>/g, '<% } %>' );

    //CONVERT <c:otherwise></c:otherwise> to ELSE-structure
    code = code.replace( /\<c\:otherwise\>/g, '<% else{ %>' );
    code = code.replace( /\<\/c\:otherwise\>/g, '<% } %>' );

    //  first remove the escapeXml-tag
    code = code.replace(/escapeXml="true" /g, "");
    //  <c:out value="${ --> <%=
    code = code.replace(/\<c\:out value\=\"\$\{/g, "<%= ");
    //  }" />  --> %>
    code = code.replace(/\}" \/\>/g, "%>");


    return code;
}

//STRUCTURE is the name of the structure we want to translate here
//CODE is the structure to apply the translation on
function translateStructure(structureTag, code)
{
    var stringToTranslate = code;
    switch(structureTag)
    {
        case '<c:if': returnTranslationIfStructure(stringToTranslate);
            break;
        case '<c:forEach': returnTranslationForEachStructure(stringToTranslate);
            break;
        case '<%@ include': returnTranslationIncludeStructure(stringToTranslate);
            break;
        //choose-tags are removed, so we search for the first when end index
        case '<c:when': returnTranslationWhenStructure(stringToTranslate);
            break;
        default : console.log("Unknown parameter; if, forEach, include, choose or variable expected!");
            break;
    }

    return stringToTranslate;

    //translates only the begin-tag
    function returnTranslationWhenStructure(whenStructure)
    {
        var beginIndexContent = whenStructure.indexOf("<c:when")+16;
        var endIndexContent = whenStructure.indexOf('}">', beginIndexContent);
        var innerContent = whenStructure.slice(beginIndexContent, endIndexContent);

        //newline to
        return "<% if(" + innerContent + "){ %>\n";
    }

    //Translates any forEachStructure
    function returnTranslationForEachStructure(forEachStructure)
    {

        //get the array-element
        var elements = getForEachVarItems(forEachStructure);
        //get the single element
        var element = getForEachVar(forEachStructure);

        var statusPresent = false;
        var varStatusElement;

        //get the element representing varStatus
        if(forEachStructure.indexOf("varStatus") >= 0)
        {
            //statusPresent is used in string newForEachStructure
            statusPresent = true;
            varStatusElement = getForEachVarStatus(forEachStructure);
        }

        //assembles a .forEach(function(){})-structure from scratch
        var newForEachStructure = "<% " + elements +".forEach(function(" + element
            + (statusPresent? ", ": "") + (statusPresent? varStatusElement : "") + "){ %>";

        return newForEachStructure;


        //FUNCTIONS


        //returns the variable representing varStatus
        function getForEachVarStatus(forEachStructure)
        {
            var beginIndex = forEachStructure.indexOf("varStatus") +11;
            var endIndex = forEachStructure.indexOf('">', beginIndex) ;

            return forEachStructure.slice(beginIndex, endIndex);
        }

        //returns the variable of FOREACH (element)
        function getForEachVar(forEachStructure)
        {
            var argumentsBegin = forEachStructure.indexOf("var") + 5;
            var argumentsEnd = forEachStructure.indexOf("items") - 2;
            var forEachVar = forEachStructure.slice(argumentsBegin, argumentsEnd);

            return forEachVar;
        }

        //returns the items-collection of FOREACH (elements)
        function getForEachVarItems(forEachStructure)
        {
            var argumentsBegin = forEachStructure.indexOf("items") + 9;
            var argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);
            var items = forEachStructure.slice(argumentsBegin, argumentsEnd);

            return items;
        }
    }

    //translates a any if-structure
    function returnTranslationIfStructure(ifStructure)
    {
        var ifStructure = translateTags(ifStructure);

        if(hasAttribute("length") | hasAttribute("empty") | hasAttribute("startsWith")){
            ifStructure = translateAttributes(ifStructure); }

        return ifStructure;

        function hasAttribute(code)
        {
            return (ifStructure.indexOf("" +code+ "") >= 0 ? true: false);
        }

        function translateTags(code)
        {
            //        ifStructure.replace(/\<c\:if test\=\"\$\{/g, '<% if(');
            code = code.replace(/\<c\:if/g, '<% if(');
            code = code.replace(/test\=\"\$\{/g, '');
            code = code.replace(/\}\"\>/g, '){ %>');
            //translate </c:if>
            code = code.replace(/\<\/c:if>/g, '<% } %>');
            return code;
        }

        function translateAttributes(codenietvertaald)
        {
            var code = codenietvertaald;
            //if EMPTY is present
            if(hasAttribute("empty"))
            {
                if(getNumberOfEmpty(code) > 1)
                {
                    var part1 = "<% if((typeof ",
                        part2 = " == undefined && ",
                        part3 = " == '') && (typeof ",
                        part4 = " == undefined && ",
                        part5 = " == '') %>",
                        variable = getVariables(code);
                    return (part1+variable+part2+variable+part3+variable+part4+variable+part5);
                }
                else
                {
                    if(hasAttribute("not empty") | hasAttribute("! empty"))
                    {
                        var part1 = "<% if(typeof ",
                            part2 = " != undefined && ",
                            part3 = " != '') %>",
                            variable = getVariables(code);

                        return (part1+variable+part2+variable+part3);
                    }
                    else
                    {
                        var part1 = "<% if(typeof ",
                            part2 = " == undefined && ",
                            part3 = " == '') %>",
                            variable = getVariables(code);
                        return (part1+variable+part2+variable+part3);
                    }
                }
            }

            //if startsWith is present
            if(hasAttribute("startsWith"))
            {
                code = code.replace(/fn\:startsWith\(/, '');
                code = code.replace(/\, \'/, '.charAt(0)=="');
                code = code.replace(/\'\)/, '"');
                return code;
            }

            //if LENGTH is present
            if(hasAttribute("length"))
            {
                code = code.replace(/fn\:length\(/g, '' );
                code = code.replace(/\)/, '.length() ');
                return code;
            }

            //returns the number of 'empty' present
            function getNumberOfEmpty(code)
            {
                //count the number of "EMPTY"
                var numberOfEmpty = code.match(/empty/g).length;
                return numberOfEmpty;
            }

            //returns the variable present inside the IF
            function getVariables(code)
            {
                var index0 = code.indexOf("empty") +6;
                var index1 = code.indexOf(")", index0) ;
                //return code.slice(index0, index1);
                return code.slice(index0, index1);
            }

            //returns all miscellaneous code between begin- and endtag
            function getContent(code)
            {
                var index0 = code.indexOf("%>") +2;
                var index1 = code.indexOf("<%", index0);
                return code.slice(index0, index1);
            }
        }

        //translates an includestructure
        function translateIncludeStructure(includeStructure)
        {
            // trim onnodige code
            //VOOR: <%@ include file="/views/....jsp" %>
            //NA:   <%  include ... %>
            //of    <% include admin/... %>

            includeStructure = includeStructure.replace('@', '');
            includeStructure = includeStructure.replace('file="/views/', '');
            includeStructure = includeStructure.replace('.jsp"', ' ');

            return includeStructure
        }
}

    //translates an include structure
    function returnTranslationIncludeStructure(includeStructure)
    {
        // trim onnodige code
        //VOOR: <%@ include file="/views/....jsp" %>
        //NA:   <%  include ... %>
        //of    <% include admin/... %>

        includeStructure = includeStructure.replace('@', '');
        includeStructure = includeStructure.replace('file="/views/', '');
        includeStructure = includeStructure.replace('.jsp"', ' ');

        return includeStructure
    }

    function returnTranslationVariable(variableStructure)
    {
        var variable = variableStructure;
        variable = variable.replace(/\$\{/, '<%= ');
        variable = variable.replace(/\}/, '%>');

        return variable;
    }
}

exports.translateAllVariables = function translateAllVariables(code)
{
    var translatedCode = "",
        codeToTranslate = code,
        totalOccurrencesVariables = (codeToTranslate.match(/\$\{/g).length);

    const index0 = 0;


    ////////////////////////////////////////
    //translate all occurrences of Out-variables
    translateOutVariables();

    //translate all occurrences of variables
    for(var i = 0; i<totalOccurrencesVariables; i++)
    {
        //place all unused code in translatedCode
        moveUnusedCode(index0, getIndexBeginOfVariable(codeToTranslate));
        translatedCode += translateVariable(getVariableStructure(codeToTranslate));
        codeToTranslate = codeToTranslate.replace(getVariableStructure(codeToTranslate), "");
    }
    appendCode();
    return translatedCode;
    /////////////////////////////////////////////////////////

    //FUNCTIONS

    //appends the left over code
    function appendCode()
    {
        if(codeToTranslate.length > 0)
        {
            translatedCode += codeToTranslate;
        }
    }

    //returns the begin-index of the variable
    function getIndexBeginOfVariable(code)
    {
        //aangepast door QUNIT-test
        return code.indexOf("${");
    }

    //moves unused code to translatedCode
    function moveUnusedCode(beginIndex, endIndex)
    {
        var removedCode = codeToTranslate.slice(beginIndex, endIndex);
        translatedCode += removedCode;
        //just to be sure index 0 is the index where code starts again
        codeToTranslate = codeToTranslate.replace(removedCode, "");
    }

    //returns the variable
    function getVariableStructure(code)
    {
        //zoeken naar ${
        var beginVar = code.indexOf("${");
        //zoeken naar de END-tag
        var endVar = code.indexOf("}", beginVar) + 1;
        var varStructure = code.slice(beginVar, endVar);

        return varStructure;
    }

    //translates the variable returned
    function translateVariable(variableStructure)
    {
        var variable = variableStructure;
        variable = variable.replace(/\$\{/, '<%= ');
        variable = variable.replace(/\}/, '%>');

        return variable;
    }

    //translates all Out-variables
    function translateOutVariables()
    {
        //  first remove the escapeXml-tag
        codeToTranslate = codeToTranslate.replace(/escapeXml="true" /g, "");
        //  <c:out value="${ --> <%=
        codeToTranslate = codeToTranslate.replace(/\<c\:out value\=\"\$\{/g, "<%= ");
        //  }" />  --> %>
        codeToTranslate = codeToTranslate.replace(/\}" \/\>/g, "%>");
    }
}