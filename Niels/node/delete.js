/**
 * Created by niels on 17/12/14.
 */
// here comes all the code that removes tags, comments, libraries....

exports.deleteAllExtranousCode = function deleteUOverflowingCode(code)
{
    //delete choose-tags
    code = code.replace(/\<c\:choose\>/g, "");
    code = code.replace(/\<\/c\:choose\>/g, "");

    //delete all code starting with '<%@ page' or '<%@ taglib' and ends with '%>'.
    code = code.replace(/\<\%\@\spage.*\%\>/g, "");
    code = code.replace(/\<\%\@\staglib.*\%\>/g, "");

    //delete all comments (Tim)
    code = code.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, "");

    //removes newlines at the beginning of the code
    code = code.replace(/\r?\n|\r/, "");

    return code;
}