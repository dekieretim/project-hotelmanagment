/**
 * Created by niels on 15/12/14.
 */


//returns the begin index of any structure defined in the function
exports.getBeginIndexOfStructure = function getBeginIndexOfStructure(code, structure)
{
    //STRUCTURE
        //when  : <c:when
        //if    : <c:if
        //choose  : <c:when
        //include : <%@
        //foreach : <c:forEach
        //variables : ${

    return code.indexOf(structure);
}

//returns the end index of any structured defined in the function
exports.getEndIndexOfStructure  = function getEndIndexOfStructure(structure, code)
{
    //STRUCTURE
        //choose  : </c:when
        //if    : </c:if
        //choose  : </c:when
        //include : .jsp"%>
        //foreach : </c:forEach
        //variables : }

    switch(structure)
    {
        case 'if': returnEndIndexOfIFStructure(code);
            break;
        case 'forEach': returnEndIndexOfForEachStructure(code);
            break;
        case 'include': returnEndIndexOfIncludeStructure(code);
            break;
        //choose-tags are removed, so we search for the first when end index
        case 'choose': returnEndIndexOfWhenStructure(code);
            break;
        case 'variable': returnEndIndexVariableStructure(code);
            break;
    }

    //FUNCTIONS
    function returnEndIndexOfWhenStructure(code)
    {
        var beginIndex = code.indexOf("<c:when");
        return (code.indexOf('">', beginIndex)) +3;
    }

    function returnEndIndexOfForEachStructure(code)
    {
        return (code.indexOf("</c:forEach")+12);
    }

    function returnEndIndexOfIFStructure(code)
    {
        var beginIndex = code.indexOf('<c:if');
        return code.indexOf('}">', beginIndex)+3;
    }

    function returnEndIndexOfIncludeStructure(code)
    {
        var beginInclude = code.indexOf("<%@ include");
        return code.indexOf('.jsp"%>', beginInclude) +7;
    }

    function returnEndIndexVariableStructure(code)
    {
        var beginVar = code.indexOf("${");
        return code.indexOf("}", beginVar) + 1;
    }
}

//returns the structure from beginIndex till endIndex
exports.getStructure = function getStructure(beginIndex, endIndex)
{
    return codeToTranslate.slice(beginIndex, endIndex);
}

exports.getNumberOfOccurrences = function getNumberOfOccurrences(code, structure)
{
    switch(structure)
    {
        case 'if': return code.match(/\<\/c\:if/g).length;
            break;
        case 'forEach': return code.match(/\<\/c\:forEach/g).length;
            break;
        case 'include': return code.match(/<%@ include/g).length;
            break;
        //choose-tags are removed, so we search for the first when end index
        case 'choose': return code.match(/\<c\:when/g).length;
            break;
        case 'variable': return code.match(/\$\{/g).length;
            break;
    }
}

exports.getStructuresPresent = function getStructuresPresent(code)
{
    var ifStr, forEachStr, includeStr, whenStr,
        arrayOfOccurrences = [ifStr, forEachStr, includeStr, whenStr],
        arrayOfStructures = ['<c:if', '<c:forEach', '<%@ include', '<c:when'];

    for(var i = 0; i < arrayOfStructures.length + 1; i++)
    {
        if(getBeginIndexOfStructure(code, arrayOfStructures[i]) >= 0)
        {
            arrayOfOccurrences[i] = true;
        }
    }

    return arrayOfOccurrences;
}