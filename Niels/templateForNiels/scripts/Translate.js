var input, output, div, fors, originalFile, typer, outputButton, filename, fileInput,
    dirInput, home, reader, files, fileNumber = 0, readFiles = [], translatedFile;
//var jspToEjs = require('./node/TranslateJSPtoEJS');
window.onload = function () {
    initializeAllElements();
    addAllListeners();
};

function initializeAllElements() {

    fileInput = document.getElementById('fileInput');
    typer = document.getElementById("translate");
    outputButton = document.getElementById("download");
    filename = document.getElementById("fileName").value;
    originalFile = document.getElementById("originalFile");
    div = document.getElementById('translate');
    dirInput = document.getElementById('dirInput');
    home = document.getElementById('Home');
    reader = new FileReader();
    translatedFile = document.getElementById('translatedFile');
}



function addAllListeners() {
//    hulpmethod om de default actie bij events te overschrijven
    function cancel(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        return false;
    }

    //    Zorgt ervoor dat de file niet geöpend wordt in de browser door "dragover" en "dragenter" events te annuleren, zodat alleen de (overgeschreven) drop functie wordt aangeroepen
    window.addEventListener('dragover', cancel, false);
    window.addEventListener('dragenter', cancel, false);


//    deze functie zorgt ervoor dat als je een file drag-en-dropt in je browservenster, de readFile method wordt aangroepen op deze file.
    window.addEventListener('drop', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        } // stops the browser from redirecting off to the image.
        var dt = e.dataTransfer;
        var items = e.dataTransfer.items;
        for (var i=0; i<items.length; i++) {
            var entry = items[i].webkitGetAsEntry();
            if (entry.isFile)
                readFile(dt);
            else {

                alert("drag and drop voor mappen nog niet ondersteund: gebruik de knop om mappen te vertalen")
            }
        }
    });
//    deze functie zorgt ervoor dat als je een file kiest met behulp van de upload-knop, de readFile method wordt aangroepen op deze file.
    fileInput.addEventListener('change', function () {
//        console.log(fileInput);
        readFile(fileInput);
    });
    dirInput.addEventListener('change', function (e) {
//        e.preventDefault();
        console.log(dirInput.files);
        readDirectory(dirInput.files);
    });
}

function openFile() {
    //checks if it already exists, adds the href, sets hreffed true (efficient version)
    !this.hreffed && (this.href = webkitURL.createObjectURL(this.file));
    this.hreffed = true;
}

/**
 * Created by niels on 2/01/15.
 */
function Vertaal(jspCode) {

    if(typeof jspCode == "string")
    {
        var codeToTranslate = jspCode;
        codeToTranslate = deleteOverflowingCode(codeToTranslate);           //deletes all overflowing code like useless endtags, imports etc
        codeToTranslate = translateAllBeginTagStructures(codeToTranslate);  //translates all begin tags present in codeToTranslate
        codeToTranslate = translateNonChangeableTags(codeToTranslate);      //translates all non changeable tags in codeToTranslate
        output = translateAllVariables(codeToTranslate);                    //translates all variables present in codeToTranslate
    } else
    {
        console.log("The parameter passed was not a string.");
        throw error("An incorrect parameter was passed to translateJSP: its type was not string.");
    }

    //FUNCTIONS

    //CHANGE
    //translates all begin tag structures
    function translateAllBeginTagStructures(code){
        if(getMatches(/<c:if/g, code) > 0){
            code = translateBeginTag(code, /<c:if/g, "<c:if", "if");
            console.log('translated IF');
        }
        if(getMatches(/<c:forEach/g, code) > 0){
            code = translateBeginTag(code, /<c:forEach/g, "<c:forEach", "forEach");
            console.log('translated FOREACH');
        }
        if(getMatches(/<%@ include/g, code) > 0){
            code = translateBeginTag(code, /<%@ include/g, "<%@", "include");
            console.log('translated INCLUDE');
        }
        if(getMatches(/<jsp:include/g, code) > 0){
            code = translateBeginTag(code, /<jsp:include/g, "<jsp:include", "jspInclude");
            console.log('translated JSP-INCLUDE');
        }
        if(getMatches(/<c:when/g, code) > 0){
            code = translateBeginTag(code, /<c:when/g, "<c:when", "when");
            console.log('translated WHEN');
        }
        if(getMatches(/<jsp:forward/g, code) > 0){
            code = translateBeginTag(code, /<jsp:forward/g, "<jsp:forward", "forward");
            console.log('translated FORWARD');
        }
        if(getMatches(/<jsp:setProperty/g, code) > 0){
            code = translateBeginTag(code, /<jsp:setProperty/g, "<jsp:setProperty", "setProperty");
            console.log('translated SETPROPERTY');
        }
        if(getMatches(/<jsp:getProperty/g, code) > 0){
            code = translateBeginTag(code, /<jsp:getProperty/g, "<jsp:getProperty", "getProperty");
            console.log('translated GETPROPERTY');
        }
        if(getMatches(/<c:out/g, code) > 0){
            code = translateBeginTag(code, /<c:out/g, "<c:out", "out");
            console.log('translated OUT');
        }
        if(getMatches(/<c:forTokens/g, code) > 0){
            code = translateBeginTag(code, /<c:forTokens/g, "<c:forTokens", "forTokens");
            console.log('translated FORTOKENS');
        }

        return code;

        //CODE = complete jsp codefile: REGEX = regex of structure to translate; JSPTAGNAME = name of jsp structure; JSTAGNAME = name of js structure
        function translateBeginTag(code, regex, jspTagName, jsTagName){
            var codeToTranslate = code,
                translatedCode = '',
                totalOccurrences = getMatches(regex, codeToTranslate);

            while(totalOccurrences !=0)
            {
                var beginIndex = getBeginIndexOfStructure(jspTagName, codeToTranslate),
                    endIndex = getEndIndexOfStructure(jsTagName, codeToTranslate),
                    structure = getStructure(beginIndex, endIndex, codeToTranslate);

                codeToTranslate = moveUnusedCode(beginIndex, codeToTranslate); //removes the 'useless' code from codeToTranslate to translatedCode
                translatedCode += translateStructure(jsTagName, structure); // adds the translated structure to translatedCode
                codeToTranslate = codeToTranslate.replace(structure, ""); //remove original structure from codeToTranslate
                totalOccurrences--; //ensure the loop ends
            }

            return translatedCode + codeToTranslate;


            //HELPER FUNCTION
            //moves "not to be translated"-code to translatedCode
            //removes "not to be translated"-code from codeToTranslate
            function moveUnusedCode(endIndex, codeToSlice)
            {
                var removedCode = codeToSlice.slice(0, endIndex);
                translatedCode += removedCode; //move unused code to translatedCode

                return codeToSlice.replace(removedCode, ""); //return after removing the unused code from codeToTranslate
            }
        }
    }

    //CHANGE
    //translates all end-tags that never changes, no matter what the situation; CODE is the complete .jsp-file
    function translateNonChangeableTags(code){
        //END-TAGS
        code = code.replace( /<\/c:if>/g, '<% } %>') //translates all </c:if>-end-tags to <% } %>
            .replace( /<\/c:forEach>/g, '<% }) %>') //translates all </c:forEach>-end-tags to <% } %>
            .replace( /<\/c:when>/g, '<% } %>' ) //CONVERT WHEN-end-tag to IF-end-tag
            .replace( /<c:otherwise>/g, '<% else{ %>' ) //CONVERT <c:otherwise></c:otherwise> to ELSE-structure - first part
            .replace( /<\/c:otherwise>/g, '<% } %>' ) // second part
            .replace( /<\/c:forTokens>/g, '<% }) %>')
            .replace( /<%--/g, '<%#')    //translates the begintags of all commented code
            .replace( /--%>/g, '%>');    //translated the endtags of all commented code
        return code;
    }

    //CHANGE
    //STRUCTURETAG is the name of the structure we want to translate here; STRUCTURECODE is actual code of the structure that needs to be translated
    function translateStructure(structureTag, structureCode){
        var translatedStructure;
        switch(structureTag)
        {
            case 'if': translatedStructure = returnTranslationIfStructure(structureCode); break;
            case 'forEach': translatedStructure = returnTranslationForEachStructure(structureCode); break;
            case 'include': translatedStructure = returnTranslationIncludeStructure(structureCode); break;
            case 'jspInclude': translatedStructure = returnTranslationJspIncludeStructure(structureCode); break;
            case 'when': translatedStructure = returnTranslationWhenStructure(structureCode); break;
            case 'variable': translatedStructure = returnTranslationWhenStructure(structureCode); break;
            case 'forward' : translatedStructure = returnTranslationForwardStructure(structureCode); break;
            case 'setProperty' : translatedStructure = returnTranslationSetPropertyStructure(structureCode); break;
            case 'getProperty' : translatedStructure = returnTranslationGetPropertyStructure(structureCode); break;
            case 'out' : translatedStructure = returnTranslationOutStructure(structureCode); break;
            case 'forTokens' : translatedStructure = returnTranslationForTokensStructure(structureCode); break;

            default : console.log("Unknown parameter; " +
                "if, forEach, include, jspInclude, choose, variable, forward, setProperty, getProperty, out or forTokens expected!");
        }

        return translatedStructure;


        //returns a translated forTokens structure
        function returnTranslationForTokensStructure(forTokensStructure){
            var forTokensItems = getForTokensItems(forTokensStructure),
                forTokensDelims = getForTokensDelims(forTokensStructure),
                forTokensVariable = getForTokensVariable(forTokensStructure);

            return
            '<% var mkntForTokensString = ' +forTokensItems+ '; %>'
                + '<% var mkntForTokensResult = mkntForTokensString.split("' +forTokensDelims+ '"); %>'
                + '<% mkntForTokensResult.forEach(function(' +forTokensVariable+ '){';


            //HELPER FUNCTIONS
            function getForTokensItems(structure)
            {
                var beginItems = structure.indexOf('items') +7;
                var endItems = structure.indexOf('"');
                return structure.slice(beginItems, endItems);
            }

            function getForTokensDelims(structure)
            {
                var beginDelims = structure.indexOf('delims') +8;
                var endDelims = structure.indexOf('"');
                return structure.slice(beginDelims, endDelims);
            }

            function getForTokensVariable(structure)
            {
                var beginVar = structure.indexOf('var') +5;
                var endVar = structure.indexOf('"');
                return structure.slice(beginVar, endVar);
            }
        }

        //returns a translated out structure
        function returnTranslationOutStructure(OutStructure){
            return "<%" +(getEscapeXMLValue(OutStructure)? "=":"-")
                + " " +getOutValue(OutStructure)+ " "
                +(getDefault(OutStructure)? ("||" +getDefault(OutStructure)+""): (""))
                + " %>";

            function getOutValue(structure)
            {
                var beginValue = structure.indexOf('value="${') +9;
                var endValue = structure.indexOf('}" />', beginValue);
                return structure.slice(beginValue, endValue);
            }

            function getEscapeXMLValue(structure)
            {
                var beginEscapeXml = structure.indexOf('escapeXML') +11;
                var endEscapeXml = structure.indexOf('"', beginEscapeXml);
                var xml = structure.slice(beginEscapeXml, endEscapeXml);

                return xml? true: false;
            }

            function getDefault(structure)
            {
                var beginDefault = structure.indexOf('default') +9;
                var endDefault = structure.indexOf('"', beginDefault);

                return structure.slice(beginDefault, endDefault);
            }
        }

        //returns a translated setproperty structure
        function returnTranslationSetPropertyStructure(setPropStructure){
            // <% myName.someProperty = someValue %>
            return "<% " +getPropName(setPropStructure)+ "." +getPropProperty(setPropStructure)+ " = " +getPropValue(setPropStructure)+ " %>";

            function getPropValue(structure)
            {
                var beginValue = structure.indexOf("value") + 7;
                var endValue = structure.indexOf('"', beginValue);

                return structure.slice(beginValue, endValue);
            }
        }

        //returns a translated getproperty structure
        function returnTranslationGetPropertyStructure(getPropStructure){
            // <% myName.someProperty %>
            return "<% " +getPropName(getPropStructure)+ "." +getPropProperty(getPropStructure)+ " %>";
        }

        //returns a translated forward structure
        function returnTranslationForwardStructure(forwardStructure){
            var beginPage = forwardStructure.indexOf("page") + 6,
                endPage = forwardStructure.indexOf('"', beginPage);

            return "<% include " +forwardStructure.slice(beginPage, endPage);+ ".ejs %>"
        }

        //returns a translated jsp include structure
        function returnTranslationJspIncludeStructure(jspIncludestructure){
            return jspIncludestructure.replace('<jsp:include page="', '<% include ')
                .replace('.jsp"', ' %');
        }

        //returns a translated when structure
        function returnTranslationWhenStructure(whenStructure){
            var beginIndexContent = whenStructure.indexOf("<c:when") + 16,
                endIndexContent = whenStructure.indexOf('}">', beginIndexContent);

            return "<% if(" + whenStructure.slice(beginIndexContent, endIndexContent) + "){ %>\n";
        }

        //returns a translated forEach structure
        function returnTranslationForEachStructure(forEachStructure){

            if((forEachStructure.indexOf("begin")>=0) | (forEachStructure.indexOf("end")>=0))
            {
                //with given step
                if(forEachStructure.indexOf("step")>=0)
                {
                    var begin = getBegin(forEachStructure),
                        end = getEnd(forEachStructure),
                        step = getStep(forEachStructure),
                        variable = getVar(forEachStructure);

                    return "<% for(var " +variable+ " = " +begin+ "; i < " +end+ "; i + " +step+ ") { %>";
                }

                //without given step (step = 1)
                else
                {
                    var begin = getBegin(forEachStructure),
                        end = getEnd(forEachStructure),
                        variable = getVar(forEachStructure);

                    return "<% for(var " +variable+ " = " +begin+ "; i < " +end+ "; i++ ) { %>";
                }

                //FUNCTIONS
                function getVar(structure)
                {
                    var beginVar = structure.indexOf("var") + 5;
                    var endVar = structure.indexOf('"', beginVar);

                    return structure.slice(beginVar, endVar);
                }

                function getBegin(structure)
                {
                    var beginStart = structure.indexOf("begin") + 7;
                    var beginEnd = structure.indexOf('"', beginStart);

                    return structure.slice(beginStart, beginEnd);
                }

                function getEnd(structure)
                {
                    var endStart = structure.indexOf("end") + 5;
                    var beginStart = structure.indexOf('"', endStart);

                    return structure.slice(endStart, beginStart);
                }

                function getStep(structure)
                {
                    var stepBegin = structure.indexOf("step") + 6;
                    //argumentsEnd = forEachStructure.indexOf('"')[2] - 1;
                    var stepEnd = structure.indexOf('"', stepBegin);
                    //var arguments = IfStructure.splice(argumentsBegin, argumentsEnd);

                    return structure.slice(stepBegin, stepEnd);
                }
            }
            //forEach loop with collections (and varstatus)
            else
            {
                var elements = getForEachVarItems(forEachStructure), //get the array-element
                    element = getForEachVar(forEachStructure), //get the single element
                    statusPresent = false,
                    varStatusElement;

                //get the element representing varStatus
                if(forEachStructure.indexOf("varStatus") >= 0){
                    varStatusElement = getForEachVarStatus(forEachStructure);
                    statusPresent = true; //statusPresent is used in string newForEachStructure
                }

                //assembles a .forEach(function(){})-structure from scratch and returns it
                return "<% " + elements +".forEach(function(" + element + (statusPresent? ", ": "") + (statusPresent? varStatusElement : "") + "){ %>";

                //FUNCTIONS
                //hier zou je met refactoren 2 functies van kunnen maken (1 & 3 kan je combineren in 1 functie)
                //maar dat maakt het en pak minder leesbaar

                //returns the variable representing varStatus
                function getForEachVarStatus(forEachStructure){
                    var beginIndex = forEachStructure.indexOf("varStatus") + 11,
                        endIndex = forEachStructure.indexOf('">', beginIndex) ;

                    return forEachStructure.slice(beginIndex, endIndex);
                }

                //returns the variable of FOREACH (element)
                function getForEachVar(forEachStructure){
                    var argumentsBegin = forEachStructure.indexOf("var") + 5,
                        argumentsEnd = forEachStructure.indexOf("items") - 2;

                    return forEachStructure.slice(argumentsBegin, argumentsEnd);
                }

                //returns the items-collection of FOREACH (elements)
                function getForEachVarItems(forEachStructure){
                    var argumentsBegin = forEachStructure.indexOf("items") + 9,
                        argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);

                    return forEachStructure.slice(argumentsBegin, argumentsEnd);
                }
            }
        }

        //returns a translated if structure
        function returnTranslationIfStructure(structure){
            var ifStructure = translateTags(structure);

            if(hasAttribute("length") || hasAttribute("empty") || hasAttribute("startsWith")){
                ifStructure = translateAttributes(ifStructure);
            }

            return ifStructure;

            //HELPER FUNCTIONS
            function hasAttribute(code){
                return ifStructure.indexOf("" +code+ "") >= 0;
            }

            function translateTags(code){
                return code.replace(/<c:if/g, '<% if(')
                    .replace(/test="\$\{/g, '')
                    .replace(/\}">/g, '){ %>')
                    .replace(/<\/c:if>/g, '<% } %>');//translate </c:if>
            }

            function translateAttributes(code){
                var variable = getVariables(code),
                    vertaaldeCode = "";

                //if EMPTY is present
                if(hasAttribute("empty")){
                    if(getNumberOfEmpty(code) > 1){
                        vertaaldeCode = "<% if((typeof " + variable + " == undefined && " + variable + " == '') && (typeof " + variable +
                            " == undefined && " + variable + " == '') %>";
                    }
                    else {
                        if(hasAttribute("not empty") || hasAttribute("! empty")){
                            vertaaldeCode = "<% if(typeof " + variable + " != undefined && " + variable + " != '') %>";
                        }
                        else {
                            vertaaldeCode = "<% if(typeof " + variable + " == undefined && " + variable + " == '') %>";
                        }
                    }
                }

                //if startsWith is present
                if(hasAttribute("startsWith")){
                    vertaaldeCode = code.replace(/fn:startsWith\(/, '')
                        .replace(/, '/, '.charAt(0)=="')
                        .replace(/'\)/, '"');
                }

                //if LENGTH is present
                if(hasAttribute("length")){
                    vertaaldeCode = code.replace(/fn:length\(/g, '' )
                        .replace(/\)/, '.length() ');
                }

                return vertaaldeCode;

                //returns the number of 'empty' present
                function getNumberOfEmpty(code){
                    return code.match(/empty/g).length;
                }

                //returns the variable present inside the IF
                function getVariables(code) {
                    var index0 = code.indexOf("empty") + 6,
                        index1 = code.indexOf(")", index0);

                    return code.slice(index0, index1);
                }
            }
        }

        //returns a translated include structure
        function returnTranslationIncludeStructure(includeStructure){
            // trim onnodige code
            //VOOR: <%@ include file="/views/....jsp" %>
            //NA:   <%  include ... %>  of  <% include admin/... %>

            return includeStructure.replace('@', '')
                .replace('file="/views/', '')
                .replace('.jsp"', ' ')
                + "\n";
        }

        //HELPER FUNCTIONS FOR GET & SET PROPERTY
        function getPropName(structure)
        {
            var beginName = structure.indexOf("name") + 6;
            var endName = structure.indexOf('"', beginName);

            return structure.slice(beginName, endName);
        }
        function getPropProperty(structure)
        {
            var beginProperty = structure.indexOf("property") + 10;
            var endProperty = structure.indexOf('"', beginProperty);

            return structure.slice(beginProperty, endProperty);
        }
    }

    //CHANGE
    //translates all variables
    function translateAllVariables(code){
        var translatedCode = "",
            codeToTranslate = code,
            totalOccurrencesVariables = getMatches(/\$\{/g, codeToTranslate);

        //translate all occurrences of variables
        while(totalOccurrencesVariables !=0){
            var beginIndexVar = getBeginIndexOfStructure("${", codeToTranslate),
                endIndexVar = getEndIndexOfStructure("variable", codeToTranslate),
                variable = getStructure(beginIndexVar, endIndexVar, codeToTranslate);

            codeToTranslate = moveUnusedCode(0, beginIndexVar, codeToTranslate); //removes the 'useless' code from codeToTranslate to translatedCode
            translatedCode += translateVariable(variable); //adds the translated structure to translatedCode
            codeToTranslate = codeToTranslate.replace(variable, ""); //remove original if structure from codeToTranslate
            totalOccurrencesVariables--;
        }

        return translatedCode + codeToTranslate;
        /////////////////////////////////////////////////////////
        //FUNCTION
        //moves unused code to translatedCode
        function moveUnusedCode(beginIndex, endIndex, code){
            var removedCode = code.slice(beginIndex, endIndex);
            translatedCode += removedCode; //move unused code to translatedCode

            return code.replace(removedCode, ""); //return after removing the unused code from codeToTranslate
        }

        //translates the variable returned
        function translateVariable(variableStructure){
            return variableStructure.replace(/\$\{/, '<%= ')
                .replace(/\}/, '%>');
        }
    }

    //DELETE
    //deletes all overflowing code (eg. endtags of include&forward, choosetags, page & lib imports etc)
    function deleteOverflowingCode(code){
        return code.replace(/<c:choose>/g, "") //delete begin of choose tags
            .replace(/<\/c:choose>/g, "") //delete end of choose tags
            .replace(/<%@\spage.*%>/g, "") //delete code starting with <%@ page and ending with %>
            .replace(/<%@\staglib.*%>/g, "") //delete code starting with <%@ taglib and ending with %>
            //.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, "") //delete all comments (Tim)
            .replace(/\r?\n|\r/, "") //removes newlines at the beginning of the code
            .replace(/<jsp:param.*\/>/g,"") //delete left-over code from jsp:include => <jsp:param name="title" value="${page.title}"/>
            .replace(/<\/jsp:include>/g, "") //</jsp:include>
            .replace(/<\/jsp:forward>/g, ""); //deletes the endtag of "forward", not needed in EJS
    }

    //SEARCH
    //returns the begin index of any structure defined in the function
    function getBeginIndexOfStructure(structure, code){
        return code.indexOf(""+structure+"");
    }

    //SEARCH
    //returns the end index of any structured defined as structureTag in code
    function getEndIndexOfStructure(structureTag, code){
        var endIndexOfStructure;
        switch(structureTag)
        {
            case 'if': endIndexOfStructure = returnEndIndex(code, "<c:if", '}">', 3); break;
            case 'forEach': endIndexOfStructure = returnEndIndex(code, "<c:forEach", '}">', 3); break;
            case 'include': endIndexOfStructure = returnEndIndex(code, "<%@ include", '.jsp"%>', 7); break;
            case 'jspInclude': endIndexOfStructure = returnEndIndex(code, "<jsp:include", ">", 1); break;
            case 'when': endIndexOfStructure = returnEndIndex(code, "<c:when", ">", 2); break;
            case 'variable': endIndexOfStructure = returnEndIndex(code, "${", "}", 1); break;
            case 'forward' : endIndexOfStructure = returnEndIndex(code, "<jsp:forward", '">', 2); break;
            case 'setProperty' : endIndexOfStructure = returnEndIndex(code, "<jsp:setProperty", '/>', 2); break;
            case 'getProperty' : endIndexOfStructure = returnEndIndex(code, "<jsp:getProperty", '/>', 2); break;
            case 'out' : endIndexOfStructure = returnEndIndex(code, "<c:out", '/>', 2); break;
        }

        return endIndexOfStructure;

        //HELPER FUNCTION
        function returnEndIndex(code, startTag, endPart, offset){
            var beginIndex = code.indexOf(startTag);
            return code.indexOf(endPart, beginIndex) + offset;
        }
    }

    //SEARCH
    //returns the structure from beginIndex till endIndex (character @ endindex NOT INCLUDED)
    function getStructure(beginIndex, endIndex, code){
        return code.slice(beginIndex, endIndex);
    }

    //SEARCH
    //returns the number of matches in code of the regex
    function getMatches(regex, code){
        var matches = code.match(regex);
        return (matches != null) ? matches.length : 0;
    }
}

function showOutput() {
    Vertaal(input);

    //document.getElementById('translatedFile').innerText = output;
    translatedFile.innerText = output;
}

function getNextFile() {
    fileNumber++;
    if (fileNumber <= files.length)
        return files[fileNumber];

    console.log("done reading", filenumber, "files");
    return null;
}

function readDirectory(userData) {
    home.innerHTML = "all files:";

//        var f = this.files;
//        var files = [];
//        for (var i = 0; i<f.length; i++){
//            files.push(f[i]);
//
//        }
    //same code as above, but more efficient:

    files = [].slice.call(userData);
    console.log("lalalalaaa");
    console.log(files);

//    files.forEach(function (file) {

    reader.onloadend = function () {
        var next = getNextFile();
        console.log(reader.result);
        readFiles.push(reader.result);

        if (next != null)
            reader.readAsText(next, function () {
                console.log("klaar met een");
            });
        var file = files[fileNumber-1];
        if (file!= null){
            var li = document.createElement('li'),
                a = document.createElement('a');
            console.log("file", file);
            a.file = file;
            a.href = '#';
            a.target = '_blank';
            a.textContent = file.name;
            //only generate if it's clicked
            a.addEventListener('click', openFile);
            li.appendChild(a);
            home.appendChild(li);
            console.log("het resultaat is");
//        console.log(reader.result);
        }

    };
//    for (var i = 0; i < files.length; i++) {


    console.log("aant lezen");
//        while(reader.readyState === reader.LOADING){
//            console.log(reader.readyState);
//        }
    reader.readAsText(files[0], function () {
        console.log("reading");

    });
//        reader.onloadend = function(){
//            console.log("for real", reader.result);

//        };

    console.log("zogezegd klar me lezen");


//    }

//    reader.onload = function() {
//
//        console.log(input);
//        var li2 = document.createElement('li'),
//            a2 = document.createElement('a');
//        a2.file = file;
//        a2.href = '#';
//        a2.target = '_blank';
//        a2.textContent = file.name;
//        a2.addEventListener('click', openFile);
//        li2.appendChild(a2);
//        home.appendChild(li2);
//    };


}


function readFile(userData) {

    reader.onload = function () {

        input = reader.result;
        console.log("succesfully read file: \n" + input);
    };
    var file = userData.files[0],
        extension = file.name.split('.').pop(),
        requiredExtention = "jsp";


//        test of de extentie ".jsp" is
    if (extension == requiredExtention) {
        console.log(file);

        reader.readAsText(file);
        reader.onloadend = function () {
            originalFile.innerText = input;
            console.log(reader.readyState);
        };
        console.log(reader.readyState);

    }
    else {
        console.log("wrong file type");
    }


}

//a function called by the html buttons, to translate the
function downloadFile(extention) {

    const MIME_TYPE = "text/plain";
    console.log("typer" + typer);
    //creates a blob (file that can be filled with text)
    var bb = new Blob([typer.innerText], {type: MIME_TYPE});

    //creates a hyperlink
    var translatedFile = document.createElement('a');
    translatedFile.download = filename + extention;
    translatedFile.href = window.URL.createObjectURL(bb);
    translatedFile.textContent = "Download";
    translatedFile.dataset.downloadurl = [MIME_TYPE, translatedFile.download, translatedFile.href].join(':');

    outputButton.innerHTML = "";

    //adds the hyperlink to the button
    outputButton.appendChild(translatedFile);

    outputButton.disabled = false;

}
