/**
 * Created by niels on 17/12/14.
 */

//DEZE FUNCTIES KUNNEN MISSCHIEN IN DE TOEKOMST OVERBODIG GEMAAKT WORDEN !!!!!!!!

//appends the leftover code in translated
exports.appendCode = function appendCode()
{
    if(codeToTranslate.length > 0)
    {
        translatedCode += codeToTranslate;
    }
}


//moves the unused code to translatedCode and deletes it from codeToTranslate
//MSS TRANSLATEDCODE EN CODETOTRANSLATE MEEGEVEN, EN DAN RETURNEN MBV EEN ARRAY
exports.moveUnusedCode = function moveUnusedCode(beginIndex, endIndex)
{
    var removedCode = codeToTranslate.slice(beginIndex, endIndex);
    translatedCode += removedCode;
    codeToTranslate = codeToTranslate.replace(removedCode, "");
}

