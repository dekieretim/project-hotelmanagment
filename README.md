Project Hotelmanagment 
======================

### Groep 06: MichÃ¨le Carette, Koen Cornelis, Tim Dekiere en Niels Kuylle
This repository is available with SSH or HTTPS

What is the goal of our project?
--------------------------------

Professionals have predicted it, programmers who have already discovered it agree and companies that keep hesitating to implement it are probably going to regret it: node.js is inevitably the software platform of this generation. That is also what the programmers of two hotels in Oostende realized, giving us: Koen, MichÃ¨le, Niels and Tim, the opportunity to translate all of their code. To complete this project, our supervisor Johan Coppieters helped us: among other things, he suggested that we should first write an automatic code-translator, to translate most of the code automatically. Afterwards, we should manually translate the remaining code manually.







