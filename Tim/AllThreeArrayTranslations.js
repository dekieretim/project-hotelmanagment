/**
 * Created by Tim on 13/05/2014.
 */

var test1 = "var[] myIntArray = new var[3];";
var test4 = "var[] myIntArray =  new  var  [ 3 ] ; ";

var test2 = "var[] myIntArray = {1,2,3};";
var test3 = "var[] myIntArray = new var[]{1,2,3};";
test1 = translateArray(test1);
test2 = translateArray(test2);
test3 = translateArray(test3);
test4 = translateArray(test4);
console.log(test1);
console.log(test2);
function translateArray(arrayToTranslate) {
    var translatedArray = arrayToTranslate;

//    removes "new var"
    translatedArray = translatedArray.replace(/=\s*new\s*var/g, "= ");
//    removes []
    translatedArray = translatedArray.replace(/\[\]/g, "");
//    replaces { by [
    translatedArray = translatedArray.replace(/\{/g, "[");
//    replaces } by ]
    translatedArray = translatedArray.replace(/\}/g, "]");
//    replaces [\d] by []
    translatedArray = translatedArray.replace(/\[\s*\d\s*\]/g, "[]");
    return translatedArray;
}