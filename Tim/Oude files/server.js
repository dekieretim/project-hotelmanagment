/**
 * Created by Tim on 28/02/14.
 */
var http = require('http');
//var test = require('connect');

http.createServer(function (request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.end('<html><body>Home, URL was: ' + request.url + '</body></html>');
    console.log(request.url);
}).listen(3000, 'localhost');

//var app = connect()
//    .use(connect.static('public'))
//    .use(function (req, res) {
//        res.end("Couldn't find it.");
//    })
//    .listen(3000);

console.log('Server running at http://localhost:3000/');