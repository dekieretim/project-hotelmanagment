var input, output, div, fors, originalFile, typer, outputButton, filename, fileInput;


window.onload = function () {

    initializeAllElements();

    addAllListeners();


};

function initializeAllElements(){
    fileInput = document.getElementById('fileInput');
    typer = document.getElementById("translate");
    outputButton = document.getElementById("download");
    filename = document.getElementById("fileName").value;
    originalFile = document.getElementById("originalFile");
    div = document.getElementById('translate');

}

function addAllListeners() {


//    hulpmethod om de default actie bij events te overschrijven
    function cancel(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        return false;
    }


    //    Zorgt ervoor dat de file niet geöpend wordt in de browser door "dragover" en "dragenter" events te annuleren, zodat alleen de (overgeschreven) drop functie wordt aangeroepen
    window.addEventListener('dragover', cancel, false);
    window.addEventListener('dragenter', cancel, false);


//    deze functie zorgt ervoor dat als je een file drag-en-dropt in je browservenster, de readFile method wordt aangroepen op deze file.
    window.addEventListener('drop', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        } // stops the browser from redirecting off to the image.

        var dt = e.dataTransfer;
        readFile(dt);
    });

//    deze functie zorgt ervoor dat als je een file kiest met behulp van de upload-knop, de readFile method wordt aangroepen op deze file.
    fileInput.addEventListener('change', function () {
        readFile(fileInput);
    });

}

function Vertaal(input) {

    input = input.replace(/String/gi, "var");
    /*gi , i -> als er string dus kleine letter staat => ook vervangen
     g -> overloopt heel de tekst en stopt niet bij de eerst gevonden "String"
     */
//    nieuwe replace => input = input.replace(,);

    input = input.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, "");
    input = input.replace(/\/\/.*/g, "");

//     remove all comments


    output = input;

}

function toonOutput() {
    var translatedFor;
    Vertaal(input);
    fors = findFors(output);
    for (var i = 0; i < fors.length; i++) {
        translatedFor = translateForLoop(fors[i]);

        output = output.replace(fors[i], translatedFor);
    }


    var theClass = findClass(output),
        length = 0;

    theClass = splitBodyInMethods(theClass);
    length = theClass.methods.length;

    output = "";
    for (var i = 0; i < length; i = i + 1) {
        console.log("Methode " + (i + 1) + " is: " + theClass.methods[i]);
        output += "Methode " + (i + 1) + " is: " + theClass.methods[i] + "\n";
    }

    translate.innerText = output;
}

function readFile(userData) {

    var file = userData.files[0],
        extension = file.name.split('.').pop(),
        requiredExtention = "jsp",
        reader;
//        test of de extentie ".java" is
    if (extension == requiredExtention) {
        reader = new FileReader();
        reader.onload = function () {
            input = reader.result;
            console.log("succesfully read file: \n" + input);
            originalFile.innerText = input;

        };

        reader.readAsText(file);

    }
    else {
        console.log("wrong file type");
    }


}

//a function called by the html buttons, to translate the
function downloadFile(extention) {

    const MIME_TYPE = "text/plain";
    console.log("typer" + typer);
    //creates a blob (file that can be filled with text)
    var bb = new Blob([typer.innerText], {type: MIME_TYPE});

    //creates a hyperlink
    var translatedFile = document.createElement('a');
    translatedFile.download = filename + extention;
    translatedFile.href = window.URL.createObjectURL(bb);
    translatedFile.textContent = "Download";
    translatedFile.dataset.downloadurl = [MIME_TYPE, translatedFile.download, translatedFile.href].join(':');

    outputButton.innerHTML = "";

    //adds the hyperlink to the button
    outputButton.appendChild(translatedFile);

    output.disabled = false;

}

