/**
 * Created by Tim on 20/03/14.
 */

var test1 = "  int ik = 'tim';";
var test2 = "arraylist al1 = new arraylist()<>";
var test3 = " for(dfghjkl;ihukygkh())){ a{} in msfji\n}\nfor(){test}";
var test = "long a = 50L";

//console.log("einde" + findFors(test3));

//test = isPrimitiveType(test);
if (isPrimitiveType(test1)) {
    test1 = replaceFirstByVar(test1);
//    console.log(test1);
}
else {
    var nonPrimitiveIdentifier = getFirstWord(test1);
//    console.log(nonPrimitiveIdentifier);
//    other code
}

if (isPrimitiveType(test2))
    test2 = replaceFirstByVar(test2);
else {
    var nonPrimitiveIdentifier2 = getFirstWord(test2);
//    console.log(nonPrimitiveIdentifier2);
//    other code
}


function IIFEfy(stringToChange) {
    var changedString;
    changedString = "function(){\n" + stringToChange + "\n}();";
    return changedString;
}

function isPrimitiveType(variableToCheck) {
    var identifier;
    identifier = getFirstWord(variableToCheck);
    if (identifier === "byte" || identifier === "short" || identifier === "int" || identifier === "long" || identifier === "float" || identifier === "double" || identifier === "boolean" || identifier === "char" || identifier === "String")
        return true;
    else
        return false;
}

function getFirstWord(longString) {
    return longString.trim().split(" ")[0];
}

function replaceFirstByVar(stringToChange) {
    return stringToChange.replace(getFirstWord(stringToChange), "var");

}


function findFors(classString) {
    var lastCurlyPos, forStrings = [], curlyClosePos, startIndexes, curlyOpenPos;
    console.log("before");
    console.log(classString);
    startIndexes = getIndices(/for\s*\(.*\)\s*\{/gm, classString);
    console.log("startindexes" + startIndexes);
    for (var i = 0; i < startIndexes.length; i++) {
        curlyClosePos = getIndices(/\}/g, classString.substr(startIndexes[i], classString.length));
        for(var a in curlyClosePos){
            a += startIndexes[i];
        }
        curlyOpenPos = getIndices(/\{/g, classString.substr(startIndexes[i], classString.length));
        console.log("curlyclosepos nummer " + i + "  " + curlyClosePos);
        lastCurlyPos = searchMethodEnd(curlyOpenPos, curlyClosePos).endIndex + 1;
        forStrings.push(classString.substr(startIndexes[i], lastCurlyPos));
        console.log(lastCurlyPos);
    console.log(curlyClosePos);
    console.log("end");
    }
    return forStrings;


}


function getIndices(regex, theString) {
    console.log(theString);
    var result, indices = [];
    while ((result = regex.exec(theString))) {
        console.log("looping");
        indices.push(result.index);

    }
    console.log("einde");
    return indices;
}

function searchMethodEnd(indicesOpenBrace, indicesClosingBrace) {
    var i = 0,
        open = 0,
        close = 0,
        compare = function (open, close) {
            if (open > close) {
                return true;
            }
        },
        returnObject = { stop: false };

    if (indicesOpenBrace.length === 1) {
        returnObject.endIndex = indicesClosingBrace[i];
        returnObject.stop = true;
        return returnObject;
    }

    do {
        if (compare(indicesOpenBrace[open], indicesClosingBrace[close])) {
            close += 1;
        } else {
            open += 1;
        }
    } while (open !== close);

    returnObject.endIndex = indicesClosingBrace[close - 1];
    returnObject.indicesOpenBrace = indicesOpenBrace.slice(open);
    returnObject.indicesClosingBrace = indicesClosingBrace.slice(close);
    return returnObject;
}

var a = "var komkool = 151611L";
console.log(fixNumberDeclaration(a));

function fixNumberDeclaration(declaration){
    var partToReplace = declaration.substr(declaration.indexOf(/\w\s*\=\s*\d[a-zA-Z]/g)),
        partToDelete = partToReplace.replace(/\w\s*\=\s*\d/g, ""),
        fixedDeclaration = declaration.replace(partToDelete, "") + ";";

    return fixedDeclaration;
}


