/**
 * Created by Tim on 09/01/2015.
 */
function getJspCode(input){
    var firstPart = input.split("<%")[1];
    var output = firstPart.split("%>")[0];
    return output;
}

test();

function test(){
    var code = "blabla<%etchtejspcode%>nietetcht";
    var code2 = "<%etchtejspcode%>";
    var res = getJspCode(code);
    var res2 = getJspCode(code2);
    console.log("1: \n" + res);
    console.log("2: \n" + res2);
}