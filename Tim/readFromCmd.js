var fs = require("fs"),
    mknt = require("mknt"),
    dir = require("node-dir"),
    vertaling,
    extension,
    path = "./vertalingen",
    stats,
    requiredExtention = "java";

init();

function init() {

    fs.exists(path, function (exists) {
        if (!exists) {
            fs.mkdir(path, function (err) {
                if (err)console.log(err);
            });
        }
    });
    for (var i = 2; i < process.argv.length; i++) {
        var input = process.argv[i];
        stats = fs.statSync(input);
        if (stats.isDirectory())
            readDir(input);
        else
            readFile(input);

    }

}

function readDir(input) {
    dir.files(input, function (err, files) {
        if (err) console.log(err);
        for (var i = 0; i < files.length; i++) {
            readFile(files[i]);
        }
    });
}

function readFile(input) {
    extension = input.split('.').pop();
    var filename = input.split('\\').pop();
    if (extension === requiredExtention) {
        fs.readFile(input, 'utf8', function (err, data) {
            if (err) console.log(err);
            //TODO: uit commentaar als het klaar is
//            var vertaling = mknt.translateJava(data);
            fs.writeFile("./vertalingen/" + filename.split('.')[0] + " vertaald.js", data, function (err) {
                if (err) console.log(err);
                else {
                    console.log("successfully translated " + filename);
                }
            });
        });
    }
    else {
        console.log("invalid file type: " + filename);
    }
}

//
//function writeFile() {
//    fs.writeFile("/tmp/vertaling.js", "Hey there!", function (err) {
//        if (err) console.log(err);
//        else {
//            console.log("The file was saved!");
//        }
//    });
//}

//function checkInput() {
//    stats = fs.statSync(input);
//    if (stats.isDirectory()) {
//        console.log("is dir");
//        readDir();
//    }
//    else {
//        console.log("is file");
//        readFile();
//    }
//
//}