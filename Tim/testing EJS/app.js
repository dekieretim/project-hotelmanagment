/**
 * Created by Tim on 02/10/2014.
 */

//    starten met "node app.js" command en in browser: http://localhost:8765/ (8080 was bezet)

var express = require('express')
    , app = module.exports = express();

app.engine('.html', require('ejs').__express);

app.set('views', __dirname + '/views');

app.set('view engine', 'html');

app.get('/', function(req, res){
    res.render('index', {
        pageTitle: 'EJS Demo'
    });
});

//to link the scripts to index.html
app.use(express.static(__dirname + '/scripts'));

if (!module.parent) {
    app.listen(8765);
    console.log('EJS Demo server started on port 8765');
}