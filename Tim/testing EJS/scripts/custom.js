var clicked;
$(document).ready( doInit );

function doInit() {
    console.log("We zijn vertrokken...");
    $("#tabs").tabs();

    $("ul.mail li").bind("click", doEdit);
    $("#new").bind("click", doNew);

    $("#not").bind("click", doClose);
    $("#ok").bind("click", doSave);

//    $("#editForm").validate();
}

function doNew() {
    clicked = null;
    $("#email").val( "" );
    $("#active").removeAttr("checked");
    $("#editForm").dialog({title: "Email bijmaken"});
}

function doEdit() {
    clicked = this;

    // checkbox juist zetten
    if ($(this).parent().hasClass("active"))
        $("#active").attr("checked", "checked");
    else
        $("#active").removeAttr("checked");

    // inhoud email veld invullen
    var email = $(this).text();
    $("#email").val( email );

    $("#editForm").dialog({title: "Email aanpassen"});
}

function doClose() {
    $("#editForm").dialog("close");
}

function doSave() {
    var email = $("#email").val();

    // check op active / not-active in form
    var isActive = $("#active").is( ":checked" );

    // juiste lijst vinden
    var theList = (!isActive) ? $("ul.active") : $("ul.nonactive");

    if (clicked == null) {
        // nieuw element -> in juiste lijst hangen
        theList.append("<li>" + email + "</li>");

    } else {
        // TODO!!!
        //   indien verschillend: in juiste lijst hangen
        //   (remove, append)
        //$(clicked).text(email);


        $(clicked).text( email );
        theList.remove("<li>" + email + "</li>");
        // theList.append("<li>" + email + "</li>");
    }
    doClose();
}

