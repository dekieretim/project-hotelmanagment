var input, output, div, fors, originalFile, typer, outputButton, filename, fileInput;


window.onload = function () {

    initializeAllElements();

    addAllListeners();


}

function initializeAllElements(){
    fileInput = document.getElementById('fileInput');
    typer = document.getElementById("translate");
    outputButton = document.getElementById("download");
    filename = document.getElementById("fileName").value;
    originalFile = document.getElementById("originalFile");
    div = document.getElementById('translate');

}

function addAllListeners() {


//    hulpmethod om de default actie bij events te overschrijven
    function cancel(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        return false;
    }


    //    Zorgt ervoor dat de file niet geöpend wordt in de browser door "dragover" en "dragenter" events te annuleren, zodat alleen de (overgeschreven) drop functie wordt aangeroepen
    window.addEventListener('dragover', cancel, false);
    window.addEventListener('dragenter', cancel, false);


//    deze functie zorgt ervoor dat als je een file drag-en-dropt in je browservenster, de readFile method wordt aangroepen op deze file.
    window.addEventListener('drop', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        } // stops the browser from redirecting off to the image.

        var dt = e.dataTransfer;
        readFile(dt);
    });

//    deze functie zorgt ervoor dat als je een file kiest met behulp van de upload-knop, de readFile method wordt aangroepen op deze file.
    fileInput.addEventListener('change', function () {
        readFile(fileInput);
    });

}

function Vertaal(input) {

    input = input.replace(/String/gi, "var");
    /*gi , i -> als er string dus kleine letter staat => ook vervangen
     g -> overloopt heel de tekst en stopt niet bij de eerst gevonden "String"
     */
//    nieuwe replace => input = input.replace(,);

    input = input.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, "");
    input = input.replace(/\/\/.*/g, "");

//     remove all comments


    output = input;

}

function toonOutput() {
    var translatedFor;
    Vertaal(input);
    fors = findFors(output);
    for (var i = 0; i < fors.length; i++) {
        translatedFor = translateForLoop(fors[i]);

        output = output.replace(fors[i], translatedFor);
    }


    var theClass = findClass(output),
        length = 0;

    theClass = splitBodyInMethods(theClass);
    length = theClass.methods.length;

    output = "";
    for (var i = 0; i < length; i = i + 1) {
        console.log("Methode " + (i + 1) + " is: " + theClass.methods[i]);
        output += "Methode " + (i + 1) + " is: " + theClass.methods[i] + "\n";
    }

    translate.innerText = output;
}

function readFile(userData) {

    var file = userData.files[0],
        extension = file.name.split('.').pop(),
        requiredExtention = "java",
        reader;
//        test of de extentie ".java" is
    if (extension == requiredExtention) {
        reader = new FileReader();
        reader.onload = function () {
            input = reader.result;
            console.log("succesfully read file: \n" + input);
            originalFile.innerText = input;

        };

        reader.readAsText(file);

    }
    else {
        console.log("wrong file type");
    }


}

//a function called by the html buttons, to translate the
function downloadFile(extention) {

    const MIME_TYPE = "text/plain";
    console.log("typer" + typer);
    //creates a blob (file that can be filled with text)
    var bb = new Blob([typer.innerText], {type: MIME_TYPE});

    //creates a hyperlink
    var translatedFile = document.createElement('a');
    translatedFile.download = filename + extention;
    translatedFile.href = window.URL.createObjectURL(bb);
    translatedFile.textContent = "Download";
    translatedFile.dataset.downloadurl = [MIME_TYPE, translatedFile.download, translatedFile.href].join(':');

    outputButton.innerHTML = "";

    //adds the hyperlink to the button
    outputButton.appendChild(translatedFile);

    output.disabled = false;

}


//function to create a class object with three properties: the imports part, the declaration part and the body part
//uses a helper function: sliceClass to do the actual slicing in order to make the code DRYer
function findClass(theString) {
    var thisClass = { imports: "", declaration: "", body: "" };

    if (typeof theString == "string") {
        var firstPublic = theString.indexOf("public"),
            firstPrivate = theString.indexOf("private"),
            firstBracket = theString.indexOf("{"),
            lastBracket = theString.lastIndexOf("}");

        if (firstPublic < firstPrivate) {
            sliceClass(firstPublic, firstBracket, lastBracket, thisClass, theString);
        } else {
            sliceClass(firstPrivate, firstBracket, lastBracket, thisClass, theString);
        }
    }

    return thisClass;
}

//function to extract the imports, the declaration and the body from a stringified Java Class
function sliceClass(beginDeclaration, beginBody, endBody, thisClass, theString) {
    thisClass.imports = theString.slice(0, beginDeclaration);
    thisClass.declaration = theString.slice(beginDeclaration, beginBody);
    thisClass.body = theString.slice(beginBody + 1, endBody);

    for (var property in thisClass) {
        property.trim(); //remove all extraneous whitespace from the beginning and ending of the string
    }

    return thisClass;
}

//function om de body op te splitsen in methodes, blocks en variabelen
//uses the following helperfunctions: getIndices, searchMethodEnd
function splitBodyInMethods(theClass) {
    var indicesSemicolon = getIndices(/;/g, theClass.body),
        indicesOpenBrace = getIndices(/\{/g, theClass.body),
        indicesClosingBrace = getIndices(/\}/g, theClass.body),
        startIndex = indicesSemicolon.filter(function (item, index, array) {
            return (item < indicesOpenBrace[0]);
        }).reverse()[0] + 1,
        splitObject = searchMethodEnd(indicesOpenBrace, indicesClosingBrace),
        i = 0;

    theClass.methods = [];

    do {
        //add the method to theclass.methods, then trim extraneous whitespace
        theClass.methods[i] = theClass.body.slice(startIndex, splitObject.endIndex + 1);
        theClass.methods[i++].trim();

        //check to see if the last method has been found, if so: break from the loop by using a return statement
        if (splitObject.stop) {
            return theClass;
        }

        //set the startindex to the right one
        startIndex = splitObject.endIndex + 1;

        //find the next method, passing in the correct indicesArrays
        splitObject = searchMethodEnd(splitObject.indicesOpenBrace, splitObject.indicesClosingBrace);
    } while (true);
}

//function to search for the ending brace of a method
function searchMethodEnd(indicesOpenBrace, indicesClosingBrace) {
    var i = 0,
        open = 0,
        close = 0,
        compare = function (open, close) {
            if (open > close) {
                return true;
            }
        },
        returnObject = { stop: false };

    if (indicesOpenBrace.length === 1) {
        returnObject.endIndex = indicesClosingBrace[i];
        returnObject.stop = true;
        return returnObject;
    }

    do {
        if (compare(indicesOpenBrace[open], indicesClosingBrace[close])) {
            close += 1;
        } else {
            open += 1;
        }
    } while (open !== close);

    returnObject.endIndex = indicesClosingBrace[close - 1];
    returnObject.indicesOpenBrace = indicesOpenBrace.slice(open);
    returnObject.indicesClosingBrace = indicesClosingBrace.slice(close);
    return returnObject;
}


//helper vars for a function to search for the block statements in a method body
//    var indicesFor = getIndices(/\s*(for){1}\s*/g, theClass.body),
//        indicesWhile = getIndices(/\s*(while){1}\s*)/g, theClass.body),
//        indicesSwitch = getIndices(/\s*(switch){1}\s*/g, theClass.body),
//        indicesIf = getIndices(/\s*(if){1}\s*/g, theClass.body),
//        indicesElse = getIndices(/\s*(else){1}\s*/g, theClass.body),
//        indicesDoWhile = getIndices(/\s*(do){1}\s*/g, theClass.body);


//helperfunctie om een array met indices te vinden voor een bepaalde regex
function getIndices(regex, theString) {
    var result, indices = [];
    while ((result = regex.exec(theString))) {
        indices.push(result.index);
    }

    return indices;
}

//this function returns an array containing every (entire) string (from "for" to the last curly bracket) of all for-loops in "classstring"
function findFors(classString) {
    var lastCurlyPos, forStrings = [], curlyClosePos, startIndexes, curlyOpenPos;
    startIndexes = getIndices(/for\s*\(.*\)\s*\{/gm, classString);
//    this regex code is required to find all occurrences of "for" that are followed by brackets, including all whitespace possibilities
    for (var i = 0; i < startIndexes.length; i++) {
        curlyClosePos = getIndices(/\}/g, classString.substr(startIndexes[i], classString.length));
        for (var a in curlyClosePos) {
            a += startIndexes[i];
        }
        curlyOpenPos = getIndices(/\{/g, classString.substr(startIndexes[i], classString.length));
        lastCurlyPos = searchMethodEnd(curlyOpenPos, curlyClosePos).endIndex + 1;
        forStrings.push(classString.substr(startIndexes[i], lastCurlyPos));

    }

    return forStrings;


}

//tranlsate any for loop string
function translateForLoop(stringToTranslate) {
    var forIdentifierPart, translatedForLoop = "could not translate for loop", indexType;

    stringToTranslate = stringToTranslate.trim();

    stringToTranslate = stringToTranslate.replace(/[ ]{2,}/g, " ");
    //this replaces all occurrences of more than one whitespace by one whitespace.

    chooseCorrectLoopTypeFunction(stringToTranslate);

    function chooseCorrectLoopTypeFunction(unidentifiedForLoop) {
        forIdentifierPart = getForIdentifierPart(unidentifiedForLoop);

        if (forIdentifierPart.indexOf(";") != -1) {
            /*index of ";" is -1 if the string does not contain ";" -->  it's a regular for loop*/
            translateRegularFor(unidentifiedForLoop);
        }

        else {
            /*it's a foreach loop*/
            translateForeachFor(unidentifiedForLoop);
        }
        //if it's not a for loop, "could not translate for loop" will be returned

    }

    function getForIdentifierPart(forLoopString) {
        return forLoopString.split("{")[0];
        /*")" would also include a possible bracket from ".size()"*/
    }

    function translateForeachFor(foreachString) {
//        converts a java foreach loop into a javascript foreach loop
//        var hulp, varname;
//        console.log("foreachfor");
        indexType = getIndexType(foreachString);
        foreachString = foreachString.replace(indexType, "var");
        foreachString = foreachString.replace(":", " in ");
//        hulp = getForIdentifierPart(foreachString);
//        hulp = hulp.replace(")", "");
//        varname = hulp.split("<").pop();
//        definitely works for this because i just inserted a "<"
//        console.log(varname);

//        foreachString = foreachString.replace(varname, varname + ".length; i++");
//        console.log(foreachString);

        translatedForLoop = foreachString;
    }

    function translateRegularFor(regularForString) {
        var help, help2, triggerPart, firstSemicolonPos, secondSemicolonPos, newForIdentifierPart;

        indexType = getIndexType(regularForString);

        regularForString = regularForString.replace(indexType, "var");
//        console.log(regularForString);

        forIdentifierPart = getForIdentifierPart(regularForString);
        newForIdentifierPart = forIdentifierPart;

//        all code after this translates .Size() and .length() to ".length"
        firstSemicolonPos = newForIdentifierPart.indexOf(";");
        secondSemicolonPos = newForIdentifierPart.lastIndexOf(";");
//        returns first and second index of a semicolon
        triggerPart = newForIdentifierPart.split(";")[1];
//        the part between the 2 semicolons
        triggerPart = triggerPart.replace(/\s/g, "");
//        removes all whitespace in this part

        newForIdentifierPart = newForIdentifierPart.slice(0, firstSemicolonPos) + "; " + triggerPart + newForIdentifierPart.slice(secondSemicolonPos);
//        put it back between the semicolons

        regularForString = regularForString.replace(forIdentifierPart, newForIdentifierPart);
        help = newForIdentifierPart.toLowerCase().indexOf(".length()");
//        if the identifier contains .length() this will return the index, else it returns -1
        help2 = newForIdentifierPart.toLowerCase().indexOf(".size()");
//        if the identifier contains .size() this will return the index, else it returns -1
        if (help != -1) {
            regularForString = regularForString.replace(/\.length\(\)/i, ".length");
//            this is usually the same as .replace(".length()".....) but safer, (/i = ignoring case)
//            This is easier and safer than trying to make a part of the identifier toLowerCase because it may contain user-defined variables and methods
        } else if (help2 != -1) {
            regularForString = regularForString.replace(/\.size\(\)/i, ".length");
//            same reason as above
        }

        translatedForLoop = regularForString;
    }

    function getIndexType(string) {

        var help4, betweenBrackets;
//        returns the type of the iterator, usually "int"
        betweenBrackets = string.split("(")[1];
//        return the part between brackets
        help4 = betweenBrackets.trim();
//        removes whitespace
        indexType = help4.split(" ")[0];
//        first word
        return indexType;
    }


    return translatedForLoop;

}

//translate any array
function translateArray(arrayToTranslate) {
    var translatedArray = arrayToTranslate;

//    removes "new var"
    translatedArray = translatedArray.replace(/=\s*new\s*var/g, "= ");
//    removes []
    translatedArray = translatedArray.replace(/\[ *\]/g, "");
//    replaces { by [
    translatedArray = translatedArray.replace(/\{/g, "[");
//    replaces } by ]
    translatedArray = translatedArray.replace(/\}/g, "]");
//    replaces [\d] by []
    translatedArray = translatedArray.replace(/\[\s*\d\s*\]/g, "[]");
    return translatedArray;
}