/**
 * Created by niels on 2/01/15.
 */
function TranslateJsp(jspCode) {

    if(typeof jspCode == "string")
    {
        var codeToTranslate = jspCode;
        codeToTranslate = translateAllBeginTagStructures(codeToTranslate);
        codeToTranslate = translateNonChangeableTags(codeToTranslate);
        codeToTranslate = deleteOverflowingCode(codeToTranslate);
        //codeToTranslate = translateAllVariables(codeToTranslate);

        //returns the translated code
        output = codeToTranslate;
    }
    else
    {
        console.log("ERROR WHILE LOADING STRING");
        return null;
    }

    //FUNCTIONS

    //CHANGE
    function translateAllBeginTagStructures(code)
    {

        if(getNumberOfOccurrences('if', code) > 0)
        {
            code = translateBeginTagsIf(code);
            console.log('translated IF');
        }
        if(getNumberOfOccurrences('forEach', code) > 0)
        {
            code = translateBeginTagsForEach(code);
            console.log('translated FOREACH');

        }
        if(getNumberOfOccurrences('include', code) > 0)
        {
            code = translateInclude(code);
            console.log('translated INCLUDE');

        }
        if(getNumberOfOccurrences('jspInclude', code) > 0)
        {
            code = translateJspInclude(code);
            console.log('translated JSP-INCLUDE');

        }
        if(getNumberOfOccurrences('when', code) > 0)
        {
            code = translateWhen(code);
            console.log('translated WHEN');

        }
        return code;


        //FUNCTIONS
        function translateBeginTagsIf(code){

            //////////////////////////////////////////////////////
            var codeToTranslate = code,
                translatedCode = '',
                totalOccurrencesIf = getNumberOfOccurrences('if', codeToTranslate);

            while(totalOccurrencesIf !=0)
            {
                var beginIndex = getBeginIndexOfStructure('<c:if', codeToTranslate),
                    endIndex = getEndIndexOfStructure('if', codeToTranslate);
                var structure = getStructure(beginIndex, endIndex, codeToTranslate);
                //removes the 'useless' code from codeToTranslate to translatedCode
                codeToTranslate = moveUnusedCode(beginIndex, codeToTranslate);
                // adds the translated structure to translatedCode
                translatedCode += translateStructure('if', structure);
                //remove original if structure from codeToTranslate
                codeToTranslate = codeToTranslate.replace(structure, "");
                totalOccurrencesIf--;
            }
            appendCode();

            return translatedCode;
            //////////////////////////////////////////////////////


            //FUNCTIONS

            //appends the left-over code
            function appendCode()
            {
                if(codeToTranslate.length > 0)
                {
                    translatedCode += codeToTranslate;
                }
            }

            //moves "not to be translated"-code to translatedCode
            //removes "not to be translated"-code from codeToTranslate
            function moveUnusedCode(endIndex, code)
            {
                var removedCode = code.slice(0, endIndex);
                //move unused code to translatedCode
                translatedCode += removedCode;
                //remove the unused code from codeToTranslate
                code = code.replace(removedCode, "");
                return code;
            }
        }
        function translateBeginTagsForEach(code){

            //////////////////////////////////////////////////////
            //////////////////////////////////////////////////////
            var codeToTranslate = code,
                translatedCode = '',
                totalOccurrencesForeach = getNumberOfOccurrences('forEach', codeToTranslate);

            while(totalOccurrencesForeach !=0)
            {
                var beginIndexForEach = getBeginIndexOfStructure('<c:forEach', codeToTranslate),
                    endIndexForEach = getEndIndexOfStructure('forEach', codeToTranslate),
                    structure = getStructure(beginIndexForEach, endIndexForEach, codeToTranslate);
                //removes the 'useless' code from codeToTranslate to translatedCode
                codeToTranslate = moveUnusedCode(beginIndexForEach, codeToTranslate);
                //adds the translated structure to translatedCode
                translatedCode += translateStructure('forEach', structure);
                //remove original forEach structure from codeToTranslate
                codeToTranslate = codeToTranslate.replace(structure, "");
                totalOccurrencesForeach--;
            }
            appendCode();
            return translatedCode;


            //FUNCTIONS

            //appends the left over code
            function appendCode()
            {
                if(codeToTranslate.length != 0)
                {
                    translatedCode += codeToTranslate;
                }
            }

            //moves "not to be translated"-code to translatedCode
            //removes "not to be translated"-code from codeToTranslate
            function moveUnusedCode(endIndex, code)
            {
                var removedCode = code.slice(0, endIndex);
                //move unused code to translatedCode
                translatedCode += removedCode;
                //remove the unused code from codeToTranslate
                code = code.replace(removedCode, "");
                return code;
            }
        }
        function translateWhen(code){
            //////////////////////////////////////////////////////
            var codeToTranslate = code,
                translatedCode = '',
                totalOccurrencesForeach = getNumberOfOccurrences('when', codeToTranslate);

            while(totalOccurrencesForeach !=0)
            {
                var beginIndexWhen = getBeginIndexOfStructure('<c:when', codeToTranslate),
                    endIndexWhen = getEndIndexOfStructure('when', codeToTranslate),
                    structure = getStructure(beginIndexWhen, endIndexWhen, codeToTranslate);
                //removes the 'useless' code from codeToTranslate to translatedCode
                codeToTranslate = moveUnusedCode(beginIndexWhen, codeToTranslate);
                //adds the translated structure to translatedCode
                translatedCode += translateStructure('when', structure);
                //remove original forEach structure from codeToTranslate
                codeToTranslate = codeToTranslate.replace(structure, "");
                totalOccurrencesForeach--;
            }
            appendCode();
            return translatedCode;
            /////////////////////////////////////////////////////

            //FUNCTIONS

            //appends the left over code
            function appendCode()
            {
                if(codeToTranslate.length > 0)
                {
                    translatedCode += codeToTranslate;
                }
            }

            //moves "not to be translated"-code to translatedCode
            //removes "not to be translated"-code from codeToTranslate
            function moveUnusedCode(endIndex, code)
            {
                var removedCode = code.slice(0, endIndex);
                //move unused code to translatedCode
                translatedCode += removedCode;
                //remove the unused code from codeToTranslate
                code = code.replace(removedCode, "");
                return code;
            }
        }
        function translateInclude(code){

            var translatedCode = "",
                codeToTranslate = code,
                totalOccurrencesInclude = getNumberOfOccurrences('include', codeToTranslate);


            //while((codeToTranslate.indexOf("<%@ include")) >= 0)
            while(totalOccurrencesInclude !=0)
            {
                var beginIndex = getBeginIndexOfStructure('<%@', codeToTranslate),
                    endIndex = getEndIndexOfStructure('include', codeToTranslate);
                var structure = getStructure(beginIndex, endIndex, codeToTranslate);
                //removes the 'useless' code from codeToTranslate to translatedCode
                codeToTranslate = moveUnusedCode(beginIndex, codeToTranslate);
                // adds the translated structure to translatedCode
                translatedCode += translateStructure('include', structure);
                //remove original structure from codeToTranslate
                codeToTranslate = codeToTranslate.replace(structure, "");
                totalOccurrencesInclude--;
            }
            appendCode();
            return translatedCode;

            //FUNCTIONS

            //appends rest of the code to translatedCode
            function appendCode()
            {
                if(codeToTranslate.length > 0)
                {
                    translatedCode += codeToTranslate;
                }
            }

            function moveUnusedCode(endIndex, code)
            {
                var removedCode = code.slice(0, endIndex);
                //move unused code to translatedCode
                translatedCode += removedCode;
                //remove the unused code from codeToTranslate
                code = code.replace(removedCode, "");
                return code;
            }
        }
        function translateJspInclude(code){
            var translatedCode = "",
                codeToTranslate = code,
                totalOccurrencesJspInclude = getNumberOfOccurrences('jspInclude', codeToTranslate);


            //while((codeToTranslate.indexOf("<%@ include")) >= 0)
            while(totalOccurrencesJspInclude !=0)
            {
                var beginIndex = getBeginIndexOfStructure('<jsp:include', codeToTranslate),
                    endIndex = getEndIndexOfStructure('jspInclude', codeToTranslate);
                var structure = getStructure(beginIndex, endIndex, codeToTranslate);
                //removes the 'useless' code from codeToTranslate to translatedCode
                codeToTranslate = moveUnusedCode(beginIndex, codeToTranslate);
                // adds the translated structure to translatedCode
                translatedCode += translateStructure('jspInclude', structure);
                //remove original structure from codeToTranslate
                codeToTranslate = codeToTranslate.replace(structure, "");
                totalOccurrencesJspInclude--;
            }
            appendCode();
            return translatedCode;

            //FUNCTIONS

            //appends rest of the code to translatedCode
            function appendCode()
            {
                if(codeToTranslate.length > 0)
                {
                    translatedCode += codeToTranslate;
                }
            }

            function moveUnusedCode(endIndex, code)
            {
                var removedCode = code.slice(0, endIndex);
                //move unused code to translatedCode
                translatedCode += removedCode;
                //remove the unused code from codeToTranslate
                code = code.replace(removedCode, "");
                return code;
            }
        }
    }
//translates all endtags that never changes, no matter what the situation
//CODE is the complete .jsp-file
    function translateNonChangeableTags(code)
    {
        //END-TAGS
        //translates all </c:if>-endtags to <% } %>
        code = code.replace(/<\/c:if>/g, '<% } %>');
        //translates all </c:forEach>-endtags to <% } %>
        code = code.replace(/<\/c:forEach>/g, '<% }) %>');
        //CONVERT WHEN-endtag to IF-endtag
        code = code.replace( /<\/c:when>/g, '<% } %>' );
        //CONVERT <c:otherwise></c:otherwise> to ELSE-structure
        code = code.replace( /<c:otherwise>/g, '<% else{ %>' );
        code = code.replace( /<\/c:otherwise>/g, '<% } %>' );

        //OUT-VARIABLE
        //  first remove the escapeXml-tag
        code = code.replace(/escapeXml="true" /g, "");
        //  <c:out value="${ --> <%=
        code = code.replace(/<c:out value="\$\{/g, "<%= ");
        //  }" />  --> %>
        code = code.replace(/\}" \/>/g, "%>");

        return code;
    }
//STRUCTURE is the name of the structure we want to translate here
//CODE is the structure to apply the translation on
    function translateStructure(structureTag, code)
    {
        var translatedStructure;
        switch(structureTag)
        {
            case 'if': translatedStructure = returnTranslationIfStructure(code);
                return translatedStructure;
                break;
            case 'forEach': translatedStructure = returnTranslationForEachStructure(code);
                return translatedStructure;
                break;
            case 'include': translatedStructure = returnTranslationIncludeStructure(code);
                return translatedStructure;
                break;
            case 'jspInclude': translatedStructure = returnTranslationJspIncludeStructure(code);
                return translatedStructure;
                break;
            //choose-tags are removed, so we search for the first when end index
            case 'when': translatedStructure = returnTranslationWhenStructure(code);
                return translatedStructure;
                break;
            case 'variable': translatedStructure = returnTranslationWhenStructure(code);
                return translatedStructure;
                break;
            default : console.log("Unknown parameter; if, forEach, include, choose or variable expected!");
                break;
        }

        function returnTranslationJspIncludeStructure(structure)
        {
            structure = structure.replace('<jsp:include page="', '<% include ');
            structure = structure.replace('.jsp"', ' %');

            return structure;
        }

        //translates only the begin-tag
        function returnTranslationWhenStructure(whenStructure)
        {
            var beginIndexContent = whenStructure.indexOf("<c:when")+16;
            var endIndexContent = whenStructure.indexOf('}">', beginIndexContent);
            var innerContent = whenStructure.slice(beginIndexContent, endIndexContent);

            return "<% if(" + innerContent + "){ %>\n";
        }

        //Translates any forEachStructure
        function returnTranslationForEachStructure(forEachStructure)
        {

            //get the array-element
            var elements = getForEachVarItems(forEachStructure);
            //get the single element
            var element = getForEachVar(forEachStructure);

            var statusPresent = false;
            var varStatusElement;

            //get the element representing varStatus
            if(forEachStructure.indexOf("varStatus") >= 0)
            {
                varStatusElement = getForEachVarStatus(forEachStructure);
                //statusPresent is used in string newForEachStructure
                statusPresent = true;
            }

            //assembles a .forEach(function(){})-structure from scratch
            var newForEachStructure = "<% " + elements +".forEach(function(" + element
                + (statusPresent? ", ": "") + (statusPresent? varStatusElement : "") + "){ %>";

            return newForEachStructure;


            //FUNCTIONS


            //returns the variable representing varStatus
            function getForEachVarStatus(forEachStructure)
            {
                var beginIndex = forEachStructure.indexOf("varStatus") +11;
                var endIndex = forEachStructure.indexOf('">', beginIndex) ;

                return forEachStructure.slice(beginIndex, endIndex);
            }

            //returns the variable of FOREACH (element)
            function getForEachVar(forEachStructure)
            {
                var argumentsBegin = forEachStructure.indexOf("var") + 5;
                var argumentsEnd = forEachStructure.indexOf("items") - 2;
                var forEachVar = forEachStructure.slice(argumentsBegin, argumentsEnd);

                return forEachVar;
            }

            //returns the items-collection of FOREACH (elements)
            function getForEachVarItems(forEachStructure)
            {
                var argumentsBegin = forEachStructure.indexOf("items") + 9;
                var argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);
                var items = forEachStructure.slice(argumentsBegin, argumentsEnd);

                return items;
            }
        }

        //translates a any if-structure
        function returnTranslationIfStructure(structure)
        {
            var ifStructure = translateTags(structure);

            if(hasAttribute("length") | hasAttribute("empty") | hasAttribute("startsWith")){
                ifStructure = translateAttributes(ifStructure); }

            return ifStructure;

            function hasAttribute(code)
            {
                return (ifStructure.indexOf("" +code+ "") >= 0 ? true: false);
            }

            function translateTags(code)
            {
                //        ifStructure.replace(/\<c\:if test\=\"\$\{/g, '<% if(');
                code = code.replace(/<c:if/g, '<% if(');
                code = code.replace(/test\=\"\$\{/g, '');
                code = code.replace(/\}">/g, '){ %>');
                //translate </c:if>
                code = code.replace(/\<\/c:if>/g, '<% } %>');
                return code;
            }

            function translateAttributes(codenietvertaald)
            {
                var code = codenietvertaald;
                //if EMPTY is present
                if(hasAttribute("empty"))
                {
                    if(getNumberOfEmpty(code) > 1)
                    {
                        var part1 = "<% if((typeof ",
                            part2 = " == undefined && ",
                            part3 = " == '') && (typeof ",
                            part4 = " == undefined && ",
                            part5 = " == '') %>",
                            variable = getVariables(code);
                        return (part1+variable+part2+variable+part3+variable+part4+variable+part5);
                    }
                    else
                    {
                        if(hasAttribute("not empty") | hasAttribute("! empty"))
                        {
                            var part1 = "<% if(typeof ",
                                part2 = " != undefined && ",
                                part3 = " != '') %>",
                                variable = getVariables(code);

                            return (part1+variable+part2+variable+part3);
                        }
                        else
                        {
                            var part1 = "<% if(typeof ",
                                part2 = " == undefined && ",
                                part3 = " == '') %>",
                                variable = getVariables(code);
                            return (part1+variable+part2+variable+part3);
                        }
                    }
                }

                //if startsWith is present
                if(hasAttribute("startsWith"))
                {
                    code = code.replace(/fn\:startsWith\(/, '');
                    code = code.replace(/\, \'/, '.charAt(0)=="');
                    code = code.replace(/\'\)/, '"');
                    return code;
                }

                //if LENGTH is present
                if(hasAttribute("length"))
                {
                    code = code.replace(/fn\:length\(/g, '' );
                    code = code.replace(/\)/, '.length() ');
                    return code;
                }

                //returns the number of 'empty' present
                function getNumberOfEmpty(code)
                {
                    //count the number of "EMPTY"
                    var numberOfEmpty = code.match(/empty/g).length;
                    return numberOfEmpty;
                }

                //returns the variable present inside the IF
                function getVariables(code)
                {
                    var index0 = code.indexOf("empty") +6;
                    var index1 = code.indexOf(")", index0) ;
                    //return code.slice(index0, index1);
                    return code.slice(index0, index1);
                }

                //returns all miscellaneous code between begin- and endtag
                function getContent(code)
                {
                    var index0 = code.indexOf("%>") +2;
                    var index1 = code.indexOf("<%", index0);
                    return code.slice(index0, index1);
                }
            }

            //translates an includestructure
            function translateIncludeStructure(includeStructure)
            {
                // trim onnodige code
                //VOOR: <%@ include file="/views/....jsp" %>
                //NA:   <%  include ... %>
                //of    <% include admin/... %>

                includeStructure = includeStructure.replace('@', '');
                includeStructure = includeStructure.replace('file="/views/', '');
                includeStructure = includeStructure.replace('.jsp"', ' ');

                return includeStructure
            }
        }

        //translates an include structure
        function returnTranslationIncludeStructure(includeStructure)
        {
            // trim onnodige code
            //VOOR: <%@ include file="/views/....jsp" %>
            //NA:   <%  include ... %>
            //of    <% include admin/... %>

            includeStructure = includeStructure.replace('@', '');
            includeStructure = includeStructure.replace('file="/views/', '');
            includeStructure = includeStructure.replace('.jsp"', ' ');

            return includeStructure + "\n";
        }

        function returnTranslationVariable(variableStructure)
        {
            var variable = variableStructure;
            variable = variable.replace(/\$\{/, '<%= ');
            variable = variable.replace(/\}/, '%>');

            return variable;
        }
    }

    function translateAllVariables(code)
    {
        var translatedCode = "",
            codeToTranslate = code,
            totalOccurrencesVariables = getNumberOfOccurrences('variable', codeToTranslate);


        ////////////////////////////////////////
        //translate all occurrences of Out-variables
        translateOutVariables();

        //translate all occurrences of variables
        while(totalOccurrencesVariables !=0)
        {
            var beginIndexVar = getBeginIndexOfStructure("${", codeToTranslate),
                endIndexVar = getEndIndexOfStructure("${", codeToTranslate),
                variable = getStructure(beginIndexVar, endIndexVar, codeToTranslate);
            //removes the 'useless' code from codeToTranslate to translatedCode
            codeToTranslate = moveUnusedCode(0, beginIndexVar, codeToTranslate);
            //adds the translated structure to translatedCode
            translatedCode += translateVariable(variable);
            //translatedCode += translateStructure("variable", variable);
            //remove original if structure from codeToTranslate
            codeToTranslate = codeToTranslate.replace(variable, "");
            totalOccurrencesVariables--;
        }
        appendCode();
        return translatedCode;
        /////////////////////////////////////////////////////////

        //FUNCTIONS

        //appends the left over code
        function appendCode()
        {
            if(codeToTranslate.length > 0)
            {
                translatedCode += codeToTranslate;
            }
        }

        //returns the begin-index of the variable
        function getIndexBeginOfVariable(code)
        {
            //aangepast door QUNIT-test
            return code.indexOf("${");
        }

        //moves unused code to translatedCode
        function moveUnusedCode(beginIndex, endIndex, code)
        {
            var removedCode = code.slice(beginIndex, endIndex);
            //move unused code to translatedCode
            translatedCode += removedCode;
            //remove the unused code from codeToTranslate
            code = code.replace(removedCode, "");
            return code;
        }

        //returns the variable
        function getVariableStructure(code)
        {
            //zoeken naar ${
            var beginVar = code.indexOf("${");
            //zoeken naar de END-tag
            var endVar = code.indexOf("}", beginVar) + 1;
            var varStructure = code.slice(beginVar, endVar);

            return varStructure;
        }

        //translates the variable returned
        function translateVariable(variableStructure)
        {
            var variable = variableStructure;
            variable = variable.replace(/\$\{/, '<%= ');
            variable = variable.replace(/\}/, '%>');

            return variable;
        }

        //translates all Out-variables
        function translateOutVariables()
        {
            //  first remove the escapeXml-tag
            codeToTranslate = codeToTranslate.replace(/escapeXml="true" /g, "");
            //  <c:out value="${ --> <%=
            codeToTranslate = codeToTranslate.replace(/<c:out value="\$\{/g, "<%= ");
            //  }" />  --> %>
            codeToTranslate = codeToTranslate.replace(/\}" \/>/g, "%>");
        }
    }

    //DELETE
    function deleteOverflowingCode(code)
    {
        //delete choose-tags
        code = code.replace(/\<c\:choose\>/g, "");
        code = code.replace(/\<\/c\:choose\>/g, "");

        //delete all code starting with '<%@ page' or '<%@ taglib' and ends with '%>'.
        code = code.replace(/\<\%\@\spage.*\%\>/g, "");
        code = code.replace(/\<\%\@\staglib.*\%\>/g, "");

        //delete all comments (Tim)
        code = code.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, "");

        //removes newlines at the beginning of the code
        code = code.replace(/\r?\n|\r/, "");

        //delete left-over code from jsp:include
        //<jsp:param name="title" value="${page.title}"/>
        code = code.replace(/<jsp:param.*\/>/g,"");
        //</jsp:include>
        code = code.replace(/<\/jsp:include>/g, "");

        return code;
    }

    //SEARCH
    //returns the begin index of any structure defined in the function
    function getBeginIndexOfStructure(structure, code)
    {
        //STRUCTURE
        //when  : <c:when
        //if    : <c:if
        //include : <%@
        //jspInclude: <jsp:include
        //foreach : <c:forEach
        //variables : ${

        return code.indexOf(""+structure+"");
    }

//returns the end index of any structured defined in the function
    function getEndIndexOfStructure(structureTag, code)
    {
        var endIndexOfStructure;
        switch(structureTag)
        {
            case 'if':          return endIndexOfStructure = returnEndIndexOfIFStructure(code);
                break;
            case 'forEach':     return endIndexOfStructure = returnEndIndexOfForEachStructure(code);
                break;
            case 'include':     return endIndexOfStructure = returnEndIndexOfIncludeStructure(code);
                break;
            case 'jspInclude':  return endIndexOfStructure = returnEndIndexOfJspIncludeStructure(code);
                break;
            case 'when':        return endIndexOfStructure = returnEndIndexOfWhenStructure(code);
                break;
            case 'variable':    return endIndexOfStructure = returnEndIndexVariableStructure(code);
                break;
        }

        //FUNCTIONS
        function returnEndIndexOfJspIncludeStructure(code)
        {
            var beginIndex = code.indexOf('<jsp:include');
            return code.indexOf('>', beginIndex)+1;
        }

        function returnEndIndexOfWhenStructure(code)
        {
            var beginIndex = code.indexOf("<c:when");
            return (code.indexOf('">', beginIndex)) +2;
        }

        function returnEndIndexOfForEachStructure(code)
        {
            //return (code.indexOf("</c:forEach")+12);
            var beginIndex = code.indexOf('<c:forEach'),
                endIndex = code.indexOf('}">', beginIndex)+3;

            return endIndex;
        }

        function returnEndIndexOfIFStructure(code)
        {
            var beginIndex = code.indexOf('<c:if'),
                endIndex = code.indexOf('}">', beginIndex)+3;

            return endIndex;
        }

        function returnEndIndexOfIncludeStructure(code)
        {
            var beginInclude = code.indexOf("<%@ include");
            return code.indexOf('.jsp"%>', beginInclude) +7;
        }

        function returnEndIndexVariableStructure(code)
        {
            var beginVar = code.indexOf("${");
            return code.indexOf("}", beginVar) + 1;
        }
    }

//returns the structure from beginIndex till endIndex
    function getStructure(beginIndex, endIndex, code)
    {
        return code.slice(beginIndex, endIndex);
    }

    //returns the number of occurrences of structure in code
    function getNumberOfOccurrences(structure, code)
    {
        switch(structure)
        {
            case 'if': if(code.match(/<c:if/g) != null)
            {
                return code.match(/<c:if/g).length;
            }
            else
            {
                return 0;
            }
                break;
            case 'forEach': if(code.match(/<c:forEach/g) != null)
            {
                return code.match(/<c:forEach/g).length;
            }
            else
            {
                return 0;
            }
                break;
            case 'include': if(code.match(/<%@ include/g) != null)
            {
                return code.match(/<%@ include/g).length;
            }
            else
            {
                return 0;
            }
                break;
            case 'jspInclude': if(code.match(/<jsp:include/g) != null)
            {
                return code.match(/<jsp:include/g).length;
            }
            else
            {
                return 0;
            }
                break;
            //choose-tags are removed, so we search for the first when end index
            case 'when': if(code.match(/<c:when/g) != null)
            {
                return code.match(/<c:when/g).length;
            }
            else
            {
                return 0;
            }
                break;
            case 'variable': if(code.match(/\$\{/g) != null)
            {
                return code.match(/\$\{/g).length;
            }
            else
            {
                return 0;
            }
                break;
        }
    }
}