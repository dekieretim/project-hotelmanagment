/**
 * Created by Badlapje on 8/10/14.
 */

//helper function to search for the ending brace of a method
exports.searchMethodEnd = function searchMethodEnd(indicesOpenBrace, indicesClosingBrace){
    var open = 0,
        close = 0,
        openLength = indicesOpenBrace.length,
        closeLength = indicesClosingBrace.length,
        returnObject = { stop: false };

    if (indicesOpenBrace.length === 1) {
        returnObject.endIndex = indicesClosingBrace[0];
        returnObject.stop = true;
        return returnObject;
    }

    do {
        if (indicesOpenBrace[open] > indicesClosingBrace[close]) {
            if (close + 1 < closeLength) {
                close++;
            }
        } else if (open + 1 == openLength) { //if last method
            returnObject.endIndex = indicesClosingBrace[closeLength - 1];
            returnObject.stop = true;
            return returnObject;
        } else {//if open is smaller then close
            if (open + 1 < openLength) {
                open++;
            }
        }
    } while (open !== close);

    returnObject.endIndex = indicesClosingBrace[close - 1];
    returnObject.indicesOpenBrace = indicesOpenBrace.slice(open);
    returnObject.indicesClosingBrace = indicesClosingBrace.slice(close);

    return returnObject;
};

//helper function to get an array of indices containing all indexes where a regex matches a certain string
exports.getIndices = function getIndices(regex, theString){
    var result, indices = [];
    while ((result = regex.exec(theString))) {
        indices.push(result.index);
    }

    return indices;
};



