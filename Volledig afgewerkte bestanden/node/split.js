/**
 * Created by Badlapje on 8/10/14.
 */

var theClass,
    search = require('./search');

//function to extract the imports, the declaration and the body from a stringified Java Class
function sliceClass(beginDeclaration, beginBody, endBody, javaString){
    theClass.imports = javaString.slice(0, beginDeclaration).trim();
    theClass.declaration = javaString.slice(beginDeclaration, beginBody).trim();
    theClass.body = javaString.slice(beginBody + 1, endBody).trim();

    return theClass;
}





//function to create a class object with three properties: the imports part, the declaration part and the body part
//uses the helper function sliceClass
exports.splitClass = function findClass(javaString, classObject){
    var firstPublic = javaString.indexOf("public"),
        firstPrivate = javaString.indexOf("private"),
        firstBracket = javaString.indexOf("{"),
        lastBracket = javaString.lastIndexOf("}");

    theClass = classObject;

    if (firstPublic < firstPrivate) {
        return sliceClass(firstPublic, firstBracket, lastBracket, javaString);
    } else {
       return  sliceClass(firstPrivate, firstBracket, lastBracket, javaString);
    }
};

//function to split the body in methods, declaration (which also get's processed) and variables
//uses the following helperfunctions: getIndices, searchMethodEnd, processFields and processMethod
exports.splitBodyInMethods = function splitBodyInMethods(classObject){
    theClass = classObject;
    var indicesSemicolon = search.getIndices(/;/g, theClass.body),
        indicesOpenBrace = search.getIndices(/\{/g, theClass.body),
        indicesClosingBrace = search.getIndices(/\}/g, theClass.body),
        startIndex = indicesSemicolon.filter(function (item, index, array) {
            return (item < indicesOpenBrace[0]);
        }).reverse()[0] + 1,
        splitObject = search.searchMethodEnd(indicesOpenBrace, indicesClosingBrace),
        i = 0;

    do {
        //if it's the first method: add the field declarations to the class
        if (!("fieldDeclarations" in theClass)) {
            theClass.fieldDeclarations = theClass.body.slice(0, startIndex).trim();
        }

        //add the method to theclass.methods, then trim extraneous whitespace
        theClass.methods[i++] = theClass.body.slice(startIndex, splitObject.endIndex + 1).trim();

        //check to see if the last method has been found, if so: break from the loop by using a return statement
        if (splitObject.stop) {
            return theClass;
        }

        //set the startindex to the right one
        startIndex = splitObject.endIndex + 1;

        //find the next method, passing in the correct indicesArrays
        splitObject = search.searchMethodEnd(splitObject.indicesOpenBrace, splitObject.indicesClosingBrace);
    } while (true); //used an infinite loop here because inside the loop there's a check which can trigger a return statement & which should eventually be triggered
};