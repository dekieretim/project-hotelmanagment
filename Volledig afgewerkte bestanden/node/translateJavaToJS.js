/**
 * Created by Badlapje on 8/10/14.
 */
var split = require('./split'),
    change = require('./change'),
    theClass = {
        imports: "",
        declaration: "",
        body: "",
        isStatic: false,
        isPrivate: false,
        final: false,
        superclass: false,
        interfaces: false,
        methodData: [],
        methods: [],
        parameters: "" ,
        publicVarMethods: "",
        privateStatic: "",
        forInnerConstructor: "",
        constructor: []
    };

//function to extend the prototype if necessary
//helperfunction for createConstructor
function extendPrototype(constructorName, superclass){
    return "Inner" + constructorName + ".prototype = Object.create(" + superclass + "*.prototype, {\r\n\t" +
        "constructor: {\r\n\t" +
        "configurable: true,\r\n" +
        "enumerable: true, \r\n" +
        "value: Inner" + constructorName + ",\r\n" +
        "writable: true\r\n" +
        "}\r\n";
}

//function to create a constructor in Java
//uses the helper function defineInnerConstructor
function createConstructor(){
    var constructorName = theClass.constructorName,
        constructorString = "var " + constructorName + " = " + "(function(){\r\n\t"; //open declaration

    constructorString += theClass.privateStatic + "\r\n\r\n"; //voeg alle private & static variables toe
    constructorString += defineInnerConstructor(constructorName, theClass.parameters); //voeg Inner Constructor toe met private vars/methods en constructorinhoud

    if (theClass.superclass) {
        constructorString += extendPrototype(constructorName, theClass.superclass) + "});";
    } //indien superklasse definieer het prototype als de superklasse

    constructorString += theClass.publicVarMethods + "\r\n"; //voeg public vars & methods toe
    return constructorString + "})();";
}

//function to enforce the use of new when calling a constructor (seperate function to make it more easily legible)
//helper function for defineInnerConstructor, uses enforceNew as a helper function
function enforceNew(constructorName, parameters){
    return "if(!(this instanceof " + constructorName + ")){ " +
        "\r\n\t\t" + "return new " + constructorName + "(" + (parameters ? parameters : "") + ");\r\n\t }" +
        "\r\n\r\n";
}

//function to define the inner constructor
//helper function for createConstructor
function defineInnerConstructor(constructorName, parameters){
    var constructorData = "",
        length = theClass.constructor.length;
    if (length >= 1) {
        constructorData = "\t" + theClass.constructor[0].bodyProper + "\r\n";
        for (var i = 0; i < length; i++) {
            if (theClass.constructor[i].length > theClass.constructor[i - 1]) {
                constructorData = "\t" + theClass.constructor[i].bodyProper + "\r\n";
            }
        }
    }
    return "function Inner" + constructorName + "(" + theClass.parameters + "){\r\n\t" + enforceNew(constructorName, parameters) + constructorData + theClass.forInnerConstructor + "}\r\n\r\n";
}

exports.translateJavaToJS = function(javaString){
    if (typeof javaString == "string") {
        var stringBeingTranslated = change.removeComments(javaString, theClass);

        theClass = split.splitClass(javaString, theClass);
        theClass = change.processClassDeclaration(theClass);
        theClass = split.splitBodyInMethods(theClass);
        theClass = change.processMethods(theClass);
        theClass = change.processFields(theClass);
        return createConstructor();
    }
};