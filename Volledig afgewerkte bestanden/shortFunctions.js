/**
 * Created by Tim on 24/04/2014.
 */



function IIFEfy(stringToChange) {
    var changedString;
    changedString = "function(){\n" + stringToChange + "\n}();";
    return changedString;
}

function isPrimitiveType(variableToCheck) {
    var identifier;
    identifier = getFirstWord(variableToCheck);
    if (identifier === "byte" || identifier === "short" || identifier === "int" || identifier === "long" || identifier === "float" || identifier === "double" || identifier === "boolean" || identifier === "char" || identifier === "String")
        return true;
    else
        return false;
}

function getFirstWord(longString) {
    return longString.trim().split(" ")[0];
}

function replaceFirstByVar(stringToChange) {
    return stringToChange.replace(getFirstWord(stringToChange), "var");

}


