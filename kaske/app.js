var express = require('express');
var http = require('http');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes');
var paniek = require('./routes/paniek');
var hart = require('./routes/hart');
var location = require('./routes/location');
var locHist = require('./routes/locHist');
var tel = require('./routes/tel');
var user = require('./routes/user');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', routes.index);

/****REST SERVICES****/
//url voor userinfo
app.get('/user', user.list);
//url voor locatie
app.get('/loc', location.list);
app.get('/location', location.list);
//url voor locatiegeschiedenis
app.get('/lochist', locHist.list);
app.get('/locationhistory', locHist.list);
//url voor heartrate-info
app.get('/hart', hart.list);
app.get('/heart', hart.list);
//url voor telefoongegevens
app.get('/tel', tel.list);
app.get('/telefoon', tel.list);
app.get('/telephone', tel.list);
//url voor indrukken paniekbutton
app.get('/paniek', paniek.help);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.render('error', {
        message: err.message,
        error: {}
    });
});


if (!module.parent) {
    app.listen(8765);
    console.log('EJS Demo server started on port 8765');
}
