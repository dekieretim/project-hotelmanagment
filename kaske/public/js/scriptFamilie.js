var data, gDebug, latitude, longitude, chart, view, map, locatie, formattedAdres;

$(document).ready(init);

function init() {
//    $(function(){
//        $('a[title]').tooltip();
//    });
    google.load("visualization", "1.0",
        { packages: ["corechart", "table"],
            callback: doChartStuff
        });
}
//            data.addRows([[naam, number]]);
//            makeChart();
//            makeTable();

function doChartStuff() {
    makeData();
    makeChart();
    makeTable();
    getLocation();
}

function makeData() {
    data = new google.visualization.DataTable();
    data.addColumn('string', 'tijd');
    data.addColumn('number', 'hearthRate');
    data.addColumn('string', 'location');

    data.addRows([
        ['12u05', 90, formattedAdres],
        ['12u10', 84, formattedAdres],
        ['12u15', 97, formattedAdres],
        ['12u20', 73, formattedAdres]
    ]);
}


function makeChart() {
    chart = new google.visualization.LineChart(document.getElementById('chart'));
    view = new google.visualization.DataView(data);

    view.setColumns([0, 1]);
    chart.draw(view, { width: 450, height: 240, title: '', vAxis: {minValue: 0}});
}

function makeTable() {
    var table1 = new google.visualization.Table(document.getElementById("table1"));
    table1.draw(data, { width: 450 });
    var table2 = new google.visualization.Table(document.getElementById("table2"));
    table2.draw(data, { width: 450 });

}

function toonFormattedAdres(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
            map.setZoom(11);
            marker = new google.maps.Marker({
                position: locatie,
                map: map
            });
            formattedAdres = results[1].formatted_address;
        }
    } else {
        alert("Geocoder failed due to: " + status);
    }
    doChartStuff();

}
function callback() {
    locatie = new google.maps.LatLng(latitude, longitude);
    console.log(latitude, " ", longitude);
    var geocoder = new google.maps.Geocoder();


    map = new google.maps.Map($("#kaartje").get(0),
        { zoom: 15, center: locatie, mapTypeId: google.maps.MapTypeId.ROADMAP});

    geocoder.geocode({'latLng': locatie}, toonFormattedAdres);
    var marker = new google.maps.Marker({ position: new google.maps.LatLng(latitude, longitude), title: "locatie "});
    marker.setMap(map);

    google.maps.event.addListener(marker, 'click', function (e) {
        gDebug = e;
    });

}

function showLocation(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;

    //alert("Latitude : " + latitude + " Longitude: " + longitude);
    callback();
}

function errorHandler(err) {
    if (err.code == 1) {
        alert("Error: Access is denied!");
    } else if (err.code == 2) {
        alert("Error: Position is unavailable!");
    }
}
function getLocation() {

    if (navigator.geolocation) {
        // timeout at 60000 milliseconds (60 seconds)
        console.log("gelukt");
        var options = {timeout: 60000};
        navigator.geolocation.getCurrentPosition(showLocation,
            errorHandler,
            options);
    } else {
        alert("Sorry, browser does not support geolocation!");
    }
}