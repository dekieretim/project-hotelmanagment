/* GET current location or SET current location. */
exports.list = function(req, res){
    res.send('respond with a resource');
};

function sendLocation(lati, longi){

        var payload =
        {
            "latitude" : lati,
            "longitude" : longi
        };

        var options =
        {
            "method" : "post",
            "payload" : payload
        };

        UrlFetchApp.fetch("https://console.developers.google.com/project/hackthefuture-team5/compute/instancesDetail/zones/asia-east1-c/instances/team54koentim", options);

}

exports.sendLocation = function(lat, long) {
    sendLocation(lat, long);
};