/**
 * Created by Tim on 11/12/2014.
 */
var client = require("twilio")("AC5e61fa5ad36dced823d68b0acabb8467", "e17371e1c2155fa3cc0d853382e2aaef");

function paniek() {
    client.sendMessage({

        to: '+32484799314', // Any number Twilio can deliver to
        from: '(650) 257-9220', // A number you bought from Twilio and can use for outbound communication
        body: 'Help! ik ben gevallen.' // body of the SMS message

    }, function (err, responseData) { //this function is executed when a response is received from Twilio

        if (!err) { // "err" is an error received during the request, if any

            // "responseData" is a JavaScript object containing data received from Twilio.
            // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
            // http://www.twilio.com/docs/api/rest/sending-sms#example-1

            console.log(responseData.from); // outputs "+14506667788"
            console.log(responseData.body); // outputs "word to your mother."
        }
    });
}

function paniekerig(telefoonnummer) {
    client.sendMessage({

        to: telefoonnummer, // Any number Twilio can deliver to
        from: '(650) 257-9220', // A number you bought from Twilio and can use for outbound communication
        body: 'Help! ik ben gevallen.' // body of the SMS message

    }, function (err, responseData) { //this function is executed when a response is received from Twilio

        if (!err) { // "err" is an error received during the request, if any

            // "responseData" is a JavaScript object containing data received from Twilio.
            // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
            // http://www.twilio.com/docs/api/rest/sending-sms#example-1

            console.log(responseData.from); // outputs "+14506667788"
            console.log(responseData.body); // outputs "word to your mother."
        }
    });
}

exports.help = function (req,res) {
    paniek();
    tel = '+' + req.query.nummer;
    paniekerig(tel);
};
