/**
 * Created by Badlapje on 8/10/14.
 */

var theClass;

//function to uppercase the first char of a class name
function createConstructorName(){
    var firstChar = theClass.className.charAt(0);
    theClass.constructorName = theClass.className.replace(firstChar, firstChar.toUpperCase());
}

//function to clear out newlines and (in the future: multi-line declarations)
function prepareFields(splitByLine){
    var blankLines = 0,
        length = splitByLine.length;

    for (var i = 0; i < length; i++) {
        var theString = splitByLine[i].trim(),
            semicolonIndices = getIndices(/;/g, theString),
            equalIndices = getIndices(/=/g, theString);
        if (theString.length) { //als het geen lege string is (dus gewoon lijn whitespace, want alle niet enter/newline whitespace wordt getrimt in de declaratie van deze var)
            if (equalIndices.length === 0) { //als er geen gelijk aan in staat
                splitByLine[i - blankLines++ - 1].content += theString;
            } else splitByLine[i - blankLines] = { content: theString, semiColons: semicolonIndices, equals: equalIndices }; //als dat wel het geval is
        } else blankLines += 1; // als het wel een lege lijn is
    }//TODO multiline declaraties waarbij er een = in de declaratie staat ergens die worden dus niet correct geparsed.  Idem voor multiline declaraties van strings via concatenatie met +  Moet in de documentatie terechtkomen.

    splitByLine.splice((length - blankLines), blankLines); //verwijder de lege lijnen

    return splitByLine;
}

//function to write values to the constructorString
//uses the helperfunction defineField
function writeToConstructor(total, value, name, isPrivate, isStatic, final){
    if (isPrivate&& isStatic) { //private static
        theClass.privateStatic += total + "\r\n\r\n"; //! geen speciale behandeling voor final aangezien private & static en op zich niet te wijzigen
    } else if (isPrivate) { //private
        theClass.forInnerConstructor += total + "\r\n\r\n";
    } else if (final) { //public final
        theClass.publicVarMethods += defineField("this", name, value, false, true, true);//TODO probleem hier uitzoeken: welke param staat hier te veel of verkeerd?!!!
    } else { //public
        theClass.publicVarMethods += "Inner" + theClass.constructorName + ".prototype." + name + " = " + value + "\r\n\r\n";
    }
}

//function to define a string for defining fields on a property, defines default values in case you don't want to specify write, configure or enumerate (in that order)
//obj and val parameters are mandatory
//in a seperate function to make it more legible
function defineField(obj, name, val, write, configure, enumerate){
    return "Object.defineProperty(" + obj + ", '" + name + "', {\r\n\t" +
        "value: " + val + ",\r\n\t" +
        "enumerable: " + (enumerate ? enumerate : true) + ",\r\n\t" +
        "configurable: " + (configure ? configure : true) + ",\r\n\t" +
        "writable: " + (write ? write : true) + "\r\n\t" +
        "});\r\n\r\n";
}

//function to process ConstructorData
function processConstructorData(methodData, index, startBodyProper){
    methodData.bodyProper = theClass.methods[index].slice(startBodyProper).trim();
    methodData.bodyProper = methodData.bodyProper.slice(1);
    methodData.bodyProper = methodData.bodyProper.slice(0, -1).trim();
    theClass.constructor.push(methodData);
}

//helper function to fully process a method: change variables, change params, change declaration
//uses helper functions changeMethodVariables, findMethodVariables, processConstructorData & writeToConstructor
function processMethod(element, index, array){
    var endParams = theClass.methods[index].indexOf(")"),
        startParams = theClass.methods[index].indexOf("("),
        startBodyProper = theClass.methods[index].indexOf("{") - 1,
        declarationBeforeParams,
        params = "",
        i,
        methodData = {
            methodName: "",
            newParams: "",
            bodyProper: "",
            isPrivate: false,
            isStatic: false,
            final: false
        };

    //process parameters
    if ((endParams - startParams) > 1) {
        params = theClass.methods[index].slice(startParams + 1, endParams);
        params = params.trim();
        params = params.split(",");
        for (i = 0; i < params.length; i++) {
            params[i] = params[i].trim().split(" ");
            methodData.newParams += params[i][1].trim() + ", ";
        }
        methodData.newParams = methodData.newParams.slice(0, -2);
    }

    //process declaration
    declarationBeforeParams = theClass.methods[index].slice(0, startParams);
    declarationBeforeParams = declarationBeforeParams.trim().split(" ");
    methodData.methodName = declarationBeforeParams[declarationBeforeParams.length - 1].trim();

    for (i = 0; i < declarationBeforeParams.length; i++) {
        switch (declarationBeforeParams[i]) {
            //public, protected, returntype (any), abstract hebben geen nut in javascript: zijn dus betekenisloos.  Multithreading verloopt op een volkomen
            //andere manier, waardoor het ook niet nuttig is om daar iets mee te doen voorlopig => zou een onderzoeksproject zijn op zich
            //TODO: nog te bekijken: native, strictfp, transient, volatile
            case "private":
                methodData.isPrivate= true;
                break;
            case "static":
                methodData.isStatic = true;
                break;
            case "final":
                methodData.final = true;
                break;
            default:
                break;
        }
    }

    //process the body variables
    if (methodData.methodName === theClass.constructorName) {
        processConstructorData(methodData, index, startBodyProper);
        return theClass;
    } else {
        methodData.bodyProper = theClass.methods[index].slice(startBodyProper).trim();
    }

    methodData.bodyProper = changeMethodVariables(findMethodVariables(methodData.bodyProper), methodData.bodyProper);
    theClass.methods[index] = "function " + methodData.methodName + "(" + methodData.newParams + ")" + methodData.bodyProper;
    theClass.methodData = [];
    theClass.methodData[index] = methodData;
    writeToConstructor(theClass.methods[index], methodData.bodyProper, methodData.methodName, methodData.isPrivate, methodData.isStatic, methodData.final);
}

//helper function for processMethod to change the type in a variable declaration to var
function changeMethodVariables(indices, method){
    var i = 0, //variable to increase the performance of the for loop
        length = indices.length, //variable to increase the performance of the for loop
        counter = 0, //variable to offset the startindex with
        partFirst = "", //variable to store the first part of the method
        partFrom = "", //variable to store the part of the method in starting at the variabele declaration till the end of the method
        type = ""; //variable to store the type of the variable for easy reference in two statements

    for (i; i < length; i++) {
        partFirst = method.slice(0, indices[i] - counter); //store the first part of the string for easy concatenation
        partFrom = method.slice(indices[i] - counter); //slice out the part of the string beginning at the variable declaration
        type = partFrom.slice(0, partFrom.search(/\s/)); //store the type of the variable for easy use in the next two statements
        partFrom = partFrom.replace(type, "var");//replace the type by var
        counter += type.length - 3; //augment counter to offset replacing the type by var;
        method = partFirst + partFrom; //reassemble method so it's usable again for the next variable
    }

    return method;
}

//function to get an array of indexes of where in a method variables are declared
function findMethodVariables(method){
    return getIndices(/\b(?!(return|new|package|import|where|set|and|on))[\w\[\]<>]+ [\w]+(;| *=)/g, method);
}



//function to remove comments in the code
exports.removeComments = function removeComments(input){
    input = input.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, ""); //remove multiline comments
    return input.replace(/\/\/.*/g, ""); //remove single line comments
};

//function to process the class declaration
exports.processClassDeclaration = function processClassDeclaration(classObject){
    theClass = classObject;
    var declaration = theClass.declaration,
        implement = theClass.declaration.indexOf("implements"),
        extend = theClass.declaration.indexOf("extends"),
        extendDeclaration,
        implementDeclaration,
        i;

    //process extends part of the declaration
    if (extend > 0) {
        extendDeclaration = (extend > implement) ? declaration.slice(extend).trim() : declaration.slice(extend, implement).trim();
        extendDeclaration = extendDeclaration.split(" ");
        theClass.superclass = extendDeclaration[1];
    }

    //process implements
    if (implement > 0) {
        theClass.interfaces = [];
        implementDeclaration = declaration.slice(implement).trim().split(",");
        for (i = 0; i < implementDeclaration.length; i++) {
            implementDeclaration[i] = implementDeclaration[i].split(" ");
            theClass.interfaces[i] = implementDeclaration[i][1].trim();
        }
    }

    //split declaration into an array of words
    if (extend > 0) {
        declaration = declaration.slice(0, extend).trim().split(" ");
    } else if (implement > 0) {
        declaration = declaration.slice(0, implement).trim().split(" ");
    } else {
        declaration = declaration.trim().split(" ");
    }

    //process modifiers & assign className + constructorName
    theClass.className = declaration[declaration.length - 1].trim();
    createConstructorName();
    for (i = 0; i < declaration.length; i++) {
        switch (declaration[i]) {
            //public, protected, abstract zijn onbestaand in javascript: zijn dus betekenisloos.
            case "private":
                theClass.isPrivate= true;
                break; //enkel voor inner classes
            case "static":
                theClass.isStatic = true;
                break; //enkel voor inner classes
            case "final":
                theClass.final = true;
                break;  //cannot be subclassed
            default:
                break;
        }
    }

    return theClass;
};

//function to process field declarations
//uses two inline helper methods and also writeToConstructor + prepareFields
exports.processFields = function processFields(classObject){
    theClass = classObject; //TODO kijken of dit nodig is, in principe wordt theClass by reference doorgegeven dus nakijken of het wel nodig is om dit te doen
    var splitByLine = prepareFields(theClass.fieldDeclarations.split("\r\n"));

    function handleFields(element, index, array){
        var firstEquals = element.content.indexOf("="),
            declaration = element.content.slice(0, firstEquals).trim(),
            declarationObject = {
                value: element.content.slice(firstEquals + 1).trim(),
                type: "",
                name: "",
                isPrivate: false,
                isStatic: false,
                final: false
            };

        function checkDeclarationModifiers(element, index, array){
            switch(element){
                //public, protected, abstract zijn onbestaand in javascript: zijn dus betekenisloos.
                case "private":
                    declarationObject.isPrivate= true;
                    break;
                case "static":
                    declarationObject.isStatic = true;
                    break;
                case "final":
                    declarationObject.name.toUpperCase();
                    declarationObject.final = true;
                    break;
                default:
                    break;
            }
        }//TODO refactoren samen met gelijkaardige code zodat dit maar 1 keer voorkomt in de code

        declaration = declaration.split(" ");
        declarationObject.name = declaration[declaration.length - 1];
        declarationObject.type = declaration[declaration.length - 2];
        declaration.forEach(checkDeclarationModifiers);
        //TODO inbouwen verwerking typesoort!
        declaration = "var " + declarationObject.name + " = " + declarationObject.value;
        writeToConstructor(declaration, declarationObject.value, declarationObject.name, declarationObject.isPrivate, declarationObject.isStatic, declarationObject.final);
    }

    splitByLine.forEach(handleFields);
    // TODO indien array/double/long/float/decimal: verwerken met Tim's functies



    return theClass;
};

//function to enumerate over all methods of a class and then process each in turn
exports.processMethods = function processMethods(classObject){
    theClass = classObject;
    theClass.methods.forEach(processMethod);
};