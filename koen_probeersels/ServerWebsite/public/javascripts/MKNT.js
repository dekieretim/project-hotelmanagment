/**
 * This app is in vanilla js and has no dependencies.  The app is the product of following authors:
 * Michèlle Carette, Koen Cornelis, Niels Cuylle, Tim Dekiere
 * The name of the app is the first letter of the firstname of each author.
 * version 0.9
 */
var MKNT = (function(){
    var theClass = { imports: "", declaration: "", body: "", static: false, private: false, final: false, superclass: false, interfaces: false, methodData: [], methods: [], parameters: "" };

    //function to create a class object with three properties: the imports part, the declaration part and the body part
    //uses the helper function sliceClass
    function findClass(javaString){
        if (typeof javaString == "string") {
            var firstPublic = javaString.indexOf("public"),
                firstPrivate = javaString.indexOf("private"),
                firstBracket = javaString.indexOf("{"),
                lastBracket = javaString.lastIndexOf("}");

            if (firstPublic < firstPrivate) {
                sliceClass(firstPublic, firstBracket, lastBracket, javaString);
            } else {
                sliceClass(firstPrivate, firstBracket, lastBracket, javaString);
            }
        }
    }

    //function to extract the imports, the declaration and the body from a stringified Java Class
    function sliceClass(beginDeclaration, beginBody, endBody, javaString){
        //slice up javastring into three distinct parts
        theClass.imports = javaString.slice(0, beginDeclaration).trim();
        theClass.declaration = javaString.slice(beginDeclaration, beginBody).trim();
        theClass.body = javaString.slice(beginBody + 1, endBody).trim();
    }

    //function to split the body in methods, declaration and variables
    //uses the following helperfunctions: getIndices, searchMethodEnd
    function splitBodyInMethods(){
        var indicesSemicolon = getIndices(/;/g, theClass.body),
            indicesOpenBrace = getIndices(/\{/g, theClass.body),
            indicesClosingBrace = getIndices(/\}/g, theClass.body),
            startIndex = indicesSemicolon.filter(function(item, index, array){
                return (item < indicesOpenBrace[0]);
            }).reverse()[0] + 1,
            splitObject = searchMethodEnd(indicesOpenBrace, indicesClosingBrace),
            i = 0;

        do{
            //if it's the first method: add the field declarations to the class
            if(!("fieldDeclarations" in theClass)){
                theClass.fieldDeclarations = theClass.body.slice(0, startIndex).trim();
            }

            //add the method to theclass.methods, then trim extraneous whitespace
            theClass.methods[i] = theClass.body.slice(startIndex, splitObject.endIndex + 1).trim();
            processMethod(i++); //process the method in a helper method

            //check to see if the last method has been found, if so: break from the loop by using a return statement
            if(splitObject.stop){
                return theClass;
            }

            //set the startindex to the right one
            startIndex = splitObject.endIndex + 1;

            //find the next method, passing in the correct indicesArrays
            splitObject = searchMethodEnd(splitObject.indicesOpenBrace, splitObject.indicesClosingBrace);
        } while(true); //used an infinite loop here because inside the loop there's a check which can trigger a return statement & which should eventually be triggered
    }

    //helper function to search for the ending brace of a method
    function searchMethodEnd(indicesOpenBrace, indicesClosingBrace){
        var open = 0,
            close = 0,
            openLength = indicesOpenBrace.length,
            closeLength = indicesClosingBrace.length,
            returnObject = { stop: false };

        if(indicesOpenBrace.length === 1){
            returnObject.endIndex = indicesClosingBrace[0];
            returnObject.stop = true;
            return returnObject;
        }

        do{
            if(indicesOpenBrace[open] > indicesClosingBrace[close])
            {
                if(close + 1 < closeLength) {
                    close++;
                }
            } else if(open + 1 == openLength){ //if last method
                returnObject.endIndex = indicesClosingBrace[closeLength - 1];
                returnObject.stop = true;
                return returnObject;
            } else {//if open is smaller then close
                if(open + 1 < openLength) {
                    open++;
                }
            }
        } while(open !== close);

        returnObject.endIndex = indicesClosingBrace[close - 1];
        returnObject.indicesOpenBrace = indicesOpenBrace.slice(open);
        returnObject.indicesClosingBrace = indicesClosingBrace.slice(close);

        return returnObject;
    }

    //helper function to get an array of indices containing all indexes where a regex matches a certain string
    function getIndices(regex, theString){
        var result, indices = [];
        while ( (result = regex.exec(theString)) ) {
            indices.push(result.index);
        }

        return indices;
    }

    //helper function to fully process a method: change variables, change params, change declaration
    //uses helper function changeMethodVariables
    function processMethod(index){
        //TODO: process constructors as they don't need to remain as a specific method
        var endParams = theClass.methods[index].indexOf(")"),
            startParams = theClass.methods[index].indexOf("("),
            startBodyProper = theClass.methods[index].indexOf("{") - 1,
            declarationBeforeParams,
            params = "",
            i,
            methodData = {
                methodName: "",
                newParams: "",
                bodyProper: "",
                private: false,
                static: false,
                final: false
            };

        //process parameters
        if ((endParams - startParams) > 1){
            console.log("methode is: " + theClass.methods[index]);
			
			
            params = theClass.methods[index].slice(startParams + 1, endParams);
            params = params.trim();
            params = params.split(",");
            for(i = 0; i < params.length; i++){
                params[i] = params[i].trim().split(" ");
                methodData.newParams += params[i][1].trim() + ", ";
            }
            methodData.newParams = methodData.newParams.slice(0, -2);
        }

        //process declaration
        declarationBeforeParams = theClass.methods[index].slice(0, startParams);
        declarationBeforeParams = declarationBeforeParams.trim().split(" ");
        methodData.methodName = declarationBeforeParams[declarationBeforeParams.length - 1].trim();
        console.log("the method name is: " + methodData.methodName);
			
        for(i = 0; i < declarationBeforeParams.length; i++){
            switch(declarationBeforeParams[i]){
                //public, protected, returntype (any), abstract hebben geen nut in javascript: zijn dus betekenisloos.  Multithreading verloopt op een volkomen
                //andere manier, waardoor het ook niet nuttig is om daar iets mee te doen voorlopig => zou een onderzoeksproject zijn op zich
                //TODO: nog te bekijken: native, strictfp, transient, volatile
                case "private": methodData.private = true; break;
                case "static": methodData.static = true; break;
                case "final": methodData.final = true; break;
                default: break;
            }
        }

        //process the body variables
        methodData.bodyProper = theClass.methods[index].slice(startBodyProper).trim();
        methodData.bodyProper = changeMethodVariables(findMethodVariables(methodData.bodyProper), methodData.bodyProper);

        theClass.methods[index] = "function " + methodData.methodName + "(" + methodData.newParams + ")" + methodData.bodyProper;
        theClass.methodData = [];
        theClass.methodData[index] = methodData;
		var output = "";
		for(var i = 0; i < theClass.methods.length; i = i + 1){
                
				output += theClass.methods[i] + "\n" ;
            }
	    translate.innerText = output;
    }

    //helper function for processMethod to change the type in a variable declaration to var
    function changeMethodVariables(indices, method){
        var i = 0, //variable to increase the performance of the for loop
            length = indices.length, //variable to increase the performance of the for loop
            counter = 0, //variable to offset the startindex with
            partFirst = "", //variable to store the first part of the method
            partFrom = "", //variable to store the part of the method in starting at the variabele declaration till the end of the method
            type = ""; //variable to store the type of the variable for easy reference in two statements

        for (i; i < length; i++) {
            partFirst = method.slice(0, indices[i] - counter); //store the first part of the string for easy concatenation
            partFrom = method.slice(indices[i] - counter); //slice out the part of the string beginning at the variable declaration
            type = partFrom.slice(0, partFrom.search(/\s/)); //store the type of the variable for easy use in the next two statements
            partFrom = partFrom.replace(type, "var");//replace the type by var
            counter += type.length - 3; //augment counter to offset replacing the type by var;
            method = partFirst + partFrom; //reassemble method so it's usable again for the next variable
        }

        return method;
    }

    //function to process field declarations
    function processFields(){
        //TODO uitwerken deze methode voor theClass.fieldDeclarations
    }

    //function to get an array of indexes of where in a method variables are declared
    function findMethodVariables(method){
        return getIndices(/\b(?!(return|new|package|import|where|set|and|on))[\w\[\]<>]+ [\w]+(;| *=)/g, method);
    }

    //function to get an array of indexes of where in a class fields are declared
    function findFields(fieldDeclarations){
        return getIndices(/\b(public |private |protected |static |final |enum |synchronized |volatile )+ *[\w\[\]<>]+ [\w]+(;| *=)/g, fieldDeclarations);
    }
    //TODO: check if the two above methods cannot be combined, and if they are even necessary?

    //function to define fields on a property, defines default values in case you don't want to specify write, configure or enumerate (in that order)
    //obj and val parameters are mandatory
    function defineField(obj, val, write, configure, enumerate){
        Object.defineProperty(obj, {
            value: val,
            enumerable: enumerate ? enumerate : true,
            configurable: configure ? configure : true,
            writable: write ? write : true
        });

        return obj;
    }

    //function to create a constructor in Java
    //uses the helper function enforceNew
    function createConstructor(){
        var i,
            numberOfMethods = theClass.methods.length,
            constructorString = "function " + theClass.constructorName + "(" + theClass.parameters + "){\r\n\t";

        constructorString += enforceNew(theClass.constructorName, theClass.parameters); //enforce the use of new to avoid this pointing to a global function or being undefined (strict mode)
        constructorString += "\t" + theClass.fieldDeclarations + "\r\n\r\n"; //add field declarations

        //TODO: for testing purposes right now => prototype moet nog geïmplementeerd worden!
        for(i = 0; i < numberOfMethods; i = i + 1){ //add methods
            constructorString += theClass.methods[i] + "\r\n\r\n";
        }

        return constructorString + "}\r\n\r\n";
    }

    //function to create a prototype
    function createPrototype(){
        var prototypeString = theClass.constructorName + ".prototype = {\r\n\t";
        //TODO uitwerken deze methode
        prototypeString += "\r\n}"
    }

    //function to enforce the use of new when calling a constructor
    //helper function for createConstructor
    function enforceNew(constructorName, parameters){
        return "if(!(this instanceof " + constructorName + ")){ \r\n\t\t" + "return new " + constructorName + "(" + (parameters ? parameters : "") + ");\r\n\t } \r\n\r\n";
    }
    //TODO: kijken of de bovenstaande methode wel apart moet

    //function to process the class declaration
    //uses the helper function createConstructorName
    function processClassDeclaration(){
        var declaration = theClass.declaration,
            implement = theClass.declaration.indexOf("implements"),
            extend = theClass.declaration.indexOf("extends"),
            modifiers,
            extendDeclaration,
            i;

        //retrieve modifiers
        if(extend > 0){
            modifiers = declaration.slice(0, extend).trim();
        } else if(implement > 0){
            modifiers = declaration.slice(0, implement).trim();
        } else{
            modifiers = declaration.trim();
        }

        //process modifiers & assign className + constructorName
        modifiers = modifiers.split(" ");
        theClass.className = modifiers[modifiers.length - 1].trim();
        createConstructorName();
        for(i = 0; i < modifiers.length; i++){
            switch(modifiers[i]){
                //public, protected, abstract zijn onbestaand in javascript: zijn dus betekenisloos.
                case "private": theClass.private = true; break; //enkel voor inner classes
                case "static": theClass.static = true; break; //enkel voor inner classes
                case "final": theClass.final = true; break;  //cannot be subclassed: TODO nakijken of dit kan geïmplementeerd worden
                default: break;
            }
        }

        //process extends
        if(extend > 0){
            extendDeclaration = (extend > implement) ? declaration.slice(extend).trim() : declaration.slice(extend, implement).trim();
            extendDeclaration = extendDeclaration.split(" ");
            theClass.superclass = extendDeclaration[1];
        }

        //process implements
        if(implement > 0){
            theClass.interfaces = [];
            implement = declaration.slice(implement).trim().split(",");
            for(i = 0; i < implement.length; i++){
                implement[i] = implement[i].split(" ");
                theClass.interfaces[i] = implement[i][1].trim();
            }
        }
    }

    //function to uppercase the first char of a class name
    function createConstructorName(){
        var firstChar = theClass.className.charAt(0);
        theClass.constructorName = theClass.className.replace(firstChar, firstChar.toUpperCase());
    }

    //function to remove comments in the code
    function removeComments(input){
        input = input.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, ""); //remove multiline comments
        return input.replace(/\/\/.*/g, ""); //remove single line comments
    }

    //geef een object terug dat de verwerking als functie heeft, maar waardoor alle methoden om de verwerking te doen private zijn
    return {

        translate: function translate(input){
            input = removeComments(input);
            findClass(input);
            splitBodyInMethods();
//            processFields();
            processClassDeclaration();
			
            for(var i = 0; i < theClass.methods.length; i = i + 1){
                console.log("Methode " + (i + 1) + " is: " + theClass.methods[i]);
				
            }
			
        }
    };
})();