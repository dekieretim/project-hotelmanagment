﻿var input, output;
window.onload = function () {
		addAllListeners();
        initializeAllElements();
};

function initializeAllElements(){
    fileInput = document.getElementById('fileInput');
    typer = document.getElementById("translate");
    outputButton = document.getElementById("download");
    filename = document.getElementById("fileName").value;
    originalFile = document.getElementById("originalFile");
    div = document.getElementById('translate');

}
function readFile(userData) {

    var file = userData.files[0],
        extension = file.name.split('.').pop(),
        requiredExtention = "java",
        reader;
//        test of de extensie ".java" is
    if (extension == requiredExtention) {
        reader = new FileReader();
        reader.onload = function () {
            input = reader.result;
            console.log("succesfully read file: \n" + input);
            originalFile.innerText = input;

        };

        reader.readAsText(file);

    }
    else {
        console.log("wrong file type");
    }


}		
function addAllListeners() {


//    hulpmethod om de default actie bij events te overschrijven
    function cancel(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        return false;
    }


    //    Zorgt ervoor dat de file niet geöpend wordt in de browser door "dragover" en "dragenter" events te annuleren, zodat alleen de (overgeschreven) drop functie wordt aangeroepen
    window.addEventListener('dragover', cancel, false);
    window.addEventListener('dragenter', cancel, false);


//    deze functie zorgt ervoor dat als je een file drag-en-dropt in je browservenster, de readFile method wordt aangroepen op deze file.
    window.addEventListener('drop', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        } // stops the browser from redirecting off to the image.

        var dt = e.dataTransfer;
        readFile(dt);
    });

//    deze functie zorgt ervoor dat als je een file kiest met behulp van de upload-knop, de readFile method wordt aangroepen op deze file.
    fileInput.addEventListener('change', function () {
        readFile(fileInput);
    });

}

function downloadFile(extention) {

    const MIME_TYPE = "text/plain";
    console.log("typer" + typer);
    //creates a blob (file that can be filled with text)
    var bb = new Blob([typer.innerText], {type: MIME_TYPE});

    //creates a hyperlink
    var translatedFile = document.createElement('a');
    translatedFile.download = filename + extention;
    translatedFile.href = window.URL.createObjectURL(bb);
    translatedFile.textContent = "Download";
    translatedFile.dataset.downloadurl = [MIME_TYPE, translatedFile.download, translatedFile.href].join(':');

    outputButton.innerHTML = "";

    //adds the hyperlink to the button
    outputButton.appendChild(translatedFile);

    //output.disabled = false;

}



