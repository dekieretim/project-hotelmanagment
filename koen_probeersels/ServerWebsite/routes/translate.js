/**
 * Created by Badlapje on 23/11/14.
 */

//get translated file
exports.index = function(req, res){
    var translate = require("../JavaToJS/index");

    res.render('translate', {
        theString: translate.translateJavaToJS(req.stringtotranslate)
    });
};
