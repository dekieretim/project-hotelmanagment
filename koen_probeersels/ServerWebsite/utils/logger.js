/**
 * Created by Badlapje on 27/11/14.
 */
var winston = require('winston');

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({ colorize: true, timestamp: true }),
        new (winston.transports.File)({ filename: __dirname + '/../logs/all-logs.log', colorize: true }),
        new (winston.transports.File)({ filename: __dirname + '/../logs/debug.log', level: 'debug', colorize: true }),
        new (winston.transports.File)({ filename: __dirname + '/../logs/error.log', handleExceptions: true, level: 'error', colorize: true })
    ]
});

logger.info('Chill Winston, the logs are being captured 4 ways - console and 3 files');

module.exports = logger;