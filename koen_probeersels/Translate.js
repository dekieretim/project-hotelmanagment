var input, div;

//function to get rid of comments in order to avoid problems when there's a { in the comments
function removeComments(input)
{
    input = input.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, ""); //remove multiline comments
    input = input.replace(/\/\/.*/g, ""); //remove single line comments
}

function toonOutput(){

    removeComments(input);
    div.innerText = input;

    var theClass = splitBodyInMethods(findClass(input)),
        length = theClass.methods.length;

//    theClass.fieldDeclarations = processFields(theClass.fieldDeclarations);
    processClassDeclaration(theClass);
    for (var i = 0; i < length; i = i + 1){
             console.log("Methode " + (i + 1) + " is: " + theClass.methods[i]);
    }
}

window.onload = function () {
    div = document.getElementById('vertaling');
    var fileInput = document.getElementById('fileInput');

    fileInput.addEventListener('change', function () {
        var file = fileInput.files[0];

        var extension = file.name.split('.').pop();
        var requiredExtention = "java";
        if (extension == requiredExtention) {
            var reader = new FileReader();

            reader.onload = function () {
                input = reader.result;
            };

            reader.readAsText(file);
        }
        else{
            console.log("wrong file type");
        }
    });
};

//function to create a class object with three properties: the imports part, the declaration part and the body part
//uses a helper function: sliceClass to do the actual slicing in order to make the code DRYer
function findClass(javaString){
    var thisClass = { imports: "", declaration: "", body: "", static: false, private: false, final: false, superclass: false, interfaces: false, methodData: [] };

    if (typeof javaString == "string") {
        var firstPublic = javaString.indexOf("public"),
            firstPrivate = javaString.indexOf("private"),
            firstBracket = javaString.indexOf("{"),
            lastBracket = javaString.lastIndexOf("}");

        if (firstPublic < firstPrivate) {
            sliceClass(firstPublic, firstBracket, lastBracket, thisClass, javaString);
        } else {
            sliceClass(firstPrivate, firstBracket, lastBracket, thisClass, javaString);
        }
    }

    return thisClass;
}

//function to extract the imports, the declaration and the body from a stringified Java Class
function sliceClass(beginDeclaration, beginBody, endBody, classObject, javaString){
    classObject.imports = javaString.slice(0, beginDeclaration);
    classObject.declaration = javaString.slice(beginDeclaration, beginBody);
    classObject.body = javaString.slice(beginBody + 1, endBody);

    for (var property in classObject){
        if(classObject.hasOwnProperty(property)){
            property.trim(); //remove all extraneous whitespace from the beginning and ending of the string
        }
    }

    return classObject;
}

//function om de body op te splitsen in methodes, blocks en variabelen
//uses the following helperfunctions: getIndices, searchMethodEnd
function splitBodyInMethods(classObject){
    var indicesSemicolon = getIndices(/;/g, classObject.body),
        indicesOpenBrace = getIndices(/\{/g, classObject.body),
        indicesClosingBrace = getIndices(/\}/g, classObject.body),
        startIndex = indicesSemicolon.filter(function(item, index, array){
            return (item < indicesOpenBrace[0]);
        }).reverse()[0] + 1;

    var splitObject = searchMethodEnd(indicesOpenBrace, indicesClosingBrace),
        i = 0;

    classObject.methods = [];

    do{
        //if it's the first method: add the field declarations to the class
        if(!("fieldDeclarations" in classObject)){
            classObject.fieldDeclarations = classObject.body.slice(0, startIndex).trim();
        }

        //add the method to theclass.methods, then trim extraneous whitespace
        classObject.methods[i] = classObject.body.slice(startIndex, splitObject.endIndex + 1).trim();
        processMethod(classObject, i++); //process the method in a helper method

        //check to see if the last method has been found, if so: break from the loop by using a return statement
        if(splitObject.stop){
            return classObject;
        }

        //set the startindex to the right one
        startIndex = splitObject.endIndex + 1;

        //find the next method, passing in the correct indicesArrays
        splitObject = searchMethodEnd(splitObject.indicesOpenBrace, splitObject.indicesClosingBrace);
    } while(true); //used an infinite loop here because inside the loop there's a check which can trigger a return statement & which should eventually be triggered
}

//helper method to fully process a method
function processMethod(classObject, index){
    var endParams = classObject.methods[index].indexOf(")"),
        startParams = classObject.methods[index].indexOf("("),
        startBodyProper = classObject.methods[index].indexOf("{") - 1,
        declarationBeforeParams,
        params = "",
        i,
        methodData = {
            methodName: "",
            newParams: "",
            bodyProper: "",
            private: false,
            static: false,
            final: false
        };

    //process parameters
    if (endParams - startParams !== 1){
        params = classObject.methods[index].slice(startParams + 1, endParams);
        params = params.trim();
        params = params.split(",");
        for(i = 0; i < params.length; i++){
            params[i] = params[i].trim().split(" ");
            methodData.newParams += params[i][1].trim() + ", ";
        }
        methodData.newParams = methodData.newParams.slice(0, -2);
    }
    //process declaration
    declarationBeforeParams = classObject.methods[index].slice(0, startParams);
    declarationBeforeParams = declarationBeforeParams.trim().split(" ");
    methodData.methodName = declarationBeforeParams[declarationBeforeParams.length - 1].trim();
    for(i = 0; i < declarationBeforeParams.length; i++){
        switch(declarationBeforeParams[i]){
            //public, protected, returntype (any), abstract hebben geen nut in javascript: zijn dus betekenisloos.  Multithreading verloopt op een volkomen
            //andere manier, waardoor het ook niet nuttig is om daar iets mee te doen voorlopig => zou een onderzoeksproject zijn op zich
            //TODO: nog te bekijken: native, strictfp, transient, volatile
            case "private": methodData.private = true; break;
            case "static": methodData.static = true; break;
            case "final": methodData.final = true; break;
            default: break;
        }
    }

    //process the body variables
    methodData.bodyProper = classObject.methods[index].slice(startBodyProper).trim();
    methodData.bodyProper = changeMethodVariables(findMethodVariables(methodData.bodyProper), methodData.bodyProper);

    classObject.methods[index] = "function " + methodData.methodName + "(" + methodData.newParams + ")" + methodData.bodyProper;
    classObject.methodData = [];
    classObject.methodData[index] = methodData;
//    console.log("de herwerkte methode is:\r\n" + classObject.methods[index]);
}

//function to search for the ending brace of a method
function searchMethodEnd(indicesOpenBrace, indicesClosingBrace){
    var open = 0,
        close = 0,
        openLength = indicesOpenBrace.length,
        closeLength = indicesClosingBrace.length,
        returnObject = { stop: false };

    if(indicesOpenBrace.length === 1){
        returnObject.endIndex = indicesClosingBrace[0];
        returnObject.stop = true;
        return returnObject;
    }

    do{
        if(indicesOpenBrace[open] > indicesClosingBrace[close])
        {
            if(close + 1 < closeLength) {
                close++;
            }
        } else if(open + 1 == openLength){ //if last method
            returnObject.endIndex = indicesClosingBrace[closeLength - 1];
            returnObject.stop = true;
            return returnObject;
        } else {//if open is smaller then close
            if(open + 1 < openLength) {
                open++;
            }
        }
    } while(open !== close);

    returnObject.endIndex = indicesClosingBrace[close - 1];
    returnObject.indicesOpenBrace = indicesOpenBrace.slice(open);
    returnObject.indicesClosingBrace = indicesClosingBrace.slice(close);

    return returnObject;
}

function getIndices(regex, theString){
    var result, indices = [];
    while ( (result = regex.exec(theString)) ) {
        indices.push(result.index);
    }

    return indices;
}

function changeMethodVariables(indices, method){
    var i = 0, //variable to increase the performance of the for loop
        length = indices.length, //variable to increase the performance of the for loop
        counter = 0, //variable to offset the startindex with
        partFirst = "", //variable to store the first part of the method
        partFrom = "", //variable to store the part of the method in starting at the variabele declaration till the end of the method
        type = ""; //variable to store the type of the variable for easy reference in two statements

    for (i; i < length; i++) {
        partFirst = method.slice(0, indices[i] - counter); //store the first part of the string for easy concatenation
        partFrom = method.slice(indices[i] - counter); //slice out the part of the string beginning at the variable declaration
        type = partFrom.slice(0, partFrom.search(/\s/)); //store the type of the variable for easy use in the next two statements
        partFrom = partFrom.replace(type, "var");//replace the type by var
        counter += type.length - 3; //augment counter to offset replacing the type by var;
        method = partFirst + partFrom; //reassemble method so it's usable again for the next variable
    }

    return method;
}

//function to process Fields
function processFields(fieldDeclarations){
    //if public: just a property
    //if private: closure it
    //if protected: treat as public
    //if static: ???
    //if final: ???
    //if enum: ???
    //if synchronized: ???
    //if volatile: ???

    //TODO uitwerken deze methode
}

//function to find the variables in a method
function findMethodVariables(method){
    return getIndices(/\b(?!(return|new|package|import|where|set|and|on))[\w\[\]<>]+ [\w]+(;| *=)/g, method);
    //oude regex: [^ "(']\b(?!(return|new))[\w<>\[\]]+ [^><=\/%\-+*][\w, =<>\(\)\[\]\s\.*\+\%\'\-\?\r\n\f\0\!"]*;
}

//function to find the variables of a class: fields/instance variables and static variables
function findFields(fieldDeclarations){
    return getIndices(/\b(public |private |protected |static |final |enum |synchronized |volatile )+ *[\w\[\]<>]+ [\w]+(;| *=)/g, fieldDeclarations);
}

//function to define fields on a property, defines default values in case you don't want to specify write, configure or enumerate (in that order)
function defineField(obj, val, write, configure, enumerate){
    Object.defineProperty(obj, {
        value: val,
        enumerable: enumerate ? enumerate : true,
        configurable: configure ? configure : true,
        writable: write ? write : true
    });

    return obj;
}

//function to create a constructor
function createConstructor(theClass){
    var i,
        numberOfMethods = theClass.methods.length,
        constructorString = "function " + theClass.constructorName + "(" + (theClass.parameters ? theClass.parameters : "") + "){\r\n\t"; //start with essentials

    constructorString += enforceNew(theClass.constructorName, theClass.parameters); //enforce the use of new to avoid this pointing to a global function or being undefined (strict mode)
    constructorString += "\t" + theClass.fieldDeclarations + "\r\n\r\n"; //add field declarations

    //for testing purposes right now => prototype moet nog geïmplementeerd worden!
    for(i = 0; i < numberOfMethods; i = i + 1){ //add methods
        constructorString += theClass.methods[i] + "\r\n\r\n";
    }

    return constructorString + "}\r\n\r\n";
}

//function to create a prototype
function createPrototype(theClass){
    var prototypeString = theClass.constructorName + ".prototype = {\r\n\t";
    //TODO uitwerken deze methode
    prototypeString += "\r\n}"
}

//function to uppercase the first char of a classname for use in defining the constructor
function createConstructorName(theClass){
    var firstChar = theClass.className.charAt(0);
    theClass.constructorName = theClass.className.replace(firstChar, firstChar.toUpperCase());
}

//function to enforce the use of new when calling a constructor
function enforceNew(constructorName, parameters){
    return "if(!(this instanceof " + constructorName + ")){ \r\n\t\t" + "return new " + constructorName + "(" + (parameters ? parameters : "") + ");\r\n\t } \r\n\r\n";
}

//function to process the class declaration
function processClassDeclaration(theClass){
    var declaration = theClass.declaration,
        implement = theClass.declaration.indexOf("implements"),
        extend = theClass.declaration.indexOf("extends"),
        modifiers,
        extendDeclaration,
        i;

    //retrieve modifiers
    if(extend > 0){
        modifiers = declaration.slice(0, extend).trim();
    } else if(implement > 0){
        modifiers = declaration.slice(0, implement).trim();
    } else{
        modifiers = declaration.trim();
    }

    //process modifiers & assign className + constructorName
    modifiers = modifiers.split(" ");
    theClass.className = modifiers[modifiers.length - 1].trim();
    createConstructorName(theClass);
    for(i = 0; i < modifiers.length; i++){
        switch(modifiers[i]){
            //public, protected, abstract zijn onbestaand in javascript: zijn dus betekenisloos.
            case "private": theClass.private = true; break; //enkel voor inner classes
            case "static": theClass.static = true; break; //enkel voor inner classes
            case "final": theClass.final = true; break;  //cannot be subclassed: TODO nakijken of dit kan geïmplementeerd worden
            default: break;
        }
    }

    //process extends
    if(extend > 0){
        extendDeclaration = (extend > implement) ? declaration.slice(extend).trim() : declaration.slice(extend, implement).trim();
        extendDeclaration = extendDeclaration.split(" ");
        theClass.superclass = extendDeclaration[1];
    }

    //process implements
    if(implement > 0){
        theClass.interfaces = [];
        implement = declaration.slice(implement).trim().split(",");
        for(i = 0; i < implement.length; i++){
            implement[i] = implement[i].split(" ");
            theClass.interfaces[i] = implement[i][1].trim();
        }
    }
}