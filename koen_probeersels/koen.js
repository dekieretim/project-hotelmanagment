/**
 * Created by Badlapje on 24/04/14.
 */
/**
 * Created by Badlapje on 13/03/14.
 */

//funtion to create a class object with three properties: the imports part, the declaration part and the body part
//uses a helper function: sliceClass to do the actual slicing in order to make the code DRYer
exports.findClass = function(theString){
    var thisClass = { imports: "", declaration: "", body: "" };

    if (typeof theString == "string") {
        var firstPublic = theString.indexOf("public"),
            firstPrivate = theString.indexOf("private"),
            firstBracket = theString.indexOf("{"),
            lastBracket = theString.lastIndexOf("}");

        if (firstPublic < firstPrivate) {
            sliceClass(firstPublic, firstBracket, lastBracket, thisClass, theString);
        } else {
            sliceClass(firstPrivate, firstBracket, lastBracket, thisClass, theString);
        };
    };

    return thisClass;
}

//function to extract the imports, the declaration and the body from a stringified Java Class
exports.sliceClass = function(beginDeclaration, beginBody, endBody, thisClass, theString){
    thisClass.imports = theString.slice(0, beginDeclaration);
    thisClass.declaration = theString.slice(beginDeclaration, beginBody);
    thisClass.body = theString.slice(beginBody + 1, endBody);

    for (var property in thisClass){
        property.trim(); //remove all extraneous whitespace from the beginning and ending of the string
    }

    return thisClass;
}

//function om de body op te splitsen in methodes, blocks en variabelen
//uses the following helperfunctions: getIndices, searchMethodEnd
exports.splitBodyInMethods = function(theClass){
    var indicesSemicolon = getIndices(/;/g, theClass.body),
        indicesOpenBrace = getIndices(/\{/g, theClass.body),
        indicesClosingBrace = getIndices(/\}/g, theClass.body),
        startIndex = indicesSemicolon.filter(function(item, index, array){
            return (item < indicesOpenBrace[0]);
        }).reverse()[0] +1,
        splitObject = searchMethodEnd(indicesOpenBrace, indicesClosingBrace),
        i = 0;

    theClass.methods = [];

    do{
        //add the method to theclass.methods, then trim extraneous whitespace
        theClass.methods[i] = theClass.body.slice(startIndex, splitObject.endIndex + 1);
        theClass.methods[i++].trim();

        //check to see if the last method has been found, if so: break from the loop by using a return statement
        if(splitObject.stop){
            return theClass;
        }

        //set the startindex to the right one
        startIndex = splitObject.endIndex += 1;

        //find the next method, passing in the correct indicesArrays
        splitObject = searchMethodEnd(splitObject.indicesOpenBrace, splitObject.indicesClosingBrace);
    } while(true);
}

//function to search for the ending brace of a method
exports.searchMethodEnd = function(indicesOpenBrace, indicesClosingBrace){
    var i = 0,
        open = 1,
        close = 0,
        compare = function(open, close){
            if (open > close) {
                return true;
            }
        },
        returnObject = { stop: false };

    if(indicesOpenBrace.length === 1){
        returnObject.endIndex = indicesClosingBrace[i];
        returnObject.stop = true;
        return returnObject;
    }

    do{
        if(compare(indicesOpenBrace[open], indicesClosingBrace[close])){
            close += 1;
        } else {
            open += 1;
        }
    } while(open !== close);

    returnObject.endIndex = indicesClosingBrace[close - 1];
    returnObject.indicesOpenBrace = indicesOpenBrace.slice(open);
    returnObject.indicesClosingBrace = indicesClosingBrace.slice(close);
    return returnObject;
}


//function to find the variables in a string
exports.findVariables = function(theString){
    return getIndices(/[^ "(']\b(?!(return))[\w<>\[\]]+ [^><=\/%\-+*][\w, =<>\(\)\[\]\s\.*\+\%\'\-\?\r\n\f\0\!"]*;/g, theString);
}

//helperfunctie om een array met indices te vinden voor een bepaalde regex
exports.getIndices = function(regex, theString){
    var result, indices = [];
    while ( (result = regex.exec(theString)) ) {
        indices.push(result.index);
    }

    return indices;
}