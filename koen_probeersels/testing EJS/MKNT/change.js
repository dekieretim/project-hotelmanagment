/**
 * Created by Badlapje on 8/10/14.
 */

var theClass,
    search = require('./search'),
    logger = require('../utils/logger');

/*
 * Function to check if there are any for loops in a method
 */
function containsFors(javaString){
    var regex = /for *\( *.*\) *\{/g,
        result = false;

    if(search.getMatches(regex, javaString) > 0){
        result = true;
    }

    return result;
}

/*
 * Function to translate for Loops
 */
function translateForLoop(stringToTranslate) {
    stringToTranslate = stringToTranslate.trim().replace(/[ ]{2,}/g, " "); //this replaces all occurrences of more than one whitespace by one whitespace.
    return chooseCorrectLoopTypeFunction(stringToTranslate);
}

/*
 * Helper function for translateForLoop
 */
function getForIdentifierPart(forLoopString) {
    return forLoopString.split("{")[0]; /*")" would also include a possible bracket from ".size()"*/
}

/*
 * Helper function for translateForLoop
 * converts a foreach loop into a regular javascript for loop
 */
function translateForeachFor(foreachString) {
    var indexType = getIndexType(foreachString); //variable for readability

    foreachString = foreachString.replace(indexType, "var");
    foreachString = foreachString.replace(":", " in ");

    return foreachString;
}

/*
 * Helper function for translateForLoop
 * Translates a regular for loop
 */
function translateRegularFor(regularForString) {
    var help, help2, triggerPart, firstSemicolonPos, secondSemicolonPos,
        indexType = getIndexType(regularForString),
        forIdentifierPart = getForIdentifierPart(regularForString.replace(indexType, "var")),
        newForIdentifierPart = forIdentifierPart;

    /*** all code after this translates .Size() and .length() to ".length" ***/
    firstSemicolonPos = newForIdentifierPart.indexOf(";"); //get first index of a semicolon
    secondSemicolonPos = newForIdentifierPart.lastIndexOf(";"); //get second index of a semicolon
    triggerPart = newForIdentifierPart.split(";")[1]; //isolate part between the 2 semicolons
    triggerPart = triggerPart.replace(/\s/g, ""); //removes all whitespace in this part
    newForIdentifierPart = newForIdentifierPart.slice(0, firstSemicolonPos) + "; " + triggerPart + newForIdentifierPart.slice(secondSemicolonPos); //put it back between the semicolons
    regularForString = regularForString.replace(forIdentifierPart, newForIdentifierPart);
    help = newForIdentifierPart.toLowerCase().indexOf(".length()"); //if the identifier contains .length() this will return the index, else it returns -1
    help2 = newForIdentifierPart.toLowerCase().indexOf(".size()"); //if the identifier contains .size() this will return the index, else it returns -1

    if (help != -1) {
        regularForString = regularForString.replace(/\.length\(\)/i, ".length"); //this is usually the same as .replace(".length()".....) but safer, (/i = ignoring case)
        //This is easier and safer than trying to make a part of the identifier toLowerCase because it may contain user-defined variables and methods
    } else if (help2 != -1) {
        regularForString = regularForString.replace(/\.size\(\)/i, ".length");
    }

    return regularForString;
}

/*
 * Helper function for translateForLoop
 * returns the type of the iterator, usually int
 */
function getIndexType(string) {
    return string.split("(")[1] //isolate the part between brackets
        .trim() //remove extraneous whitespace
        .split(" ")[0]; //take the first word which is the indexType
}

/*
 * Helper function for translateForLoop
 */
function chooseCorrectLoopTypeFunction(unidentifiedForLoop) {
    var forIdentifierPart = getForIdentifierPart(unidentifiedForLoop),
        translatedString = "Could not translate for loop."; //if it's not a for loop

    if (forIdentifierPart.indexOf(";") != -1) {// index of ";" is -1 if the string does not contain ";" -->  it's a regular for loop
        translatedString = translateRegularFor(unidentifiedForLoop);
    } else { //it's a foreach loop
        translatedString = translateForeachFor(unidentifiedForLoop);
    }

    return translatedString;
}

/*
 * heavy-lifting function that translates begintags of structures.
 * uses two parameters:
 * STRUCTURE is the name of the structure we want to translate here
 * CODE is the structure to apply the translation on
 * all helper functions are included within the function
 */
function translateStructure(structureTag, code){
    var translatedStructure;
    switch(structureTag)
    {
        case 'if': translatedStructure = returnTranslationIfStructure(code); break;
        case 'forEach': translatedStructure = returnTranslationForEachStructure(code); break;
        case 'include': translatedStructure = returnTranslationIncludeStructure(code); break;
        case 'jspInclude': translatedStructure = returnTranslationJspIncludeStructure(code); break;
        case 'when': translatedStructure = returnTranslationWhenStructure(code); break;
        case 'variable': translatedStructure = returnTranslationWhenStructure(code); break;
        case 'forward' : translatedStructure = returnTranslationForwardStructure(code); break;
        case 'setProperty' : translatedStructure = returnTranslationSetPropertyStructure(code); break;
        case 'getProperty' : translatedStructure = returnTranslationGetPropertyStructure(code); break;
        case 'out' : translatedStructure = returnTranslationOutStructure(code); break;
        case 'forTokens' : translatedStructure = returnTranslationForTokensStructure(code); break;
        default : console.log("Unknown parameter; if, forEach, include, jspInclude, choose, variable, forward, setProperty, getProperty, out or forTokens expected!");
    }

    return translatedStructure;

    //returns a translated forward structure
    function returnTranslationForwardStructure(forwardStructure){
        var beginPage = forwardStructure.indexOf("page") + 6,
            endPage = forwardStructure.indexOf('"', beginPage);
        return "<% include " + forwardStructure.slice(beginPage, endPage) + ".ejs %>"
    }

    //returns a translated getproperty structure
    function returnTranslationGetPropertyStructure(getPropStructure){
        // <% myName.someProperty %>
        return "<% " +getPropName(getPropStructure)+ "." +getPropProperty(getPropStructure)+ " %>";
    }

    //returns a translated setproperty structure
    function returnTranslationSetPropertyStructure(setPropStructure){
        // <% myName.someProperty = someValue %>
        return "<% " +getPropName(setPropStructure)+ "." +getPropProperty(setPropStructure)+ " = " +getPropValue(setPropStructure)+ " %>";

        function getPropValue(structure)
        {
            var beginValue = structure.indexOf("value") + 7;
            var endValue = structure.indexOf('"', beginValue);
            return structure.slice(beginValue, endValue);
        }
    }

    //returns a translated out structure
    function returnTranslationOutStructure(OutStructure){
        return "<%" +(getEscapeXMLValue(OutStructure)? "=":"-")
            + " " +getOutValue(OutStructure)+ " "
            +(getDefault(OutStructure)? ("||" +getDefault(OutStructure)+""): (""))
            + " %>";

        function getOutValue(structure)
        {
            var beginValue = structure.indexOf('value="${') + 9;
            var endValue = structure.indexOf('}" />', beginValue);
            return structure.slice(beginValue, endValue);
        }

        function getEscapeXMLValue(structure)
        {
            var beginEscapeXml = structure.indexOf('escapeXML') + 11;
            var endEscapeXml = structure.indexOf('"', beginEscapeXml);
            var xml = structure.slice(beginEscapeXml, endEscapeXml);
            return xml? true: false;
        }

        function getDefault(structure)
        {
            var beginDefault = structure.indexOf('default') + 9;
            var endDefault = structure.indexOf('"', beginDefault);
            return structure.slice(beginDefault, endDefault);
        }
    }

    //returns a translated forTokens structure
    function returnTranslationForTokensStructure(forTokensStructure){
        var forTokensItems = getForTokensItems(forTokensStructure),
            forTokensDelims = getForTokensDelims(forTokensStructure),
            forTokensVariable = getForTokensVariable(forTokensStructure);

        return '<% var mkntForTokensString = ' +forTokensItems+ '; %>'
            + '<% var mkntForTokensResult = mkntForTokensString.split("' +forTokensDelims+ '"); %>'
            + '<% mkntForTokensResult.forEach(function(' +forTokensVariable+ '){';

        //HELPER FUNCTIONS
        function getForTokensItems(structure)
        {
            var beginItems = structure.indexOf('items') + 7;
            var endItems = structure.indexOf('"');
            return structure.slice(beginItems, endItems);
        }

        function getForTokensDelims(structure)
        {
            var beginDelims = structure.indexOf('delims') + 8;
            var endDelims = structure.indexOf('"');
            return structure.slice(beginDelims, endDelims);
        }

        function getForTokensVariable(structure)
        {
            var beginVar = structure.indexOf('var') + 5;
            var endVar = structure.indexOf('"');
            return structure.slice(beginVar, endVar);
        }
    }

    //translates a jsp:include structure
    function returnTranslationJspIncludeStructure(structure){
        return structure.replace('<jsp:include page="', '<% include ')
            .replace('.jsp"', ' %');
    }

    //only translates the begin-tag
    function returnTranslationWhenStructure(whenStructure){
        var beginIndexContent = whenStructure.indexOf("<c:when") + 16,
            endIndexContent = whenStructure.indexOf('}">', beginIndexContent);

        return "<% if(" + whenStructure.slice(beginIndexContent, endIndexContent) + "){ %>\n";
    }

    //Translates any forEachStructure
    function returnTranslationForEachStructure(forEachStructure){
        var begin, end, step, variable;

        if((forEachStructure.indexOf("begin")>=0) || (forEachStructure.indexOf("end")>=0))
        {
            //with given step
            if(forEachStructure.indexOf("step")>=0)
            {
                begin = getBegin(forEachStructure);
                end = getEnd(forEachStructure);
                step = getStep(forEachStructure);
                variable = getVar(forEachStructure);
                return "<% for(var " + variable + " = " + begin + "; i < " + end + "; i + " + step + ") { %>";
            }

            //without given step (step = 1)
            else
            {
                begin = getBegin(forEachStructure);
                end = getEnd(forEachStructure);
                variable = getVar(forEachStructure);
                return "<% for(var " + variable + " = " + begin + "; i < " + end + "; i++ ) { %>";
            }

            //FUNCTIONS
            function getVar(structure)
            {
                var beginVar = structure.indexOf("var") + 5;
                var endVar = structure.indexOf('"', beginVar);
                return structure.slice(beginVar, endVar);
            }

            function getBegin(structure)
            {
                var beginStart = structure.indexOf("begin") + 7;
                var beginEnd = structure.indexOf('"', beginStart);
                return structure.slice(beginStart, beginEnd);
            }

            function getEnd(structure)
            {
                var endStart = structure.indexOf("end") + 5;
                var beginStart = structure.indexOf('"', endStart);
                return structure.slice(endStart, beginStart);
            }

            function getStep(structure)
            {
                var stepBegin = structure.indexOf("step") + 6;
                //argumentsEnd = forEachStructure.indexOf('"')[2] - 1;
                var stepEnd = structure.indexOf('"', stepBegin);
                //var arguments = IfStructure.splice(argumentsBegin, argumentsEnd);

                return structure.slice(stepBegin, stepEnd);
            }
        }
        //forEach loop with collections (and varstatus)
        else
        {
            var elements = getForEachVarItems(forEachStructure), //get the array-element
                element = getForEachVar(forEachStructure), //get the single element
                statusPresent = false,
                varStatusElement;

            //get the element representing varStatus
            if(forEachStructure.indexOf("varStatus") >= 0){
                varStatusElement = getForEachVarStatus(forEachStructure);
                statusPresent = true; //statusPresent is used in string newForEachStructure
            }

            //assembles a .forEach(function(){})-structure from scratch and returns it
            return "<% " + elements +".forEach(function(" + element + (statusPresent? ", ": "") + (statusPresent? varStatusElement : "") + "){ %>";

            //FUNCTIONS
            //hier zou je met refactoren 2 functies van kunnen maken (1 & 3 kan je combineren in 1 functie)
            //maar dat maakt het en pak minder leesbaar

            //returns the variable representing varStatus
            function getForEachVarStatus(forEachStructure){
                var beginIndex = forEachStructure.indexOf("varStatus") + 11,
                    endIndex = forEachStructure.indexOf('">', beginIndex) ;

                return forEachStructure.slice(beginIndex, endIndex);
            }

            //returns the variable of FOREACH (element)
            function getForEachVar(forEachStructure){
                var argumentsBegin = forEachStructure.indexOf("var") + 5,
                    argumentsEnd = forEachStructure.indexOf("items") - 2;

                return forEachStructure.slice(argumentsBegin, argumentsEnd);
            }

            //returns the items-collection of FOREACH (elements)
            function getForEachVarItems(forEachStructure){
                var argumentsBegin = forEachStructure.indexOf("items") + 9,
                    argumentsEnd = forEachStructure.indexOf("}", argumentsBegin);

                return forEachStructure.slice(argumentsBegin, argumentsEnd);
            }
        }
    }

    //translates a any if-structure
    function returnTranslationIfStructure(structure){
        var ifStructure = translateTags(structure);

        if(hasAttribute("length") || hasAttribute("empty") || hasAttribute("startsWith")){
            ifStructure = translateAttributes(ifStructure);
        }

        return ifStructure;

        //HELPER FUNCTIONS
        function hasAttribute(code){
            return ifStructure.indexOf("" +code+ "") >= 0;
        }

        function translateTags(code){
            return code.replace(/<c:if/g, '<% if(')
                .replace(/test="\$\{/g, '')
                .replace(/\}">/g, '){ %>')
                .replace(/<\/c:if>/g, '<% } %>');//translate </c:if>
        }

        function translateAttributes(code){
            var variable = getVariables(code),
                vertaaldeCode = "";

            //if EMPTY is present
            if(hasAttribute("empty")){
                if(getNumberOfEmpty(code) > 1){
                    vertaaldeCode = "<% if((typeof " + variable + " == undefined && " + variable + " == '') && (typeof " + variable +
                        " == undefined && " + variable + " == '') %>";
                }
                else {
                    if(hasAttribute("not empty") || hasAttribute("! empty")){
                        vertaaldeCode = "<% if(typeof " + variable + " != undefined && " + variable + " != '') %>";
                    }
                    else {
                        vertaaldeCode = "<% if(typeof " + variable + " == undefined && " + variable + " == '') %>";
                    }
                }
            }

            //if startsWith is present
            if(hasAttribute("startsWith")){
                vertaaldeCode = code.replace(/fn:startsWith\(/, '')
                    .replace(/, '/, '.charAt(0)=="')
                    .replace(/'\)/, '"');
            }

            //if LENGTH is present
            if(hasAttribute("length")){
                vertaaldeCode = code.replace(/fn:length\(/g, '' )
                    .replace(/\)/, '.length() ');
            }

            return vertaaldeCode;

            //returns the number of 'empty' present
            function getNumberOfEmpty(code){
                return code.match(/empty/g).length;
            }

            //returns the variable present inside the IF
            function getVariables(code) {
                var index0 = code.indexOf("empty") + 6,
                    index1 = code.indexOf(")", index0);

                return code.slice(index0, index1);
            }
        }
    }

    //translates an include structure
    function returnTranslationIncludeStructure(includeStructure){
        // trim onnodige code
        //VOOR: <%@ include file="/views/....jsp" %>
        //NA:   <%  include ... %>  of  <% include admin/... %>

        return includeStructure.replace('@', '')
            .replace('file="/views/', '')
            + "\n";
    }

    //HELPER FUNCTIONS FOR GET & SET PROPERTY
    function getPropName(structure)
    {
        var beginName = structure.indexOf("name") + 6;
        var endName = structure.indexOf('"', beginName);

        return structure.slice(beginName, endName);
    }

    function getPropProperty(structure)
    {
        var beginProperty = structure.indexOf("property") + 10;
        var endProperty = structure.indexOf('"', beginProperty);

        return structure.slice(beginProperty, endProperty);
    }
}

//function to uppercase the first char of a class name
function createConstructorName(){
    var firstChar = theClass.className.charAt(0);
    theClass.constructorName = theClass.className.replace(firstChar, firstChar.toUpperCase());
}

//function to clear out newlines and (in the future: multi-line declarations)
function prepareFields(splitByLine){
    var blankLines = 0,
        length = splitByLine.length;

    for (var i = 0; i < length; i++) {
        var theString = splitByLine[i].trim(),
            semicolonIndices = search.getIndices(/;/g, theString),
            equalIndices = search.getIndices(/=/g, theString);
        if (theString.length) { //als het geen lege string is (dus gewoon lijn whitespace, want alle niet enter/newline whitespace wordt getrimt in de declaratie van deze var)
            if (equalIndices.length === 0) { //als er geen gelijk aan in staat
                splitByLine[i - blankLines++ - 1].content += theString;
            } else splitByLine[i - blankLines] = { content: theString, semiColons: semicolonIndices, equals: equalIndices }; //als dat wel het geval is
        } else blankLines += 1; // als het wel een lege lijn is
    }

    splitByLine.splice((length - blankLines), blankLines); //verwijder de lege lijnen

    return splitByLine;
}

//function to write values to the constructorString
//uses the helperfunction defineField
function writeToConstructor(total, value, name, isPrivate, isStatic, final){
    if (isPrivate&& isStatic) { //private static
        theClass.privateStatic += total + "\r\n\t"; //! geen speciale behandeling voor final aangezien private & static en op zich niet te wijzigen
    } else if (isPrivate) { //private
        theClass.forInnerConstructor += total + "\r\n\r\n\t";
    } else if (final) { //public final
        theClass.publicVarMethods += defineField("this", name, value, false, true, true);//TODO probleem hier uitzoeken: welke param staat hier te veel of verkeerd?!!!
    } else { //public
        theClass.publicVarMethods += "Inner" + theClass.constructorName + ".prototype." + name + " = " + value + "\r\n\r\n\t";
    }
}

//function to define a string for defining fields on a property, defines default values in case you don't want to specify write, configure or enumerate (in that order)
//obj and val parameters are mandatory
//in a seperate function to make it more legible
function defineField(obj, name, val, write, configure, enumerate){
    return "Object.defineProperty(" + obj + ", '" + name + "', {\r\n\t" +
        "value: " + val + ",\r\n\t" +
        "enumerable: " + (enumerate ? enumerate : true) + ",\r\n\t" +
        "configurable: " + (configure ? configure : true) + ",\r\n\t" +
        "writable: " + (write ? write : true) + "\r\n\t" +
        "});\r\n\r\n";
}

//function to process ConstructorData
function processConstructorData(methodData, index, startBodyProper){
    methodData.bodyProper = theClass.methods[index].slice(startBodyProper).trim();
    methodData.bodyProper = methodData.bodyProper.slice(1);
    methodData.bodyProper = methodData.bodyProper.slice(0, -1).trim();
    theClass.constructor.push(methodData);
}

//helper function to fully process a method: change variables, change params, change declaration
//uses helper functions changeMethodVariables, findMethodVariables, processConstructorData & writeToConstructor
function processMethod(element, index, array){
    var endParams = theClass.methods[index].indexOf(")"),
        startParams = theClass.methods[index].indexOf("("),
        startBodyProper = theClass.methods[index].indexOf("{") - 1,
        declarationBeforeParams,
        params = "",
        i,
        methodData = {
            methodName: "",
            newParams: "",
            bodyProper: "",
            isPrivate: false,
            isStatic: false,
            final: false
        };

    //process parameters
    if ((endParams - startParams) > 1) {
        params = theClass.methods[index].slice(startParams + 1, endParams);
        params = params.trim();
        params = params.split(",");
        for (i = 0; i < params.length; i++) {
            params[i] = params[i].trim().split(" ");
            if(params[i][1]){
                methodData.newParams += params[i][1].trim() + ", ";
            }
        }
        methodData.newParams = methodData.newParams.slice(0, -2);
    }

    //process declaration
    declarationBeforeParams = theClass.methods[index].slice(0, startParams);
    declarationBeforeParams = declarationBeforeParams.trim().split(" ");
    methodData.methodName = declarationBeforeParams[declarationBeforeParams.length - 1].trim();

    for (i = 0; i < declarationBeforeParams.length; i++) {
        switch (declarationBeforeParams[i]) {
            //public, protected, returntype (any), abstract hebben geen nut in javascript: zijn dus betekenisloos.  Multithreading verloopt op een volkomen
            //andere manier, waardoor het ook niet nuttig is om daar iets mee te doen voorlopig => zou een onderzoeksproject zijn op zich
            //TODO: nog te bekijken: native, strictfp, transient, volatile
            case "private":
                methodData.isPrivate= true;
                break;
            case "static":
                methodData.isStatic = true;
                break;
            case "final":
                methodData.final = true;
                break;
            default:
                break;
        }
    }

    //process the body variables
    if (methodData.methodName === theClass.constructorName) {
        processConstructorData(methodData, index, startBodyProper);
        return theClass;
    } else {
        methodData.bodyProper = theClass.methods[index].slice(startBodyProper).trim();
    }

    //processForLoops
    if(containsFors(methodData.bodyProper)){
        methodData.bodyProper = translateForLoop(methodData.bodyProper);
    }

    methodData.bodyProper = changeMethodVariables(search.findMethodVariables(methodData.bodyProper), methodData.bodyProper);
    theClass.methods[index] = "function " + methodData.methodName + "(" + methodData.newParams + ")" + methodData.bodyProper;
    theClass.methodData = [];
    theClass.methodData[index] = methodData;
    writeToConstructor(theClass.methods[index], methodData.bodyProper, methodData.methodName, methodData.isPrivate, methodData.isStatic, methodData.final);
}

//helper function for processMethod to change the type in a variable declaration to var
function changeMethodVariables(indices, method){
    var i = 0, //variable to increase the performance of the for loop
        length = indices.length, //variable to increase the performance of the for loop
        counter = 0, //variable to offset the startindex with
        partFirst = "", //variable to store the first part of the method
        partFrom = "", //variable to store the part of the method in starting at the variabele declaration till the end of the method
        type = ""; //variable to store the type of the variable for easy reference in two statements

    for (i; i < length; i++) {
        partFirst = method.slice(0, indices[i] - counter); //store the first part of the string for easy concatenation
        partFrom = method.slice(indices[i] - counter); //slice out the part of the string beginning at the variable declaration
        type = partFrom.slice(0, partFrom.search(/\s/)); //store the type of the variable for easy use in the next two statements
        partFrom = partFrom.replace(type, "var");//replace the type by var
        counter += type.length - 3; //augment counter to offset replacing the type by var;
        method = partFirst + partFrom; //reassemble method so it's usable again for the next variable
    }

    return method;
}

//function to remove comments in the code
exports.removeComments = function removeComments(input){
//      input = input.replace(/(\/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+\/)|(\/\/.*)/gm);
    input = input.replace(/\/\*([^*]|[\r\n]|(\*([^\/]|[\r\n])))*\*\//g, "")
        .replace(/[^:]\/\/.*/g, ""); //remove multiline comments and then single line comments

    return input;
};

//function to process the class declaration
exports.processClassDeclaration = function processClassDeclaration(classObject){
    theClass = classObject;
    var declaration = theClass.declaration,
        implement = theClass.declaration.indexOf("implements"),
        extend = theClass.declaration.indexOf("extends"),
        extendDeclaration,
        implementDeclaration,
        i;

    //process extends part of the declaration
    if (extend > 0) {
        extendDeclaration = (extend > implement) ? declaration.slice(extend).trim() : declaration.slice(extend, implement).trim();
        extendDeclaration = extendDeclaration.split(" ");
        theClass.superclass = extendDeclaration[1];
    }

    //process implements
    if (implement > 0) {
        theClass.interfaces = [];
        implementDeclaration = declaration.slice(implement).trim().split(",");
        for (i = 0; i < implementDeclaration.length; i++) {
            implementDeclaration[i] = implementDeclaration[i].split(" ");
            theClass.interfaces[i] = implementDeclaration[i][1].trim();
        }
    }

    //split declaration into an array of words
    if (extend > 0) {
        declaration = declaration.slice(0, extend).trim().split(" ");
    } else if (implement > 0) {
        declaration = declaration.slice(0, implement).trim().split(" ");
    } else {
        declaration = declaration.trim().split(" ");
    }

    //process modifiers & assign className + constructorName
    theClass.className = declaration[declaration.length - 1].trim();
    createConstructorName();
    for (i = 0; i < declaration.length; i++) {
        switch (declaration[i]) {
            //public, protected, abstract zijn onbestaand in javascript: zijn dus betekenisloos.
            case "private":
                theClass.isPrivate= true;
                break; //enkel voor inner classes
            case "static":
                theClass.isStatic = true;
                break; //enkel voor inner classes
            case "final":
                theClass.final = true;
                break;  //cannot be subclassed
            default:
                break;
        }
    }

    return theClass;
};

//function to process field declarations
//uses two inline helper methods and also writeToConstructor + prepareFields
exports.processFields = function processFields(classObject){
    theClass = classObject;
    var splitByLine = prepareFields(theClass.fieldDeclarations.split("\r\n"));

    function handleFields(element){
        var firstEquals = element.content.indexOf("="),
            declaration = element.content.slice(0, firstEquals).trim(),
            declarationObject = {
                value: element.content.slice(firstEquals + 1).trim(),
                type: "",
                name: "",
                isPrivate: false,
                isStatic: false,
                final: false
            };

        function checkDeclarationModifiers(element){
            switch(element){
                //public, protected, abstract zijn onbestaand in javascript: zijn dus betekenisloos.
                case "private":
                    declarationObject.isPrivate= true;
                    break;
                case "static":
                    declarationObject.isStatic = true;
                    break;
                case "final":
                    declarationObject.name.toUpperCase();
                    declarationObject.final = true;
                    break;
                default:
                    break;
            }
        }//TODO refactoren samen met gelijkaardige code zodat dit maar 1 keer voorkomt in de code

        declaration = declaration.split(" ");
        declarationObject.name = declaration[declaration.length - 1];
        declarationObject.type = declaration[declaration.length - 2];
        declaration.forEach(checkDeclarationModifiers);
        //TODO inbouwen verwerking typesoort!
        declaration = "var " + declarationObject.name + " = " + declarationObject.value;
        writeToConstructor(declaration, declarationObject.value, declarationObject.name, declarationObject.isPrivate, declarationObject.isStatic, declarationObject.final);
    }

    splitByLine.forEach(handleFields);
    // TODO indien array/double/long/float/decimal: verwerken met Tim's functies

    return theClass;
};

//function to enumerate over all methods of a class and then process each in turn
exports.processMethods = function processMethods(classObject){
    theClass = classObject;
    theClass.methods.forEach(processMethod);
    return theClass;
};

//function to translate begin-tags of JSP structures
exports.translateAllBeginTags = function translateAllBeginTags(code){
    if(search.getMatches(/<c:if/g, code) > 0){
        code = translateBeginTag(code, /<c:if/g, "<c:if", "if");
        console.log('translated IF');
    }
    if(search.getMatches(/<c:forEach/g, code) > 0){
        code = translateBeginTag(code, /<c:forEach/g, "<c:forEach", "forEach");
        console.log('translated FOREACH');
    }
    if(search.getMatches(/<%@ include/g, code) > 0){
        code = translateBeginTag(code, /<%@ include/g, "<%@", "include");
        console.log('translated INCLUDE');
    }
    if(search.getMatches(/<jsp:include/g, code) > 0){
        code = translateBeginTag(code, /<jsp:include/g, "<jsp:include", "jspInclude");
        console.log('translated JSP-INCLUDE');
    }
    if(search.getMatches(/<c:when/g, code) > 0){
        code = translateBeginTag(code, /<c:when/g, "<c:when", "when");
        console.log('translated WHEN');
    }

    return code;

    function translateBeginTag(code, regex, jspTagName, jsTagName){
        var codeToTranslate = code,
            translatedCode = '',
            totalOccurrences = search.getMatches(regex, codeToTranslate);

        while(totalOccurrences !=0)
        {
            var beginIndex = search.getBeginIndexOfStructure(jspTagName, codeToTranslate),
                endIndex = search.getEndIndexOfStructure(jsTagName, codeToTranslate),
                structure = search.getStructure(beginIndex, endIndex, codeToTranslate);

            codeToTranslate = moveUnusedCode(beginIndex, codeToTranslate); //removes the 'useless' code from codeToTranslate to translatedCode
            translatedCode += translateStructure(jsTagName, structure); // adds the translated structure to translatedCode
            codeToTranslate = codeToTranslate.replace(structure, ""); //remove original structure from codeToTranslate
            totalOccurrences--; //ensure the loop ends
        }

        return translatedCode + codeToTranslate;
        //////////////////////////////////////////////////////
        //HELPER FUNCTION

        //moves "not to be translated"-code to translatedCode
        //removes "not to be translated"-code from codeToTranslate
        function moveUnusedCode(endIndex, codeToSlice)
        {
            var removedCode = codeToSlice.slice(0, endIndex);
            translatedCode += removedCode; //move unused code to translatedCode

            return codeToSlice.replace(removedCode, ""); //return after removing the unused code from codeToTranslate
        }
    }
};

//translates all end-tags that never changes, no matter what the situation
//CODE is the complete .jsp-file
exports.translateNonChangeableTags = function translateNonChangeableTags(code){
    //END-TAGS
    return code.replace(/<\/c:if>/g, '<% } %>') //translates all </c:if>-end-tags to <% } %>
        .replace(/<\/c:forEach>/g, '<% }) %>') //translates all </c:forEach>-end-tags to <% } %>
        .replace(/<\/c:when>/g, '<% } %>') //CONVERT WHEN-end-tag to IF-end-tag
        .replace(/<c:otherwise>/g, '<% else{ %>') //CONVERT <c:otherwise></c:otherwise> to ELSE-structure - first part
        .replace(/<\/c:otherwise>/g, '<% } %>') // second part
        .replace( /<\/c:forTokens>/g, '<% }) %>')
        .replace( /<%--/g, '<%#')    //translates the begintags of all commented code
        .replace( /--%>/g, '%>');    //translated the endtags of all commented code
};

exports.deleteOverflowingCode = function deleteOverflowingCode(code){
    return code.replace(/<c:choose>/g, "") //delete begin of choose tags
        .replace(/<\/c:choose>/g, "") //delete end of choose tags
        .replace(/<%@\spage.*%>/g, "") //delete code starting with <%@ page and ending with %>
        .replace(/<%@\staglib.*%>/g, "") //delete code starting with <%@ taglib and ending with %>
        .replace(/\r?\n|\r/, "") //removes newlines at the beginning of the code
        .replace(/<jsp:param.*\/>/g,"") //delete left-over code from jsp:include => <jsp:param name="title" value="${page.title}"/>
        .replace(/<\/jsp:include>/g, "") //</jsp:include>
        .replace(/<\/jsp:forward>/g, ""); //deletes the endtag of "forward", not needed in EJS
};

exports.translateAllVariables = function translateAllVariables(code){
    var translatedCode = "",
        codeToTranslate = code,
        totalOccurrencesVariables = search.getMatches(/\$\{/g, codeToTranslate);

    //translate all occurrences of variables
    while(totalOccurrencesVariables !=0){
        var beginIndexVar = search.getBeginIndexOfStructure("${", codeToTranslate),
            endIndexVar = search.getEndIndexOfStructure("${", codeToTranslate),
            variable = search.getStructure(beginIndexVar, endIndexVar, codeToTranslate);

        codeToTranslate = moveUnusedCode(0, beginIndexVar, codeToTranslate); //removes the 'useless' code from codeToTranslate to translatedCode
        translatedCode += translateVariable(variable); //adds the translated structure to translatedCode
        codeToTranslate = codeToTranslate.replace(variable, ""); //remove original if structure from codeToTranslate
        totalOccurrencesVariables--;
    }

    return translatedCode + codeToTranslate;
    /////////////////////////////////////////////////////////
    //FUNCTION
    //moves unused code to translatedCode
    function moveUnusedCode(beginIndex, endIndex, code){
        var removedCode = code.slice(beginIndex, endIndex);
        translatedCode += removedCode; //move unused code to translatedCode

        return code.replace(removedCode, ""); //return after removing the unused code from codeToTranslate
    }

    //translates the variable returned
    function translateVariable(variableStructure){
        return variableStructure.replace(/\$\{/, '<%= ')
            .replace(/\}/, '%>');
    }
};

