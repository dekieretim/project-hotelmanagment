/**
 * Created by Badlapje on 8/10/14.
 * This is the main file in the module which uses the other files as necessary.
 */
var split = require('./split'),
    change = require('./change'),
    logger = require('../utils/logger'),
    theClass = {
        imports: "",
        declaration: "",
        body: "",
        isStatic: false,
        isPrivate: false,
        final: false,
        superclass: false,
        interfaces: false,
        methodData: [],
        methods: [],
        parameters: "" ,
        publicVarMethods: "",
        privateStatic: "",
        forInnerConstructor: "",
        constructor: []
    };

//function to extend the prototype if necessary
//helperfunction for createConstructor
function extendPrototype(constructorName, superclass){
    return "\tInner" + constructorName + ".prototype = Object.create(" + superclass + ".prototype, {\r\n\t\t" +
        "constructor: {\r\n\t\t\t" +
        "configurable: true,\r\n\t\t\t" +
        "enumerable: true, \r\n\t\t\t" +
        "value: Inner" + constructorName + ",\r\n\t\t\t" +
        "writable: true\r\n\t\t" +
        "}\r\n\t";
}

//function to create a constructor in Java
//uses the helper function defineInnerConstructor
function createConstructor(){
    var constructorName = theClass.constructorName,
        constructorString = "var " + constructorName + " = " + "(function(){\r\n\t"; //open declaration

    constructorString += theClass.privateStatic + "\r\n"; //voeg alle private & static variables toe
    constructorString += defineInnerConstructor(constructorName, theClass.parameters); //voeg Inner Constructor toe met private vars/methods en constructorinhoud

    if (theClass.superclass) {
        constructorString += extendPrototype(constructorName, theClass.superclass) + "});\r\n\r\n\t";
    } //indien superklasse definieer het prototype als de superklasse

    constructorString += theClass.publicVarMethods + "\r\n"; //voeg public vars & methods toe
    return constructorString + "})();";
}

//function to enforce the use of new when calling a constructor (seperate function to make it more easily legible)
//helper function for defineInnerConstructor, uses enforceNew as a helper function
function enforceNew(constructorName, parameters){
    return "if(!(this instanceof " + constructorName + ")){ " +
        "\r\n\t\t\t" + "return new " + constructorName + "(" + (parameters ? parameters : "") + ");\r\n\t\t }" +
        "\r\n";
}

//function to define the inner constructor
//helper function for createConstructor
function defineInnerConstructor(constructorName, parameters){
    var constructorData = "",
        length = theClass.constructor.length;
    if (length >= 1) {
        constructorData = "\t" + theClass.constructor[0].bodyProper + "\r\n";
        for (var i = 0; i < length; i++) {
            if (theClass.constructor[i].length > theClass.constructor[i - 1]) {
                constructorData = "\t" + theClass.constructor[i].bodyProper + "\r\n";
            }
        }
    }
    return "\tfunction Inner" + constructorName + "(" + theClass.parameters + "){\r\n\t\t" + enforceNew(constructorName, parameters) + constructorData + theClass.forInnerConstructor + "\t}\r\n\r\n";
}

//only exposed function: translates a Java file to JS
exports.translateJavaToJS = function translateJavaToJS(javaString){
    if (typeof javaString == "string")
    {
        var stringBeingTranslated = change.removeComments(javaString);
        split.splitClass(stringBeingTranslated, theClass);
        change.processClassDeclaration(theClass);
        split.splitBodyInMethods(theClass);
        change.processMethods(theClass);
        change.processFields(theClass);
        return createConstructor();
    } else throw Error("The parameter passed was not a string.");
};

exports.translateJSPToEJS = function translateJSPToEJS(jspCode){
    if(typeof jspCode == "string")
    {
        var codeToTranslate = jspCode;
        codeToTranslate = change.deleteOverflowingCode(codeToTranslate);
        codeToTranslate = change.translateAllBeginTags(codeToTranslate);
        codeToTranslate = change.translateNonChangeableTags(codeToTranslate);

        return change.translateAllVariables(codeToTranslate);
    } else {
        logger.log("The parameter passed was not a string.");
        throw error("An incorrect parameter was passed to translateJSP: it's type was not string.");
    }
};