/**
 * Created by Badlapje on 8/10/14.
 */

//helper function to search for the ending brace of a method
exports.searchMethodEnd = function searchMethodEnd(indicesOpenBrace, indicesClosingBrace){
    var open = 0,
        close = 0,
        openLength = indicesOpenBrace.length,
        closeLength = indicesClosingBrace.length,
        returnObject = { stop: false };

    if (indicesOpenBrace.length === 1) {
        returnObject.endIndex = indicesClosingBrace[0];
        returnObject.stop = true;
        return returnObject;
    }

    do {
        if (indicesOpenBrace[open] > indicesClosingBrace[close]) {
            if (close + 1 < closeLength) {
                close++;
            }
        } else if (open + 1 == openLength) { //if last method
            returnObject.endIndex = indicesClosingBrace[closeLength - 1];
            returnObject.stop = true;
            return returnObject;
        } else {//if open is smaller then close
            if (open + 1 < openLength) {
                open++;
            }
        }
    } while (open !== close);

    returnObject.endIndex = indicesClosingBrace[close - 1];
    returnObject.indicesOpenBrace = indicesOpenBrace.slice(open);
    returnObject.indicesClosingBrace = indicesClosingBrace.slice(close);

    return returnObject;
};

//helper function to get an array of indices containing all indexes where a regex matches a certain string
exports.getIndices = function getIndices(regex, theString){
    var result, indices = [];
    while ((result = regex.exec(theString))) {
        indices.push(result.index);
    }

    return indices;
};

//function to get an array of indexes of where in a method variables are declared
exports.findMethodVariables = function findMethodVariables(method){
    return exports.getIndices(/\b(?!(return|new|package|import|where|set|and|on))[\w\[\]<>]+ [\w]+(;| *=)/g, method);
};

exports.getMatches = function getMatches(regex, code){
    var matches = code.match(regex);
    return (matches != null) ? matches.length : 0;
};

//returns the structure from beginIndex till endIndex
exports.getStructure = function getStructure(beginIndex, endIndex, code){
    return code.slice(beginIndex, endIndex);
};

//returns the begin index of any structure defined in the function
exports.getBeginIndexOfStructure = function getBeginIndexOfStructure(structure, code){
    return code.indexOf(""+structure+"");
};

//returns the end index of any structured defined in the function
exports.getEndIndexOfStructure = function getEndIndexOfStructure(structureTag, code){
    var endIndexOfStructure;
    switch(structureTag)
    {
        case 'if': endIndexOfStructure = returnEndIndex(code, "<c:if", '}">', 3); break;
        case 'forEach': endIndexOfStructure = returnEndIndex(code, "<c:forEach", '}">', 3); break;
        case 'include': endIndexOfStructure = returnEndIndex(code, "<%@ include", '.jsp"%>', 7); break;
        case 'jspInclude': endIndexOfStructure = returnEndIndex(code, "<jsp:include", ">", 1); break;
        case 'when': endIndexOfStructure = returnEndIndex(code, "<c:when", ">", 2); break;
        case 'variable': endIndexOfStructure = returnEndIndex(code, "${", "}", 1); break;
    }

    return endIndexOfStructure;

    //HELPER FUNCTION
    function returnEndIndex(code, startTag, endPart, offset){
        var beginIndex = code.indexOf(startTag);
        return code.indexOf(endPart, beginIndex) + offset;
    }
};

