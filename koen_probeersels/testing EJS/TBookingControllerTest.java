package hotel;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import rWorks.TApplication;
import rWorks.TController;
import rWorks.TSQL;
import rWorks.TRecord;


public class TBookingController extends THotelController {
	private static final long serialVersionUID = 1L;

	private static final boolean kDoCash = false;

	public String testje = "1 = 2";
	public String test2 = "dit is ongelooflijk; hoe erg dat het is";
	public String test3 = "dit is nog erger; want 1 = 3";
	public String zouhet = "dit is een eerste deel van de string"
	+ "dit is het tweede deel van de string"
	+ "dit is = 2 het derde deel van de string";


	// Class methods exceptionally at the bottom, sorry.
	
	// To the reader
	//  - RoomService = the service line created for the rooms (has nothing to do with 'roomservice')
	//  - The above service has a negative 'extra'-id -> -1 for 1 room, -2 for 2 rooms.
	//
	
	/*------------*/
	/* Dispatcher */
	/*------------*/
	public String HandleRequest() {
		
		int anId = this.getParam("id", -1);
		if (this.isRequest("Revert")) this.setRequest("edit");

		for(int i = 0; i < jefke.length(); i++){
			blahblah
		}		

		// Ajax requests
		if (this.isRequest("getPrice")) {
			this.returnPrice();
			return null;
			
		} else if (this.isRequest("findPackages")) {
			this.genPackageList(anId);
			return "/views/BO/package-list.jsp";
			
		} else if (this.isRequest("assign")) {
			this.genAssignSheet();
			return "/views/BO/service-assign.jsp";
		}
		
		
		// Search operations
		if (this.isRequest("Search")) {
			this.search();
			
		} else if (this.isRequest("Open Payments")) {
			this.openPayments();

		} else if (this.isRequest("Open Invoices")) {
			this.openInvoices();

		} else if (this.isRequest("Departures")) {
			this.departures();
			
		} else if (this.isRequest("Arrivals")) {
			this.arrivals("arrival >= date(now())");
		
		/*
		} else if (this.isRequest("Today")) {
			this.arrivals("arrival = date(now())");
		} else if (this.isRequest("Tomorrow")) {
			this.arrivals("arrival = date_add(date(now()), interval 1 day)");
		} else if (this.isRequest("7 days")) {
			this.arrivals("arrival between date(now()) and date_add(date(now()), interval 7 day)");
		*/
		}
		
		
		// Services
		if (this.isRequest("Add")) {
			this.addService();
			this.setRequest("edit");
			
		} else if (this.isRequest("Delete")) {
			this.deleteService();
			this.setRequest("edit");
			
		} else if (this.isRequest("Assign Service")) {
			this.AssignServices();
			this.setRequest("edit");
		}
		
		// Payments, Invoicing
		if (this.isRequest("Request")) {
			this.askPayment(anId);
			this.setRequest("edit");
			
		} else if (this.isRequest("Make Invoice")) {
			TInvoiceController.makeInvoice(this, anId);
			this.setRequest("edit");
			
		} else if (this.isRequest("Accept")) {
			this.acceptPayment(anId);
			this.setRequest("edit");
			
		}

		// Packages
		if (this.isRequest("Select")) {
			this.selectPackage(anId);
			this.setRequest("edit");
		}
		
		// Bookings
		if (this.isRequest("Save")) {
			anId = this.saveBooking(anId);
			// only load booking from database if everything went well (no occupation problem)
			if (anId > 0) {
				this.getBooking(anId);
				this.getServices(anId);
				this.getInvoices(anId);
			}
			this.getRefs();
			this.setRequest("edit");
			
		} else if (this.isRequest("New Booking")) {
			this.newBooking();
			this.getRefs();
			this.setRequest("edit");
			
		} else if (this.isRequest("edit")) {
			this.getRefs();
			this.getBooking(anId);
			this.getServices(anId);
			this.getInvoices(anId);
			
		} else if (this.isRequest("Cancel Booking")) {
			this.deleteBookingAndServices(anId);
			
		}
		
		
		return "/views/BO/bookings.jsp";
	}
	
	
	void getRefs() {
		TCashController.getDrawers(this, false);
		this.setVar("rooms", TRoomController.addUse(new TSQL(this, "select id, name, nr, used, roomtype from rooms order by nr").getList()));
		this.setVar("extras", TExtraController.getList(this, "en"));
	}
	
	
	/*----------*/
	/* Services */
	/*----------*/
	void addService() {
		int anId = this.getParam("id", -1);
		int extra = this.getParam("extra", -1);
		if (extra != -1) {			
			new TSQL(this, "insert into services (pax, nr, booking, extra, price) select ?, ?, ?, id, price * ? from extras where id = ?")
			  .setValue( this.getParam("pax", 1) )
			  .setValue( this.getParam("nr", 1) )
			  .setValue( anId )
			  .setValue( this.getParam("pax", 1) * this.getParam("nr", 1) )
			  .setValue( extra )
			  .execute();
			this.updateTotal( anId );
			this.feedBack(true, "Extra service added - price updated");	
		} else {
			this.feedBack(false, "No valid extra service selected.");	
		}
	}
	
	void deleteService() {
		int service = this.getParam("service", -1);
		if (service != -1) {
			// if it's a new, not yet invoiced, service, delete it
			new TSQL(this, "delete from services where invoice = 0 and id = ?").setValue(service).execute();

			// if the service is already invoiced, add a credit
			new TSQL(this, "insert into services (booking, extra, nr, pax, price) " +
					"select booking, extra, nr, pax, - price from services where id = ? and invoice != 0").setValue(service).execute();
			
			// delete the associated assignments, if any
			new TSQL(this, "delete from assignments where service = ?").setValue(service).execute();

			this.updateTotal( this.getParam("id", -1) );
			this.feedBack(true, "Extra service deleted - price updated.");	
		} else {
			this.feedBack(false, "No valid extra service selected.");	
		}
	}
	
	void changeService() {
		// not called anymore: each change needs an invoice so -> delete / add...
		// warning 1, should it be used: invoices !!
		// warning 2, should it be used: assignments !!
		int extra = this.getParam("extra", -1);
		int service = this.getParam("service", -1);
		if ((extra != -1) && (service != -1)){			
				new TSQL(this, "update services set pax = ?, nr = ?, extra = ?, price = (select price * ? from extras where id = ?) where id = ?")
				  .setValue( this.getParam("pax", 1) )
				  .setValue( this.getParam("nr", 1) )
				  .setValue( extra )
				  .setValue( this.getParam("pax", 1)  * this.getParam("nr", 1) )
				  .setValue( extra )
				  .setValue( service )
				  .execute();
				this.updateTotal( this.getParam("id", -1) );
				this.feedBack(true, "Extra service updated - price updated.");	
		} else {
			this.feedBack(false, "No valid extra service selected.");	
		}
	}
	
	void updateRoomService(int theBooking, double thePrice) {
		// not called anymore: each change needs an invoice so -> delete / add... -- 12/10/2012
		new TSQL(this, "update services set price = ? where booking = ? and extra < 0 and price > 0")
			.setValue(thePrice).setValue(theBooking)
			.execute();
	}

	void addObligatories(long theNrNights, int thePax, int theId, boolean isExisting) {
		if (isExisting) {
			// don't touch services with price < 0 -> these need to be credited.
			
			// if not yet invoiced -> just delete
			new TSQL(this, "delete from services where booking = ? and invoice = 0 and price > 0 and extra in (select id from extras where kind in ('W','V'))")
				.setValue(theId)
				.execute();
			
			// if invoiced -> insert as "credits"
			new TSQL(this, "insert into services (booking, extra, nr, pax, price) select booking, extra, nr, pax, - price from services where booking = ? and price > 0 and invoice != 0 and extra in (select id from extras where kind in ('V','W'))")
				.setValue(theId)
				.execute();
		}
		
		// V = obligatory  per pax x nights
		if (thePax * theNrNights > 0)
			new TSQL(this, "insert into services (booking, extra, nr, pax, price) select ?, id, ?, ?, ? * price from extras where kind = 'V'")
				.setValue(theId)
				.setValue(theNrNights)
				.setValue(thePax)
				.setValue(thePax * theNrNights)
				.execute();
			
		// W = obligatory per pax
		if (thePax > 0)
			new TSQL(this, "insert into services (booking, extra, nr, pax, price) select ?, id, ?, ?, ? * price from extras where kind = 'W'")
				.setValue(theId)
				.setValue(theNrNights)
				.setValue(thePax)
				.setValue(thePax)
				.execute();
			
	}
	
	void getServices(int theId) {
		ArrayList<TRecord> services = new TSQL(this, "select services.*, name_en as name, kind, extras.asset " +
				"from services, extras where services.extra = extras.id " +
				" and services.booking = ? order by extras.sortorder")
			.setValue(theId)
			.getList();
		
		for (TRecord S: services) {
			ArrayList<TRecord> assignments = new TSQL(this, 
				"select * from assignments, assets where assets.id = assignments.asset and service = ? order by at, slot").setValue(S.getI("id")).getList();
			S.put("assignments", assignments);
			for (TRecord A: assignments) {
				A.put("slotname", A.get("slot" + A.get("slot")));
			}
		}
		this.setVar("services", services);
	}
	
	void genAssignSheet() {
		Date arr = this.getParam("arrival", new Date(), "dd-MM-yyyy");
		Date dep = this.getParam("departure", new Date(), "dd-MM-yyyy");
		
		//int theNr = this.getParam("nr", 0);
		//int thePax = this.getParam("pax", 0);
		int theAsset = this.getParam("asset", 0);
		int theService = this.getParam("service", 0);
		
		// get date list from occupancies from arr to dep -- joined with assignments
		this.setVar("days", 
			new TSQL(this, "select distinct O.at,A.nr,A.slot from occupancies O left join assignments A on A.at = O.at and A.service = ? " +
				"where O.at >= date(?) and O.at <= date(?) order by O.at").setValue(theService).setValue(arr).setValue(dep).getList());

		this.setVar("slots", TAssetController.getSlots(this, theAsset));
	}
	
	void AssignServices() {
		int service = this.getParam("service", 0);
		int asset = this.getParam("asset", 0);
		
		// put default at day after arrival
		Date arrival = this.getParam("arrival", new Date(), "dd-MM-yyyy");
		arrival = TCalendarController.addDays(arrival, 1);
		
		// delete old assignments
		new TSQL(this, "delete from assignments where service = ?").setValue(service).execute();
		
		// Add new assignments
		int cnt = 0;
		Date at = this.getParam("A"+cnt, null, "dd-MM-yyyy");
		while (at != null) {
			// insert
			int nr = this.getParam("N"+cnt, -1);
			if (nr > 0)
				new TSQL(this, "insert into assignments (service, at, slot, asset, nr) values (?, ?, ?, ?, ?)")
					.setValue(service)
					.setValue( at )
					.setValue( this.getParam("S"+cnt, 0) )
					.setValue( asset )
					.setValue( nr )
					.execute();
			
			// go next 
			cnt++;
			at = this.getParam("A"+cnt, null, "dd-MM-yyyy");
		}
	}

	
	/*----------*/
	/* Packages */
	/*----------*/
	void genPackageList(int theBooking) {
		Date arrival = this.getParam("arrival", new Date(), "dd-MM-yyyy");
		String aLan = this.getParam("language", "nl");
		
		ArrayList<TRecord> packages = TPackageController.searchPackages(this, arrival, aLan);
		
		this.setVar("packages", packages);
		this.setVar("id", theBooking);
		this.setVar("arrival", arrival);
		this.setVar("language", aLan);
	}
	
	void selectPackage(int theBooking) {
		int aRoom = this.getParam("room", 0);
		int aValid = this.getParam("valid", 0);

		if (aRoom != 0) {
		String aLan = this.getParam("language", "nl");
			TRecord pack = new TSQL(this, 
				"select packagevalids.*, name_" + aLan + " as name, price, roomtype, pax, nights " +
				"from packagevalids left join packages on packages.id = packagevalids.package " +
				"where packagevalids.id = ?")
				.setValue( aValid )
				.getRecord();
			
			// delete all services and assignments -- not the booking
			deleteBookingAndServices(this, theBooking, false);
			
			String packName = pack.getS("name");
			double packPrice = pack.getD("price");
			Date arrival =  this.getParam("arrival", new Date(), "dd-MM-yyyy");
			int pax = pack.getI("pax", 0);
			int nights = pack.getI("nights", 0);
			Date departure = TCalendarController.addDays( arrival, nights );
	
			TApplication.printLog("TBookingController.selectPackage: " + packName + ", at = " + packPrice + ", room = " + aRoom + ", from = " + arrival + ", to = " +  departure);

			// add services (obligatories and included)
			this.addObligatories( nights, pax, theBooking, false );
			double servicesPrice = this.addPackageServices(theBooking, pack.getI("package"));
			double restPrice = packPrice - servicesPrice;
	
			// add the "room" services
			TBookingController.genRoomService(this, theBooking, 1, nights, pax, restPrice, false);
	
			// mark the rooms as occupied in this period
			this.updateOccupancy(true, theBooking, aRoom, 0, arrival, departure);
			
			// update the booking
			this.updateBookingFromPackage(departure, packName, restPrice, pax, aRoom, theBooking);
			this.feedBack(true, "Booking " + BNr(theBooking) + " updated with package '" + packName + "'.");
			
		} else
			this.feedBack(false, "No room selected");
	}
	
	void updateBookingFromPackage(Date departure, String packName, double restPrice, int pax, int theRoom, int theBooking) {
		new TSQL(this, "update bookings set `departure` = date(?), `arrangement` = ?, " +
				"`R1price` = ?, `R1pax` = ?, `R1room` = ?, `R2price` = 0.0, `R2pax` = 0, `R2room` = 0, " +
				" `total` = ifnull((select sum(price) from services where booking = ?),0)" +
				" where id = ?")
			.setValue( departure )
			.setValue( packName )
			.setValue( restPrice )
			.setValue( pax )
			.setValue( theRoom )
			.setValue( theBooking )
			.setValue( theBooking )
			.execute();
	}
	
	double addPackageServices(int theBooking, int thePackage) {
		ArrayList<TRecord> extras = new TSQL(this, 
			"select packageextras.*, extras.price from packageextras left join extras on packageextras.extra = extras.id where package = ?")
			.setValue(thePackage)
			.getList();
		
		double total = 0.0;
		for (TRecord E: extras) {
			int nr = E.getI("nr");
			int pax = E.getI("pax");
			double p = E.getD("price") * nr * pax;
			total += p;
			new TSQL(this, 
					"insert into services (booking, extra, nr, pax ,price) values (?, ?, ?, ?, ?)")
				.setValue(theBooking)
				.setValue( E.getI("extra") )
				.setValue( nr )
				.setValue( pax )
				.setValue( p )
				.execute();
		}
		return total;
	}
	
	
	/*----------*/
	/* Bookings */
	/*----------*/
	void updateOccupancy(boolean isNew, int theId, int theR1, int theR2, Date theArrival, Date theDeparture) {
		// Should be 1 transaction !!!
		if (! isNew)
			new TSQL(this, "update occupancies set booking = null where booking = ?")
				.setValue(theId)
				.execute();
		if (theR1 != 0)
			new TSQL(this, "update occupancies set booking = ? where room = ? and at >= date(?) and at < date(?)")
				.setValue(theId)
				.setValue(theR1)
				.setValue(theArrival)
				.setValue(theDeparture)
				.execute();
		if (theR2 != 0)
			new TSQL(this, "update occupancies set booking = ? where room = ? and at >= date(?) and at < date(?)")
				.setValue(theId)
				.setValue(theR2)
				.setValue(theArrival)
				.setValue(theDeparture)
				.execute();
		
		TRoomController.updateUsedCount(this);
	}
	
	void updateTotal(int theId) {
		new TSQL(this, 
			"update bookings set total = ifnull((select sum(price) from services where booking = ?),0) where id = ?")
		.setValue( theId )
		.setValue( theId )
		.execute();
	}
	
	
	void acceptPayment(int theId) {
		// cash payment methods are Ex, with x the id of a drawer
		String pm = this.getParam("payment", "U");
		String pmf = pm.substring(0, 1);
		
		int invoice = this.getParam("invoice", 0);
		new TSQL(this, "update invoices set open = 0, payment = ? where id = ?")
			.setValue( pmf )
			.setValue( invoice )
			.execute();
		
		double paid = new TSQL(this, "select price from invoices where id = ?").setValue(invoice).getValue(0.0);
		
		// add to paid (do we still need this??)
		new TSQL(this, "update bookings set paid = paid + ? where id = ?")
			.setValue(paid)
			.setValue(theId)
			.execute();

		if (TBookingController.kDoCash) {
			int drawer = 1;
			if (pmf.equalsIgnoreCase("E")) {
				if (pm.length() > 1) {
					String pmd = pm.substring(1, pm.length());
					drawer = Integer.parseInt(pmd);
				}
				long year = new TSQL(this, "select year(at) from invoices where id = ?").setValue(invoice).getValue(0L);
				TCashController.transaction(this, paid, drawer, Integer.parseInt((String) this.getRequest().getLogin().get("id")), 
				   "Payment invoice " + TInvoiceController.getINr((Long) year, invoice), invoice);
			}
		}
	}
	
	
	void sendPaymentRequest(String theTitle, String theFirstName, String theName, int theBNr, String theEmail, String theLan, double willPay) {
		String aBody = "Dear " + theTitle + " " + theFirstName + " " + theName + "\n\n" +
				"Thank you for your booking at the " + this.hotel + ".\n\n" +
				"You are kindly requested to pay the outstanding amount of " + String.format("%.2f", willPay) + " euro within \n" +
						" on account " + this.bank +
				" and with the mention of 'booking " + theBNr + "' within 7 days.\n\n" + 
				"To view your booking and invoice details, please click at the link below.\n" +
				"http://" + this.url + "/" + theLan + "/reservation/" + theBNr + "\n\n" +
				"Kind regards,\n\n" + this.hotel + "\n\n" +
				"T: " + this.phone + "\n" +
				"http://" + this.url + "\n" +
				"mailto:" + this.toEmail + "\n";

		this.sendMail(this.fromEmail, this.fromName, "Payment " + this.hotel, 
				aBody, 
				theEmail, theFirstName + " " + theName, 
				"", "", "", "");
	}

	
	void askPayment(int theId) {
		double aPrice = TBookingController.getTotal(this, "select sum(open) as total from invoices where booking = ?", theId);
		String email = this.getParam("email", "");
		if (email.isEmpty())
			this.feedBack(false, "No email address -> no payment request has been sent !!");
		else {
			this.sendPaymentRequest(this.getParam("title", ""), this.getParam("firstname", ""), this.getParam("name", ""), 
									BNr(theId), email, this.fRequest.getLanguage(), aPrice);
			this.feedBack(true,  "Payment request for booking " + BNr(theId) + " has been sent to " + email);
		}
	}
	

	
	void returnPrice() {
		Date arrival = this.getParam("arrival", new Date(), "dd-MM-yyyy");
		Date departure = this.getParam("departure", new Date(), "dd-MM-yyyy");
		int room = this.getParam("room", -1);
		int booking = this.getParam("booking", -1);
		
		double p = getPrice(arrival, departure, room, booking);
		if (p == -1.0) {
			this.fRequest.gen("NA,0.0");
		
		} else {
			DecimalFormat DF = new DecimalFormat("#0.00");	
			this.fRequest.gen("OK," + DF.format(p));
		}
	}
	
	
	double getPrice(Date theArrival, Date theDeparture, int theRoom, int theBooking) {
		TApplication.printLog("TBookingController.getPrice: room = " + theRoom + ", booking = " + theBooking + ", arrival = " + theArrival + ", departure = " + theDeparture);

		if (theDeparture.getTime() <= theArrival.getTime()) return 0.0;
		
		// get booking horizon
		int bh = TBookingController.getBookingHorizon(this, theArrival);
				
		// loop through dates, get price per period
		Date day = new Date(theArrival.getTime());
		double tot = 0.0;
		TSQL aSQL = new TSQL(this, "select price" + bh + " as price from prices, occupancies, rooms " +
				"where rooms.id = ? and occupancies.room = rooms.id and (occupancies.booking is null or occupancies.booking = ?)" +
				"  and prices.period = occupancies.period and prices.roomtype = rooms.roomtype and at = date(?)");
		while ( day.getTime() < theDeparture.getTime() ) {
			TRecord prR = aSQL.setValue(theRoom).setValue(theBooking).setValue(day).getRecord(false);
			
			if (prR == null) {
				aSQL.close();
				return -1.0;
			} else {
				tot += prR.getD("price");
				day = TCalendarController.addDays( day, 1);
			}
		}
		aSQL.close();
		
		return tot;
	}


	boolean available(Date theArrival, Date theDeparture, int theRoom, int theBooking) {
		// check if the room is available -- discard our booking (if any)
		double p = this.getPrice(theArrival, theDeparture, theRoom, theBooking);
		
		return (p > 0.0);
	}
	
	
	TRecord copyInput() {
		TRecord R = new TRecord();
		R.put("title", this.getParam("title", "") );
		R.put("firstname", this.getParam("firstname", "") );
		R.put("email", this.getParam("email", "") );
		R.put("phone", this.getParam("phone", "") );
		R.put("language", this.getParam("language", "") );
		R.put("name", this.getParam("name", "") );
		R.put("street", this.getParam("street", "") );
		R.put("zipcity", this.getParam("zipcity", "") );
		R.put("country", this.getParam("country", "Belgium") );
		R.put("VAT", this.getParam("VAT", "") );
		R.put("born", this.getParam("born", "") );
		R.put("bornat", this.getParam("bornat", "") );
		R.put("idnr", this.getParam("idnr", "") );
		R.put("note", this.getParam("note", "") );
		R.put("arrangement", this.getParam("arrangement", "") );
		R.put("arrival", this.getParam("arrival", new Date(), "dd-MM-yyyy") );
		R.put("departure", this.getParam("departure", new Date(), "dd-MM-yyyy") );
		R.put("R1price", this.getParam("R1price", 0.0) );
		R.put("R1pax", this.getParam("R1pax", 0) );
		R.put("R1room", this.getParam("R1room", 0) );
		R.put("R2price", this.getParam("R2price", 0.0) );
		R.put("R2pax", this.getParam("R2pax", 0) );
		R.put("R2room", this.getParam("R2room", 0) );
		R.put("total", this.getParam("total", 0.0) );
		R.put("paid", this.getParam("paid", 0.0) );
		R.put("checkedin", this.getParam("checkedin", "N") );
		R.put("checkedout", this.getParam("checkedout", "N") );
		R.put("arrivaltime", this.getParam("arrivaltime", "") );
		R.put("id", this.getParam("id", -1) );
		return R;
	}
	
	TSQL prepBookings(TSQL theSQL, Date arr, Date dep, double r1price, int r1pax, int r1, double r2price, int r2pax, int r2) {
		return theSQL
			.setValue( this.getParam("VAT", "") )
			.setValue( this.getParam("client", 0) )
			.setValue( this.getParam("note", "") )
			.setValue( this.getParam("arrangement", "") )
			.setValue( arr )
			.setValue( dep )
			.setValue( r1price )
			.setValue( r1pax )
			.setValue( r1 )
			.setValue( r2price )
			.setValue( r2pax )
			.setValue( r2 )
			.setValue( this.getParam("checkedin", "N") )
			.setValue( this.getParam("checkedout", "N") )
			.setValue( this.getParam("arrivaltime", "") )
			.setValue( this.getParam("iname", "") )
			.setValue( this.getParam("istreet", "") )
			.setValue( this.getParam("izipcity", "") )
			.setValue( this.getParam("origin", "Y") );
	}
	
	int saveBooking(int theId) {
		// check to see if we have all the data
		if (this.getParam("arrival") == null) {
			this.feedBack(false, "Could not complete the request");
			return theId;
		}
		TRecord R = copyInput();

		Date arr =  (Date) R.get("arrival");
		Date dep = (Date) R.get("departure");
		long nights = TCalendarController.nrDays(arr, dep);
		// TApplication.printLog("nights = " + nights + " ( " + dep.getTime() + " - " + arr.getTime() + " ) -> " 
		// 					  + (dep.getTime() - arr.getTime()) / TCalendarController.kOneDay);
		int r1 = (Integer) R.get("R1room");
		int r2 = (Integer) R.get("R2room");
		int r1pax = this.getParam("R1pax", 0);
		int r2pax = this.getParam("R2pax", 0);
		double r1price = this.getParam("R1price", 0.0);
		double r2price = this.getParam("R2price", 0.0);
		int rooms = ((r1 != 0) ? 1 : 0) + ((r2 != 0) ? 1 : 0);
		
		
		if ((r1 != 0) && (! this.available(arr, dep, r1, theId))) {
			this.feedBack(false, "First room is not available for this period");
			this.setVar("booking", R);
			return 0;
		}
		if ((r2 != 0) && (! this.available(arr, dep, r2, theId))) {
			this.feedBack(false, "Second room is not available for this period");
			this.setVar("booking", R);
			return 0;
		}
		
		if (theId == -1) {
			theId = prepBookings(new TSQL(this, "insert into bookings (`VAT`, `client`, " +
						"`note`, `arrangement`, `arrival`, `departure`, " +
						"`R1price`, `R1pax`, `R1room`, `R2price`, `R2pax`, `R2room`," +
						"`checkedin`, `checkedout`, `arrivaltime`,    `at`, " +
						"`iname`, `istreet`, `izipcity`, `origin`) values " +
						"(?, ?,  ?, ?, ?, ?,    ?, ?, ?, ?, ?, ?,  ?, ?, ?,   date(now()),   ?, ?, ?, ? )", true)
					, arr, dep, r1price, r1pax, r1, r2price, r2pax, r2)
				.executeGetKey();
			
			TBookingController.genRoomService(this, theId, rooms, nights, r1pax + r2pax, r1price + r2price, false);

			this.addObligatories( nights, r1pax + r2pax, theId, false );
			this.updateOccupancy(true, theId, r1, r2, arr, dep);
			
			this.feedBack(true, "Booking " + BNr(theId) + " created and obligatory services added.");
			
		} else {
			prepBookings(new TSQL(this, "update bookings set `VAT` = ?, client = ?, " +
					"`note` = ?, `arrangement` = ?, `arrival` = date(?), `departure` = date(?), " +
					"`R1price` = ?, `R1pax` = ?, `R1room` = ?, `R2price` = ?, `R2pax` = ?, `R2room` = ?," +
					"`checkedin` = ?, `checkedout` = ?, `arrivaltime` = ?," +
					"`iname` = ?, `istreet` = ?, `izipcity` = ?, `origin` = ? where id = ?")
				, arr, dep, r1price, r1pax, r1, r2price, r2pax, r2)
				.setValue( theId )
				.execute();
			
			if (this.nppChanged(nights, r1pax, r2pax))
				// nnp = Nr nights, Pax in room1, Pax in room2
				this.addObligatories( nights, r1pax + r2pax, theId, true );
			
			if (this.rradChanged(r1, r2, arr, nights)) {
				// substantial parameters are changes -> update occupancies, etc...
				// rrad = R1, R2, Arrival, Duration
				this.updateOccupancy(false, theId, r1, r2, arr, dep);
				TBookingController.genRoomService(this, theId, rooms, nights, r1pax + r2pax, r1price + r2price, true);
				
			} else if (this.ppChanged(r1price, r2price)){
				// added check -- 12/10/2012
				TBookingController.genRoomService(this, theId, rooms, nights, r1pax + r2pax, r1price + r2price, true);
			}
			
			
			this.feedBack(true, "Booking " + BNr(theId) + " updated");
		}
		
		this.updateTotal(theId);
		return theId;
	}
	
	void newBooking() {
		TRecord C;
		TRecord R = new TRecord();
		
		// check to see if we received a client id as prefiller
		int client = this.getParam("client", 0);
		if (client != 0) {
			C = new TSQL(this, "select * from clients where id = ?").setValue(client).getRecord();
			R.put("VAT", C.get("VAT", ""));
			R.put("iname", C.get("iname", ""));
			R.put("istreet", C.get("istreet", ""));
			R.put("izipcity", C.get("izipcity", ""));
		} else {
			C = new TRecord();
		}
		this.setVar("client", C);
		
		// prepare an empty booking record
		R.put("client", client);
		R.put("id", "-1");
		
		// check to see if we received some prefillers 
		Date anArrival = this.getParam("arrival", null, "yyyy-MM-dd");
		if (anArrival != null) R.put("arrival", anArrival);
		int aRoom = this.getParam("room", 0);
		if (aRoom != 0) { R.put("R1room", aRoom); R.put("R1pax", 2); }
		
		this.setVar("booking", R);
	}
	

	void getInvoices(int theId) {
		this.setVar("invoices", TInvoiceController.addINr(
				new TSQL(this, "select *, year(at) as year, if(open=0,'Y','N') as ok from invoices where booking = ? order by at")
					.setValue(theId)
					.getList()));
	}
	
	void getBooking(int theId) {
		TRecord B = new TSQL(this, "select bookings.*, r1.name as R1name, r2.name as R2name, datediff(departure, arrival) as nrdays," +
				" (select sum(invoices.price) from invoices where invoices.booking = bookings.id and open > 0) as open, " +
				" (select sum(services.price) from services where services.booking = bookings.id and invoice = 0) as topay " +
				"from bookings left outer join rooms r1 on R1room = r1.id " +
				" left outer join rooms r2 on R2room = r2.id " +
				"where bookings.id = ?")
			.setValue(theId)
			.getRecord();
		
		this.setVar("booking", addBNr(B));
		TClientController.getClient(this, B.get("client", 0), true);
		
		this.setChecksums(B);
	}
	
	void deleteBookingAndServices(int theId) {
		deleteBookingAndServices(this, theId, true);
	}
	
	
	// npp = Nrdays, Pax1, Pax2
	// rrad = Room1, Room2, Arrival, Departure (arr+nrdays)
	void setChecksums(TRecord B) {
		long npp = ((Long)B.get("nrdays") * 1000 + (Integer)B.get("R1pax")) * 1000 + (Integer)B.get("R2pax");
		this.setVar("npp", npp);
		// TApplication.printLog("npp -> " + npp + ", nrdays = " + (Long)B.get("nrdays") );
		
		long rrad = ((Integer)B.get("R1room") * 1000 + (Integer)B.get("R2room")) * 1000 + (Long)B.get("nrdays") + ((Date)B.get("arrival")).getTime();
		this.setVar("rrad",  rrad);
		
		long pp = (long)(B.get("R1price", 0.0) * 1000 + B.get("R2price", 0.0));
		this.setVar("pp",  pp);
	}
	boolean nppChanged(long nrdays, int R1pax, int R2pax) {
		long nppL = (nrdays * 1000 + R1pax) * 1000 + R2pax;
		String npp = this.getParam("npp");
		// TApplication.printLog("npp check: db = " + nppL + " - web = " + npp + " => " + ((Long) nppL).toString().equals(npp));
		return !((Long) nppL).toString().equals(npp);
	}
	boolean rradChanged(int R1room, int R2room, Date arrival, long nrdays) {
		long rradL = ((R1room * 1000) + R2room) * 1000 + nrdays + arrival.getTime();
		String rrad = this.getParam("rrad");
		// TApplication.printLog("rrad check: db = " + rradL + " - web = " + rrad + " => " + ((Long) rradL).toString().equals(rrad));
		return !((Long) rradL).toString().equals(rrad);
	}
	boolean ppChanged(double R1price, double R2price) {
		long ppL = (long)(R1price * 1000 + R2price);
		String pp = this.getParam("pp");
		return !((Long) ppL).toString().equals(pp);
	}
	
	
	
	/*----------------*/
	/* Search methods */
	/*----------------*/
	
	void search() {
		String q = this.getParam("q", ""); 
		int bNr = TApplication.asNum(q, -1);
		this.setVar("datelabel", "Arrival");
		
		if (bNr != -1) {
			this.setVar("bookings", addBNr(
				new TSQL(this, "select bookings.id, concat(clients.name,', ',clients.firstname) as name, paid, total, arrival as datevalue, checkedin, checkedout, " +
						" r1.nr as R1nr, r2.nr as R2nr " +
						"from bookings left outer join rooms r1 on R1room = r1.id  " +
						" left outer join rooms r2 on R2room = r2.id " +
						" left join clients on clients.id = bookings.client " +
						"where bookings.id = ? or bookings.id = ?")
				.setValue(remBNr(bNr))
				.setValue(bNr)
				.getList()));
		} else {
			this.setVar("bookings", addBNr(
				new TSQL(this, "select bookings.id, concat(clients.name,', ',clients.firstname) as name, paid, total, arrival as datevalue, checkedin, checkedout, " +
						" r1.nr as R1nr, r2.nr as R2nr " +
						"from bookings left outer join rooms r1 on R1room = r1.id " +
						" left outer join rooms r2 on R2room = r2.id " +
						" left join clients on clients.id = bookings.client " +
						"where concat(clients.firstname,' ',clients.name,' ',clients.firstname) like ? or bookings.iname like ? order by clients.name limit 200")
				.setValue("%"+q+"%")
				.setValue("%"+q+"%")
				.getList()));
		}
	}
	
	void openPayments() {
		String q = this.getParam("q", ""); 
		this.setVar("datelabel", "Arrival");
		
		this.setVar("bookings", addBNr(
			new TSQL(this, "select bookings.id, concat(clients.name,', ',clients.firstname) as name, paid, total, arrival as datevalue, checkedin, checkedout, " +
					" r1.nr as R1nr, r2.nr as R2nr, if(checkedin = 'Y', 'red', if(datediff(arrival,now()) < 7, 'orange', '')) as class " +
					"from bookings left outer join rooms r1 on R1room = r1.id " +
					" left outer join rooms r2 on R2room = r2.id " +
					" left join clients on clients.id = bookings.client " +
					"where clients.name like ? and " +
					"  (((select ifnull(sum(open),0) from invoices where booking = bookings.id) > 0)" +
					"or ((select ifnull(sum(price),0) from services where booking = bookings.id and invoice = 0) > 0)) order by arrival limit 400")
			.setValue("%"+q+"%")
			.getList()));
	}
	
	void openInvoices() {
		String q = this.getParam("q", ""); 
		this.setVar("datelabel", "Arrival");
		
		this.setVar("bookings", addBNr(
			new TSQL(this, "select bookings.id, concat(clients.name,', ',clients.firstname) as name, paid, total, arrival as datevalue, checkedin, checkedout, " +
					" r1.nr as R1nr, r2.nr as R2nr, if(checkedin = 'Y', 'red', if(datediff(arrival,now()) < 7, 'orange', '')) as class " +
					"from bookings left outer join rooms r1 on R1room = r1.id " +
					" left outer join rooms r2 on R2room = r2.id " +
					" left join clients on clients.id = bookings.client " +
					"where clients.name like ? and " +
					"  ((select ifnull(sum(open),0) from invoices where booking = bookings.id) > 0)" +
					" order by arrival limit 400")
			.setValue("%"+q+"%")
			.getList()));
	}
	
	void arrivals(String theConstraint) {
		String q = this.getParam("q", ""); 
		this.setVar("datelabel", "Arrival");
				 
		this.setVar("bookings", addBNr(
			new TSQL(this, "select bookings.id, concat(clients.name,', ',clients.firstname) as name, paid, total, arrival as datevalue, checkedin, checkedout, " +
					" r1.nr as R1nr, r2.nr as R2nr, " +
					" if(arrival<=date(now()), " +
					" if(((select ifnull(sum(open),0) from invoices where booking = bookings.id) > 0) " +
					" or ((select ifnull(sum(price),0) from services where booking = bookings.id and invoice = 0) > 0), 'red', 'blue'), " +
					"  if(((select ifnull(sum(open),0) from invoices where booking = bookings.id) > 0) " +
					"  or ((select ifnull(sum(price),0) from services where booking = bookings.id and invoice = 0) > 0), 'orange', '')) as class " +
					"from bookings left outer join rooms r1 on R1room = r1.id " +
					" left outer join rooms r2 on R2room = r2.id " +
					" left join clients on clients.id = bookings.client " +
					"where clients.name like ? and " + theConstraint + " and checkedin = 'N' order by arrival, clients.name limit 200")
			.setValue("%"+q+"%")
			.getList()
			));
	}
	
	
	void departures() {
		String q = this.getParam("q", ""); 
		this.setVar("datelabel", "Departure");
				 
		this.setVar("bookings", addBNr(
			new TSQL(this, "select bookings.id, concat(clients.name,', ',clients.firstname) as name, paid, total, departure as datevalue, checkedin, checkedout, " +
					" r1.nr as R1nr, r2.nr as R2nr, if(departure<=date(now()), if((select sum(open) from invoices where booking = bookings.id) > 0, 'red', 'blue'), '') as class " +
					"from bookings left outer join rooms r1 on R1room = r1.id " +
					" left outer join rooms r2 on R2room = r2.id " +
					" left join clients on clients.id = bookings.client " +
					"where clients.name like ? and checkedout = 'N' order by departure, clients.name limit 200")
			.setValue("%"+q+"%")
			.getList()
			));
	}
	
	
	
	
	/*---------------*/
	/* Class methods */
	/*---------------*/

	static double getTotal(TController theCtl, String theSQL, int theId) {
		//return new TSQL(theController, theSQL).setValue(theId).getValue(0.0);
			Object anObject = new TSQL(theCtl, theSQL).setValue(theId).getValue();
			if (anObject == null)
				return 0.0;
			else
				// hack, because some version of mysql return a BigDecimal, others a Double...
				return ((java.math.BigDecimal)anObject).doubleValue();
	}
	
	public static void genRoomService(TController theCtl, int theBooking, int rooms, long nights, int pax, double price, boolean isExisting) {
		if (isExisting) {
			// delete non-invoiced rooms (credit of debet)
			new TSQL(theCtl, "delete from services where booking = ? and invoice = 0 and extra < 0")
				.setValue(theBooking).execute();
			
			// credit already invoices rooms
			new TSQL(theCtl, "insert into services (booking, extra, nr, pax, price, invoice) " +
					" select booking, extra, nr, pax, -price, 0 from services where extra < 0 and price > 0 and invoice != 0 and booking = ?")
				.setValue(theBooking).execute();
		}
		// add the new rooms
		if (rooms > 0)
			new TSQL(theCtl, "insert into services (booking, extra, nr, pax, price, invoice) value(?, ?, ?, ?, ?, 0)")
				.setValue(theBooking).setValue(-rooms).setValue(nights).setValue(pax).setValue(price)
				.execute();

	}
	

	public static void deleteBookingAndServices(TController theCtl, int theId, boolean delBooking) {
		
		long nrInvoices = new TSQL(theCtl, "select count(*) from invoices where booking = ?").setValue(theId).getValue(0l);
		if (nrInvoices > 0) {
			TApplication.printLog("TBookingController.deleteBookingAndServices: ERROR, tried to delete booking when there is already at least 1 invoice.");
			theCtl.feedBack(false, "Can't delete a booking when there is already at least 1 invoice.");
			
		} else {
			new TSQL(theCtl, "update occupancies set booking = null where booking = ?").setValue(theId).execute();
			new TSQL(theCtl, "delete from assignments where service in (select id from services where booking = ?)").setValue(theId).execute();
			new TSQL(theCtl, "delete from services where booking = ?").setValue(theId).execute();
			if (delBooking)
				new TSQL(theCtl, "delete from bookings where id = ?").setValue(theId).execute();
			
			TRoomController.updateUsedCount(theCtl);
		}
	}
	
	
	public static void cleanUp(TController theCtl) {
		ArrayList<TRecord> aList = new TSQL(theCtl, 
			"select id from bookings where pending='Y' and now() > date_add(at, interval 60 minute)").getList();
		
		if (aList.isEmpty()) 
			TApplication.printLog("TBookingController.cleanup: no bookings to clean up.");
		else
			for (TRecord B: aList) {
				int anId = B.getI("id");
				TApplication.printLog("TBookingController.cleanup: deleting booking, id = " + anId);
				deleteBookingAndServices(theCtl, anId, true);
			}
	}
	
	public static int BNr(int theId) {
		return (9 - theId % 9) * 1000000 + theId * 10 + theId % 9;
	}
	public static int remBNr(int theBNr) {
		int anId = (theBNr / 10) % 100000;
		
		// check correctness
		if (BNr(anId) == theBNr)
			return anId;
		else
			return -1;
	}
	public static TRecord addBNr(TRecord R) {
		int anId = (Integer)R.get("id");
		R.put("bnr", BNr(anId));
		return R;
	}
	public static ArrayList<TRecord> addBNr(ArrayList<TRecord> theList) {
		for (TRecord R: theList) 
			addBNr(R);
		return theList;
	}
	
	public static TRecord addClient(TController theCtl, TRecord B) {
		int aClient = (Integer)B.get("client");
		TRecord C = new TSQL(theCtl, 
				"select title, firstname, name, email, phone, street, zipcity,country, VAT, language, idnr, born, bornat from clients where id = ?")
					.setValue(aClient).getRecord();
		B.putAll(C);
		return B;
	}
	
	public static int getBookingHorizon (TController theCtl, Date theArrival) {
		TRecord bhR = new TSQL(theCtl, 
				"select id from horizons where datediff(?,date(now())) > nr order by nr desc limit 1")
			.setValue(theArrival)
			.getRecord();
		return  (bhR == null) ? 1 : (Integer) bhR.get("id");
	}
	
	static ArrayList<TRecord> getRooms(TController theCtl, Date arr, int roomtype, int nrDays, int maxnr, int horizon, String theLan) {
		return new TSQL(theCtl, "SELECT rooms.id, rooms.nr, rooms.name as roomname, " +
				"  max(rooms.maxpax) as maxpax, " +
				"  sum(prices.price" + horizon + ") as price, " +
				"  max(roomtypes.id) as roomtype, " +
				"  max(roomtypes.name_" + theLan + ") as name, " +
				"  max(roomtypes.nrpax) as nrpax " +

				"FROM occupancies " +
				" LEFT JOIN rooms on occupancies.room = rooms.id " +
				" LEFT JOIN roomtypes on rooms.roomtype = roomtypes.id " +
				" LEFT JOIN prices on prices.period = occupancies.period and prices.roomtype = rooms.roomtype " +

				"WHERE at >= date(?) AND at < date_add(date(?), interval ? day) AND booking is null AND rooms.roomtype = ? " +
				" GROUP by rooms.id, rooms.nr HAVING count(*) >= ? order by rooms.used asc limit ?")
			.setValue(arr)
			.setValue(arr)
			.setValue(nrDays)
			.setValue(roomtype)
			.setValue(nrDays)
			.setValue(maxnr)
			.getList();
	}


}
