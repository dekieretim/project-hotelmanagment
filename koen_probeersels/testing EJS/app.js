/**
 * Created by Tim on 02/10/2014.
 */

//    starten met "node app.js" command en in browser: http://localhost:8765/

var express = require('express'),
    app = module.exports = express(),
    logger = require('./utils/logger'),
    translate = require('./MKNT'),
    fs = require('fs'),
    multer = require('multer'),
    //helmet = require("helmet"),
    //session = require("session"),
    csrf = require("csurf"),
    io = require('socket.io')(8766),
    AdmZip = require('adm-zip'),
    done = false;

app.use(multer({ dest: './files/',
    onFileUploadStart: function (file) {
        logger.info(file.originalname + ' is starting ...');
    },
    onFileUploadComplete: function (file) {
        logger.info(file.fieldname + ' uploaded to  ' + file.path);
        done=true;
    }
}));

//app.use(session
//({
//    secret: 'this',
//    cookie: {
//        httpOnly: true,
//        secure: true
//    }
//}));
//
app.use(csrf());

app.use(function(req, res, next) {
    res.locals._csrf = req.csrfToken();
    next();
});

var helmet = require("helmet");
app.use(helmet.xssFilter());
app.use(helmet.xframe());
app.use(helmet.hsts({
    maxAge: 7776000000,
    includeSubdomains: true
}));
app.use(helmet.hidePoweredBy({ setTo: 'do a barrel roll!' }));

logger.info('application started with all variables loaded... ready to go');

app.engine('.html', require('ejs').__express);

app.set('views', __dirname + '/views');

app.set('view engine', 'html');

//default pagina => index.html
app.get('/', function(req, res){
    res.render('index', {
        pageTitle: 'Java to JS and JSP to EJS translator'
    });
});

//tranlate java to javascript when sending a file
app.post('/translateJava', function(req,res){
        logger.info("now entering the post handler for /translateJava");

        //wait till file has finished uploading
        if(done){
            var file = req.files.javaFile,
                path = './files/' + file.name;
            logger.info(path);

            fs.readFile(path, { encoding: 'utf-8' }, function (err, data) {
                var translation = "";
                if (err) throw err;
                if(file.extension === 'java' || file.mimetype === 'text/x-java-source,java'){
                    try{
                        logger.warn('i am now trying to translate a java-string to javascript');
                        translation = translate.translateJavaToJS(data);

                        fs.writeFile(path, translation, function(err){
                            if(err) throw err;

                            logger.warn('info written to file');
                        });
                        res.sendFile(path, { root: __dirname});
                    } catch(e){
                        res.send(e.message);
                    }
                }
            });
        }
    unlink(path);
});

//tranlate JSP to EJS when sending a file
app.post('/translateJSP', function(req,res){
    logger.info("now entering the post handler for /translateJSP");

    //wait till file has finished uploading
    if(done){
        var file = req.files.JSPFile,
            path = './files/' + file.name;
        fs.readFile(path, { encoding: 'utf-8' }, function (err, data) {
            if (err) throw err;
            if(file.extension === 'jsp' || file.mimetype === 'type=magnus-internal/jsp'){
                try{
                    logger.warn('i am now trying to translate a JSP-string to EJS');
                    res.send(translate.translateJSPToEJS(data));
                } catch(e){
                    res.send(e.message);
                }
            } else {
                res.send('De file die u doorstuurde is geen jsp file.  Gelieve een geldige file door te sturen.');
            }
            unlink(path);
        });
    }
});

io.on('connection', function (socket) {
    logger.warn("ik heb nu een socket-connection gekregen");
    socket.emit('news', { hello: 'world' });

    socket.on('translateJava', function(data){
        try {
            var translation = translateJava(data.javaFile);
            socket.emit('javaTranslation', { translation: translation });
        } catch(e) {
            socket.emit('javaTranslation', { translation: e.message });
        }
    });

    socket.on('translateJSP', function(data){
        try {
            var translation = translateJSP(data.JSPFile);
            socket.emit('JSPTranslation', { translation: translation });
        } catch(e) {
            socket.emit('JSPTranslation', { translation: e.message });
        }
    });

    socket.on('feedback', function(data){
        try {
            console.dir(data);
            var formattedData = "{\r\n\tTijdstip: " + new Date() + ",\r\n\tnaam: " + data.naam + ",\r\n\temail: " + data.email +
                ",\r\n\tfeedback: " + data.feedback + "\r\n}\r\n";
            console.log(formattedData);
            fs.appendFile('./logs/feedback.log', formattedData, function (err) {
                if (err) throw err;
                socket.emit('feedbackWorked', { message: 'The feedback has been registered.' });
            });
        } catch (e) {
            socket.emit('feedbackWorked', { message: "The feedback has failed due to the following error: " + e.message })
        }
    });

    socket.on('translateMultiple', function(data){
        var files = data.files,
            zip = new AdmZip();
        console.log(files);
        files.forEach(function(element, index){
            if(element.extension === 'java'){
                zip.addFile(element.name, translateJava(element.file));
            } else{
                zip.addFile(element.name, translateJSP(element.file));
            }
        });

        zip.toBuffer(function(buffer){
            socket.emit('translatedFiles', { translatedFiles: buffer });
        });
    });
});

//to link the scripts to index.html
app.use(express.static(__dirname + '/scripts'));

if (!module.parent) {
    app.listen(8765);
    console.log('EJS Demo server started on port 8765');
}

function translateJava(file){
    var javaString = file.toString('utf-8'),
        translation;

    translation  = translate.translateJavaToJS(javaString);
    return new Buffer(translation, 'utf-8');
}

function translateJSP(file){
    var JSPString = file.toString('utf-8'),
        translation;

    translation  = translate.translateJSPToEJS(JSPString);
    return new Buffer(translation, 'utf-8');
}

//functie om een file te deleten
function unlink(path){
    fs.unlink(path, function (err) {
        if (err) throw err;
        console.log('successfully deleted ' + path);
    });
}