var input, output, div, fors, originalFile, typer, outputButton, file, filename, fileInput, reader, wrapper, files, fileNumber = 0, readFiles = [], socket;


$(document).ready( doInit );

function doInit() {
    console.log("We zijn vertrokken...");
    $("#tabs").tabs();

    initializeAllElements();
    addAllListeners();
    initializeSocket();

    $("#feedbackDialog").dialog({
        autoOpen: false,
        title: "Feedback result"
    });
}

function initializeAllElements(){
    fileInput = document.getElementById('fileInput');
    typer = document.getElementById("translated");
    outputButton = document.getElementById("download");
    originalFile = document.getElementById("originalFile");
    div = document.getElementById('translated');
    dirInput = document.getElementById('dirInput');
    wrapper = $("#wrapper");
    reader = new FileReader();
}

function addAllListeners() {
    //hulpmethode om de default actie bij events te overschrijven
    function cancel(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        return false;
    }

    //Zorgt ervoor dat de file niet geöpend wordt in de browser door "dragover" en "dragenter" events te annuleren, zodat alleen de (overgeschreven) drop functie wordt aangeroepen
    window.addEventListener('dragover', cancel, false);
    window.addEventListener('dragenter', cancel, false);

    //deze functie zorgt ervoor dat als je een file drag-en-dropt in je browservenster, de readFile method wordt aangroepen op deze file.
    window.addEventListener('drop', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        } // stops the browser from redirecting off to the image.



        var dt = e.dataTransfer;
        var items = e.dataTransfer.items;
        for (var i=0; i<items.length; i++) {
            var entry = items[i].webkitGetAsEntry();
            if (entry.isFile)
                readFile(dt);
            else {

                alert("drag and drop voor mappen nog niet ondersteund: gebruik de knop om mappen te vertalen")
            }
        }
    });

    //deze functie zorgt ervoor dat als je een file kiest met behulp van de upload-knop, de readFile method wordt aangroepen op deze file.
    fileInput.addEventListener('change', function () {
        readFile(fileInput);
    });

    dirInput.addEventListener('change', function (e) {
        readDirectory(dirInput.files);
    });


    //annuleer defaultgedrag van een form submit
//    $('form').on('submit', cancel);

    //attach de handlers van de buttons
    $('#java').click(getJavaTranslation);
    $('#jsp').click(getJSPTranslation);
    $('#ok').click(logFeedback);
}

function initializeSocket(){
    socket = io.connect('http://localhost:8766');
    socket.on('news', function (data) {
        console.log(data);
        socket.emit('my other event', { my: 'data' });
    });
    socket.on('javaTranslation', function(data){
        showResultSingle(data, "#translated");
    });

    socket.on('JSPTranslation', function(data){
        showResultSingle(data, "#translated");
    });

    socket.on('feedbackWorked', function(data){
        console.log('ik zit nu in feedbackWorked');
        alertFeedbackFinished(data);
    });

    socket.on('translatedFiles', function(data){
        console.dir(data.translatedFiles);
        console.log(data.translatedFiles);
        downloadZip(data.translatedFiles);

    });
}

function getNextFile() {
    fileNumber++;
    if (fileNumber <= files.length)
        return files[fileNumber];

    console.log("done reading", filenumber, "files");
    return null;
}

function readDirectory(userData) {

    var filesToTranslate = [];
    wrapper.html("<p>All java and jsp files:</p>");
    wrapper.toggle();

//        var f = this.files;
//        var files = [];
//        for (var i = 0; i<f.length; i++){
//            files.push(f[i]);
//
//        }
    //same code as above, but more efficient:
    files = [].slice.call(userData);
    console.log(files);

    files.forEach(function(element){
        var fileExtension = element.name.split('.').pop();
        if(fileExtension === 'java' || fileExtension === 'jsp'){
            filesToTranslate.push({ file: element, extension: fileExtension, name: element.name });
        }
    });

    filesToTranslate.forEach(function(element){
        var li = document.createElement('li'),
            a = document.createElement('a');
        a.file = element.file;
        a.href = '#';
        a.target = '_blank';
        a.textContent = element.file.name;
        //only generate if it's clicked
        a.addEventListener('click', openFile);
        li.appendChild(a);
        wrapper.append(li);
    });
    console.log(filesToTranslate);

    socket.emit("translateMultiple", { files: filesToTranslate });

    console.log("data doorgestuurd naar de server");
}

function openFile() {
    //checks if it already exists, adds the href, sets hreffed true (efficient version)
    !this.hreffed && (this.href = webkitURL.createObjectURL(this.file));
    this.hreffed = true;
}


function toonOutput() {
    var translatedFor;
    Vertaal(input);
    fors = findFors(output);
    for (var i = 0; i < fors.length; i++) {
        translatedFor = translateForLoop(fors[i]);

        output = output.replace(fors[i], translatedFor);
    }

    var theClass = findClass(output),
        length;

    theClass = splitBodyInMethods(theClass);
    length = theClass.methods.length || 0;

    output = "";
    for (i = 0; i < length; i = i + 1) {
        console.log("Methode " + (i + 1) + " is: " + theClass.methods[i]);
        output += "Methode " + (i + 1) + " is: " + theClass.methods[i] + "\n";
    }

    translate.innerText = output;
}

//function to read a file as text
function readFile(userData) {
    file = userData.files[0];
    var extension = file.name.split('.').pop(),
        reader;

    if (extension == "java" || extension == "jsp") {
        reader = new FileReader();

        reader.onloadend = function () {
            input = reader.result;
            console.log("successfully read file: \n" + input);
            originalFile.innerText = input;
            reader.result = null;
        };
        reader.readAsText(file);
    } else {
        console.log("wrong file type");
    }
}
//
//function readFile(userData) {
//
//    reader.onload = function () {
//
//        input = reader.result;
//        console.log("succesfully read file: \n" + input);
//    };
//    file = userData.files[0];
//        var extension = file.name.split('.').pop(),
//        requiredExtention = "java",
//        requiredExtention2 = "jsp";
//
//
////        test of de extentie ".java" is
//    if (extension == requiredExtention || extension == requiredExtention2) {
//        console.log(file);
//
//
//        reader.readAsText(file);
//        reader.onloadend = function () {
//            originalFile.innerText = input;
//            console.log(reader.readyState);
//        };
//        console.log(reader.readyState);
//
//    }
//    else {
//        console.log("wrong file type");
//    }
//
//
//}

//a function called by the html buttons, to translate the
function downloadFile(extention) {
    filename = document.getElementById("fileName").value;
    const MIME_TYPE = "text/plain";
    console.log("typer" + typer);
    //creates a blob (file that can be filled with text)
    var bb = new Blob([typer.innerText], {type: MIME_TYPE});

    //creates a hyperlink
    var translatedFile = document.createElement('a');
    translatedFile.download = filename + extention;
    translatedFile.href = window.URL.createObjectURL(bb);
    translatedFile.textContent = "Download";
    translatedFile.dataset.downloadurl = [MIME_TYPE, translatedFile.download, translatedFile.href].join(':');

    outputButton.innerHTML = "";

    //adds the hyperlink to the button
    outputButton.appendChild(translatedFile);

    outputButton.disabled = false;
}

function downloadZip(data) {
    filename = "data";

    const MIME_TYPE = "application/octet-stream";
    var translatedZip = document.createElement('a');
    translatedZip.download = filename + ".zip";

    var int8arr = new Int8Array(data);
    console.log(int8arr);
    var arr = Array.prototype.slice.call(int8arr);
    console.log(arr);
    var bb = new Blob(arr);
    console.log(bb);


    translatedZip.href = window.URL.createObjectURL(bb);
    //translatedZip.href = window.URL.createObjectURL(new DataView(data, 0));
    translatedZip.textContent = "Download Zip";
    translatedZip.dataset.downloadurl = [MIME_TYPE, translatedZip.download, translatedZip.href].join(':');
    outputButton.innerHTML = "";

    var btn = document.createElement('button');
    btn.appendChild(translatedZip);
    wrapper.html(btn);


}

//function to log user feedback
function logFeedback(e){
    e.preventDefault();
    socket.emit('feedback', {
        feedback: $('#feedback').val(),
        email: $('#email').val(),
        naam: $('#naam').val()
    });
}

//function to get the Java translation via socket.io
function getJavaTranslation(e){
    e.preventDefault();
    wrapper.toggle("slow");
    socket.emit('translateJava', { javaFile: file });
}

//function to get the JSP translation via socket.io
function getJSPTranslation(e){
    e.preventDefault();
    wrapper.toggle("slow");
    socket.emit('translateJSP', { JSPFile: file });
}

function translateDirectory(){
    e.preventDefault();
}

//function to show the results when a single translation is asked for
function showResultSingle(data, id){
    // The decode() method takes a DataView as a parameter, which is a wrapper on top of the ArrayBuffer.
    var dataView = new DataView(data.translation);
    // The TextDecoder interface is documented at http://encoding.spec.whatwg.org/#interface-textdecoder
    var decoder = new TextDecoder('utf-8');
    var decodedString = decoder.decode(dataView);
    $(id).text(decodedString);
}

function alertFeedbackFinished(data){
    var feedbackDialog = $("#feedbackDialog");

    //clear out values
    $('#feedback').val("");
    $('#email').val("");
    $('#naam').val("");

    //open dialogue with response
    feedbackDialog.text(data.message);
    feedbackDialog.dialog('open');
}