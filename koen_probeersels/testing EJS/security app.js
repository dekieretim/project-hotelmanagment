/**
 * Created by Tim on 02/10/2014.
 */

//    starten met "node app.js" command en in browser: http://localhost:8765/

var express = require('express'),
    app = module.exports = express(),
    logger = require('./utils/logger'),
    translate = require('./MKNT'),
    fs = require('fs'),
    multer = require('multer'),
    //added modules
    helmet = require("helmet"),
    session = require("session"),
    csrf = require("csurf"),
    done = false;


//lijn 17 aangepast
app.use('/api/upload', multer({ dest: './files/',

    onFileUploadStart: function (file) {
        logger.info(file.originalname + ' is starting ...');
    },
    onFileUploadComplete: function (file) {
        logger.info(file.fieldname + ' uploaded to  ' + file.path);
        done=true;
    }
}));

//TOEGEVOEGD
app.use(session({
        secret: 'this',
        cookie: {
            httpOnly: true,
            secure: true
        }
    })
);

app.use(csrf());

app.use(function(req, res, next) {
    res.locals._csrf = req.csrfToken();
    next();
});

//

logger.info('application started with all variables loaded... ready to go');

app.engine('.html', require('ejs').__express);

app.set('views', __dirname + '/views');

app.set('view engine', 'html');

//default pagina => index.html
app.get('/', function(req, res){
    res.render('index', {
        pageTitle: 'Java to JS and JSP to EJS translator'
    });
});

//tranlate java to javascript when sending a file
app.post('/translateJava', function(req,res){
    logger.info("now entering the post handler for /translateJava");

    //wait till file has finished uploading
    if(done){
        var file = req.files.javaFile,
            path = './files/' + file.name;
        logger.info(path);

        fs.readFile('./files/' + file.name, { encoding: 'utf-8' }, function (err, data) {
            if (err) throw err;
            if(file.extension === 'java' || file.mimetype === ''){
                try{
                    logger.warn('i am now trying to translate a java-string to javascript');
                    res.send(translate.translateJavaToJS(data));
                } catch(e){
                    res.send(e.message);
                }
            }
            unlink(path);
        });
    }
});

//tranlate JSP to EJS when sending a file
app.post('/translateJSP', function(req,res){
    logger.info("now entering the post handler for /translateJSP");

    //wait till file has finished uploading
    if(done){
        var file = req.files.JSPFile,
            path = './files/' + file.name;
        fs.readFile(path, { encoding: 'utf-8' }, function (err, data) {
            if (err) throw err;
            if(file.extension === 'jsp' || file.mimetype === ''){
                try{
                    logger.warn('i am now trying to translate a JSP-string to EJS');
                    res.send(translate.translateJSPToEJS(data));
                } catch(e){
                    res.send(e.message);
                }
            } else {
                res.send('De file die u doorstuurde is geen jsp file.  Gelieve een geldige file door te sturen.');
            }
            unlink(path);
        });
    }
});

//to link the scripts to index.html
app.use(express.static(__dirname + '/scripts'));

if (!module.parent) {
    app.listen(8765);
    console.log('EJS Demo server started on port 8765');
}

//functie om een file te deleten
function unlink(path){
    fs.unlink(path, function (err) {
        if (err) throw err;
        console.log('successfully deleted ' + path);
    });
}