/**
 * Created by Badlapje on 27/11/14.
 */
var winston = require('winston');

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({ colorize: true, timestamp: true, level: "silly"}),
        new (winston.transports.File)({ filename: __dirname + '/../logs/all-logs.log', colorize: true, level: "silly", name: "file.all" }),
        new (winston.transports.File)({ filename: __dirname + '/../logs/debug.log', colorize: true, level: "debug", name: "file.debug" }),
        new (winston.transports.File)({ filename: __dirname + '/../logs/verbose.log', colorize: true, level: "verbose", name: "file.verbose" }),
        new (winston.transports.File)({ filename: __dirname + '/../logs/info.log', colorize: true, level: "info", name: "file.info" }),
        new (winston.transports.File)({ filename: __dirname + '/../logs/warn.log', colorize: true, level: "warn", name: "file.warn" }),
        new (winston.transports.File)({ filename: __dirname + '/../logs/errors.log', colorize: true, level: "error", name: "file.error", handleExceptions: true })
    ]
});

module.exports = logger;