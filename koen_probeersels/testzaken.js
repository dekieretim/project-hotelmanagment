var string1 = "int 0 = 7;",
    string2 = " int 0 = 7;";

console.log("Resultaat string 1 = " + string1.search(/\s/));
console.log("Resultaat string 2 = " + string2.search(/\s/));

/*function TestConstructor() {    var self = this;

    this.getClient = Object.defineProperty(self, "getClient", {
        writable: false,
        value: function getClient(theCtl, theClient, getBookings){return TestConstructor.getClient(theCtl, theClient, getBookings);}
    });

    this.serialversionUID = Object.defineProperty(self, "serialversionUID", {
        writable: false,
        value: TestConstructor.serialversionUID
    });

    this.notWritable = TestConstructor.notWritable;
}

TestConstructor.notWritable = 8;

Object.defineProperty(TestConstructor, "serialversionUID", {
    writable: false,
    value: 1
});

Object.defineProperty(TestConstructor, "getClient", {
    writable: false,
    value: function getClient(theCtl, theClient, getBookings){
        console.log("theCtl werd hier succesvol doorgegeven en is: " + theCtl);
        console.log("theClient werd hier succesvol doorgegeven en is: " + theClient);
        console.log("getBookings werd hier succesvol doorgegeven en is: " + getBookings);
    }
});

var testObject = new TestConstructor();

TestConstructor.getClient(8, "this is a stringetje", false);
console.log("De serialversionUID van de TestConstructor is: " + TestConstructor.serialversionUID);
console.log("De notWritable van de TestConstructor is: " + TestConstructor.notWritable);

testObject.getClient(5, "this is a string", true);
console.log("De serialversionUID van het testObject is: " + testObject.serialversionUID);
console.log("De notWritable van het testObject is: " + testObject.notWritable);

testObject.serialversionUID = 2;
console.log("De serialversionUID van het testObject is: " + testObject.serialversionUID);
*/

