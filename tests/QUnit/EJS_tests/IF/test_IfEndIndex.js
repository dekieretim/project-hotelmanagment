/**
 * Created by niels on 13/11/14.
 */
function getIndexEndOfIFStructure(code)
{
    //+4 omdat anders niet de hele IF gereturnd wordt
    return (code.indexOf("</c:if>")+7);
}


QUnit.module("getEndIndex");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic1",
            Ifstructure: "01234567</c:if>",
            expected: 15},
        {title: "basic2",
            Ifstructure: "<c:if </c:if>",
            expected: 13}
    ]).test("Test getEndIndexIF function", function(params){
        var actualResult = getIndexEndOfIFStructure(params.Ifstructure);
        strictEqual(actualResult, params.expected);
        notStrictEqual(actualResult, params.Ifstructure);
    });
