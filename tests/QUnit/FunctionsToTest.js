/**
 * Created by Badlapje on 23/10/14.
 */

function translateArray(arrayToTranslate) {
    var translatedArray = arrayToTranslate;

//    removes "new var"
    translatedArray = translatedArray.replace(/=\s*new\s*var/g, "= ");
//    removes []
    translatedArray = translatedArray.replace(/\[ *\]/g, "");
//    replaces { by [
    translatedArray = translatedArray.replace(/\{/g, "[");
//    replaces } by ]
    translatedArray = translatedArray.replace(/\}/g, "]");
//    replaces [\d] by []
    translatedArray = translatedArray.replace(/\[\s*\d\s*\]/g, "[]");
    return translatedArray;
}

function translateForLoop(stringToTranslate) {
    var forIdentifierPart, translatedForLoop = "could not translate for loop", indexType;

    stringToTranslate = stringToTranslate.trim();

    stringToTranslate = stringToTranslate.replace(/[ ]{2,}/g, " ");
    //this replaces all occurrences of more than one whitespace by one whitespace.

    chooseCorrectLoopTypeFunction(stringToTranslate);

    function chooseCorrectLoopTypeFunction(unidentifiedForLoop) {
        forIdentifierPart = getForIdentifierPart(unidentifiedForLoop);

        if (forIdentifierPart.indexOf(";") != -1) {
            /*index of ";" is -1 if the string does not contain ";" -->  it's a regular for loop*/
            translateRegularFor(unidentifiedForLoop);
        }

        else {
            /*it's a foreach loop*/
            translateForeachFor(unidentifiedForLoop);
        }
        //if it's not a for loop, "could not translate for loop" will be returned

    }

    function getForIdentifierPart(forLoopString) {
        return forLoopString.split("{")[0];
        /*")" would also include a possible bracket from ".size()"*/
    }

    function translateForeachFor(foreachString) {
//        converts a java foreach loop into a javascript foreach loop
//        var hulp, varname;
//        console.log("foreachfor");
        indexType = getIndexType(foreachString);
        foreachString = foreachString.replace(indexType, "var");
        foreachString = foreachString.replace(":", " in ");
//        hulp = getForIdentifierPart(foreachString);
//        hulp = hulp.replace(")", "");
//        varname = hulp.split("<").pop();
//        definitely works for this because i just inserted a "<"
//        console.log(varname);

//        foreachString = foreachString.replace(varname, varname + ".length; i++");
//        console.log(foreachString);

        translatedForLoop = foreachString;
    }

    function translateRegularFor(regularForString) {
        var help, help2, triggerPart, firstSemicolonPos, secondSemicolonPos, newForIdentifierPart;

        indexType = getIndexType(regularForString);

        regularForString = regularForString.replace(indexType, "var");
//        console.log(regularForString);

        forIdentifierPart = getForIdentifierPart(regularForString);
        newForIdentifierPart = forIdentifierPart;

//        all code after this translates .Size() and .length() to ".length"
        firstSemicolonPos = newForIdentifierPart.indexOf(";");
        secondSemicolonPos = newForIdentifierPart.lastIndexOf(";");
//        returns first and second index of a semicolon
        triggerPart = newForIdentifierPart.split(";")[1];
//        the part between the 2 semicolons
        triggerPart = triggerPart.replace(/\s/g, "");
//        removes all whitespace in this part

        newForIdentifierPart = newForIdentifierPart.slice(0, firstSemicolonPos) + "; " + triggerPart + newForIdentifierPart.slice(secondSemicolonPos);
//        put it back between the semicolons

        regularForString = regularForString.replace(forIdentifierPart, newForIdentifierPart);
        help = newForIdentifierPart.toLowerCase().indexOf(".length()");
//        if the identifier contains .length() this will return the index, else it returns -1
        help2 = newForIdentifierPart.toLowerCase().indexOf(".size()");
//        if the identifier contains .size() this will return the index, else it returns -1
        if (help != -1) {
            regularForString = regularForString.replace(/\.length\(\)/i, ".length");
//            this is usually the same as .replace(".length()".....) but safer, (/i = ignoring case)
//            This is easier and safer than trying to make a part of the identifier toLowerCase because it may contain user-defined variables and methods
        } else if (help2 != -1) {
            regularForString = regularForString.replace(/\.size\(\)/i, ".length");
//            same reason as above
        }

        translatedForLoop = regularForString;
    }

    function getIndexType(string) {

        var help4, betweenBrackets;
//        returns the type of the iterator, usually "int"
        betweenBrackets = string.split("(")[1];
//        return the part between brackets
        help4 = betweenBrackets.trim();
//        removes whitespace
        indexType = help4.split(" ")[0];
//        first word
        return indexType;
    }


    return translatedForLoop;

}

function findClass(javaString){
    try {
        if (typeof javaString == "string") {
            var firstPublic = javaString.indexOf("public"),
                firstPrivate = javaString.indexOf("private"),
                firstBracket = javaString.indexOf("{"),
                lastBracket = javaString.lastIndexOf("}");

            if (firstPublic < firstPrivate) {
                sliceClass(firstPublic, firstBracket, lastBracket, javaString);
            } else {
                sliceClass(firstPrivate, firstBracket, lastBracket, javaString);
            }
        }
    } catch (e) {
        sendError(e.stack);
    }
}

//function to extract the imports, the declaration and the body from a stringified Java Class
function sliceClass(beginDeclaration, beginBody, endBody, javaString){
        theClass.imports = javaString.slice(0, beginDeclaration).trim();
        theClass.declaration = javaString.slice(beginDeclaration, beginBody).trim();
        theClass.body = javaString.slice(beginBody + 1, endBody).trim();
}

//function to split the body in methods, declaration (which also get's processed) and variables
//uses the following helperfunctions: getIndices, searchMethodEnd
function splitBodyInMethods(){
    try {
        var indicesSemicolon = getIndices(/;/g, theClass.body),
            indicesOpenBrace = getIndices(/\{/g, theClass.body),
            indicesClosingBrace = getIndices(/\}/g, theClass.body),
            startIndex = indicesSemicolon.filter(function (item, index, array) {
                return (item < indicesOpenBrace[0]);
            }).reverse()[0] + 1,
            splitObject = searchMethodEnd(indicesOpenBrace, indicesClosingBrace),
            i = 0;

        do {
            //if it's the first method: add the field declarations to the class
            if (!("fieldDeclarations" in theClass)) {
                theClass.fieldDeclarations = theClass.body.slice(0, startIndex).trim();
                processFields();
            }

            //add the method to theclass.methods, then trim extraneous whitespace
            theClass.methods[i] = theClass.body.slice(startIndex, splitObject.endIndex + 1).trim();
            processMethod(i++); //process the method in a helper method

            //check to see if the last method has been found, if so: break from the loop by using a return statement
            if (splitObject.stop) {
                return theClass;
            }

            //set the startindex to the right one
            startIndex = splitObject.endIndex + 1;

            //find the next method, passing in the correct indicesArrays
            splitObject = searchMethodEnd(splitObject.indicesOpenBrace, splitObject.indicesClosingBrace);
        } while (true); //used an infinite loop here because inside the loop there's a check which can trigger a return statement & which should eventually be triggered
    } catch (e) {
        sendError(e.stack);
    }
}

//helper function to search for the ending brace of a method
function searchMethodEnd(indicesOpenBrace, indicesClosingBrace){
    try {
        var open = 0,
            close = 0,
            openLength = indicesOpenBrace.length,
            closeLength = indicesClosingBrace.length,
            returnObject = { stop: false };

        if (indicesOpenBrace.length === 1) {
            returnObject.endIndex = indicesClosingBrace[0];
            returnObject.stop = true;
            return returnObject;
        }

        do {
            if (indicesOpenBrace[open] > indicesClosingBrace[close]) {
                if (close + 1 < closeLength) {
                    close++;
                }
            } else if (open + 1 == openLength) { //if last method
                returnObject.endIndex = indicesClosingBrace[closeLength - 1];
                returnObject.stop = true;
                return returnObject;
            } else {//if open is smaller then close
                if (open + 1 < openLength) {
                    open++;
                }
            }
        } while (open !== close);

        returnObject.endIndex = indicesClosingBrace[close - 1];
        returnObject.indicesOpenBrace = indicesOpenBrace.slice(open);
        returnObject.indicesClosingBrace = indicesClosingBrace.slice(close);

        return returnObject;
    } catch (e) {
        sendError(e.stack);
    }
}

//helper function to get an array of indices containing all indexes where a regex matches a certain string
function getIndices(regex, theString){
    var result, indices = [];
    while ((result = regex.exec(theString))) {
        indices.push(result.index);
    }

    return indices;
}

//helper function to fully process a method: change variables, change params, change declaration
//uses helper function changeMethodVariables
function processMethod(index){
    try {
        var endParams = theClass.methods[index].indexOf(")"),
            startParams = theClass.methods[index].indexOf("("),
            startBodyProper = theClass.methods[index].indexOf("{") - 1,
            declarationBeforeParams,
            params = "",
            i,
            methodData = {
                methodName: "",
                newParams: "",
                bodyProper: "",
                isPrivate: false,
                isStatic: false,
                final: false
            };

        //process parameters
        if ((endParams - startParams) > 1) {
            params = theClass.methods[index].slice(startParams + 1, endParams);
            params = params.trim();
            params = params.split(",");
            for (i = 0; i < params.length; i++) {
                params[i] = params[i].trim().split(" ");
                methodData.newParams += params[i][1].trim() + ", ";
            }
            methodData.newParams = methodData.newParams.slice(0, -2);
        }

        //process declaration
        declarationBeforeParams = theClass.methods[index].slice(0, startParams);
        declarationBeforeParams = declarationBeforeParams.trim().split(" ");
        methodData.methodName = declarationBeforeParams[declarationBeforeParams.length - 1].trim();

        for (i = 0; i < declarationBeforeParams.length; i++) {
            switch (declarationBeforeParams[i]) {
                //public, protected, returntype (any), abstract hebben geen nut in javascript: zijn dus betekenisloos.  Multithreading verloopt op een volkomen
                //andere manier, waardoor het ook niet nuttig is om daar iets mee te doen voorlopig => zou een onderzoeksproject zijn op zich
                //TODO: nog te bekijken: native, strictfp, transient, volatile
                case "private":
                    methodData.isPrivate= true;
                    break;
                case "static":
                    methodData.isStatic = true;
                    break;
                case "final":
                    methodData.final = true;
                    break;
                default:
                    break;
            }
        }

        //process the body variables
        if (methodData.methodName === theClass.constructorName) {
            processConstructorData(methodData, index, startBodyProper);
            return;
        } else {
            methodData.bodyProper = theClass.methods[index].slice(startBodyProper).trim();
        }

        methodData.bodyProper = changeMethodVariables(findMethodVariables(methodData.bodyProper), methodData.bodyProper);
        theClass.methods[index] = "function " + methodData.methodName + "(" + methodData.newParams + ")" + methodData.bodyProper;
        theClass.methodData = [];
        theClass.methodData[index] = methodData;
        writeToConstructor(theClass.methods[index], methodData.bodyProper, methodData.methodName, methodData.isPrivate, methodData.isStatic, methodData.final);

    } catch (e) {
        sendError(e.stack);
    }
}

//function to process ConstructorData
function processConstructorData(methodData, index, startBodyProper){
    try {
        methodData.bodyProper = theClass.methods[index].slice(startBodyProper).trim();
        methodData.bodyProper = methodData.bodyProper.slice(1);
        methodData.bodyProper = methodData.bodyProper.slice(0, -1).trim();
        theClass.constructor.push(methodData);
    } catch (e) {
        sendError(e.stack);
    }
}

//helper function for processMethod to change the type in a variable declaration to var
function changeMethodVariables(indices, method){
    try {
        var i = 0, //variable to increase the performance of the for loop
            length = indices.length, //variable to increase the performance of the for loop
            counter = 0, //variable to offset the startindex with
            partFirst = "", //variable to store the first part of the method
            partFrom = "", //variable to store the part of the method in starting at the variabele declaration till the end of the method
            type = ""; //variable to store the type of the variable for easy reference in two statements

        for (i; i < length; i++) {
            partFirst = method.slice(0, indices[i] - counter); //store the first part of the string for easy concatenation
            partFrom = method.slice(indices[i] - counter); //slice out the part of the string beginning at the variable declaration
            type = partFrom.slice(0, partFrom.search(/\s/)); //store the type of the variable for easy use in the next two statements
            partFrom = partFrom.replace(type, "var");//replace the type by var
            counter += type.length - 3; //augment counter to offset replacing the type by var;
            method = partFirst + partFrom; //reassemble method so it's usable again for the next variable
        }

        return method;
    } catch (e) {
        sendError(e.stack);
    }
}

//function to clear out newlines and (in the future: multi-line declarations)
function prepareFields(splitByLine){
    try {
        var blankLines = 0,
            length = splitByLine.length;

        for (var i = 0; i < length; i++) {
            var theString = splitByLine[i].trim(),
                semicolonIndices = getIndices(/;/g, theString),
                equalIndices = getIndices(/=/g, theString);
            if (theString.length) { //als het geen lege string is (dus gewoon lijn whitespace, want alle niet enter/newline whitespace wordt getrimt in de declaratie van deze var)
                if (equalIndices.length === 0) { //als er geen gelijk aan in staat
                    splitByLine[i - blankLines++ - 1].content += theString;
                } else splitByLine[i - blankLines] = { content: theString, semiColons: semicolonIndices, equals: equalIndices }; //als dat wel het geval is
            } else blankLines += 1; // als het wel een lege lijn is
        }//TODO multiline declaraties waarbij er een = in de declaratie staat ergens die worden dus niet correct geparsed.  Idem voor multiline declaraties van strings via concatenatie met +  Moet in de documentatie terechtkomen.

        splitByLine.splice((length - blankLines), blankLines); //verwijder de lege lijnen

        return splitByLine;
    } catch (e) {
        sendError(e.stack);
    }
}


//function to process field declarations
function processFields(){
    var splitByLine = prepareFields(theClass.fieldDeclarations.split("\r\n"));

    function handleFields(element, index, array){
        var firstEquals = element.content.indexOf("="),
            declaration = element.content.slice(0, firstEquals).trim(),
            declarationObject = {
                value: element.content.slice(firstEquals + 1).trim(),
                type: "",
                name: "",
                isPrivate: false,
                isStatic: false,
                final: false
            };

        function checkDeclarationModifiers(element, index, array){
            switch(element){
                //public, protected, abstract zijn onbestaand in javascript: zijn dus betekenisloos.
                case "private":
                    declarationObject.isPrivate= true;
                    break;
                case "static":
                    declarationObject.isStatic = true;
                    break;
                case "final":
                    declarationObject.name.toUpperCase();
                    declarationObject.final = true;
                    break;
                default:
                    break;
            }
        }//TODO refactoren samen met gelijkaardige code zodat dit maar 1 keer voorkomt in de code

        declaration = declaration.split(" ");
        declarationObject.name = declaration[declaration.length - 1];
        declarationObject.type = declaration[declaration.length - 2];
        declaration.forEach(checkDeclarationModifiers);
        //TODO inbouwen verwerking typesoort!
        declaration = "var " + declarationObject.name + " = " + declarationObject.value;
        writeToConstructor(declaration, declarationObject.value, declarationObject.name, declarationObject.isPrivate, declarationObject.isStatic, declarationObject.final);
    }

    try {
        splitByLine.forEach(handleFields);
    } catch (e) {
        sendError(e.stack);
    }
    // TODO indien array/double/long/float/decimal: verwerken met Tim's functies
}

//function to write values to the constructorString
function writeToConstructor(total, value, name, isPrivate, isStatic, final){
        if (isPrivate&& isStatic) { //private static
            theClass.privateStatic += total + "\r\n\r\n"; //! geen speciale behandeling voor final aangezien private & static en op zich niet te wijzigen
        } else if (isPrivate) { //private
            theClass.forInnerConstructor += total + "\r\n\r\n";
        } else if (final) { //public final
            theClass.publicVarMethods += defineField("this", name, value, false, true, true);
        } else { //public
            theClass.publicVarMethods += "Inner" + theClass.constructorName + ".prototype." + name + " = " + value + "\r\n\r\n";
        }

}

//function to get an array of indexes of where in a method variables are declared
function findMethodVariables(method){
    try {
        return getIndices(/\b(?!(return|new|package|import|where|set|and|on|goto))[\w\ *[\]<,>]+ [\w]+(;| *=)/g, method);
    } catch (e) {
        sendError(e.stack);
    }
}

//function to define a string for defining fields on a property, defines default values in case you don't want to specify write, configure or enumerate (in that order)
//obj and val parameters are mandatory
//in a seperate function to make it more legible
function defineField(obj, name, val, write, configure, enumerate){

        return "Object.defineProperty(" + obj + ", '" + name + "', {\r\n\t" +
            "value: " + val + ",\r\n\t" +
            "enumerable: " + (enumerate ? enumerate : true) + ",\r\n\t" +
            "configurable: " + (configure ? configure : true) + ",\r\n\t" +
            "writable: " + (write ? write : true) + "\r\n\t" +
            "});\r\n\r\n";

}


//function to create a constructor in Java
//uses the helper function enforceNew
function createConstructor(){
    try {
        var constructorName = theClass.constructorName,
            constructorString = "var " + constructorName + " = " + "(function(){\r\n\t"; //open declaration

        constructorString += theClass.privateStatic + "\r\n\r\n"; //voeg alle private & static variables toe
        constructorString += defineInnerConstructor(constructorName, theClass.parameters); //voeg Inner Constructor toe met private vars/methods en constructorinhoud

        if (theClass.superclass) {
            constructorString += extendPrototype(constructorName, theClass.superclass) + "});";
        } //indien superklasse definieer het prototype als de superklasse

        constructorString += theClass.publicVarMethods + "\r\n"; //voeg public vars & methods toe
        return constructorString + "})();";
    } catch (e) {
        sendError(e.stack);
    }
}

//function to extend the prototype if necessary
//helperfunction for createConstructor
function extendPrototype(constructorName, superclass){
    try {
        return "Inner" + constructorName + ".prototype = Object.create(" + superclass + ".prototype, {\r\n\t" +
            "constructor: {\r\n\t\t" +
            "configurable: true,\r\n\t\t" +
            "enumerable: true, \r\n\t\t" +
            "value: Inner" + constructorName + ",\r\n\t\t" +
            "writable: true\r\n\t" +
            "}\r\n";
    } catch (e) {
        sendError(e.stack);
    }
}

//function to define the inner constructor
//helper function for createConstructor
function defineInnerConstructor(constructorName, parameters){
    try {
        var constructorData = "",
            length = theClass.constructor.length;
        if (length >= 1) {
            constructorData = "\t" + theClass.constructor[0].bodyProper + "\r\n";
            console.log("constructorData voor de eerste for lus is: " + constructorData);
            for (var i = 1; i < length; i++) {
                if (theClass.constructor[i].bodyProper.length > theClass.constructor[i - 1].bodyProper.length) {
                    constructorData = "\t" + theClass.constructor[i].bodyProper + "\r\n";
                }
            }
        }
        return "function Inner" + constructorName + "(" + theClass.parameters + "){\r\n\t" + enforceNew(constructorName, parameters) + constructorData + theClass.forInnerConstructor + "}\r\n\r\n";
    } catch (e) {
        sendError(e.stack);
    }
}


//function to enforce the use of new when calling a constructor (seperate function to make it more easily legible)
//helper function for defineInnerConstructor
function enforceNew(constructorName, parameters){
    try {
        return "if(!(this instanceof " + constructorName + ")){ " +
            "\r\n\t\t" + "return new " + constructorName + "(" + (parameters ? parameters : "") + ");\r\n\t }" +
            "\r\n\r\n";
    } catch (e) {
        sendError(e.stack);
    }
}

//function to process the class declaration
//uses the helper function createConstructorName
function processClassDeclaration(){
    try {
        var declaration = theClass.declaration,
            implement = theClass.declaration.indexOf("implements"),
            extend = theClass.declaration.indexOf("extends"),
            modifiers,
            extendDeclaration,
            i;

        //retrieve modifiers
        if (extend > 0) {
            modifiers = declaration.slice(0, extend).trim();
        } else if (implement > 0) {
            modifiers = declaration.slice(0, implement).trim();
        } else {
            modifiers = declaration.trim();
        }

        //process modifiers & assign className + constructorName
        modifiers = modifiers.split(" ");
        theClass.className = modifiers[modifiers.length - 1].trim();
        createConstructorName();
        for (i = 0; i < modifiers.length; i++) {
            switch (modifiers[i]) {
                //public, protected, abstract zijn onbestaand in javascript: zijn dus betekenisloos.
                case "private":
                    theClass.isPrivate= true;
                    break; //enkel voor inner classes
                case "static":
                    theClass.isStatic = true;
                    break; //enkel voor inner classes
                case "final":
                    theClass.final = true;
                    break;  //cannot be subclassed
                default:
                    break;
            }
        }

        //process extends
        if (extend > 0) {
            extendDeclaration = (extend > implement) ? declaration.slice(extend).trim() : declaration.slice(extend, implement).trim();
            extendDeclaration = extendDeclaration.split(" ");
            theClass.superclass = extendDeclaration[1];
        }

        //process implements
        if (implement > 0) {
            theClass.interfaces = [];
            implement = declaration.slice(implement).trim().split(",");
            for (i = 0; i < implement.length; i++) {
                implement[i] = implement[i].split(" ");
                theClass.interfaces[i] = implement[i][1].trim();
            }
        }
    } catch (e) {
        sendError(e.stack);
    }
}

//function to uppercase the first char of a class name
function createConstructorName(){
    try {
        var firstChar = theClass.className.charAt(0);
        theClass.constructorName = theClass.className.replace(firstChar, firstChar.toUpperCase());
    } catch (e) {
        sendError(e.stack);
    }
}

//function to remove comments in the code
function removeComments(input){
    try {
        input = input.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, ""); //remove multiline comments
        return input.replace(/\/\/.*/g, ""); //remove single line comments
    } catch (e) {
        sendError(e.stack);
    }
}

//function to send an Error to the creators of this module so they can analyse problems
function sendError(error)
{
    var r= confirm("Something went wrong.\nPlease share it with us. \nSo we can fix the error. \nThank You, MKNT");
    if(r==true)
    {
        window.location.href="mailto:michele.carette@student.howest.be;tim.dekiere@student.howest.be;niels.kuylle@student.howest.be;koen.cornelis@student.howest.be?subject=Error%20Message%20Project&body=" +
            escape(error);
    }
}

