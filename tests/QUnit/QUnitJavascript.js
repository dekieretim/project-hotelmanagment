/**
 * Created by Badlapje on 23/10/14.
 */
//common variable for all functions
var theClass = {
    imports: "",
    declaration: "",
    body: "",
    isStatic: false,
    isPrivate: false,
    final: false,
    superclass: false,
    interfaces: false,
    methodData: [],
    methods: [],
    parameters: "" ,
    publicVarMethods: "",
    privateStatic: "",
    forInnerConstructor: "",
    constructor: []
};

//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.module("changeMethodVariables", {
    setup: function(){

    },
    teardown: function(){

    }
});
QUnit.cases([
        {title: "basic example", indices: [0], method: "string test = 'this is a testfunction;'", expected: "var test = 'this is a testfunction;'"},
        {title: "with semi-colons and equal signs", indices: [0], method: "string test = 'dit is ook een string met een ; en een = en ook een = en een ;';", expected: "var test = 'dit is ook een string met een ; en een = en ook een = en een ;';"},
        {title: "for an ArrayList", indices: [2], method: "\r\nArrayList<test> testing;", expected: "\r\nvar testing;"},
        {title: "for an array definition", indices: [0], method: "int[] jos = new int[3];", expected: "var jos = new int[3];"},
        {title: "for an array literal", indices: [0], method: "int[] jos = {1,2,3};", expected: "var jos = {1,2,3};"},
        {title: "for a twodimensional array", indices: [0], method: "int[][] jefke = new int[3][2];", expected: "var jefke = new int[3][2];"},
        {title: "for a custom type", indices: [0], method: "TBookingController book;", expected: "var book;"},
        {title: "for several variables", indices: [0, 26, 85], method: "TBookingController book;\r\nint[][] jefke = new int[3][2];\r\nif(jefke){dosomething;}\r\n\r\nint[] jos = {1,2,3};", expected: "var book;\r\nvar jefke = new int[3][2];\r\nif(jefke){dosomething;}\r\n\r\nvar jos = {1,2,3};"}
    ]).test("Test changeMethodVariables function", 2, function(params){
        var actualResult = changeMethodVariables(params.indices, params.method);
        strictEqual(actualResult, params.expected);
        notStrictEqual(actualResult, params.method);
    });

QUnit.module("createConstructorName");
QUnit.cases([
        {title: "met enkel letters", className: "speler", expected: "Speler"},
        {title: "met enkel letters begin hoofdletter", className: "Speler", expected: "Speler"},
        {title: "met letters en cijfers begin cijfer", className: "speler1", expected: "Speler1"},
        {title: "met underscore als begin", className: "_speler", expected: "_speler"},
        {title: "met $-teken als begin", className: "$speler", expected: "$speler"}
    ]).test("Test createConstructorName function", 1, function(params){
        theClass.className = params.className;
        createConstructorName();
        var actualResult = theClass.constructorName;
        strictEqual(actualResult, params.expected);
    });

QUnit.module("defineInnerConstructor", {
    setup: function(){
            theClass.constructor = [];
            theClass.parameters =  "var een, var twee";
            theClass.forInnerConstructor = "dit is een tekstje dat in de innerconstructor komt";
        }
    });
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "met een korte body", constructorName: "TBookingController", constructor: [{bodyProper: "return Math.max(een, twee);"}], expected: "function InnerTBookingController(var een, var twee){\r\n\tif(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var een, var twee);\r\n\t }\r\n\r\n\treturn Math.max(een, twee);\r\ndit is een tekstje dat in de innerconstructor komt}\r\n\r\n"},
        {title: "met een middellange body", constructorName: "TBookingController", constructor: [{bodyProper: "return Math.max(een, twee);"}, {bodyProper: "Date from = (Date) this.fRequest.getVar(TApplication.kSession, \"from\");\r\nthis.fStartDate = this.getParam(\"from\", (from == null) ? new Date() : from, \"dd-MM-yyyy\");\r\nthis.fRequest.setVar(TApplication.kSession, \"from\", this.fStartDate);"}], expected: "function InnerTBookingController(var een, var twee){\r\n\tif(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var een, var twee);\r\n\t }\r\n\r\n\tDate from = (Date) this.fRequest.getVar(TApplication.kSession, \"from\");\r\nthis.fStartDate = this.getParam(\"from\", (from == null) ? new Date() : from, \"dd-MM-yyyy\");\r\nthis.fRequest.setVar(TApplication.kSession, \"from\", this.fStartDate);\r\ndit is een tekstje dat in de innerconstructor komt}\r\n\r\n"},
        {title: "met een lange body", constructorName: "TBookingController", constructor: [{bodyProper: "return Math.max(een, twee);"}, {bodyProper: "Date from = (Date) this.fRequest.getVar(TApplication.kSession, \"from\");\r\nthis.fStartDate = this.getParam(\"from\", (from == null) ? new Date() : from, \"dd-MM-yyyy\");\r\nthis.fRequest.setVar(TApplication.kSession, \"from\", this.fStartDate);"}, {bodyProper: "String s = this.getParam(\"sorter\",\"rt\");\r\nthis.setVar(\"sorter\",s);\r\nString sort = (s.equals(\"rt\")) ? \"roomtypes.name_nl, rooms.nr\" : \"rooms.nr, roomtypes.name_nl\";\r\nArrayList<TRecord> aList = new TSQL(this,\r\n\"select occupancies.*, rooms.nr, rooms.name as name, if(booking is null,1,0) as free, date_format(occupancies.at, '%a') as weekday,\" +\" if(bookings.checkedin='Y','cin','') as cin, if(bookings.checkedout='Y','cout','') as cout, occupancies.at, rooms.status \" +\r\n\" from occupancies join rooms on rooms.id = occupancies.room join roomtypes on rooms.roomtype = roomtypes.id\" +\r\n\"   left join bookings on occupancies.booking = bookings.id \" +\r\n\" where occupancies.at between date(?) and date(?) order by \" + sort + \", occupancies.at\")\r\n.setValue( this.fStartDate )\r\n.setValue( new Date( this.fStartDate.getTime() + 21 * TCalendarController.kOneDay ))\r\n.getList();\r\nthis.setVar(\"rcalendar\", aList);\r\n\r\nif (! aList.isEmpty())\r\nthis.genDateList();"}], expected: "function InnerTBookingController(var een, var twee){\r\n\tif(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var een, var twee);\r\n\t }\r\n\r\n\tString s = this.getParam(\"sorter\",\"rt\");\r\nthis.setVar(\"sorter\",s);\r\nString sort = (s.equals(\"rt\")) ? \"roomtypes.name_nl, rooms.nr\" : \"rooms.nr, roomtypes.name_nl\";\r\nArrayList<TRecord> aList = new TSQL(this,\r\n\"select occupancies.*, rooms.nr, rooms.name as name, if(booking is null,1,0) as free, date_format(occupancies.at, '%a') as weekday,\" +\" if(bookings.checkedin='Y','cin','') as cin, if(bookings.checkedout='Y','cout','') as cout, occupancies.at, rooms.status \" +\r\n\" from occupancies join rooms on rooms.id = occupancies.room join roomtypes on rooms.roomtype = roomtypes.id\" +\r\n\"   left join bookings on occupancies.booking = bookings.id \" +\r\n\" where occupancies.at between date(?) and date(?) order by \" + sort + \", occupancies.at\")\r\n.setValue( this.fStartDate )\r\n.setValue( new Date( this.fStartDate.getTime() + 21 * TCalendarController.kOneDay ))\r\n.getList();\r\nthis.setVar(\"rcalendar\", aList);\r\n\r\nif (! aList.isEmpty())\r\nthis.genDateList();\r\ndit is een tekstje dat in de innerconstructor komt}\r\n\r\n"}
    ]).test("Test defineInnerConstructor function", 1, function(params){
        theClass.constructor = params.constructor;
        var actualResult = defineInnerConstructor(params.constructorName, theClass.parameters);
        strictEqual(actualResult, params.expected);
    });

QUnit.module("enforceNew");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "zonder parameters", constructorName: "TBookingController", deParameters: "", expected: "if(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController();\r\n\t }\r\n\r\n"},
        {title: "met één parameter", constructorName: "TBookingController", deParameters: "var parameter", expected: "if(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var parameter);\r\n\t }\r\n\r\n"},
        {title: "met meerdere parameters", constructorName: "TBookingController", deParameters: "var parameter1, var parameter2, var parameter3", expected: "if(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var parameter1, var parameter2, var parameter3);\r\n\t }\r\n\r\n"}
    ]).test("Test enforceNew function", 1, function(params){
        var actualResult = enforceNew(params.constructorName, params.deParameters);
        strictEqual(actualResult, params.expected);
    });

QUnit.module("ExtendPrototype");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic example", constructorName: "TBookingController", superclass: "THotelController", expected: "InnerTBookingController.prototype = Object.create(THotelController.prototype, {\r\n\tconstructor: {\r\n\t\tconfigurable: true,\r\n\t\tenumerable: true, \r\n\t\tvalue: InnerTBookingController,\r\n\t\twritable: true\r\n\t}\r\n"}
    ]).test("Test ExtendPrototype function", 1, function(params){ //TODO provide more cases for extendprototype
        var actualResult = extendPrototype(params.constructorName, params.superclass);
        strictEqual(actualResult, params.expected);
    });

QUnit.module("forTranslator");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic initialisation", indices: [0], method: "for ( int i = 0;  i < test.length ( ) + 1;  i++) {" + "System.out.println( size length {}();: .size() .length() string var int test[i]);" + "}",
            expected: "for ( var i = 0; i<test.length+1; i++) {" + "System.out.println( size length {}();: .size() .length() string var int test[i]);" + "}"},
        {title: "with semi-colons and equal signs", method: "for (String i : mylist){" + 'System.out.println("foreachlist " + i);' + "}",
            expected: "for (var i  in  mylist){" + 'System.out.println("foreachlist " + i);' + "}"}
    ]).test("Test changeMethodVariables function", 2, function(params){
        var actualResult = translateForLoop(params.method);
        strictEqual(actualResult, params.expected);
        notStrictEqual(actualResult, params.method);
    });

QUnit.module("removeComments");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "MultilineComments", indices: [0], method: "ActualCode/*commentstring\n\
                commentstring\n\
                commentstring*/ActualCode\n\
                ActualCode\n\
                /***commentstring\n\
                 *commentstring\n\
                 *commentstring\n\
                 *commentstring\n\
                 */ActualCode\n\
                ActualCode\n\
                ActualCode\n\
                ActualCode\n\
                ActualCode",
            expectedCommentLocation: -1,
            expectedNrOfRemainingCode: 8},
        {title: "MultilineComments", indices: [0], method: "ActualCode\n\
                //commentstring\n\
                ActualCode\n\
                //commentstring\n\
                ActualCode",
            expectedCommentLocation: -1,
            expectedNrOfRemainingCode: 3}
    ]).test("Test changeMethodVariables function", 3, function(params){
        var actualResult = removeComments(params.method);
        strictEqual(actualResult.indexOf(/commentstring/g), params.expectedCommentLocation);
        strictEqual(actualResult.match(/ActualCode/g).length, params.expectedNrOfRemainingCode);
        notStrictEqual(actualResult, params.method);
    });

QUnit.module("ArrayTranslator");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic example", method: "var[] myIntArray = new var[3];", expected: /var *myIntArray *\= *\[ *\] *\; */g},
        {title: "initialization with elements", method: "var[] myIntArray = {1,2,3};", expected: /var *myIntArray *\= *\[ *1 *, *2 *, *3 *\] *\; */g},
        {title: "initialization with elements v2", method: "var[] myIntArray = new var[]{1,2,3};", expected: /var *myIntArray *\= *\[ *1 *, *2 *, *3 *\] *\; */g},
        {title: "whitespace test", method: "var    [      ]    myIntArray    =      {        1      ,       2       ,       3 }   ;   ", expected: /var *myIntArray *\= *\[ *1 *, *2 *, *3 *\] *\; */g}
    ]).test("Test ArrayTransloator function", 2, function(params){
        var actualResult = translateArray(params.method);
        equal(params.expected.test(actualResult), true);
        notStrictEqual(actualResult, params.method);
    });

QUnit.module("FindMethodVariables");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic initialisation int", method: "int a = 5;",
            expected: [0]},
        {title: "basic initialisation String", method: 'String b = "hallo";',
            expected: [0]},
        {title: "string array 1", method: "String [] c = new String[3];",
            expected: [0]},
        {title: "string array 2", method: "int[] d = new int[3];",
            expected: [0]},
        {title: "string array 3", method: "int[] e = {1,2,3};",
            expected: [0]},
        {title: "string array 4", method: "int[] f = new int []{1,2,3};",
            expected: [0]},
        {title: "basic declaration int", method: "int g;",
            expected: [0]},
        {title: "failtest keyword", method: "return h;",
            expected: []},
        {title: "arraylist initialization 1", method: "ArrayList<int> i = new ArrayList<String>() {{" +
            "add(8);" +
            "add(9);" +
            "add(10);" +
            "}}",
            expected: [0]},
        {title: "arraylist initialization 2", method: "ArrayList<String> j = new ArrayList<String>();",
            expected: [0]},
        {title: "map initialization", method: "Map <int, String> k = new HashMap<int, String>();",
            expected: [0]},
        {title: "other class test", method: "TboekingControler l = new TboekingControler();",
            expected: [0]},
        {title: "basic map initialisation", method: "Map <int, String> k = new HashMap<int, String>();",
            expected: [0]},
        {title: "map inside map initialization", method: "Map <int, Map<String, int>> k = new HashMap<int, Map<String, int>();",
            expected: [0]},
        {title: "2D array initialization", method: "int[][] l = new int[5][];",
            expected: [0]},
        {title: "element initialization (should not match regex)", method: "l[0] = new int[10];",
            expected: []},
        {title: "2d array initialization", method: "int[][] multi = new int[][]{" +
            "{ 0, 0, 0, 0}," +
            "{ 0, 0, 0, 0}" +
            "};",
            expected: [0]}
    ]).test("Test ExtendPrototype function", function(params){
        var actualResult = findMethodVariables(params.method);
        deepEqual(actualResult, params.expected);
    });
//module
QUnit.module("getIndices");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "basic test", regex: /dit/g, testString: "dit is een string, waar dit woordje dit dus veel in staat.  ditto trouwens voor ditritus, er, lapsus: detritus!",
        expected: [0, 24, 36, 60, 80]},
    {title: "basic test 2", regex:  /\bdit\b/g, testString: "dit is een string, waar dit woordje dit dus veel in staat.  ditto trouwens voor ditritus, er, lapsus: detritus!",
        expected: [0, 24, 36]}

]).test("Test getIndices function", function(params){
    var actualResult = getIndices(params.regex, params.testString);
    deepEqual(actualResult, params.expected);
});


QUnit.module("prepareFields");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "1", splitByLine: ["private static final long serialVersionUID = 1L;", "", "	private static final boolean kDoCash = false;", "", "	public String testje = '1 = 2';", "	public String test2 = 'dit is ongelooflijk; hoe erg dat het is';", "	public String test3 = 'dit is nog erger; want 1 = 3';"],
        expected: [
            {
                content: "private static final long serialVersionUID = 1L;",
                equals: [43],
                semiColons: [47]
            },
            {
                content: "private static final boolean kDoCash = false;",
                equals: [37],
                semiColons: [44]
            },
            {
                content: "public String testje = '1 = 2';",
                equals: [21, 26],
                semiColons: [30]
            },
            {
                content: "public String test2 = 'dit is ongelooflijk; hoe erg dat het is';",
                equals: [20],
                semiColons: [42, 63]
            },
            {
                content: "public String test3 = 'dit is nog erger; want 1 = 3';",
                equals: [20, 48],
                semiColons: [39, 52]
            }
        ]}

]).test("Test getIndices function", function(params){
    var actualResult = prepareFields(params.splitByLine);
    deepEqual(actualResult, params.expected);
});

var theClass = null;
function resetTheClass(){
    theClass = {
        constructor: [],
        privateStatic: "",
        forInnerConstructor: "",
        publicVarMethods: "",
        constructorName: "TBookingController"
    };

}

QUnit.module("FindMethodVariables");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "1", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"dit is ongelooflijk; hoe erg dat het is",
        name: "test2", isPrivate:true, isStatic:true, final: false, expected: "var test2 = 'dit is ongelooflijk; hoe erg dat het is';\r\n\r\n", location: 1},
    {title: "2", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"dit is ongelooflijk; hoe erg dat het is",
        name: "test2", isPrivate:true, isStatic:true, final: true, expected: "var test2 = 'dit is ongelooflijk; hoe erg dat het is';\r\n\r\n", location: 1},
    {title: "3", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"dit is ongelooflijk; hoe erg dat het is",
        name: "test2", isPrivate:true, isStatic:false, final: true, expected: "var test2 = 'dit is ongelooflijk; hoe erg dat het is';\r\n\r\n", location: 2},
    {title: "4", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"dit is ongelooflijk; hoe erg dat het is",
        name: "test2", isPrivate:true, isStatic:false, final: false, expected: "var test2 = 'dit is ongelooflijk; hoe erg dat het is';\r\n\r\n", location: 2},
    {title: "5", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"'dit is ongelooflijk; hoe erg dat het is'",
        name: "test2", isPrivate:false, isStatic:false, final: false, expected: "InnerTBookingController.prototype.test2 = 'dit is ongelooflijk; hoe erg dat het is'\r\n\r\n", location: 3},
    {title: "6", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"'dit is ongelooflijk; hoe erg dat het is'",
        name: "test2", isPrivate:false, isStatic:true, final: false, expected: "InnerTBookingController.prototype.test2 = 'dit is ongelooflijk; hoe erg dat het is'\r\n\r\n", location: 3},
    {title: "9", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val: "'dit is ongelooflijk; hoe erg dat het is'",
        name: "test2", isPrivate:false, isStatic:false, final: true, expected: "Object.defineProperty(this, 'test2', {\r\n\tvalue: 'dit is ongelooflijk; hoe erg dat het is',\r\n\tenumerable: true,\r\n\tconfigurable: true,\r\n\twritable: true\r\n\t});\r\n\r\n", location: 3}

]).test("Test WriteToConstructor function", function(params){
    resetTheClass();
    writeToConstructor(params.stringToWrite, params.val, params.name, params.isPrivate, params.isStatic, params.final);
    deepEqual(theClass[Object.keys(theClass)[params.location]], params.expected);
});