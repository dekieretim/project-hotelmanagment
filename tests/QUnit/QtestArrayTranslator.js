/**
 * Created by Tim on 04/11/2014.
 */
function translateArray(arrayToTranslate) {
    var translatedArray = arrayToTranslate;

//    removes "new var"
    translatedArray = translatedArray.replace(/=\s*new\s*var/g, "= ");
//    removes []
    translatedArray = translatedArray.replace(/\[ *\]/g, "");
//    replaces { by [
    translatedArray = translatedArray.replace(/\{/g, "[");
//    replaces } by ]
    translatedArray = translatedArray.replace(/\}/g, "]");
//    replaces [\d] by []
    translatedArray = translatedArray.replace(/\[\s*\d\s*\]/g, "[]");
    return translatedArray;
}

QUnit.module("TestArrayTranslator");

//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "initialization 1", method: "var[] myIntArray = new var[3];",
        expected: /var *myIntArray *\= *\[ *\] *\; */g},
    {title: "initialisation 2", method: "var[] myIntArray = {1,2,3};",
        expected: /var *myIntArray *\= *\[ *1 *, *2 *, *3 *\] *\; */g},
    {title: "initialisation 3", method: "var[] myIntArray = new var[]{1,2,3};",
        expected: /var *myIntArray *\= *\[ *1 *, *2 *, *3 *\] *\; */g},
    {title: "initialisation 4", method: "var    [      ]    myIntArray    =      {        1      ,       2       ,       3 }   ;   ",
        expected: /var *myIntArray *\= *\[ *1 *, *2 *, *3 *\] *\; */g}

]).test("Test ExtendPrototype function", function(params){
    var actualResult = translateArray(params.method);
    strictEqual(actualResult.match(params.expected).length, 1);
});