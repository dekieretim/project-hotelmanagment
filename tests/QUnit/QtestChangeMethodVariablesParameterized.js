//function to test
function changeMethodVariables(indices, method){
    var i = 0, //variable to increase the performance of the for loop
        length = indices.length, //variable to increase the performance of the for loop
        counter = 0, //variable to offset the startindex with
        partFirst = "", //variable to store the first part of the method
        partFrom = "", //variable to store the part of the method in starting at the variabele declaration till the end of the method
        type = ""; //variable to store the type of the variable for easy reference in two statements

    for (i; i < length; i++) {
        partFirst = method.slice(0, indices[i] - counter); //store the first part of the string for easy concatenation
        partFrom = method.slice(indices[i] - counter); //slice out the part of the string beginning at the variable declaration
        type = partFrom.slice(0, partFrom.search(/\s/)); //store the type of the variable for easy use in the next two statements
        partFrom = partFrom.replace(type, "var");//replace the type by var
        counter += type.length - 3; //augment counter to offset replacing the type by var;
        method = partFirst + partFrom; //reassemble method so it's usable again for the next variable
    }

    return method;
}

//module
QUnit.module("changeMethodVariables");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "basic example", indices: [0], method: "string test = 'this is a testfunction;'", expected: "var test = 'this is a testfunction;'"},
        {title: "with semi-colons and equal signs", indices: [0], method: "string test = 'dit is ook een string met een ; en een = en ook een = en een ;';", expected: "var test = 'dit is ook een string met een ; en een = en ook een = en een ;';"},
        {title: "for an ArrayList", indices: [2], method: "\r\nArrayList<test> testing;", expected: "\r\nvar testing;"},
        {title: "for an array definition", indices: [0], method: "int[] jos = new int[3];", expected: "var jos = new int[3];"},
        {title: "for an array literal", indices: [0], method: "int[] jos = {1,2,3};", expected: "var jos = {1,2,3};"},
        {title: "for a twodimensional array", indices: [0], method: "int[][] jefke = new int[3][2];", expected: "var jefke = new int[3][2];"},
        {title: "for a custom type", indices: [0], method: "TBookingController book;", expected: "var book;"},
        {title: "for several variables", indices: [0, 26, 85], method: "TBookingController book;\r\nint[][] jefke = new int[3][2];\r\nif(jefke){dosomething;}\r\n\r\nint[] jos = {1,2,3};", expected: "var book;\r\nvar jefke = new int[3][2];\r\nif(jefke){dosomething;}\r\n\r\nvar jos = {1,2,3};"}
    ]).test("Test changeMethodVariables function", function(params){
        var actualResult = changeMethodVariables(params.indices, params.method);
        strictEqual(actualResult, params.expected);
        notStrictEqual(actualResult, params.method);
    });
