//function to test
function createConstructorName(){
    var firstChar = theClass.className.charAt(0);
    theClass.constructorName = theClass.className.replace(firstChar, firstChar.toUpperCase());
}

//module
QUnit.module("createConstructorName");

console.log(theClass);
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "met enkel letters", className: "speler", expected: "Speler"},
        {title: "met enkel letters begin hoofdletter", className: "Speler", expected: "Speler"},
        {title: "met letters en cijfers begin cijfer", className: "speler1", expected: "Speler1"},
        {title: "met underscore als begin", className: "_speler", expected: "_speler"},
        {title: "met $-teken als begin", className: "$speler", expected: "$speler"}
    ]).test("Test createConstructorName function", function(params){
        theClass.className = params.className;
        createConstructorName()
        var actualResult = theClass.constructorName;
        strictEqual(actualResult, params.expected);
    });