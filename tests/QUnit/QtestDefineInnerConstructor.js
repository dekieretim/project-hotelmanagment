//common object
var theClass = {
    constructor: [],
    parameters: "",
    forInnerConstructor: ""
};

//function to test
function defineInnerConstructor(constructorName, parameters){
    var constructorData = theClass.constructor[0] == undefined ? "" : ("\t" + theClass.constructor[0].bodyProper + "\r\n"),
        length = theClass.constructor.length;
    if (length > 1) {
        for (var i = 1; i < length; i++) {
            if (theClass.constructor[i].bodyProper.length > theClass.constructor[i - 1].bodyProper.length) {
                constructorData = "\t" + theClass.constructor[i].bodyProper + "\r\n";
            }
        }
    }
    return "function Inner" + constructorName + "(" + theClass.parameters + "){\r\n\t" + enforceNew(constructorName, parameters) + constructorData + theClass.forInnerConstructor + "}\r\n\r\n";
}

//helper function
function enforceNew(constructorName, parameters){
    return "if(!(this instanceof " + constructorName + ")){ " +
        "\r\n\t\t" + "return new " + constructorName + "(" + (parameters ? parameters : "") + ");\r\n\t }" +
        "\r\n\r\n";
}

//module
QUnit.module("defineInnerConstructor", {
    setup: function(){
        theClass.constructor = [];
        theClass.parameters =  "var een, var twee";
        theClass.forInnerConstructor = "dit is een tekstje dat in de innerconstructor komt";
    }
});
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "met een korte body", constructorName: "TBookingController", constructor: [{bodyProper: "return Math.max(een, twee);"}], expected: "function InnerTBookingController(var een, var twee){\r\n\tif(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var een, var twee);\r\n\t }\r\n\r\n\treturn Math.max(een, twee);\r\ndit is een tekstje dat in de innerconstructor komt}\r\n\r\n"},
        {title: "met een middellange body", constructorName: "TBookingController", constructor: [{bodyProper: "return Math.max(een, twee);"}, {bodyProper: "Date from = (Date) this.fRequest.getVar(TApplication.kSession, \"from\");\r\nthis.fStartDate = this.getParam(\"from\", (from == null) ? new Date() : from, \"dd-MM-yyyy\");\r\nthis.fRequest.setVar(TApplication.kSession, \"from\", this.fStartDate);"}], expected: "function InnerTBookingController(var een, var twee){\r\n\tif(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var een, var twee);\r\n\t }\r\n\r\n\tDate from = (Date) this.fRequest.getVar(TApplication.kSession, \"from\");\r\nthis.fStartDate = this.getParam(\"from\", (from == null) ? new Date() : from, \"dd-MM-yyyy\");\r\nthis.fRequest.setVar(TApplication.kSession, \"from\", this.fStartDate);\r\ndit is een tekstje dat in de innerconstructor komt}\r\n\r\n"},
        {title: "met een lange body", constructorName: "TBookingController", constructor: [{bodyProper: "return Math.max(een, twee);"}, {bodyProper: "Date from = (Date) this.fRequest.getVar(TApplication.kSession, \"from\");\r\nthis.fStartDate = this.getParam(\"from\", (from == null) ? new Date() : from, \"dd-MM-yyyy\");\r\nthis.fRequest.setVar(TApplication.kSession, \"from\", this.fStartDate);"}, {bodyProper: "String s = this.getParam(\"sorter\",\"rt\");\r\nthis.setVar(\"sorter\",s);\r\nString sort = (s.equals(\"rt\")) ? \"roomtypes.name_nl, rooms.nr\" : \"rooms.nr, roomtypes.name_nl\";\r\nArrayList<TRecord> aList = new TSQL(this,\r\n\"select occupancies.*, rooms.nr, rooms.name as name, if(booking is null,1,0) as free, date_format(occupancies.at, '%a') as weekday,\" +\" if(bookings.checkedin='Y','cin','') as cin, if(bookings.checkedout='Y','cout','') as cout, occupancies.at, rooms.status \" +\r\n\" from occupancies join rooms on rooms.id = occupancies.room join roomtypes on rooms.roomtype = roomtypes.id\" +\r\n\"   left join bookings on occupancies.booking = bookings.id \" +\r\n\" where occupancies.at between date(?) and date(?) order by \" + sort + \", occupancies.at\")\r\n.setValue( this.fStartDate )\r\n.setValue( new Date( this.fStartDate.getTime() + 21 * TCalendarController.kOneDay ))\r\n.getList();\r\nthis.setVar(\"rcalendar\", aList);\r\n\r\nif (! aList.isEmpty())\r\nthis.genDateList();"}], expected: "function InnerTBookingController(var een, var twee){\r\n\tif(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var een, var twee);\r\n\t }\r\n\r\n\tString s = this.getParam(\"sorter\",\"rt\");\r\nthis.setVar(\"sorter\",s);\r\nString sort = (s.equals(\"rt\")) ? \"roomtypes.name_nl, rooms.nr\" : \"rooms.nr, roomtypes.name_nl\";\r\nArrayList<TRecord> aList = new TSQL(this,\r\n\"select occupancies.*, rooms.nr, rooms.name as name, if(booking is null,1,0) as free, date_format(occupancies.at, '%a') as weekday,\" +\" if(bookings.checkedin='Y','cin','') as cin, if(bookings.checkedout='Y','cout','') as cout, occupancies.at, rooms.status \" +\r\n\" from occupancies join rooms on rooms.id = occupancies.room join roomtypes on rooms.roomtype = roomtypes.id\" +\r\n\"   left join bookings on occupancies.booking = bookings.id \" +\r\n\" where occupancies.at between date(?) and date(?) order by \" + sort + \", occupancies.at\")\r\n.setValue( this.fStartDate )\r\n.setValue( new Date( this.fStartDate.getTime() + 21 * TCalendarController.kOneDay ))\r\n.getList();\r\nthis.setVar(\"rcalendar\", aList);\r\n\r\nif (! aList.isEmpty())\r\nthis.genDateList();\r\ndit is een tekstje dat in de innerconstructor komt}\r\n\r\n"}
    ]).test("Test defineInnerConstructor function", 1, function(params){
        theClass.constructor = params.constructor;
        var actualResult = defineInnerConstructor(params.constructorName, theClass.parameters);
        strictEqual(actualResult, params.expected);
    });


