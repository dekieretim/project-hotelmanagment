//function to test
function enforceNew(constructorName, parameters){
    return "if(!(this instanceof " + constructorName + ")){ " +
        "\r\n\t\t" + "return new " + constructorName + "(" + (parameters ? parameters : "") + ");\r\n\t }" +
        "\r\n\r\n";
}

//common object
var theClass = {
    constructor: [],
    forInnerConstructor: "dit is een tekstje dat in de innerconstructor komt"
};

//module
QUnit.module("enforceNew");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
        {title: "zonder parameters", constructorName: "TBookingController", deParameters: "", expected: "if(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController();\r\n\t }\r\n\r\n"},
        {title: "met één parameter", constructorName: "TBookingController", deParameters: "var parameter", expected: "if(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var parameter);\r\n\t }\r\n\r\n"},
        {title: "met meerdere parameters", constructorName: "TBookingController", deParameters: "var parameter1, var parameter2, var parameter3", expected: "if(!(this instanceof TBookingController)){ \r\n\t\treturn new TBookingController(var parameter1, var parameter2, var parameter3);\r\n\t }\r\n\r\n"}
    ]).test("Test enforceNew function", function(params){
        var actualResult = enforceNew(params.constructorName, params.deParameters);
        strictEqual(actualResult, params.expected);
    });

