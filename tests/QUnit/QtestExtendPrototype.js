/**
 * Created by Tim on 23/10/2014.
 */
function extendPrototype(constructorName, superclass){
    return "Inner" + constructorName + ".prototype = Object.create(" + superclass + ".prototype, {\r\n\t" +
        "constructor: {\r\n\t\t" +
        "configurable: true,\r\n\t\t" +
        "enumerable: true, \r\n\t\t" +
        "value: Inner" + constructorName + ",\r\n\t\t" +
        "writable: true\r\n\t" +
        "}\r\n";
}

//mock methods


QUnit.module("ExtendPrototype");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "basic example", constructorName: "TBookingController", superclass: "THotelController", expected: "InnerTBookingController.prototype = Object.create(THotelController.prototype, {\r\n\tconstructor: {\r\n\t\tconfigurable: true,\r\n\t\tenumerable: true, \r\n\t\tvalue: InnerTBookingController,\r\n\t\twritable: true\r\n\t}\r\n"}
]).test("Test ExtendPrototype function", function(params){
    var actualResult = extendPrototype(params.constructorName, params.superclass);
    strictEqual(actualResult, params.expected);
});
