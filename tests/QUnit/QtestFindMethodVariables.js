/**
 * Created by Tim on 23/10/2014.
 */
function findMethodVariables(method){
    try {
        return getIndices(/\b(?!(return|new|package|import|where|set|and|on|goto))[\w\ *[\]<,>]+ [\w]+(;| *=)/g, method);
    } catch (e) {
        sendError(e.stack);
    }
}

function getIndices(regex, theString){
    try {
        var result, indices = [];
        while ((result = regex.exec(theString))) {
            indices.push(result.index);
        }

        return indices;
    } catch (e) {
        sendError(e.stack);
    }
}

QUnit.module("FindMethodVariables");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "basic initialisation int", method: "int a = 5;",
        expected: [0]},
    {title: "basic initialisation String", method: 'String b = "hallo";',
        expected: [0]},
    {title: "string array 1", method: "String [] c = new String[3];",
        expected: [0]},
    {title: "string array 2", method: "int[] d = new int[3];",
        expected: [0]},
    {title: "string array 3", method: "int[] e = {1,2,3};",
        expected: [0]},
    {title: "string array 4", method: "int[] f = new int []{1,2,3};",
        expected: [0]},
    {title: "basic declaration int", method: "int g;",
        expected: [0]},
    {title: "failtest keyword", method: "return h;",
        expected: []},
    {title: "arraylist initialization 1", method: "ArrayList<int> i = new ArrayList<String>() {{" +
        "add(8);" +
        "add(9);" +
        "add(10);" +
        "}}",
        expected: [0]},
    {title: "arraylist initialization 2", method: "ArrayList<String> j = new ArrayList<String>();",
        expected: [0]},
    {title: "map initialization", method: "Map <int, String> k = new HashMap<int, String>();",
        expected: [0]},
    {title: "other class test", method: "TboekingControler l = new TboekingControler();",
        expected: [0]},
    {title: "basic map initialisation", method: "Map <int, String> k = new HashMap<int, String>();",
        expected: [0]},
    {title: "map inside map initialization", method: "Map <int, Map<String, int>> k = new HashMap<int, Map<String, int>();",
        expected: [0]},
    {title: "2D array initialization", method: "int[][] l = new int[5][];",
        expected: [0]},
    {title: "element initialization (should not match regex)", method: "l[0] = new int[10];",
        expected: []},
    {title: "2d array initialization", method: "int[][] multi = new int[][]{" +
    "{ 0, 0, 0, 0}," +
    "{ 0, 0, 0, 0}" +
    "};",
        expected: [0]}
]).test("Test ExtendPrototype function", function(params){
    var actualResult = findMethodVariables(params.method);
    deepEqual(actualResult, params.expected);
});

// for future reference

//int a = 5;
//String b = "hallo";
//String [] c = new String[3];
//int[] d = new int[3];
//int[] e = {1,2,3};
//int[] f = new int []{1,2,3};
//int g;
//return h;
//ArrayList<int> i = new ArrayList<String>() {{
//    add(8);
//    add(9);
//    add(10);
//}}
//ArrayList<String> j = new ArrayList<String>();
//Map <int, String> k = new HashMap<int, String>();
//TboekingControler l = new TboekingControler();
//Map <int, String> k = new HashMap<int, String>();
//Map <int, Map<String, int>> k = new HashMap<int, Map<String, int>();
//int[][] l = new int[5][];
//l[0] = new int[10];
//int[][] multi = new int[][]{
//    { 0, 0, 0, 0},
//    { 0, 0, 0, 0}
//};
