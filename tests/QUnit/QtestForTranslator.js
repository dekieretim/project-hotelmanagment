/**
 * Created by Tim on 16/10/2014.
 */

function translateForLoop(stringToTranslate) {
    var forIdentifierPart, translatedForLoop = "could not translate for loop", indexType;

    stringToTranslate = stringToTranslate.trim();

    stringToTranslate = stringToTranslate.replace(/[ ]{2,}/g, " ");
    //this replaces all occurrences of more than one whitespace by one whitespace.

    chooseCorrectLoopTypeFunction(stringToTranslate);

    function chooseCorrectLoopTypeFunction(unidentifiedForLoop) {
        forIdentifierPart = getForIdentifierPart(unidentifiedForLoop);

        if (forIdentifierPart.indexOf(";") != -1) {
            /*index of ";" is -1 if the string does not contain ";" -->  it's a regular for loop*/
            translateRegularFor(unidentifiedForLoop);
        }

        else {
            /*it's a foreach loop*/
            translateForeachFor(unidentifiedForLoop);
        }
        //if it's not a for loop, "could not translate for loop" will be returned

    }

    function getForIdentifierPart(forLoopString) {
        return forLoopString.split("{")[0];
        /*")" would also include a possible bracket from ".size()"*/
    }

    function translateForeachFor(foreachString) {
//        converts a java foreach loop into a javascript foreach loop
//        var hulp, varname;
//        console.log("foreachfor");
        indexType = getIndexType(foreachString);
        foreachString = foreachString.replace(indexType, "var");
        foreachString = foreachString.replace(":", " in ");
//        hulp = getForIdentifierPart(foreachString);
//        hulp = hulp.replace(")", "");
//        varname = hulp.split("<").pop();
//        definitely works for this because i just inserted a "<"
//        console.log(varname);

//        foreachString = foreachString.replace(varname, varname + ".length; i++");
//        console.log(foreachString);

        translatedForLoop = foreachString;
    }

    function translateRegularFor(regularForString) {
        var help, help2, triggerPart, firstSemicolonPos, secondSemicolonPos, newForIdentifierPart;

        indexType = getIndexType(regularForString);

        regularForString = regularForString.replace(indexType, "var");
//        console.log(regularForString);

        forIdentifierPart = getForIdentifierPart(regularForString);
        newForIdentifierPart = forIdentifierPart;

//        all code after this translates .Size() and .length() to ".length"
        firstSemicolonPos = newForIdentifierPart.indexOf(";");
        secondSemicolonPos = newForIdentifierPart.lastIndexOf(";");
//        returns first and second index of a semicolon
        triggerPart = newForIdentifierPart.split(";")[1];
//        the part between the 2 semicolons
        triggerPart = triggerPart.replace(/\s/g, "");
//        removes all whitespace in this part

        newForIdentifierPart = newForIdentifierPart.slice(0, firstSemicolonPos) + "; " + triggerPart + newForIdentifierPart.slice(secondSemicolonPos);
//        put it back between the semicolons

        regularForString = regularForString.replace(forIdentifierPart, newForIdentifierPart);
        help = newForIdentifierPart.toLowerCase().indexOf(".length()");
//        if the identifier contains .length() this will return the index, else it returns -1
        help2 = newForIdentifierPart.toLowerCase().indexOf(".size()");
//        if the identifier contains .size() this will return the index, else it returns -1
        if (help != -1) {
            regularForString = regularForString.replace(/\.length\(\)/i, ".length");
//            this is usually the same as .replace(".length()".....) but safer, (/i = ignoring case)
//            This is easier and safer than trying to make a part of the identifier toLowerCase because it may contain user-defined variables and methods
        } else if (help2 != -1) {
            regularForString = regularForString.replace(/\.size\(\)/i, ".length");
//            same reason as above
        }

        translatedForLoop = regularForString;
    }

    function getIndexType(string) {

        var help4, betweenBrackets;
//        returns the type of the iterator, usually "int"
        betweenBrackets = string.split("(")[1];
//        return the part between brackets
        help4 = betweenBrackets.trim();
//        removes whitespace
        indexType = help4.split(" ")[0];
//        first word
        return indexType;
    }


    return translatedForLoop;

}
//module
QUnit.module("forTranslator");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "basic initialisation", indices: [0], method: "for ( int i = 0;  i < test.length ( ) + 1;  i++) {" + "System.out.println( size length {}();: .size() .length() string var int test[i]);" + "}",
        expected: "for ( var i = 0; i<test.length+1; i++) {" + "System.out.println( size length {}();: .size() .length() string var int test[i]);" + "}"},
    {title: "with semi-colons and equal signs", method: "for (String i : mylist){" + 'System.out.println("foreachlist " + i);' + "}",
        expected: "for (var i  in  mylist){" + 'System.out.println("foreachlist " + i);' + "}"}
]).test("Test changeMethodVariables function", function(params){
    var actualResult = translateForLoop(params.method);
    strictEqual(actualResult, params.expected);
    notStrictEqual(actualResult, params.method);
});
