/**
 * Created by Tim on 13/11/2014.
 */


function getIndices(regex, theString){
    var result, indices = [];
    while ((result = regex.exec(theString))) {
        indices.push(result.index);
    }

    return indices;
}
//module
QUnit.module("getIndices");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "basic test", regex: /dit/g, testString: "dit is een string, waar dit woordje dit dus veel in staat.  ditto trouwens voor ditritus, er, lapsus: detritus!",
        expected: [0, 24, 36, 60, 80]},
    {title: "basic test 2", regex:  /\bdit\b/g, testString: "dit is een string, waar dit woordje dit dus veel in staat.  ditto trouwens voor ditritus, er, lapsus: detritus!",
        expected: [0, 24, 36]}

]).test("Test getIndices function", function(params){
    var actualResult = getIndices(params.regex, params.testString);
    console.log(actualResult);
    deepEqual(actualResult, params.expected);
});