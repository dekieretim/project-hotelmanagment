/**
 * Created by Tim on 13/11/2014.
 */
function prepareFields(splitByLine){
    var blankLines = 0,
        length = splitByLine.length;

    for (var i = 0; i < length; i++) {
        var theString = splitByLine[i].trim(),
            semicolonIndices = getIndices(/;/g, theString),
            equalIndices = getIndices(/=/g, theString);
        if (theString.length) { //als het geen lege string is (dus gewoon lijn whitespace, want alle niet enter/newline whitespace wordt getrimt in de declaratie van deze var)
            if (equalIndices.length === 0) { //als er geen gelijk aan in staat
                splitByLine[i - blankLines++ - 1].content += theString;
            } else splitByLine[i - blankLines] = { content: theString, semiColons: semicolonIndices, equals: equalIndices }; //als dat wel het geval is
        } else blankLines += 1; // als het wel een lege lijn is
    }//TODO multiline declaraties waarbij er een = in de declaratie staat ergens die worden dus niet correct geparsed.  Idem voor multiline declaraties van strings via concatenatie met +  Moet in de documentatie terechtkomen.

    splitByLine.splice((length - blankLines), blankLines); //verwijder de lege lijnen

    return splitByLine;
}

function getIndices(regex, theString){
    var result, indices = [];
    while ((result = regex.exec(theString))) {
        indices.push(result.index);
    }

    return indices;
}


QUnit.module("prepareFields");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "1", splitByLine: ["private static final long serialVersionUID = 1L;", "", "	private static final boolean kDoCash = false;", "", "	public String testje = '1 = 2';", "	public String test2 = 'dit is ongelooflijk; hoe erg dat het is';", "	public String test3 = 'dit is nog erger; want 1 = 3';"],
        expected: [
            {
                content: "private static final long serialVersionUID = 1L;",
                equals: [43],
                semiColons: [47]
            },
            {
                content: "private static final boolean kDoCash = false;",
                equals: [37],
                semiColons: [44]
            },
            {
                content: "public String testje = '1 = 2';",
                equals: [21, 26],
                semiColons: [30]
            },
            {
                content: "public String test2 = 'dit is ongelooflijk; hoe erg dat het is';",
                equals: [20],
                semiColons: [42, 63]
            },
            {
                content: "public String test3 = 'dit is nog erger; want 1 = 3';",
                equals: [20, 48],
                semiColons: [39, 52]
            }
        ]}

]).test("Test getIndices function", function(params){
    var actualResult = prepareFields(params.splitByLine);
    deepEqual(actualResult, params.expected);
});