/**
 * Created by Tim on 16/10/2014.
 */

function removeComments(input){
    try {
        input = input.replace(/\/\*([^*]|[\r\n]|(\*([^/]|[\r\n])))*\*\//g, ""); //remove multiline comments
        return input.replace(/\/\/.*/g, ""); //remove single line comments
    } catch (e) {
        sendError(e.stack);
    }
}

//module
QUnit.module("removeComments");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "MultilineComments", indices: [0], method: "ActualCode/*commentstring\n\
                commentstring\n\
                commentstring*/ActualCode\n\
                ActualCode\n\
                /***commentstring\n\
                 *commentstring\n\
                 *commentstring\n\
                 *commentstring\n\
                 */ActualCode\n\
                ActualCode\n\
                ActualCode\n\
                ActualCode\n\
                ActualCode",
        expectedCommentLocation: -1,
        expectedNrOfRemainingCode: 8},
    {title: "MultilineComments", indices: [0], method: "ActualCode\n\
                //commentstring\n\
                ActualCode\n\
                //commentstring\n\
                ActualCode",
        expectedCommentLocation: -1,
        expectedNrOfRemainingCode: 3}
]).test("Test changeMethodVariables function", function(params){
    var actualResult = removeComments(params.method);
    strictEqual(actualResult.indexOf(/commentstring/g), params.expectedCommentLocation);
    strictEqual(actualResult.match(/ActualCode/g).length, params.expectedNrOfRemainingCode);
    notStrictEqual(actualResult, params.method);
});
