/**
 * Created by Tim on 16/10/2014.
 */
/**
 * Created by Tim on 16/10/2014.
 */
//function to test
function translateArray(arrayToTranslate) {
    var translatedArray = arrayToTranslate;

//    removes "new var"
    translatedArray = translatedArray.replace(/=\s*new\s*var/g, "= ");
//    removes []
    translatedArray = translatedArray.replace(/\[ *\]/g, "");
//    replaces { by [
    translatedArray = translatedArray.replace(/\{/g, "[");
//    replaces } by ]
    translatedArray = translatedArray.replace(/\}/g, "]");
//    replaces [\d] by []
    translatedArray = translatedArray.replace(/\[\s*\d\s*\]/g, "[]");
    return translatedArray;
}

//mock methods


QUnit.module("ArrayTranslator");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "basic example", method: "var[] myIntArray = new var[3];", expected: /var *myIntArray *\= *\[ *\] *\; */g},
    {title: "initialization with elements", method: "var[] myIntArray = {1,2,3};", expected: /var *myIntArray *\= *\[ *1 *, *2 *, *3 *\] *\; */g},
    {title: "initialization with elements v2", method: "var[] myIntArray = new var[]{1,2,3};", expected: /var *myIntArray *\= *\[ *1 *, *2 *, *3 *\] *\; */g},
    {title: "whitespace test", method: "var    [      ]    myIntArray    =      {        1      ,       2       ,       3 }   ;   ", expected: /var *myIntArray *\= *\[ *1 *, *2 *, *3 *\] *\; */g}
]).test("Test ArrayTransloator function", function(params){
    var actualResult = translateArray(params.method);
    equal(params.expected.test(actualResult), true);
    notStrictEqual(actualResult, params.method);
});

