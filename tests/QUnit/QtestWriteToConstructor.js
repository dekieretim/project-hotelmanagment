/**
 * Created by Tim on 13/11/2014.
 */
function writeToConstructor(total, value, name, isPrivate, isStatic, final){
    if (isPrivate&& isStatic) { //private static
        theClass.privateStatic += total + "\r\n\r\n"; //! geen speciale behandeling voor final aangezien private & static en op zich niet te wijzigen
    } else if (isPrivate) { //private
        theClass.forInnerConstructor += total + "\r\n\r\n";
    } else if (final) { //public final
        theClass.publicVarMethods += defineField("this", name, value, false, true, true);
    } else { //public
        theClass.publicVarMethods += "Inner" + theClass.constructorName + ".prototype." + name + " = " + value + "\r\n\r\n";
    }

}

function defineField(obj, name, val, write, configure, enumerate){
        return "Object.defineProperty(" + obj + ", '" + name + "', {\r\n\t" +
            "value: " + val + ",\r\n\t" +
            "enumerable: " + (enumerate ? enumerate : true) + ",\r\n\t" +
            "configurable: " + (configure ? configure : true) + ",\r\n\t" +
            "writable: " + (write ? write : true) + "\r\n\t" +
            "});\r\n\r\n";
}



var theClass;
function resetTheClass(){
    theClass = {
        constructor: [],
        privateStatic: "",
        forInnerConstructor: "",
        publicVarMethods: "",
        constructorName: "TBookingController"
    };

}

QUnit.module("FindMethodVariables");
//setup of testdata in .cases(), after that running that test data against the test defined in .test()
QUnit.cases([
    {title: "1", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"dit is ongelooflijk; hoe erg dat het is",
        name: "test2", isPrivate:true, isStatic:true, final: false, expected: "var test2 = 'dit is ongelooflijk; hoe erg dat het is';\r\n\r\n", location: 1},
    {title: "2", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"dit is ongelooflijk; hoe erg dat het is",
        name: "test2", isPrivate:true, isStatic:true, final: true, expected: "var test2 = 'dit is ongelooflijk; hoe erg dat het is';\r\n\r\n", location: 1},
    {title: "3", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"dit is ongelooflijk; hoe erg dat het is",
        name: "test2", isPrivate:true, isStatic:false, final: true, expected: "var test2 = 'dit is ongelooflijk; hoe erg dat het is';\r\n\r\n", location: 2},
    {title: "4", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"dit is ongelooflijk; hoe erg dat het is",
        name: "test2", isPrivate:true, isStatic:false, final: false, expected: "var test2 = 'dit is ongelooflijk; hoe erg dat het is';\r\n\r\n", location: 2},
    {title: "5", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"'dit is ongelooflijk; hoe erg dat het is'",
        name: "test2", isPrivate:false, isStatic:false, final: false, expected: "InnerTBookingController.prototype.test2 = 'dit is ongelooflijk; hoe erg dat het is'\r\n\r\n", location: 3},
    {title: "6", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val:"'dit is ongelooflijk; hoe erg dat het is'",
        name: "test2", isPrivate:false, isStatic:true, final: false, expected: "InnerTBookingController.prototype.test2 = 'dit is ongelooflijk; hoe erg dat het is'\r\n\r\n", location: 3},
    {title: "9", stringToWrite:"var test2 = 'dit is ongelooflijk; hoe erg dat het is';", val: "'dit is ongelooflijk; hoe erg dat het is'",
        name: "test2", isPrivate:false, isStatic:false, final: true, expected: "Object.defineProperty(this, 'test2', {\r\n\tvalue: 'dit is ongelooflijk; hoe erg dat het is',\r\n\tenumerable: true,\r\n\tconfigurable: true,\r\n\twritable: true\r\n\t});\r\n\r\n", location: 3}

]).test("Test WriteToConstructor function", function(params){
    resetTheClass();
    writeToConstructor(params.stringToWrite, params.val, params.name, params.isPrivate, params.isStatic, params.final);
    deepEqual(theClass[Object.keys(theClass)[params.location]], params.expected);
});