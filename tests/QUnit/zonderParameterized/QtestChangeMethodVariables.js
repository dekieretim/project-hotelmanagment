//function to test
function changeMethodVariables(indices, method){
    var i = 0, //variable to increase the performance of the for loop
        length = indices.length, //variable to increase the performance of the for loop
        counter = 0, //variable to offset the startindex with
        partFirst = "", //variable to store the first part of the method
        partFrom = "", //variable to store the part of the method in starting at the variabele declaration till the end of the method
        type = ""; //variable to store the type of the variable for easy reference in two statements

    for (i; i < length; i++) {
        partFirst = method.slice(0, indices[i] - counter); //store the first part of the string for easy concatenation
        partFrom = method.slice(indices[i] - counter); //slice out the part of the string beginning at the variable declaration
        type = partFrom.slice(0, partFrom.search(/\s/)); //store the type of the variable for easy use in the next two statements
        partFrom = partFrom.replace(type, "var");//replace the type by var
        counter += type.length - 3; //augment counter to offset replacing the type by var;
        method = partFirst + partFrom; //reassemble method so it's usable again for the next variable
    }

    return method;
}

//mock methods
var method = ["string test = 'this is a testfunction;'", "string test = 'dit is ook een string met een ; en een = en ook een = en een ;';", "\r\nArrayList<test> testing;",
        "int[] jos = new int[3];", "int[] jos = {1,2,3};",  "int[][] jefke = new int[3][2];",
        "TBookingController book;", "TBookingController book;\r\nint[][] jefke = new int[3][2];\r\nif(jefke){dosomething;}\r\n\r\nint[] jos = {1,2,3};"],
    expectedResult = ["var test = 'this is a testfunction;'", "var test = 'dit is ook een string met een ; en een = en ook een = en een ;';", "\r\nvar testing;",
        "var jos = new int[3];", "var jos = {1,2,3};", "var jefke = new int[3][2];", "var book;",
        "var book;\r\nvar jefke = new int[3][2];\r\nif(jefke){dosomething;}\r\n\r\nvar jos = {1,2,3};"];

QUnit.module("changeMethodVariables");
QUnit.test("test changeMethodVariables with a basic example", function(assert){
    assert.strictEqual(changeMethodVariables([0], method[0]), expectedResult[0], "changed variable");
    assert.notStrictEqual(changeMethodVariables([0], method[0]), method[0], "did not change variable");
});

QUnit.test("test changeMethodVariables with when the string contains semi-colons and equal signs", function(assert){
    assert.strictEqual(changeMethodVariables([0], method[1]), expectedResult[1], "changed variable");
    assert.notStrictEqual(changeMethodVariables([0], method[1]), method[1], "did not change variable");
});

QUnit.test("test changeMethodVariables for an ArrayList", function(assert){
    assert.strictEqual(changeMethodVariables([2], method[2]), expectedResult[2], "changed variable");
    assert.notStrictEqual(changeMethodVariables([0], method[2]), method[2], "did not change variable");
});

QUnit.test("test changeMethodVariables for an array definition", function(assert){
    assert.strictEqual(changeMethodVariables([0], method[3]), expectedResult[3], "changed variable");
    assert.notStrictEqual(changeMethodVariables([0], method[3]), method[3], "did not change variable");
});

QUnit.test("test changeMethodVariables for an array literal", function(assert){
    assert.strictEqual(changeMethodVariables([0], method[4]), expectedResult[4], "changed variable");
    assert.notStrictEqual(changeMethodVariables([0], method[4]), method[4], "did not change variable");
});

QUnit.test("test changeMethodVariables for an twodimensional array", function(assert){
    assert.strictEqual(changeMethodVariables([0], method[5]), expectedResult[5], "changed variable");
    assert.notStrictEqual(changeMethodVariables([0], method[5]), method[5], "did not change variable");
});

QUnit.test("test changeMethodVariables for a custom type", function(assert){
    assert.strictEqual(changeMethodVariables([0], method[6]), expectedResult[6], "changed variable");
    assert.notStrictEqual(changeMethodVariables([0], method[6]), method[6], "did not change variable");
});

QUnit.test("test changeMethodVariables for several variables", function(assert){
    assert.strictEqual(changeMethodVariables([0, 26, 85], method[7]), expectedResult[7], "changed variable");
    assert.notStrictEqual(changeMethodVariables([0, 26, 85], method[7]), method[7], "did not change variable");
});